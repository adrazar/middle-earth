#pragma once
#include "../Middle-Earth/GameConfigurer.h"
#include "../Middle-Earth/GameSettings.h"
#include <nlohmann/json.hpp>
#include <string>
#include <initializer_list>
#include <unordered_map>
#include <vector>

void saveGameSettings(const GameSettings & settings, nlohmann::json & jsonSettings, bool compress);
void loadGameSettings(GameSettings & settings, GameConfigurer & configurer, const nlohmann::json & jsonSettings);
void replay(MetaGame & metaGame);

template<class T>
struct TwoWayMap {
    TwoWayMap(std::initializer_list<std::pair<T, std::string>> values);
    const std::string & toString(const T & key) const;
    const T & toEnum(const std::string & key) const;

private:
    std::unordered_map<T, std::string> enumClassToString;
    std::unordered_map<std::string, T> stringToEnumClass;
};
