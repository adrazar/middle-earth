#pragma once
#include "ServerTypes.h"

#include <nlohmann/json.hpp>
#include <unordered_map>


class Server;

class RequestParser {
public:
    RequestParser(Server & server);
    void parse(server::Handle handle, const nlohmann::json & request);

private:
    using CommandFunction = void(*)(Server & server, server::Handle handle, const nlohmann::json & args);
    using CommandMap = std::unordered_map<std::string, CommandFunction>;
    void executeCommand(CommandMap::iterator it, server::Handle handle, const nlohmann::json & request);

    Server & server_;
    static CommandMap commandMap_;
};
