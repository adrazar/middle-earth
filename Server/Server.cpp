#include "ResponseBuilder.h"
#include "SaveGame.h"
#include "Server.h"

#include "../CardSets/CardSetLoader.h"
#include "../Middle-Earth/GameConfigurer.h"

#include <nlohmann/json.hpp>

#include <iostream>
#include <string>


namespace {

server::TableID nextTableID = 0;

}  // namespace

int Table::assignPlayer(Handle handle) {
    std::size_t i;

    for (i = 0; i < nPlayers; i++)
        if (assignedPlayers[i] == handle)
            assignedPlayers[i] = -1;

    for (i = 0; i < nPlayers; i++)
        if (assignedPlayers[i] == -1) {
            assignedPlayers[i] = handle;
            return i;
        }

    return -1;
}

void Table::assignPlayer(Handle handle, int player) {
    std::size_t p(player);

    if (p < 0 || p >= nPlayers)
        return;

    for (std::size_t i = 0; i < nPlayers; i++)
        if (assignedPlayers[i] == handle)
            assignedPlayers[i] = -1;

    assignedPlayers[p] = handle;
}

void Table::setNumberOfPlayers(std::size_t players) {
    if (players < 2 || players > 4 || players == nPlayers)
        return;

    if (players < nPlayers)
        for (std::size_t player = players; player < nPlayers; player++)
            assignedPlayers[player] = -1;

    nPlayers = players;
}

Server::Server(boost::asio::io_context & ioContext, std::ostream & ostream)
    : ioContext_(ioContext)
    , clients_(ioContext, *this, 2)
    , parser_(*this)
    , ostream(ostream)
{ ; }

void Server::start() {
    nextTableID = 0;
    clients_.startAccept();
    ostream << "Server started" << std::endl;
}

void Server::stop() {
    clients_.stopAccept();
}

void Server::receiveHandler(Clients::Handle handle, const std::string & msg) {
    try {
        ostream << msg << std::endl;
        nlohmann::json request = nlohmann::json::parse(msg);
        parser_.parse(handle, request);
    }
    catch (std::exception & ex) {
        ostream << ex.what() << std::endl;
    }
}

void Server::validateHeader(const server::TableRequestHeader & header) {
    validateTableId(header.tableId);
    if (!isClientAtTable(header.handle, header.tableId))
        throw server::InvalidRequestException(std::string("Client is not at table ") + std::to_string(header.tableId));
    // Don't mind the stateId here, some requests may be state independent
}
void Server::validateTableId(server::TableID tableId) {
    auto it = tables.find(tableId);
    if (it == tables.end())
        throw server::InvalidRequestException(std::string("Table does not exist: ") + std::to_string(tableId));
}

server::TableID Server::createTable() {
    server::TableID tableId = nextTableID++;
    tables[tableId] = std::make_unique<Table>();

    // Default setup
    MetaGame & metaGame = tables.at(tableId)->metaGame;
    loadCardSet(metaGame.settings, CardSet::CoreCards);
    loadCardSet(metaGame.settings, CardSet::DwarfDeck);
    loadCardSet(metaGame.settings, CardSet::OrcDeck);
    auto decks = loadDeckDefinitions();
    GameConfigurer configurer(metaGame);
    PlayerConfigurer dwarfPlayer = configurer.addPlayer();
    dwarfPlayer.loadDeck(decks["Dwarven Quest"]);
    PlayerConfigurer orcPlayer = configurer.addPlayer();
    orcPlayer.loadDeck(decks["Stealthy Tribe"]);

    return tableId;
}

void Server::createTable(Handle handle, size_t nPlayers, int requestId) {
    server::TableID tableId = createTable();
    Table & table = *tables[tableId];
    clients_.getClientSettings(handle).tableAssignments.insert_or_assign(tableId, -1);
    table.joiners.insert(handle);
    table.nPlayers = nPlayers;
    nlohmann::json response = {
        { "type", "createTable" },
        { "tableId", tableId },
        { "nPlayers", nPlayers },
    };
    if (requestId >= 0)
        response["requestId"] = requestId;
    clients_.send(handle, std::move(response.dump()));
}
void Server::listTables(Handle handle, const std::string & filter) {
    nlohmann::json response = {
        { "type", "listTables" },
    };
    auto & jsonTables = response["tables"];
    jsonTables = nlohmann::json::array();
    for (auto & tableData : tables) {
        Table & table = *tableData.second;
        nlohmann::json jsonTable;
        jsonTable["tableId"] = tableData.first;
        jsonTable["seats"] = table.nPlayers;
        auto & jsonPlayers = jsonTable["players"];
        jsonPlayers = nlohmann::json::array();
        for (size_t i = 0; i < table.nPlayers; i++) {
            Handle handle = table.assignedPlayers[i];
            if (handle < 0)
                jsonPlayers.push_back("-");
            else
                jsonPlayers.push_back(clients_.getClientSettings(handle).userName);
        }
        jsonTable["joiners"] = table.joiners.size();
        switch (table.gameProgress) {
        case Table::GameProgress::NotStarted:
            jsonTable["gameProgress"] = "Not started";
            break;
        case Table::GameProgress::Playing:
            jsonTable["gameProgress"] = "Playing";
            break;
        default:
            jsonTable["gameProgress"] = "Finished";
        }
        jsonTables.push_back(std::move(jsonTable));
    }
    clients_.send(handle, std::move(response.dump()));
}
void Server::joinTable(Handle handle, TableID tableId) {
    validateTableId(tableId);
    if (isClientAtTable(handle, tableId))
        throw server::InvalidRequestException("Already at table " + std::to_string(tableId));

    Table & table = *tables[tableId];
    clients_.getClientSettings(handle).tableAssignments.insert_or_assign(tableId, -1);
    table.joiners.insert(handle);

    nlohmann::json response = {
        { "type", "joinTable" },
        { "tableId", tableId },
        { "nPlayers", table.nPlayers },
    };
    clients_.send(handle, std::move(response.dump()));
}
void Server::leaveTable(Handle handle, TableID tableId) {
    validateTableId(tableId);
    if (!isClientAtTable(handle, tableId))
        throw server::InvalidRequestException("Not at table " + std::to_string(tableId));

    Table & table = *tables[tableId];
    auto & tableAssignments = clients_.getClientSettings(handle).tableAssignments;
    int position = tableAssignments[tableId];
    tableAssignments.erase(tableId);
    if (position >= 0)
        table.assignedPlayers[position] = -1;
    table.joiners.erase(handle);
    if (table.joiners.empty())
        tables.erase(tableId);

    nlohmann::json response = {
        { "type", "leaveTable" },
        { "tableId", tableId }
    };
    clients_.send(handle, std::move(response.dump()));
}
void Server::sit(Handle handle, TableID tableId, int position) {
    validateTableId(tableId);
    Table & table = *tables[tableId];
    if (position < 0 || position >= int(table.nPlayers))
        position = -1;
    if (position < 0) {
        position = table.assignPlayer(handle);
        if (position < 0)
            throw server::InvalidRequestException("No available seats at table " + std::to_string(tableId));
    } else {
        if (table.assignedPlayers[position] != -1)
            throw server::InvalidRequestException("Seat " + std::to_string(position) + " aleady taken");
        table.assignPlayer(handle, position);
    }
    clients_.getClientSettings(handle).tableAssignments.insert_or_assign(tableId, position);
    table.joiners.insert(handle);

    nlohmann::json response = {
        { "type", "sit" },
        { "tableId", tableId },
        { "position", position }
    };
    clients_.send(handle, std::move(response.dump()));
}
void Server::rise(Handle handle, TableID tableId) {
    validateTableId(tableId);
    if (!isClientAtTable(handle, tableId))
        throw server::InvalidRequestException("Not at table " + std::to_string(tableId));
    auto & tableAssignments = clients_.getClientSettings(handle).tableAssignments;
    int position = tableAssignments[tableId];
    if (position < 0)
        throw server::InvalidRequestException("Not seated at table " + std::to_string(tableId));

    tableAssignments[tableId] = -1;
    tables[tableId]->assignedPlayers[position] = -1;

    nlohmann::json response = {
        { "type", "rise" },
        { "tableId", tableId },
        { "position", position }
    };
    clients_.send(handle, std::move(response.dump()));
}

void Server::authenticate(Handle handle, const std::string & userName) {
    clients_.activateClient(handle);
    ClientSettings & settings = clients_.getClientSettings(handle);
    settings.userName = userName;
    //int player = tables[0]->assignPlayer(handle);
    //settings.tableAssignments[0] = player;
    ostream << userName << " joined the session" << std::endl;
}

void Server::error(Handle handle, int requestId, std::string & msg) {
    nlohmann::json error = {
        {"type", "error"},
        {"requestId", requestId},
        {"message", msg},
    };
    clients_.send(handle, std::move(error.dump()));
}

bool Server::isClientAtTable(Handle handle, TableID tableId) {
    auto & settings = clients_.getClientSettings(handle);
    auto foundIt = settings.tableAssignments.find(tableId);
    return foundIt != settings.tableAssignments.end();
}

void Server::syncCards(const server::TableRequestHeader & header, const CardSyncRequest & request) {
    int requestingPlayer = clients_.getClientSettings(header.handle).tableAssignments[header.tableId];
    Table & table = *tables[header.tableId];
    MetaGame & metaGame = table.metaGame;
    Game & game = metaGame.gameStates.back();

    ResponseBuilder builder(game, requestingPlayer);

    nlohmann::json response = {
        { "type", "syncCards" },
        { "stateId", table.stateId },
        { "requestId", header.requestId }
    };

    nlohmann::json & jsonSides = response["sides"];
    jsonSides = nlohmann::json::array();
    for (const CardSyncRequest::Side & side : request.sides) {
        if (side.player < 0 || game.sides.size() <= std::size_t(side.player))
            throw server::InvalidRequestException(std::string("Invalid player: ") + std::to_string(side.player));

        auto & gameSide = game.sides[side.player];
        auto jsonSide = nlohmann::json::object();
        jsonSide["player"] = side.player;
        if (!side.decks.empty()) {
            auto & jsonDecks = jsonSide["decks"];
            jsonDecks = nlohmann::json::array();
            for (DeckName deckName : side.decks)
                builder.addDeck(game, deckName, gameSide, jsonDecks);
        }
        if (std::holds_alternative<std::string>(side.companies)) {
            const std::string & keyword = std::get<std::string>(side.companies);
            if (keyword == "all") {
                std::vector<CompanyID> companies;
                for (const Company & company : gameSide.companies)
                    companies.push_back(company.id);
                builder.addCompanies(game, gameSide, companies, jsonSide);
            }
        } else {
            auto & companies = std::get<std::vector<CompanyID>>(side.companies);
            if (!companies.empty())
                builder.addCompanies(game, gameSide, companies, jsonSide);
        }
        jsonSides.push_back(std::move(jsonSide));
    }

    builder.addCardData(game, response);

    clients_.send(header.handle, std::move(response.dump()));
}

void Server::getContext(const server::TableRequestHeader & header) {
    int requestingPlayer = clients_.getClientSettings(header.handle).tableAssignments[header.tableId];
    Table & table = *tables[header.tableId];
    MetaGame & metaGame = table.metaGame;
    Game & game = metaGame.gameStates.back();

    ResponseBuilder builder(game, requestingPlayer);
    ContextToJson contextTranslator(game, builder);

    nlohmann::json response = {
        { "type", "context" },
        { "stateId", table.stateId },
        { "requestId", header.requestId }
    };
    nlohmann::json & jsonContexts = response["contexts"];
    jsonContexts = nlohmann::json::array();

    for (size_t i = 0; i < game.contextStack.size(); i++)
        jsonContexts.push_back(contextTranslator.visit(i));
    builder.addCardData(game, response);
    clients_.send(header.handle, std::move(response.dump()));
}

void Server::getContext(const server::TableRequestHeader & header, int index) {
    int requestingPlayer = clients_.getClientSettings(header.handle).tableAssignments[header.tableId];
    Table & table = *tables[header.tableId];
    MetaGame & metaGame = table.metaGame;
    Game & game = metaGame.gameStates.back();

    ResponseBuilder builder(game, requestingPlayer);
    ContextToJson contextTranslator(game, builder);

    int contextIndex;
    const int size(game.contextStack.size());
    if (index < 0) {
        contextIndex = index + size;
        if (contextIndex < 0)
            contextIndex = 0;
    } else {
        contextIndex = (index < size) ? index : size - 1;
    }

    nlohmann::json response = {
        { "type", "context" },
        { "stateId", table.stateId },
        { "requestId", header.requestId }
    };
    response["index"] = contextIndex;
    response["context"] = std::visit(contextTranslator, game.contextStack[contextIndex]->context);

    builder.addCardData(game, response);
    clients_.send(header.handle, std::move(response.dump()));
}

void Server::saveGame(const server::TableRequestHeader & header, const std::string & fileName, bool compress) {
    Table & table = *tables[header.tableId];
    GameSettings & settings = table.metaGame.settings;

    nlohmann::json response = {
        { "type", "saveGame" },
        { "stateId", table.stateId },
        { "requestId", header.requestId },
        { "fileName", fileName },
    };

    saveGameSettings(settings, response["game"], compress);
    clients_.send(header.handle, std::move(response.dump()));
}
void Server::loadGame(const server::TableRequestHeader & header, const nlohmann::json & jsonGame) {
    Table & table = *tables[header.tableId];
    if (table.gameProgress != Table::GameProgress::NotStarted)
        throw server::InvalidRequestException("Can't load game after starting game");

    MetaGame & metaGame = table.metaGame;
    metaGame.gameStates.clear();
    metaGame.gameStates.emplace_back(metaGame.settings);

    GameConfigurer configurer(metaGame);
    loadGameSettings(metaGame.settings, configurer, jsonGame);
    replay(metaGame);

    nlohmann::json response = {
        { "type", "loadGame" },
        { "stateId", table.stateId },
        { "requestId", header.requestId }
    };
    clients_.send(header.handle, std::move(response.dump()));
}
