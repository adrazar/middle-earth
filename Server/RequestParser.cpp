#include "RequestParser.h"
#include "Server.h"

namespace {

using Handle = server::Handle;

void authenticate(Server & server, Handle handle, const nlohmann::json & request) {
    if (!request.contains("userName"))
        return;
    server.authenticate(handle, request["userName"]);
}

template <typename Type>
void validateJsonType(const nlohmann::json & json, const std::string & key) {
    if constexpr (std::is_same_v<Type, std::string>) {
        if (!json.is_string())
            throw server::InvalidJsonException(std::string("Expected '") + key + "' to hold a string value");
    }
    else if constexpr (std::is_same_v<Type, int>) {
        if (!json.is_number())
            throw server::InvalidJsonException(std::string("Expected '") + key + "' to hold a numeric value");
    }
    else if constexpr (std::is_same_v<Type, nlohmann::json>) {
        if (!json.is_object())
            throw server::InvalidJsonException(std::string("Expected '") + key + "' to hold an object");
    }
    else if constexpr (std::is_same_v<Type, nlohmann::json::array_t>) {
        if (!json.is_array())
            throw server::InvalidJsonException(std::string("Expected '") + key + "' to hold an array");
    }
}

template <typename Type>
Type getRequired(const nlohmann::json & json, const std::string & key) {
    auto it = json.find(key);
    if (it == json.end())
        throw server::InvalidJsonException(std::string("Expected key '") + key + "'");
    validateJsonType<Type>(*it, key);
    return it->get<Type>();
}

template <typename Type>
Type getOptional(const nlohmann::json & json, const std::string & key, Type defaultValue) {
    auto it = json.find(key);
    if (it == json.end())
        return defaultValue;
    validateJsonType<Type>(*it, key);
    return it->get<Type>();
}

void createTable(Server & server, Handle handle, const nlohmann::json & request) {
    int requestId = getOptional<int>(request, "requestId", -1);
    size_t nPlayers(getOptional<int>(request, "players", 2));
    server.createTable(handle, nPlayers, requestId);
}
void listTables(Server & server, Handle handle, const nlohmann::json & request) {
    const std::string filter = getOptional<std::string>(request, "filter", "");
    server.listTables(handle, filter);
}
void joinTable(Server & server, Handle handle, const nlohmann::json & request) {
    server::TableID tableId(getRequired<int>(request, "tableId"));
    server.joinTable(handle, tableId);
}
void leaveTable(Server & server, Handle handle, const nlohmann::json & request) {
    server::TableID tableId(getRequired<int>(request, "tableId"));
    server.leaveTable(handle, tableId);
}
void sit(Server & server, Handle handle, const nlohmann::json & request) {
    server::TableID tableId(getRequired<int>(request, "tableId"));
    int position = getOptional<int>(request, "seat", -1);
    server.sit(handle, tableId, position);
}
void rise(Server & server, Handle handle, const nlohmann::json & request) {
    server::TableID tableId(getRequired<int>(request, "tableId"));
    server.rise(handle, tableId);
}

server::TableRequestHeader && getHeader(Server & server, server::Handle handle, const nlohmann::json & request) {
    auto requestId = getRequired<int>(request, "requestId");
    auto stateId = getRequired<int>(request, "stateId");
    auto tableId = getOptional<int>(request, "tableId", 0);
    server::TableRequestHeader header = { requestId, stateId, handle, tableId };
    server.validateHeader(header);
    return std::move(header);
}

void syncCards(Server & server, Handle handle, const nlohmann::json & request) {
    server::TableRequestHeader header = getHeader(server, handle, request);
    CardSyncRequest parsedRequest;

    const auto & jsonSides = getRequired<nlohmann::json::array_t>(request, "sides");
    for (const nlohmann::json & jsonSide : jsonSides) {
        const auto player = getRequired<int>(jsonSide, "player");
        parsedRequest.sides.push_back(player);
        CardSyncRequest::Side & side = parsedRequest.sides.back();

        const auto & jsonDecks = getRequired<nlohmann::json::array_t>(jsonSide, "decks");
        for (const nlohmann::json & jsonDeckName : jsonDecks) {
            auto deckName = jsonDeckName.get<int>();
            side.decks.push_back(DeckName(deckName));
        }

        auto it = jsonSide.find("companies");
        if (it != jsonSide.end()) {
            if (it->is_string()) {
                const auto & keyword = it->get<std::string>();
                if (keyword == "all")
                    side.companies = keyword;
                else
                    throw server::InvalidJsonException(std::string("Company keyword '") + keyword + "' not recognised");
            } else {
                side.companies = {};
                auto & companies = std::get<std::vector<CompanyID>>(side.companies);
                if (it->is_array()) {
                    auto & jsonCompanies = *it;
                    for (CompanyID id : jsonCompanies)
                        companies.push_back(id);
                }
            }
        }
    }

    server.syncCards(header, parsedRequest);
}
void context(Server & server, Handle handle, const nlohmann::json & request) {
    server::TableRequestHeader header = getHeader(server, handle, request);
    auto it = request.find("index");
    if (it == request.end())
        server.getContext(header);
    else {
        int index = getRequired<int>(request, "index");
        server.getContext(header, index);
    }
}

void save(Server & server, Handle handle, const nlohmann::json & request) {
    auto header = getHeader(server, handle, request);
    auto fileName = getRequired<std::string>(request, "fileName");
    auto compress = getOptional<bool>(request, "compress", false);
    server.saveGame(header, fileName, compress);
}

void load(Server & server, Handle handle, const nlohmann::json & request) {
    auto header = getHeader(server, handle, request);
    auto it = request.find("game");
    if (it == request.end())
        throw server::InvalidJsonException(std::string("Property 'game' missing in request"));
    server.loadGame(header, *it);
}

}  // namespace

RequestParser::CommandMap RequestParser::commandMap_ = {
    { "authenticate", authenticate },
    { "createTable", createTable },
    { "listTables", listTables },
    { "joinTable", joinTable },
    { "leaveTable", leaveTable },
    { "sit", sit },
    { "rise", rise },
    { "syncCards", syncCards },
    { "context", context },
    { "save", save },
    { "load", load },
};

RequestParser::RequestParser(Server & server)
    : server_(server)
{ ; }

void RequestParser::executeCommand(CommandMap::iterator it, server::Handle handle, const nlohmann::json & request) {
    try {
        it->second(server_, handle, request);
    } catch (std::exception & ex) {
        throw std::runtime_error(it->first + ": " + ex.what());
    }
}

void RequestParser::parse(Handle handle, const nlohmann::json & request) {
    int requestId = getOptional<int>(request, "requestId", -1);

    try {
        if (!request.contains("type"))
            throw server::InvalidJsonException(std::string("Please provide a type"));

        std::string type = getRequired<std::string>(request, "type");
        auto found = commandMap_.find(type);
        if (found == commandMap_.end())
            throw server::InvalidRequestException(std::string("Invalid type '" + type + "'"));

        executeCommand(found, handle, request);

    } catch (server::InvalidJsonException & ex) {
           std::string msg(ex.what());
        server_.error(handle, requestId, msg);
    } catch (server::InvalidRequestException & ex) {
        std::string msg(ex.what());
        server_.error(handle, requestId, msg);
    } catch (std::exception & ex) {
        std::string msg(ex.what());
        server_.error(handle, requestId, msg);
    }
}