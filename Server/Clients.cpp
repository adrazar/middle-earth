#include "Clients.h"
#include "Server.h"

#include <boost/asio.hpp>
#include <boost/asio/yield.hpp>

#include <charconv>
#include <iostream>
#include <string>


namespace {

Clients::Handle nextHandle = 0;
const std::size_t headerSize = 8;
const int base = 16;

void createMsgHeader(std::string & header, std::size_t msgSize) {
    header.clear();
    header.resize(headerSize);
    auto * headerAddress = &(*header.begin());
    std::to_chars(headerAddress, headerAddress + headerSize, long(msgSize), base);
}

int readMsgHeader(const std::string & header) {
    return stol(header, nullptr, base);
}

void handleReceiveError(const boost::system::error_code & ec, Clients::Client & client) {
    if (ec == boost::asio::error::eof || ec == boost::asio::error::connection_reset) {
        client.ostream << "Client disconnected: " << client.handle;
        if (!client.settings.userName.empty())
            client.ostream << " (" << client.settings.userName << ")";
        client.ostream << std::endl;
        client.activeConnection = false;
    }
    else if (ec == boost::asio::error::operation_aborted) {
        client.ostream << "Socket read operation aborted" << std::endl;
    }
    else {
        client.ostream << "ReceiveHandler unexpected error: " << ec.message() << std::endl;
    }
}

} // namespace


Clients::Clients(boost::asio::io_context & ioContext, Server & server, int nPlayers)
    : ioContext_(ioContext)
    , acceptor_(ioContext)
    , acceptHandler_(*this)
    , server_(server)
    , ostream(server.ostream)
{ ; }

Clients::Client::Client(Handle handle, Socket && socket, Server & server)
    : handle(handle)
    , socket_(std::move(socket))
    , server_(server)
    , timer_(socket.get_executor())
    , ostream(server.ostream)
{ ostream << "Created new client instance" << std::endl; }

void Clients::Client::send(std::string && msg) {
    std::string header;
    createMsgHeader(header, msg.size());
    sendMsg(std::move(header));
    sendMsg(std::move(msg));
}

void Clients::Client::receive() {
    int msgSize;
    ostream << "Clients::Client::receive invoked" << std::endl;

    auto handler = [this](const boost::system::error_code & ec, const std::size_t bytesReceived) {
        ostream << "ReceiveHandler invoked" << std::endl;
        if (!ec)
            receive();
        else
            handleReceiveError(ec, *this);
    };

    reenter(this) {
        for (;;) {
            receiveBuffer_.resize(headerSize);
            yield boost::asio::async_read(socket_, boost::asio::buffer(receiveBuffer_), std::move(handler));
            msgSize = readMsgHeader(receiveBuffer_);
            receiveBuffer_.resize(msgSize);
            yield boost::asio::async_read(socket_, boost::asio::buffer(receiveBuffer_), std::move(handler));
            server_.receiveHandler(handle, receiveBuffer_);
        }
    }
}

void Clients::Client::startAuthentication(Clients & clients) {
    auto rejectHandler = [this, &clients](const boost::system::error_code & ec) {
        (void)ec;

        if (activeConnection)
            return;

        ostream << "Authentication timeout" << std::endl;
        clients.kick(handle);
    };

    timer_.expires_after(boost::asio::chrono::seconds(5));
    timer_.async_wait(std::move(rejectHandler));
}

void Clients::Client::closeSocket() {
    socket_.shutdown(boost::asio::socket_base::shutdown_both);
    socket_.close();
}

void Clients::Client::sendMsg(std::string && msg) {
    bool noSendInProgress = sendQueue_.empty();
    sendQueue_.emplace(std::move(msg));
    if (noSendInProgress)
        sendNextQueuedMsg();
}

void Clients::Client::sendNextQueuedMsg() {
    std::string & msg = sendQueue_.front();
    boost::asio::async_write(socket_, boost::asio::buffer(msg), [this](const boost::system::error_code & ec, std::size_t bytesSent) {
        if (!ec) {
            sendQueue_.pop();
            if (!sendQueue_.empty())
                sendNextQueuedMsg();
        }
        else if (ec == boost::asio::error::operation_aborted) {
            ostream << "Socket write operation aborted" << std::endl;
        }
        else {
            ostream << "SendHandler unexpected error: " << ec.message() << std::endl;
        }
    });
}

Clients::AcceptHandler::AcceptHandler(Clients & clients)
    : clients_(clients)
{ ; }

void Clients::AcceptHandler::operator()(const boost::system::error_code & ec, Socket socket) {
    if (!ec)
        clients_.acceptConnection(std::move(socket));
}

void Clients::startAccept() {
    if (acceptor_.is_open())
        return;
    boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::tcp::v4(), 8289);
    acceptor_.open(endpoint.protocol());
    acceptor_.bind(endpoint);
    acceptor_.listen();
    acceptor_.async_accept(acceptHandler_);
}

void Clients::stopAccept() {
    if (!acceptor_.is_open())
        return;
    acceptor_.close();
}

void Clients::acceptConnection(Socket && socket) {
    Handle handle = nextHandle++;
    ostream << "Assigned handle: " << handle << std::endl;
    connectedClients_.emplace(std::make_pair(handle, std::make_unique<Client>(handle, std::move(socket), server_)));
    ostream << "Number of connected clients_: " << connectedClients_.size() << std::endl;
    acceptor_.async_accept(acceptHandler_);
    Client & client = *connectedClients_.at(handle);
    // Start reading from socket
    client.receive();
    client.startAuthentication(*this);
}

void Clients::activateClient(Handle handle) {
    connectedClients_.at(handle)->activeConnection = true;
}

void Clients::send(Handle handle, std::string && msg) {
    connectedClients_.at(handle)->send(std::move(msg));
}

void Clients::kick(Handle handle, boost::asio::coroutine coro) {
    reenter(coro) {
        ostream << "Kicking client " << handle;
        {
            ClientSettings & clientSettings = getClientSettings(handle);
            if (!getClientSettings(handle).userName.empty())
                ostream << " (" << clientSettings.userName << ")";
            ostream << std::endl;

            for (auto & assignment : clientSettings.tableAssignments) {
                Table & table = *server_.tables[assignment.first];
                if (assignment.second >= 0)
                    table.assignedPlayers[assignment.second] = -1;
                table.joiners.erase(handle);
                if (table.joiners.empty())
                    server_.tables.erase(assignment.first);
            }
        }
        connectedClients_.at(handle)->closeSocket();
        yield boost::asio::post(ioContext_, [this, handle, coro]() { kick(handle, coro); });
        connectedClients_.erase(handle);
        ostream << "Client kicked: " << handle << std::endl;
    }
}

ClientSettings & Clients::getClientSettings(Handle handle) {
    return connectedClients_.at(handle)->settings;
}
