#pragma once
#include "ServerTypes.h"

#include <boost/asio.hpp>
#include <boost/asio/yield.hpp>

#include <array>
#include <iostream>
#include <memory>
#include <queue>
#include <unordered_map>


class Server;

struct ClientSettings {
    std::string userName;
    std::unordered_map<server::TableID, int> tableAssignments;  // -1 when merely spectating
};

class Clients {
public:
    Clients(boost::asio::io_context & ioContext, Server & server, int nPlayers);
    using Socket = boost::asio::ip::tcp::socket;
    using Handle = server::Handle;

    void startAccept();
    void stopAccept();
    void activateClient(Handle handle);
    void send(Handle handle, std::string && msg);
    void kick(Handle handle, boost::asio::coroutine coro = {});
    ClientSettings & getClientSettings(Handle handle);

    std::ostream & ostream;

    class Client : public boost::asio::coroutine {
    public:
        using SendQueue = std::queue<std::string>;

        Client(Handle handle, Socket && socket, Server & server);
        void send(std::string && msg);
        void receive();
        void startAuthentication(Clients & clients);
        void closeSocket();

        const Handle handle;
        ClientSettings settings;
        bool activeConnection = false;   // false = disconnected or awaiting authentication
        std::ostream & ostream;

    private:
        void sendMsg(std::string && msg);
        void sendNextQueuedMsg();

        Socket socket_;
        Server & server_;
        std::string receiveBuffer_;
        SendQueue sendQueue_;
        boost::asio::steady_timer timer_;
    };

    class AcceptHandler {
    public:
        AcceptHandler(Clients & clients);
        void operator()(const boost::system::error_code & ec, Socket socket);

    private:
        Clients & clients_;
    };

private:
    void acceptConnection(Socket && socket);

    boost::asio::io_context & ioContext_;
    boost::asio::ip::tcp::acceptor acceptor_;
    AcceptHandler acceptHandler_;
    std::unordered_map<Handle, std::unique_ptr<Client>> connectedClients_;
    Server & server_;
};
