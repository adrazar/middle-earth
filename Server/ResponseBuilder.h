#pragma once
#include "../Middle-Earth/ContextInterface.h"
#include <nlohmann/json.hpp>

struct ResponseBuilder {
    ResponseBuilder(Game & game, int requestingPlayer)
        : requestingPlayer(requestingPlayer)
        , aliasTable(*game.sides[requestingPlayer].aliasTable)
    { ; }

    CardAlias toAlias(CardID id);

    void addCard(const Game & game, CardID card, nlohmann::json & jsonDeck);
    void addDeck(const Game & game, DeckName deckName, Side & side, nlohmann::json & jsonDeck);
    void addCharacter(const Game & game, CardID character, nlohmann::json & jsonCharacter);
    void addCompany(const Game & game, const Company & company, nlohmann::json & jsonSide);
    void addCompanies(const Game & game, const Side & side, const std::vector<CompanyID> & companies, nlohmann::json & jsonSide);
    void addCardData(const Game & game, nlohmann::json & response);

    std::unordered_set<CardID> referencedCards;
    const int requestingPlayer;
    const CardAliasTable & aliasTable;
};

struct ContextToJson {
    ContextToJson(Game & game, ResponseBuilder & responseBuilder)
        : game(game)
        , responseBuilder(responseBuilder)
        , scene(std::make_unique<Scene>(game, 0))
    { ; }

    nlohmann::json visit(size_t contextIndex);

    nlohmann::json operator()(RollDice & rollDice);
    nlohmann::json operator()(BodyCheck & bodyCheck);
    nlohmann::json operator()(Strike & strike);
    nlohmann::json operator()(AssignStrikes & assign);
    nlohmann::json operator()(Attack & attack);
    nlohmann::json operator()(Encounter & encounter);
    nlohmann::json operator()(CorruptionCheck & corruptionCheck);
    nlohmann::json operator()(ResolveCorruptionChecks & resolve);
    nlohmann::json operator()(StoreCard & storeCard);
    nlohmann::json operator()(RemoveCorruptionCard & remove);
    nlohmann::json operator()(InfluenceAttempt & influence);
    nlohmann::json operator()(RingTest & ringTest);
    nlohmann::json operator()(SpecialTest & specialTest);
    nlohmann::json operator()(Movement & movement);
    nlohmann::json operator()(DrawCards & drawCards);
    nlohmann::json operator()(Arrival & arrival);
    nlohmann::json operator()(Explore & explore);
    nlohmann::json operator()(SelectCompany & selectCompany);
    nlohmann::json operator()(TransferCards & transfer);
    nlohmann::json operator()(TransferItems & transfer);
    nlohmann::json operator()(ReturnToHandSize & handSize);
    nlohmann::json operator()(ExhaustDeck & exhaust);
    nlohmann::json operator()(OrganizeCompanies & organize);
    nlohmann::json operator()(DiscardSubset & discard);
    nlohmann::json operator()(TapCharacters & tap);
    nlohmann::json operator()(ChainOfEffects & chain);
    nlohmann::json operator()(BottomContext & bottomContext);

    Game & game;
    ResponseBuilder & responseBuilder;
    std::unique_ptr<Scene> scene;
};
