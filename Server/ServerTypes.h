#pragma once
#include "../Middle-Earth/CardTopology.h"
#include <stdexcept>
#include <vector>

namespace server {

using Handle = long;
using TableID = long;
using StateID = long;
using RequestID = long;

struct TableRequestHeader {
    RequestID requestId;
    StateID stateId;
    Handle handle;
    TableID tableId;
};

class InvalidJsonException : public std::runtime_error {
public:
    using runtime_error::runtime_error;
};
class InvalidRequestException : public std::runtime_error {
public:
    using runtime_error::runtime_error;
};

}  // namespace server
