#include "ResponseBuilder.h"

namespace {

std::unordered_map<DeckName, std::string> deckNames = {
    { Hand, "hand" },
    { PlayDeck, "playDeck" },
    { DiscardPile, "discardPile" },
    { LocationDeck, "locationDeck" },
    { Sideboard, "sideboard" },
    { OutOfPlay, "outOfPlay"},
    { Eliminated, "eliminated"},
    { Kills, "kills"},
    { Stored, "stored"},
    { Factions, "factions"},
    { PermanentEvents, "permanentEvents"},
    { LongEvents, "longEvents"},
    { Creatures, "creatures"},
    { Pending, "pending"},
};

std::vector<std::string> cardStateNames = { "untapped", "tapped" , "wounded" };
std::vector<std::string> strikeStateNames = { "Play hazards", "Assign excess strikes", "Tap character", "Play resources", "Roll dice", "Done" };
std::vector<std::string> exploreStateNames = { "Enter site", "Reveal on-guard", "Explore site", "Done" };
std::vector<std::string> assignStrikeStateNames = { "Defender's turn", "Attacker's turn", "Done" };

nlohmann::json keyedTo(KeyedToType key) {
    if (std::holds_alternative<PathSymbol>(key)) {
        return {
            { "type", "Path symbol" },
            { "key", std::get<PathSymbol>(key) },
        };
    } else if (std::holds_alternative<SiteType>(key)) {
        return {
            { "type", "Site type" },
            { "key", std::get<SiteType>(key) },
        };
    } else if (std::holds_alternative<RegionNumber>(key)) {
        return {
            { "type", "Region" },
            { "key", std::get<RegionNumber>(key) },
        };
    } else {
        return {
            { "type", "Site by name" },
            { "key", std::get<std::string>(key) },
        };
    }
}

} // namespace

bool cardVisible(const Card & card, int requestingPlayer) {
    return card.faceUp || card.deck != PlayDeck && (requestingPlayer < 0 || requestingPlayer == card.owner);
}

void ResponseBuilder::addCardData(const Game & game, nlohmann::json & response) {
    nlohmann::json & jsonCardData = response["cardData"];
    jsonCardData = nlohmann::json::array();
    for (CardID id : referencedCards) {
        jsonCardData.push_back(nlohmann::json());
        nlohmann::json & jsonCard = jsonCardData.back();
        Card card = game.cards[id];
        jsonCard["id"] = aliasTable.toAlias.at(id);
        jsonCard["faceUp"] = card.faceUp;
        jsonCard["state"] = cardStateNames[card.state];
        jsonCard["name"] = cardVisible(card, requestingPlayer) ?
            getName(game.settings.cardDefinitions[card.dataIndex]->type).front() : "";
        if (card.parent >= 0)
            jsonCard["parent"] = aliasTable.toAlias.at(card.parent);
    }
}

CardAlias ResponseBuilder::toAlias(CardID id) {
    referencedCards.insert(id);
    return aliasTable.toAlias.at(id);
}

void ResponseBuilder::addCard(const Game & game, CardID card, nlohmann::json & jsonDeck) {
    jsonDeck.push_back(toAlias(card));
    const auto & possessions = game.cards[card].possessions;
    for (CardID possession : possessions)
        addCard(game, possession, jsonDeck);
}

void ResponseBuilder::addDeck(const Game & game, DeckName deckName, Side & side, nlohmann::json & jsonDecks) {
    jsonDecks.push_back(nlohmann::json());
    auto & jsonDeck = jsonDecks.back();
    jsonDeck["deck"] = deckNames.at(deckName);
    auto & jsonCards = jsonDeck["cards"];
    jsonCards = nlohmann::json::array();
    auto & deck = side.getDeck(deckName);
    for (CardID card : deck)
        addCard(game, card, jsonCards);
}

void ResponseBuilder::addCharacter(const Game & game, CardID character, nlohmann::json & jsonCharacter) {
    const Card & card = game.cards[character];
    jsonCharacter["id"] = toAlias(character);
    auto & jsonPossessions = jsonCharacter["possessions"];
    jsonPossessions = nlohmann::json::array();
    for (const CardID possession : card.possessions)
        addCard(game, possession, jsonPossessions);
    auto & jsonFollowers = jsonCharacter["followers"];
    jsonFollowers = nlohmann::json::array();
}

void ResponseBuilder::addCompany(const Game & game, const Company & company, nlohmann::json & jsonCompanies) {
    jsonCompanies.push_back(nlohmann::json());
    nlohmann::json & jsonCompany = jsonCompanies.back();
    jsonCompany["id"] = company.id;
    jsonCompany["currentLocation"] = toAlias(company.currentLocation);
    jsonCompany["newLocation"] = toAlias(company.newLocation);
    auto & jsonCharacters = jsonCompany["characters"];
    jsonCharacters = nlohmann::json::array();
    // Non-follower characters
    for (CardID character : company.characters) {
        if (game.cards[character].parent == NoParent) {
            jsonCharacters.push_back(nlohmann::json());
            addCharacter(game, character, jsonCharacters.back());
        }
    }
    // Followers
    for (CardID character : company.characters) {
        CardID parent = game.cards[character].parent;
        if (parent >= 0) {
            for (auto & jsonCharacter : jsonCharacters) {
                CardAlias cardAlias = jsonCharacter.find("id")->get<CardAlias>();
                CardID cardId = aliasTable.fromAlias.at(cardAlias);
                if (cardId == parent) {
                    auto & jsonFollowers = *jsonCharacter.find("followers");
                    jsonFollowers.push_back(aliasTable.toAlias.at(character));
                    break;
                }
            }
        }
    }
    for (CardID character : company.characters) {
        if (game.cards[character].parent >= 0) {
            jsonCharacters.push_back(nlohmann::json());
            addCharacter(game, character, jsonCharacters.back());
        }
    }
    // Permanent events
    auto & jsonPermanentEvents = jsonCompany["permanentEvents"];
    jsonPermanentEvents = nlohmann::json::array();
    for (CardID permanentEvent : company.permanentEvents)
        addCard(game, permanentEvent, jsonPermanentEvents);
    // On-guard
    auto & jsonOnGuard = jsonCompany["onGuard"];
    jsonOnGuard = nlohmann::json::array();
    for (CardID onGuard : company.onGuard)
        jsonOnGuard.push_back(toAlias(onGuard));
}

void ResponseBuilder::addCompanies(const Game & game, const Side & side, const std::vector<CompanyID> & companies, nlohmann::json & jsonSide) {
    auto & jsonCompanies = jsonSide["companies"];
    jsonCompanies = nlohmann::json::array();
    for (CompanyID id : companies) {
        for (const Company & company : side.companies) {
            if (company.id == id) {
                addCompany(game, company, jsonCompanies);
                break;
            }
        }
    }
}

nlohmann::json ContextToJson::visit(size_t contextIndex) {
    scene = std::make_unique<Scene>(game, contextIndex);
    return std::visit(*this, game.contextStack[contextIndex]->context);
}

nlohmann::json ContextToJson::operator()(RollDice & context) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Dice roll";
    jsonContext["player"] = context.player;
    jsonContext["rolls"] = context.nRolls;
    jsonContext["result"] = context.rolls;
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(BodyCheck & context) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Body check";
    jsonContext["target"] = responseBuilder.toAlias(context.target);
    jsonContext["body"] = context.body + context.modifier;
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(Strike & strike) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Strike";
    jsonContext["state"] = strikeStateNames[strike.state];
    jsonContext["card"] = strike.card < 0 ? -1 : responseBuilder.toAlias(strike.card);
    jsonContext["creatureType"] = strike.creatureType;
    jsonContext["creatureProwess"] = scene->getCreatureStat(Prowess, strike.card, strike.creatureType, strike.creatureBaseProwess) + strike.strikesSpent;
    jsonContext["creatureBody"] = scene->getCreatureStat(Body, strike.card, strike.creatureType, strike.creatureBaseBody);
    jsonContext["target"] = responseBuilder.toAlias(strike.target);
    jsonContext["targetProwess"] = scene->getTargetProwess(strike);
    jsonContext["targetBody"] = scene->getTargetBody(strike);
    jsonContext["modifiersSpent"] = strike.strikesSpent;
    jsonContext["modifiersLeft"] = strike.excessStrikes;
    jsonContext["detainment"] = strike.detainment ? "true" : "false";
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(AssignStrikes & context) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Assign strikes";
    jsonContext["state"] = assignStrikeStateNames[context.state];
    jsonContext["company"] = context.company;
    jsonContext["unassignedStrikes"] = context.unassignedStrikes;
    auto & jsonAssignments = jsonContext["assignments"];
    jsonAssignments = nlohmann::json::array();
    for (auto & assignment : context.assignments)
        jsonAssignments.push_back(responseBuilder.toAlias(assignment.target));
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(Attack & attack) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Attack";
    jsonContext["keyedTo"] = keyedTo(attack.keyedTo);
    jsonContext["card"] = attack.card < 0 ? -1 : responseBuilder.toAlias(attack.card);
    jsonContext["creatureType"] = attack.creatureType;
    jsonContext["prowess"] = scene->getCreatureStat(Prowess, attack.card, attack.creatureType, attack.prowess);
    jsonContext["body"] = scene->getCreatureStat(Body, attack.card, attack.creatureType, attack.body);
    jsonContext["strikes"] = attack.strikes;
    jsonContext["detainment"] = attack.detainment ? "true" : "false";
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(Encounter & encounter) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Encounter";
    jsonContext["keyedTo"] = keyedTo(encounter.keyedTo);
    jsonContext["company"] = encounter.company;
    jsonContext["card"] = encounter.card < 0 ? -1 : responseBuilder.toAlias(encounter.card);
    jsonContext["attacks"] = encounter.attacks.size();
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(CorruptionCheck & context) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Corruption check";
    jsonContext["character"] = responseBuilder.toAlias(context.targetCharacter);
    jsonContext["corruption"] = context.base + context.modifier;
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(ResolveCorruptionChecks & context) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Corruption checks";
    auto & jsonCorruptionChecks = jsonContext["corruptionChecks"];
    for (auto & corruptionCheck : context.unresolvedChecks)
        jsonCorruptionChecks.push_back({
            { "character", corruptionCheck.target },
            { "modifier", corruptionCheck.modifier },
        });
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(StoreCard & storeCard) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Store card";
    jsonContext["card"] = responseBuilder.toAlias(storeCard.target);
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(RemoveCorruptionCard & context) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Remove corruption card";
    jsonContext["character"] = responseBuilder.toAlias(context.target);
    jsonContext["requiredNumber"] = context.requiredNumber;
    jsonContext["modifier"] = context.modifier;
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(InfluenceAttempt & context) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Influence attempt";
    jsonContext["influencingCharacter"] = responseBuilder.toAlias(context.influencingCharacter);
    jsonContext["target"] = responseBuilder.toAlias(context.target);
    jsonContext["requiredInfluence"] = context.requiredInfluence;
    jsonContext["modifier"] = context.modifier;
    if (context.revealedCard >= 0)
        jsonContext["revealedCard"] = responseBuilder.toAlias(context.revealedCard);
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(RingTest & context) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Test gold ring";
    jsonContext["ring"] = responseBuilder.toAlias(context.targetRing);
    jsonContext["modifier"] = context.modifier;
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(SpecialTest & context) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Special test";
    jsonContext["player"] = context.player;
    jsonContext["host"] = responseBuilder.toAlias(context.host);
    jsonContext["target"] = responseBuilder.toAlias(context.target);
    jsonContext["modifier"] = context.modifier;
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(Movement & movement) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Movement";
    jsonContext["company"] = movement.company;
    jsonContext["origin"] = responseBuilder.toAlias(movement.siteOfOrigin);
    jsonContext["destination"] = responseBuilder.toAlias(movement.destination);
    auto & jsonSitePath = jsonContext["sitePath"];
    jsonSitePath = nlohmann::json::array();
    SitePath sitePath = scene->getSitePath();
    for (PathSymbol & pathSymbol : sitePath)
        jsonSitePath.push_back(pathSymbol);
    auto & jsonRegions = jsonContext["regions"];
    jsonRegions = nlohmann::json::array();
    for (RegionNumber & region : movement.regions)
        jsonRegions.push_back(region);
    jsonContext["hazardLimit"] = movement.hazardLimit;
    jsonContext["hazardsPlayed"] = movement.hazardsPlayed;
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(DrawCards & context) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Draw cards";
    jsonContext["company"] = context.company;
    jsonContext["playerLimit"] = context.playerLimit;
    jsonContext["playerCardsDrawn"] = context.playerCardsDrawn;
    jsonContext["opponentLimit"] = context.opponentLimit;
    jsonContext["opponentCardsDrawn"] = context.opponentCardsDrawn;
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(Arrival & context) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Company conflict";
    auto & jsonCompanies = jsonContext["conflictingCompanies"];
    jsonCompanies = nlohmann::json::array();
    for (auto & company : context.returnToOrigin)
        jsonCompanies.push_back(company);
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(Explore & explore) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Site phase";
    jsonContext["state"] = exploreStateNames[explore.state];
    jsonContext["company"] = explore.company;
    jsonContext["site"] = responseBuilder.toAlias(explore.site);
    auto & jsonPlayable = jsonContext["playable"];
    jsonPlayable = nlohmann::json::array();
    for (auto & itemType : explore.playable)
        jsonPlayable.push_back(itemType);
    if (explore.containsHoard)
        jsonContext["containsHoard"] = "true";
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(SelectCompany & context) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Select company";
    auto & jsonCompanies = jsonContext["companies"];
    jsonCompanies = nlohmann::json::array();
    for (CompanyID & id : context.remainingCompanies)
        jsonCompanies.push_back(id);
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(TransferCards & context) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Transfer cards";
    auto & jsonTransactions = jsonContext["transactions"];
    jsonTransactions = nlohmann::json::array();
    for (auto & transaction : context.transactions) {
        jsonTransactions.push_back(nlohmann::json());
        auto & jsonTransaction = jsonTransactions.back();
        jsonTransaction["player"] = transaction.first;
        auto & jsonTransactionOptions = jsonTransaction["options"];
        jsonTransactionOptions = nlohmann::json::array();
        for (auto & option : transaction.second) {
            jsonTransactionOptions.push_back(nlohmann::json());
            auto & jsonTransactionOption = jsonTransactionOptions.back();
            jsonTransactionOption["sourceDeck"] = option.sourceDeck;
            jsonTransactionOption["destinationDeck"] = option.destinationDeck;
            auto & jsonCardTypes = jsonTransactionOption["cardTypes"];
            jsonCardTypes = nlohmann::json::array();
            for (auto & cardType : option.categories)
                jsonCardTypes.push_back(cardType);
            jsonTransactionOption["minimum"] = option.minimum;
            jsonTransactionOption["maximum"] = option.maximum;
        }
    }
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(TransferItems & context) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Transfer items";
    jsonContext["player"] = context.player;
    jsonContext["company"] = context.company;
    jsonContext["allowedTransfers"] = context.allowedTransfers;
    auto & jsonItems = jsonContext["items"];
    jsonItems = nlohmann::json::array();
    for (CardID item : context.items)
        jsonItems.push_back(responseBuilder.toAlias(item));
    auto & jsonTaken = jsonContext["taken"];
    jsonTaken = nlohmann::json::array();
    for (auto & taken : context.assignments) {
        jsonTaken.push_back(nlohmann::json());
        auto & jsonEntry = jsonTaken.back();
        jsonEntry["character"] = responseBuilder.toAlias(taken.second);
        jsonEntry["item"] = responseBuilder.toAlias(taken.first);
    }
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(ReturnToHandSize & context) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Return to hand size";
    jsonContext["resourcePlayerMayDiscard"] = context.resourcePlayerMayDiscard ? "true" : "false";
    jsonContext["hazardPlayerMayDiscard"] = context.hazardPlayerMayDiscard ? "true" : "false";
    jsonContext["resourcePlayerHandSize"] = context.resourcePlayerHandSize;
    jsonContext["hazardPlayerHandSize"] = context.hazardPlayerHandSize;
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(ExhaustDeck & exhaust) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Play deck exhausted";
    jsonContext["player"] = exhaust.player;
    if (responseBuilder.requestingPlayer != exhaust.player)
        return jsonContext;

    auto & jsonSideboard = jsonContext["fromSideboard"];
    jsonSideboard = nlohmann::json::array();
    for (CardID card : exhaust.fromSideboard)
        jsonSideboard.push_back(responseBuilder.toAlias(card));
    auto & jsonDiscardPile = jsonContext["fromDiscardPile"];
    jsonDiscardPile = nlohmann::json::array();
    for (CardID card : exhaust.fromDiscardPile)
        jsonDiscardPile.push_back(responseBuilder.toAlias(card));
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(OrganizeCompanies & context) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Organization phase";
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(DiscardSubset & discard) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Discard cards";
    jsonContext["player"] = discard.player;
    jsonContext["numberToDiscard"] = discard.subsetSize;
    if (responseBuilder.requestingPlayer != discard.player)
        return jsonContext;

    auto & jsonCards = jsonContext["cards"];
    jsonCards = nlohmann::json::array();
    for (CardID card : discard.cards)
        jsonCards.push_back(responseBuilder.toAlias(card));
    auto & jsonSelection = jsonContext["selection"];
    jsonSelection = nlohmann::json::array();
    for (CardID card : discard.selection)
        jsonSelection.push_back(responseBuilder.toAlias(card));
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(TapCharacters & tap) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Tap characters";
    jsonContext["player"] = tap.player;
    jsonContext["numberToTap"] = tap.numberOfCharactersToTap;
    auto & jsonCharacters = jsonContext["characters"];
    jsonCharacters = nlohmann::json::array();
    for (CardID character : tap.characters)
        jsonCharacters.push_back(responseBuilder.toAlias(character));
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(ChainOfEffects & context) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Chain of effects";
    return jsonContext;
}

nlohmann::json ContextToJson::operator()(BottomContext & context) {
    nlohmann::json jsonContext;
    jsonContext["context"] = "Turn sequence";
    jsonContext["phase"] = game.turnSequence.phase;
    jsonContext["player"] = game.currentPlayer;
    return jsonContext;
}
