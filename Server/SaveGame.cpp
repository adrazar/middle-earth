#include "SaveGame.h"
#include "../CardSets/CardSetLoader.h"
#include "../CardSets/CardSets.h"
#include "../Middle-Earth/GameInterface.h"
#include "../Middle-Earth/GameRecord.h"

namespace {

const TwoWayMap<CardSet> cardSetNames{
    { CardSet::CoreCards, "CoreCards" },
    { CardSet::DwarfDeck, "DwarfDeck" },
    { CardSet::OrcDeck, "OrcDeck" },
};

const TwoWayMap<GameLength> gameLengthNames{
    { GameLength::ShortGame, "ShortGame" },
    { GameLength::LongGame, "LongGame" },
    { GameLength::CampaignGame, "CampaignGame" },
};

const TwoWayMap<RecordEntryType> recordTypeNames{
    { RecordEntryType::Done, "done" },
    { RecordEntryType::Action, "action" },
    { RecordEntryType::Roll, "roll" },
    { RecordEntryType::RollResult, "rollResult" },
    { RecordEntryType::SelectRoll, "selectRoll" },
    { RecordEntryType::ShuffleDeck, "shuffleDeck" },
    { RecordEntryType::SelectDestination, "selectDestination" },
    { RecordEntryType::TakeFollower, "takeFollower" },
    { RecordEntryType::DropFollower, "dropFollower" },
    { RecordEntryType::TransferCharacter, "transferCharacter" },
    { RecordEntryType::DiscardCharacter, "discardCharacter" },
    { RecordEntryType::RearrangeEquipment, "rearrangeEquipment" },
    { RecordEntryType::TransferItem, "transferItem" },
    { RecordEntryType::Store, "store" },
    { RecordEntryType::ChooseCompany, "chooseCompany" },
    { RecordEntryType::DrawCard, "drawCard" },
    { RecordEntryType::Discard, "discard" },
    { RecordEntryType::TransferCards, "transferCards" },
    { RecordEntryType::ExchangeCards, "exchangeCards" },
    { RecordEntryType::SetKeyedTo, "setKeyedTo" },
    { RecordEntryType::PlaceOnGuard, "placeOnGuard" },
    { RecordEntryType::AssignStrikes, "assignStrikes" },
    { RecordEntryType::UnassignStrike, "unassignStrike" },
    { RecordEntryType::ResolveStrike, "resolveStrike" },
    { RecordEntryType::UseStrikes, "useStrikes" },
    { RecordEntryType::Tap, "tap" },
    { RecordEntryType::DistributePossessions, "distributePossessions" },
    { RecordEntryType::CancelHomeSite, "cancelHomeSite" },
    { RecordEntryType::EnterSite, "enterSite" },
    { RecordEntryType::AttackOpponent, "attackOpponent" },
    { RecordEntryType::InfluenceOpponent, "influenceOpponent" },
    { RecordEntryType::RevealIdenticalCard, "revealIdenticalCard" },
    { RecordEntryType::TakeInfluencedCard, "takeInfluencedCard" },
    { RecordEntryType::ResolveCorruptionCheck, "resolveCorruptionCheck" },
};

void done(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.done(entry.player);
}
void action(MetaGame & metaGame, const RecordEntry & entry) {
    Side & side = metaGame.gameStates.back().sides[entry.player];
    side.selectedCards.clear();
    for (CardID card : entry.args)
        side.selectedCards.push_back(card);
    metaGame.useAction(entry.player, entry.option);
}
void roll(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.roll(entry.player);
}
void selectRoll(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.selectRoll(entry.player, entry.option);
}
void selectDestination(MetaGame & metaGame, const RecordEntry & entry) {
    auto it = entry.args.begin();
    CompanyID id = *it++;
    CardID destination = *it++;
    Company & company = *metaGame.gameStates.back().sides[entry.player].findCompany(id);
    company.regions.clear();
    while (it != entry.args.end())
        company.regions.push_back(RegionNumber(*it));
    metaGame.selectDestination(entry.player, id, destination, company.regions);
}
void takeFollower(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.takeFollower(entry.player, entry.args[0], entry.args[1]);
}
void dropFollower(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.dropFollower(entry.player, entry.args[1]);
}
void transferCharacter(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.transferCharacter(entry.player, entry.args[0], entry.args[1]);
}
void discardCharacter(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.discardCharacter(entry.player, entry.args.front());
}
void rearrangeEquipment(MetaGame & metaGame, const RecordEntry & entry) {
    auto it = entry.args.begin();
    CardID target = *it++;
    std::vector<CardID> equipment(it, entry.args.end());
    metaGame.rearrangeEquipment(entry.player, target, equipment);
}
void transferItem(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.transferItem(entry.player, entry.args[0], entry.args[1]);
}
void store(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.store(entry.player, entry.args.front());
}
void chooseCompany(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.chooseCompany(entry.player, entry.args.front());
}
void drawCard(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.drawCard(entry.player);
}
void discard(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.discard(entry.player, entry.args);
}
void transferCards(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.transferCards(entry.player, entry.args, entry.option);
}
void exchangeCard(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.exchangeCard(entry.player, entry.args.front());
}
void exchangeCards(MetaGame & metaGame, const RecordEntry & entry) {
    size_t n = entry.args.size() / 2;
    if (n == 0)
        return;
    auto it = entry.args.begin();
    auto & context = std::get<ExhaustDeck>(metaGame.gameStates.back().contextStack.back()->context);
    context.fromSideboard.insert(it, it + n);
    it += n;
    context.fromDiscardPile.insert(it, it + n);
}
void setKeyedTo(MetaGame & metaGame, const RecordEntry & entry) {
    KeyedToType key;
    switch (entry.option) {
    case 0:
        key = PathSymbol(entry.args.back());
        break;
    case 1:
        key = SiteType(entry.args.back());
        break;
    case 2:
        key = RegionNumber(entry.args.back());
        break;
    case 3:
        std::string siteName;
        for (CardID c : entry.args)
            siteName.push_back(char(c));
        key = siteName;
        break;
    }
    metaGame.setKeyedTo(entry.player, key);
}
void placeOnGuard(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.placeOnGuard(entry.player, entry.args.front());
}
void assignStrikes(MetaGame & metaGame, const RecordEntry & entry) {
    for (CardID target : entry.args)
        metaGame.assignStrike(entry.player, target);
}
void unassignStrike(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.unassignStrike(entry.player, entry.args.front());
}
void resolveStrike(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.resolveStrike(entry.player, entry.args.front());
}
void useStrikes(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.useStrikes(entry.player, entry.option);
}
void tap(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.tap(entry.player, entry.args.front());
}
void distributePossessions(MetaGame & metaGame, const RecordEntry & entry) {
    auto it = entry.args.begin();
    while (it != entry.args.end())
        metaGame.selectItemRecipient(entry.player, *it++, *it++);
}
void cancelHomeSite(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.cancelHomeSite(entry.player, entry.args.front());
}
void enterSite(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.enterSite(entry.player);
}
void attackOpponent(MetaGame & metaGame, const RecordEntry & entry) {
    // Not implemented
}
void influenceOpponent(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.influenceOpponent(entry.player, entry.args[0], entry.args[1]);
}
void revealIdenticalCard(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.revealIdenticalCard(entry.player, entry.args.front());
}
void takeInfluencedCard(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.takeInfluencedCard(entry.player, entry.args.front());
}
void resolveCorruptionCheck(MetaGame & metaGame, const RecordEntry & entry) {
    metaGame.cancelHomeSite(entry.player, entry.args.front());
}

using ReplayFunction = void(*)(MetaGame & metaGame, const RecordEntry & entry);
const std::unordered_map<RecordEntryType, ReplayFunction> replayFunctions{
    { RecordEntryType::Done, done },
    { RecordEntryType::Action, action },
    { RecordEntryType::Roll, roll },
    { RecordEntryType::SelectRoll, selectRoll },
    { RecordEntryType::SelectDestination, selectDestination },
    { RecordEntryType::TakeFollower, takeFollower },
    { RecordEntryType::DropFollower, dropFollower },
    { RecordEntryType::TransferCharacter, transferCharacter },
    { RecordEntryType::DiscardCharacter, discardCharacter },
    { RecordEntryType::RearrangeEquipment, rearrangeEquipment },
    { RecordEntryType::TransferItem, transferItem },
    { RecordEntryType::Store, store },
    { RecordEntryType::ChooseCompany, chooseCompany },
    { RecordEntryType::DrawCard, drawCard },
    { RecordEntryType::Discard, discard },
    { RecordEntryType::TransferCards, transferCards },
    { RecordEntryType::ExchangeCard, exchangeCard },
    { RecordEntryType::ExchangeCards, exchangeCards },
    { RecordEntryType::SetKeyedTo, setKeyedTo },
    { RecordEntryType::PlaceOnGuard, placeOnGuard },
    { RecordEntryType::AssignStrikes, assignStrikes },
    { RecordEntryType::UnassignStrike, unassignStrike },
    { RecordEntryType::ResolveStrike, resolveStrike },
    { RecordEntryType::UseStrikes, useStrikes },
    { RecordEntryType::Tap, tap },
    { RecordEntryType::DistributePossessions, distributePossessions },
    { RecordEntryType::CancelHomeSite, cancelHomeSite },
    { RecordEntryType::EnterSite, enterSite },
    { RecordEntryType::AttackOpponent, attackOpponent },
    { RecordEntryType::InfluenceOpponent, influenceOpponent },
    { RecordEntryType::RevealIdenticalCard, revealIdenticalCard },
    { RecordEntryType::TakeInfluencedCard, takeInfluencedCard },
    { RecordEntryType::ResolveCorruptionCheck, resolveCorruptionCheck },
};

struct ExchangeCards {
    void clear() {
        fromSideboard.clear();
        fromDiscardPile.clear();
    }
    std::unordered_set<CardID> & getDeck(int option) {
        return option == 0 ? fromSideboard : fromDiscardPile;
    }

    int player;
    std::unordered_set<CardID> fromSideboard, fromDiscardPile;
};

struct DistributePossessions {
    void assign(std::vector<CardID> newAssignments) {
        CardID item, target;
        bool assign;
        auto it = newAssignments.begin();
        while (it != newAssignments.end()) {
            item = *it++;
            target = *it++;
            assign = true;
            for (auto it2 = assignments.begin(); it2 != assignments.end(); ++it2)
                if (it2->second == target) {
                    assign = (item != it2->first);
                    assignments.erase(it2);
                    break;
                }
            if (assign)
                assignments[item] = target;
        }
    }

    int player;
    std::unordered_map<CardID, CardID> assignments;
};

struct AssignStrikes {
    int player;
    std::unordered_set<CardID> assignments;
};

struct StrikesSpent {
    int player;
    int strikes = 0;
};

struct RearrangeEquipment {
    using Rearrangements = std::unordered_map<CardID, std::vector<CardID>>;
    std::unordered_map<int, Rearrangements> rearrangements;
};

struct ManageFollowers {
    using LeaderToFollowers = std::unordered_map<CardID, std::unordered_set<CardID>>;
    void processFollower(CardID leader, CardID follower, LeaderToFollowers & insertMap, LeaderToFollowers & deleteMap) {
        auto it = deleteMap.find(leader);
        if (it == deleteMap.end())
            insertMap[leader].insert(follower);
        else {
            it->second.erase(follower);
            if (it->second.empty())
                deleteMap.erase(it);
        }
    }
    void takeFollower(CardID leader, CardID follower) {
        processFollower(leader, follower, takenFollowers, droppedFollowers);
    }
    void dropFollower(CardID leader, CardID follower) {
        processFollower(leader, follower, droppedFollowers, takenFollowers);
    }
    bool empty() {
        return takenFollowers.empty() && droppedFollowers.empty();
    }

    int player;
    LeaderToFollowers takenFollowers, droppedFollowers;
};

struct SelectDestination {
    void insertDestination(RecordEntry & entry) {
        player = entry.player;
        destinations[entry.args[0]] = entry.args;
    }

    int player;
    std::unordered_map<CompanyID, std::vector<CardID>> destinations;
};

void commitRearrangements(RearrangeEquipment & rearrangeEquipment, std::vector<RecordEntry> & record) {
    for (auto & rearrangements : rearrangeEquipment.rearrangements) {
        for (auto & rearrangement : rearrangements.second) {
            record.emplace_back(RecordEntryType::RearrangeEquipment, rearrangements.first);
            auto & args = record.back().args;
            args.push_back(rearrangement.first);
            for (CardID item : rearrangement.second)
                args.push_back(item);
        }
    }
    rearrangeEquipment.rearrangements.clear();
}

void commitManageFollowers(ManageFollowers & manageFollowers, std::vector<RecordEntry> & record) {
    for (auto & takenFollowers : manageFollowers.takenFollowers)
        for (CardID takenFollower : takenFollowers.second)
            record.emplace_back(RecordEntryType::TakeFollower, manageFollowers.player, std::vector<CardID>{ takenFollowers.first, takenFollower });
    for (auto & droppedFollowers : manageFollowers.droppedFollowers)
        for (CardID droppedFollower : droppedFollowers.second)
            record.emplace_back(RecordEntryType::TakeFollower, manageFollowers.player, std::vector<CardID>{ droppedFollowers.first, droppedFollower });
    manageFollowers.takenFollowers.clear();
    manageFollowers.droppedFollowers.clear();
}

std::vector<RecordEntry> compressGameRecord(const std::vector<RecordEntry> & rawRecord) {
    std::vector<RecordEntry> record;
    DistributePossessions distributePossessions;
    RearrangeEquipment rearrangeEquipment;
    SelectDestination selectDestination;
    ManageFollowers manageFollowers;
    ExchangeCards exchangeCards;
    AssignStrikes assignStrikes;
    StrikesSpent strikesSpent;
    // TransferCharacter compression might interfer with take/drop followers compression

    for (const RecordEntry & entry : rawRecord) {
        if (entry.type == RecordEntryType::ExchangeCard) {
            exchangeCards.player = entry.player;
            std::unordered_set<CardID> & deck = exchangeCards.getDeck(entry.option);
            auto it = deck.find(entry.args.front());
            if (it != deck.end())
                deck.erase(it);
            else
                deck.insert(entry.args.front());
        } else if (entry.type == RecordEntryType::AssignStrikes) {
            assignStrikes.player = entry.player;
            assignStrikes.assignments.insert(entry.args.front());
        } else if (entry.type == RecordEntryType::UnassignStrike) {
            assignStrikes.player = entry.player;
            assignStrikes.assignments.erase(entry.args.front());
        } else if (entry.type == RecordEntryType::UseStrikes) {
            strikesSpent.player = entry.player;
            strikesSpent.strikes = entry.option;
        } else if (entry.type == RecordEntryType::DistributePossessions) {
            distributePossessions.player = entry.player;
            distributePossessions.assign(entry.args);
        } else if (entry.type == RecordEntryType::RearrangeEquipment) {
            auto & rearrangement = rearrangeEquipment.rearrangements[entry.player];
            rearrangement[entry.args.front()] = std::vector<CardID>(entry.args.begin() + 1, entry.args.end());
        } else if (entry.type == RecordEntryType::TakeFollower) {
            manageFollowers.player = entry.player;
            manageFollowers.takeFollower(entry.args[0], entry.args[1]);
        } else if (entry.type == RecordEntryType::DropFollower) {
            manageFollowers.player = entry.player;
            manageFollowers.dropFollower(entry.args[0], entry.args[1]);
        } else if (entry.type == RecordEntryType::Done) {
            if (!rearrangeEquipment.rearrangements.empty())
                commitRearrangements(rearrangeEquipment, record);
            if (!manageFollowers.empty())
                commitManageFollowers(manageFollowers, record);

            if (!exchangeCards.fromSideboard.empty()) {
                record.emplace_back(RecordEntryType::ExchangeCards, exchangeCards.player);
                auto & args = record.back().args;
                for (CardID card : exchangeCards.fromSideboard)
                    args.push_back(card);
                for (CardID card : exchangeCards.fromDiscardPile)
                    args.push_back(card);
                exchangeCards.clear();
            }
            else if (!assignStrikes.assignments.empty()) {
                record.emplace_back(RecordEntryType::AssignStrikes, assignStrikes.player);
                auto & args = record.back().args;
                for (CardID card : assignStrikes.assignments)
                    args.push_back(card);
                assignStrikes.assignments.clear();
            }
            else if (strikesSpent.strikes > 0) {
                record.emplace_back(RecordEntryType::UseStrikes, strikesSpent.player, std::vector<CardID>(), strikesSpent.strikes);
                strikesSpent.strikes = 0;
            }
            else if (!distributePossessions.assignments.empty()) {
                record.emplace_back(RecordEntryType::DistributePossessions, distributePossessions.player);
                auto & args = record.back().args;
                for (auto & assignment : distributePossessions.assignments) {
                    args.push_back(assignment.first);
                    args.push_back(assignment.second);
                }
                distributePossessions.assignments.clear();
            }
            else if (!selectDestination.destinations.empty()) {
                // Some of the registered company ids may have vanished at this point,
                // but this will not cause problems for metaGame.selectDestination.
                for (auto & travel : selectDestination.destinations)
                    RecordEntry & recordEntry = record.emplace_back(RecordEntryType::SelectDestination, selectDestination.player, travel.second);
            }

            record.emplace_back(RecordEntryType::Done, entry.player);
        } else {
            if (!rearrangeEquipment.rearrangements.empty())
                commitRearrangements(rearrangeEquipment, record);
            if (!manageFollowers.empty())
                commitManageFollowers(manageFollowers, record);

            record.emplace_back(entry);
        }
    }

    return record;
}

} // namespace

template<class T>
TwoWayMap<T>::TwoWayMap(std::initializer_list<std::pair<T, std::string>> values) {
    for (auto & value : values) {
        enumClassToString[value.first] = value.second;
        stringToEnumClass[value.second] = value.first;
    }
}

template<class T>
const std::string & TwoWayMap<T>::toString(const T & key) const {
    const auto & it = enumClassToString.find(key);
    return it->second;
}

template<class T>
const T & TwoWayMap<T>::toEnum(const std::string & key) const {
    const auto & it = stringToEnumClass.find(key);
    return it->second;
}

void to_json(nlohmann::json & j, const RecordEntry & entry) {
    j["type"] = recordTypeNames.toString(entry.type);
    j["player"] = entry.player;
    j["args"] = entry.args;
    j["option"] = entry.option;
}

void from_json(const nlohmann::json & j, RecordEntry & entry) {
    std::string typeName;
    j.at("type").get_to(typeName);
    entry.type = recordTypeNames.toEnum(typeName);
    j.at("player").get_to(entry.player);
    j.at("args").get_to(entry.args);
    j.at("option").get_to(entry.option);
}

void saveGameSettings(const GameSettings & settings, nlohmann::json & jsonSettings, bool compress) {
    auto & jsonCardSets = jsonSettings["cardSets"];
    jsonCardSets = nlohmann::json::array();
    for (const auto & cardSet : settings.loadedCardSets)
        jsonCardSets.push_back(cardSetNames.toString(cardSet));
    jsonSettings["decks"] = settings.decks;
    jsonSettings["gameLength"] = gameLengthNames.toString(settings.gameLength);
    try {
        if (compress)
            jsonSettings["gameRecord"] = compressGameRecord(settings.gameRecord.record);
        else
            jsonSettings["gameRecord"] = settings.gameRecord.record;
    } catch(std::runtime_error &) {
        throw std::exception("Error encountered during game record compression");
    }
}

void loadGameSettings(GameSettings & settings, GameConfigurer & configurer, const nlohmann::json & jsonSettings) {
    settings.loadedCardSets.clear();
    settings.cardDefinitions.clear();
    settings.cardNameToDefinitionIndex.clear();
    settings.decks.clear();

    for (const std::string & cardSetString : jsonSettings["cardSets"])
        loadCardSet(settings, cardSetNames.toEnum(cardSetString));

    for (const nlohmann::json & deck : jsonSettings["decks"]) {
        PlayerConfigurer player = configurer.addPlayer();
        player.loadDeck(deck);
    }

    settings.gameLength = gameLengthNames.toEnum(jsonSettings["gameLength"]);

    auto & record = settings.gameRecord.record;
    record.clear();
    record = jsonSettings["gameRecord"];
}

void replay(MetaGame & metaGame) {
    GameRecord & record = metaGame.settings.gameRecord;

    record.startReplay();
    while (record.replayPos != record.record.end()) {
        const RecordEntry & entry = record.nextEntry();
        replayFunctions.at(entry.type)(metaGame, entry);
    }
    record.replay = false;
}
