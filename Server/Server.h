#pragma once
#include "Clients.h"
#include "RequestParser.h"
#include "ServerTypes.h"
#include "../ServerApi/RequestTypes.h"
#include "../Middle-Earth/GameInterface.h"
#include <nlohmann/json.hpp>
#include <memory>
#include <ostream>
#include <string>
#include <unordered_map>


struct Table {
    enum class GameProgress { NotStarted, Playing, Finished };
    using Handle = server::Handle;
    int  assignPlayer(Handle handle);                // Assign client to first unassigned player, if any
    void assignPlayer(Handle handle, int player);    // Assign client to a given player
    void setNumberOfPlayers(std::size_t players);

    size_t nPlayers = 2;
    std::array<Handle, 4> assignedPlayers = { -1, -1, -1, -1 };
    std::unordered_set<Handle> joiners;
    server::StateID stateId = 0;
    MetaGame metaGame;
    GameProgress gameProgress = GameProgress::NotStarted;
};

class Server {
    using Handle = server::Handle;
    using TableID = server::TableID;
    using Tables = std::unordered_map<server::TableID, std::unique_ptr<Table>>;

public:
    Server(boost::asio::io_context & ioContext, std::ostream & ostream_ = std::cout);
    void start();
    void stop();
    void receiveHandler(Handle handle, const std::string & msg);

    void validateHeader(const server::TableRequestHeader & header);
    void validateTableId(server::TableID tableId);

    server::TableID createTable();
    void listTables(Handle handle, const std::string & filter);
    void createTable(Handle handle, size_t nPlayers, int requestId);
    void joinTable(Handle handle, TableID tableId);
    void leaveTable(Handle handle, TableID tableId);
    void sit(Handle handle, TableID tableId, int position);
    void rise(Handle handle, TableID tableId);

    void authenticate(Handle handle, const std::string & userName);
    bool isClientAtTable(Handle handle, TableID tableId);
    void error(Handle handle, int requestId, std::string & msg);
    void syncCards(const server::TableRequestHeader & header, const CardSyncRequest & request);
    void getContext(const server::TableRequestHeader & header);
    void getContext(const server::TableRequestHeader & header, int index);

    void saveGame(const server::TableRequestHeader & header, const std::string & fileName, bool compress);
    void loadGame(const server::TableRequestHeader & header, const nlohmann::json & jsonGame);

    Tables tables;
    std::ostream & ostream;

private:
    boost::asio::io_context & ioContext_;
    Clients clients_;
    RequestParser parser_;
};
