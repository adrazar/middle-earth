#include "CoreCards.h"
#include "Middle-Earth.h"
#include "GameInterface.h"
#include "CardSetLoader.h"
#include <iostream>
#include <memory>
#include <utility>

int main() {
    MetaGame metaGame;
    GameSettings & settings = metaGame.settings;
    loadCardSet(metaGame.settings, CardSet::DwarfDeck);
    for (std::unique_ptr<CardDefinition> & def : settings.cardDefinitions) {
        std::cout << getName(def->type).front() << std::endl;
    }
    return 0;
}
