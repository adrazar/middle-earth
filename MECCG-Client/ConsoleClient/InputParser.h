#pragma once

#include <sstream>
#include <unordered_map>
#include "CommandApi.h"
#include "RequestTypes.h"

class InputParser {
public:
    InputParser(CommandApi & commandApi);
    std::string parseInputLine(const std::string & line, Table & table);

private:
    using CommandFunction = void(*)(std::istream & stream, CommandApi & commandApi, Table & table);
    using CommandMap = std::unordered_map<std::string, CommandFunction>;

    CommandApi & commandApi_;
    static CommandMap commandMap_;
};
