#pragma once
#include "../../ServerApi/BoostServerApi.h"
#include "CommandApi.h"
#include "ConsoleUiApi.h"
#include <nlohmann/json.hpp>
#include <string>

using CustomHandlerMap = std::unordered_map<int, CustomHandler>;

class ReceiveHandler {
public:
    ReceiveHandler(ConsoleUiApi & uiApi, BoostServerApi & serverApi, CustomHandlerMap & customHandlers);
    void operator()(const std::error_code & ec, const std::string & msg);

private:
    ConsoleUiApi & uiApi_;
    BoostServerApi & serverApi_;
    CustomHandlerMap & customHandlers_;
};
