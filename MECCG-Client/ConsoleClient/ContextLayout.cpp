#include "ContextLayout.h"
#include "EnumNames.h"
#include "ResponseOutput.h"
#include <unordered_set>

std::vector<std::string> contextOutput(const nlohmann::json & contextResponse, size_t screenWidth) {
    ContextLayout contextLayout(contextResponse);
    return contextLayout.generateOutput(screenWidth);
}

namespace {

std::string leftPadding(const std::string & string, size_t width) {
    return (string.size() >= width) ? string : std::string(width - string.size(), ' ') + string;
}

std::string keyedTo(const nlohmann::json & jsonKeyedTo) {
    const std::string & keyedToType = jsonKeyedTo["type"];
    if (keyedToType == "PathSymbol") {
        return pathSymbolNames[jsonKeyedTo["key"]];
    } else if (keyedToType == "Site type") {
        return siteTypeNames[jsonKeyedTo["key"]];
    } else if (keyedToType == "Region") {
        return regionNames[jsonKeyedTo["key"]];
    } else {
        return jsonKeyedTo["key"];
    }
}

std::string mayDiscardMessage(bool mayDiscard) {
    std::string negation = mayDiscard ? " " : "not ";
    return "May " + negation + "discard one card before returning to hand size";
}

using RenderFunction = void(*)(StringList & lines, const ContextLayout & context);

void renderRoll(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    lines.push_back("Player: " + std::to_string(int(jsonContext["player"])));
    lines.push_back("Number of rolls: " + std::to_string(int(jsonContext["rolls"])));
    std::string rolls;
    auto & jsonRolls = jsonContext["result"];
    for (int roll : jsonRolls)
        rolls += " " + std::to_string(roll);
    lines.push_back("Rolls:" + rolls);
}

void renderBodyCheck(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    lines.push_back(contextLayout.cardName(jsonContext["target"]));
    lines.push_back("Body: " + std::to_string(int(jsonContext["body"])));
}

void renderStrike(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    std::string detainment = jsonContext["detainment"] == "true" ? " (detainment)" : "";
    lines.push_back(std::string(jsonContext["context"]) + detainment);
    lines.push_back(jsonContext["state"]);
    lines.push_back("Excess strikes spent: " +
        std::to_string(int(jsonContext["modifiersSpent"])) + "/" +
        std::to_string(int(jsonContext["modifiersLeft"])));

    lines.push_back("");
    CardID card = jsonContext["card"];
    std::string creatureType = " (" + raceNames[int(jsonContext["creatureType"])] + ")";
    lines.push_back(contextLayout.cardName(card) + creatureType);
    lines.push_back("Prowess: " + leftPadding(jsonContext["creatureProwess"], 2));
    int body = jsonContext["creatureBody"];
    lines.push_back("Body:    " + leftPadding((card <= 0 ? "-" : std::to_string(body)), 2));

    lines.push_back("");
    lines.push_back(contextLayout.cardName(jsonContext["target"]));
    lines.push_back("Prowess: " + leftPadding(jsonContext["targetProwess"], 2));
    lines.push_back("Body:    " + leftPadding(jsonContext["targetBody"], 2));
}

void renderAssignStrikes(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    lines.push_back(jsonContext["state"]);
    lines.push_back("");
    lines.push_back("Company: " + std::to_string(int(jsonContext["company"])));
    lines.push_back("Unassigned strikes: " + std::to_string(int(jsonContext["unassignedStrikes"])));
    lines.push_back("");
    lines.push_back("Assignments");
    const auto & jsonAssignments = jsonContext["assignments"];
    if (jsonAssignments.empty())
        lines.push_back("<None>");
    else
        for (CardID target : jsonAssignments)
            lines.push_back(contextLayout.cardName(target));
}

void renderAttack(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    std::string detainment = jsonContext["detainment"] == "true" ? " (detainment)" : "";
    lines.push_back(std::string(jsonContext["context"]) + detainment);
    lines.push_back("Keyed to " + keyedTo(jsonContext["keyedTo"]));

    lines.push_back("");
    CardID card = jsonContext["card"];
    std::string creatureType = " (" + raceNames[int(jsonContext["creatureType"])] + ")";
    lines.push_back(contextLayout.cardName(card) + creatureType);
    lines.push_back("Strikes: " + leftPadding(jsonContext["strikes"], 2));
    lines.push_back("Prowess: " + leftPadding(jsonContext["prowess"], 2));
    int body = jsonContext["body"];
    lines.push_back("Body:    " + leftPadding((card <= 0 ? "-" : std::to_string(body)), 2));
}

void renderEncounter(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    lines.push_back("Keyed to " + keyedTo(jsonContext["keyedTo"]));
    lines.push_back("Company: " + std::to_string(int(jsonContext["company"])));
    CardID card = jsonContext["card"];
    lines.push_back(contextLayout.cardName(card));
    lines.push_back("Attacks: " + leftPadding(jsonContext["attacks"], 2));
}

void renderCorruptionCheck(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    lines.push_back(contextLayout.cardName(jsonContext["character"]));
    lines.push_back("Corruption: " + jsonContext["corruption"]);
}

void renderCorruptionChecks(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    const auto & jsonCorruptionChecks = jsonContext["corruptionChecks"];
    for (const auto & jsonCorruptionCheck : jsonCorruptionChecks) {
        int modifier = jsonCorruptionCheck["modifier"];
        std::string modifierString = modifier == 0 ? "" : "(" + std::to_string(modifier) + ")";
        lines.push_back(contextLayout.cardName(jsonContext["character"]) + modifierString);
    }
}

void renderStoreCard(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    lines.push_back(contextLayout.cardName(jsonContext["card"]));
}

void renderRemoveCorruptionCard(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    lines.push_back(contextLayout.cardName(jsonContext["character"]));
    lines.push_back("Roll greater than " + jsonContext["requiredNumber"]);
    lines.push_back("Modifier: " + jsonContext["modifier"]);
}

void renderInflucenceAttempt(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    lines.push_back(contextLayout.cardName(jsonContext["influencingCharacter"]));
    lines.push_back("attempts to influence");
    lines.push_back(contextLayout.cardName(jsonContext["target"]));
    lines.push_back("Roll greater than " + jsonContext["requiredInfluence"]);
    lines.push_back("Modifier: " + jsonContext["modifier"]);
    auto it = jsonContext.find("revealedCard");
    if (it != jsonContext.end())
        lines.push_back("Revealed card: " + contextLayout.cardName(*it));
}

void renderRingTest(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    lines.push_back(contextLayout.cardName(jsonContext["ring"]));
    lines.push_back("Modifier: " + jsonContext["modifier"]);
}

void renderSpecialTest(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    lines.push_back("Player: " + std::to_string(int(jsonContext["player"])));
    lines.push_back("Initated by: " + contextLayout.cardName(jsonContext["host"]));
    lines.push_back("Affects:     " + contextLayout.cardName(jsonContext["target"]));
    lines.push_back("Modifier: " + jsonContext["modifier"]);
}

void renderMovement(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    lines.push_back("Company: " + std::to_string(int(jsonContext["company"])));
    lines.push_back("Origin:      " + contextLayout.cardName(jsonContext["origin"]));
    lines.push_back("Destination: " + contextLayout.cardName(jsonContext["destination"]));
    lines.push_back("");

    const auto & jsonSitePath = jsonContext["sitePath"];
    if (!jsonSitePath.empty()) {
        lines.push_back("Site path:");
        for (int pathSymbol : jsonSitePath)
            lines.push_back(std::to_string(pathSymbol) + " " + pathSymbolNames[pathSymbol]);
        lines.push_back("");
    }

    const auto & jsonRegions = jsonContext["regions"];
    if (!jsonRegions.empty()) {
        lines.push_back("Regions:");
        for (int region : jsonRegions)
            lines.push_back(leftPadding(std::to_string(region), 2) + " " + regionNames[region]);
        lines.push_back("");
    }

    lines.push_back("Hazard limit:   " + std::to_string(int(jsonContext["hazardLimit"])));
    lines.push_back("Hazards played: " + std::to_string(int(jsonContext["hazardsPlayed"])));
}

void renderDrawCards(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    lines.push_back("Company: " + std::to_string(int(jsonContext["company"])));
    lines.push_back("Cards drawn by player:   " +
        std::to_string(int(jsonContext["playerCardsDrawn"])) + "/" +
        std::to_string(int(jsonContext["playerLimit"])));
    lines.push_back("Cards drawn by opponent: " +
        std::to_string(int(jsonContext["opponentCardsDrawn"])) + "/" +
        std::to_string(int(jsonContext["opponentLimit"])));
}

void renderArrival(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    const auto & jsonCompanies = jsonContext["conflictingCompanies"];
    std::string companies;
    for (int company : jsonCompanies)
        companies += " " + std::to_string(company);
    lines.push_back("Select company to return to origin:" + companies);
}

void renderExplore(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    lines.push_back(jsonContext["state"]);
    lines.push_back("Company: " + std::to_string(int(jsonContext["company"])));
    lines.push_back(contextLayout.cardName(jsonContext["site"]));

    std::string playable;
    const auto & jsonPlayable = jsonContext["playable"];
    for (int type : jsonPlayable)
        playable += " " + itemTypeNames[type] + ",";
    if (!playable.empty())
        playable.pop_back();
    lines.push_back("Playable:" + playable);
    if (jsonContext.find("containsHoard") != jsonContext.end())
        lines.push_back("Site contains a hoard");
}

void renderSelectCompany(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    const auto & jsonCompanies = jsonContext["companies"];
    std::string companies;
    for (int company : jsonCompanies)
        companies += " " + std::to_string(company);
    lines.push_back("Choices:" + companies);
}

void renderTransferCards(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    lines.push_back("");
    const auto & jsonTransactions = jsonContext["transactions"];
    for (const auto & jsonTransaction : jsonTransactions) {
        lines.push_back("Player: " + std::to_string(int(jsonTransaction["player"])));
        const auto & jsonOptions = jsonTransaction["options"];
        for (const auto & jsonOption : jsonOptions) {
            std::string range = std::to_string(int(jsonOption["minimum"])) + "-" + std::to_string(int(jsonOption["maximum"]));
            std::string cardTypes = " ";
            const auto & jsonCardTypes = jsonOption["cardTypes"];
            for (int cardType : jsonCardTypes)
                cardTypes += cardCategoryNames[cardType] + "s/";
            cardTypes.pop_back();
            lines.push_back(range + cardTypes);
            lines.push_back("From " + deckNames[jsonOption["sourceDeck"]] +
                " to " + deckNames[jsonOption["destinationDeck"]]);
            lines.push_back("  OR");
        }
        lines.back() = "";
    }
    lines.pop_back();
}

void renderTransferItems(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    lines.push_back("Player: " + std::to_string(int(jsonContext["player"])));
    lines.push_back("Company: " + std::to_string(int(jsonContext["company"])));
    lines.push_back("Allowed transfers: " + std::to_string(int(jsonContext["allowedTransfers"])));
    
    lines.push_back("");
    lines.push_back("Available items:");
    const auto & jsonItems = jsonContext["items"];
    const auto & jsonTaken = jsonContext["taken"];
    std::unordered_set<CardID> availableItems;
    for (CardID item : jsonItems)
        availableItems.insert(item);
    for (CardID item : jsonTaken)
        availableItems.erase(item);
    for (CardID item : availableItems)
        lines.push_back(contextLayout.cardName(item));

    lines.push_back("");
    lines.push_back("Taken items:");
    for (const auto & jsonEntry : jsonTaken) {
        lines.push_back(contextLayout.cardName(jsonTaken["character"]));
        lines.push_back("    " + contextLayout.cardName(jsonTaken["item"]));
    }
}

void renderReturnToHandSize(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());

    lines.push_back("");
    lines.push_back("Player hand size: " + std::to_string(int(jsonContext["resourcePlayerHandSize"])));
    lines.push_back(mayDiscardMessage(jsonContext["resourcePlayerMayDiscard"] == "true"));

    lines.push_back("");
    lines.push_back("Opponent hand size: " + std::to_string(int(jsonContext["hazardPlayerHandSize"])));
    lines.push_back(mayDiscardMessage(jsonContext["hazardPlayerMayDiscard"] == "true"));
}

void renderExhaustDeck(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    lines.push_back("Player: " + std::to_string(int(jsonContext["player"])));
    auto it = jsonContext.find("fromSideboard");
    if (it == jsonContext.end())
        return;

    lines.push_back("");
    lines.push_back("From sideboard:");
    const auto & jsonSideboard = *it;
    for (CardID card : jsonSideboard)
        lines.push_back(contextLayout.cardName(card));

    lines.push_back("");
    lines.push_back("From discard pile:");
    const auto & jsonDiscardPile = jsonContext["fromDiscardPile"];
    for (CardID card : jsonDiscardPile)
        lines.push_back(contextLayout.cardName(card));
}

void renderOrganizeCompanies(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
}

void renderDiscardCards(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    lines.push_back("Player: " + std::to_string(int(jsonContext["player"])));

    lines.push_back("");
    lines.push_back("Discard " + std::to_string(int(jsonContext["numberToDiscard"])) + " of these cards:");
    const auto & jsonCards = jsonContext["cards"];
    for (CardID card : jsonCards)
        lines.push_back(contextLayout.cardName(card));

    const auto & jsonSelection = jsonContext["selection"];
    if (jsonSelection.empty())
        return
    lines.push_back("");
    lines.push_back("Selected cards");
    for (CardID card : jsonSelection)
        lines.push_back(contextLayout.cardName(card));
}

void renderTapCharacters(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    lines.push_back("Player: " + std::to_string(int(jsonContext["player"])));
    lines.push_back("Tap " + std::to_string(int(jsonContext["numberToTap"])) + ":");
    const auto & jsonCharacters = jsonContext["characters"];
    for (CardID character : jsonCharacters)
        lines.push_back(contextLayout.cardName(character));
}

void renderChainOfEffects(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
}

void renderBottomContext(StringList & lines, const ContextLayout & contextLayout) {
    const auto & jsonContext = contextLayout.context;
    lines.push_back(contextLayout.contextHeader());
    lines.push_back("Player " + std::to_string(int(jsonContext["player"])) + "'s turn");
    lines.push_back(phaseNames[jsonContext["phase"]]);
}

std::unordered_map<std::string, RenderFunction> contextRenderer = {
    { "Dice roll", renderRoll },
    { "Body check", renderBodyCheck },
    { "Strike", renderStrike },
    { "Assign strikes", renderAssignStrikes },
    { "Attack", renderAttack },
    { "Encounter", renderEncounter },
    { "Corruption check", renderCorruptionCheck },
    { "Corruption checks", renderCorruptionChecks },
    { "Store card", renderStoreCard },
    { "Remove corruption card", renderRemoveCorruptionCard },
    { "Influence attempt", renderInflucenceAttempt },
    { "Test gold ring", renderRingTest },
    { "Special test", renderSpecialTest },
    { "Movement", renderMovement },
    { "Draw cards", renderDrawCards },
    { "Company conflict", renderArrival },
    { "Site phase", renderExplore },
    { "Select company", renderSelectCompany },
    { "Transfer cards", renderTransferCards },
    { "Transfer items", renderTransferItems },
    { "Return to hand size", renderReturnToHandSize },
    { "Play deck exhausted", renderExhaustDeck },
    { "Organization phase", renderOrganizeCompanies },
    { "Discard cards", renderDiscardCards },
    { "Tap characters", renderTapCharacters },
    { "Chain of effects", renderChainOfEffects },
    { "Turn sequence", renderBottomContext },
};

} // namespace


ContextLayout::ContextLayout(const nlohmann::json & context)
    : context(context["context"])
    , contextIndex(context["index"])
    , cardData(context["cardData"])
{}

std::string ContextLayout::contextHeader() const {
    return std::string(context["context"]) + " (index " + std::to_string(contextIndex) + ")";
}

std::string ContextLayout::cardName(CardID card) const {
    std::string idString = std::to_string(card);
    size_t padding = idString.size() < 3 ? 3 : idString.size();
    return (card >= 0) ? leftPadding(idString, padding) + " " + cardData.cardName(card) : "???";
}

StringList ContextLayout::generateOutput(size_t width) const {
    StringList lines;
    const std::string & contextName = context["context"];
    contextRenderer[contextName](lines, *this);
    lines.push_back("");
    return lines;
}
