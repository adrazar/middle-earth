#pragma once
#include "CardData.h"

struct CardLayout {
    std::string generateOutput(size_t idWidth = 0);

    size_t indentation;
    CardID id;
    std::string name;
};

struct CardColumn {
    void insertString(const std::string & string);
    void insertCard(CardID id, const CardData & cardData, size_t indentation);
    void insertDeck(const nlohmann::json & jsonDeck, const CardData & cardData);
    void insertPossessions(size_t indentation, PossessionNode possessions, const CardData & cardData);
    void generateOutput(StringList & lines);
    size_t getWidth();

    std::vector<CardLayout> cards;
    size_t width = 0;
    size_t idWidth = 0;
};

struct CardColumns {
    CardColumn & addColumn();
    void addDeck(const nlohmann::json & jsonDeck, const CardData & cardData);
    void generateOutput(StringList & lines, size_t width);

    std::vector<CardColumn> columns;
};

struct CompanyLayout {
    CompanyLayout(const nlohmann::json & jsonCompany, const CardData & cardData);
    void generateOutput(StringList & lines, size_t width);

    std::string companyTitle;
    std::string location;
    std::vector<CardID> onGuard;
    CardColumn permanentEvents;
    CardColumns characters;
};

struct SideLayout {
    SideLayout(const nlohmann::json & jsonSide, const CardData & cardData);
    void generateOutput(StringList & lines, size_t width);

    std::string player;
    std::vector<CompanyLayout> companies;
    CardColumns decks;
};

class CardSyncLayout {
public:
    CardSyncLayout(const nlohmann::json & cardSyncResponse);
    StringList generateOutput(size_t width);

private:
    std::vector<SideLayout> sides;
};
