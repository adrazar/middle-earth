#include "CardData.h"

CardData::CardData(const nlohmann::json & jsonCardData) {
    for (const auto & jsonCard : jsonCardData)
        cards.emplace(jsonCard["id"], jsonCard);

    for (const auto & cardData : cards) {
        CardID card = cardData.first;
        auto & node = possessions.try_emplace(card, std::make_shared<Possessions>(card)).first->second;
        const auto & jsonCard = cardData.second;
        auto it = jsonCard.find("parent");
        if (it != jsonCard.end()) {
            auto & parent = possessions.try_emplace(*it, std::make_shared<Possessions>(*it)).first->second;
            parent->possessions.push_back(node);
        }
    }
}

std::string CardData::cardName(CardID id) const {
    auto & card = cards.at(id);
    const std::string & name = card["name"];
    const std::string & status = card["state"];
    std::string statusIndicator;
    if (status == "tapped")
        statusIndicator = "(T)";
    else if (status == "wounded")
        statusIndicator = "(W)";
    return name.empty() ? statusIndicator : name + ' ' + statusIndicator;
}
