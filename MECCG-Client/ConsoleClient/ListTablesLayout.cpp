#include "ListTablesLayout.h"
#include "ResponseOutput.h"

std::vector<std::string> listTablesOutput(const nlohmann::json & listTablesResponse, size_t screenWidth) {
    Column tableIds("Table ID");
    Column seats("Players");
    std::array<Column, 4> players = {
        Column("Player 0"),
        Column("Player 1"),
        Column("Player 2"),
        Column("Player 3"),
    };
    Column status("Status");

    auto & jsonTables = listTablesResponse["tables"];
    for (auto & jsonTable : jsonTables) {
        tableIds.insert(std::to_string(jsonTable["tableId"].get<int>()));
        size_t nPlayers = jsonTable["seats"];
        seats.insert(std::to_string(nPlayers));
        auto & jsonPlayers = jsonTable["players"];
        for (size_t i = 0; i < 4; i++) {
            if (i < nPlayers)
                players[i].insert(jsonPlayers[i]);
            else
                players[i].insert("");
        }
        status.insert(jsonTable["gameProgress"]);
    }

    size_t maxPlayerWidth = 0;
    for (Column & playerColumn : players)
        if (playerColumn.width > maxPlayerWidth)
            maxPlayerWidth = playerColumn.width;
    if (maxPlayerWidth > 10)
        maxPlayerWidth = 10;
    for (Column & playerColumn : players)
        playerColumn.width = maxPlayerWidth;

    std::vector<std::string> lines;
    size_t rows = tableIds.entries.size();
    for (size_t i = 0; i < rows; i++) {
        lines.push_back("");
        std::string & line = lines.back();
        tableIds.appendEntry(line, i);
        seats.appendEntry(line, i);
        for (auto & player : players)
            player.appendEntry(line, i);
        status.appendEntry(line, i);
    }
    return lines;
}

void Column::insert(const std::string & string) {
    if (width < string.size())
        width = string.size();
    entries.push_back(string);
}

void Column::appendEntry(std::string & string, size_t index) {
    std::string & entry = entries[index];
    if (entry.size() <= width) {
        string.append(entry);
        string.append(std::string(width - entry.size(), ' '));
    } else {
        string.append(entry.substr(0, width));
    }
    string.append(std::string(padding, ' '));
}
