#include "ResponseOutput.h"
#include "ConsoleUiApi.h"
#include "Console.h"

#include <fstream>
#include <sstream>
#include <string>


ConsoleUiApi::ConsoleUiApi(ConsoleOutputWindow & consoleOutput)
    : consoleOutput_(consoleOutput)
{ ; }

void ConsoleUiApi::connectSucceeded() {
    consoleOutput_.pushString("Connect to server succeeded");
}

void ConsoleUiApi::connecting() {
    consoleOutput_.pushString("Connecting...");
}

void ConsoleUiApi::connectFailed(const std::string & error) {
    consoleOutput_.pushString(error);
}

void ConsoleUiApi::connectionLost() {
    consoleOutput_.pushString("Server connection lost");
    table_.tableId = -1;
}

void ConsoleUiApi::shuttingDownSocket() {
    consoleOutput_.pushString("Disconnected from server");
    table_.tableId = -1;
}

void ConsoleUiApi::serverError(const std::string & error) {
    consoleOutput_.pushString("Error: " + error);
}

void ConsoleUiApi::unexpectedError(const std::string & error) {
    consoleOutput_.pushString("Unexpected error: " + error);
}

void ConsoleUiApi::listTables(const nlohmann::json & response) {
    auto lines = listTablesOutput(response, consoleOutput_.width());
    consoleOutput_.pushLines(lines);
}

void ConsoleUiApi::createTable(int tableId, int nPlayers) {
    consoleOutput_.pushString("Created new table " + std::to_string(tableId));
    table_.tableId = tableId;
    table_.nPlayers = nPlayers;
}

void ConsoleUiApi::sit(int tableId, int player) {
    consoleOutput_.pushString("You are player " + std::to_string(player) + " on table " + std::to_string(tableId));
    table_.tableId = tableId;
    table_.player = player;
}

void ConsoleUiApi::leave(int tableId) {
    consoleOutput_.pushString("You have left table " + std::to_string(tableId));
    table_.tableId = -1;
    table_.player = -1;
}

void ConsoleUiApi::syncCards(const nlohmann::json & response) {
    auto lines = cardSyncOutput(response, consoleOutput_.width());
    consoleOutput_.pushLines(lines);
}

void ConsoleUiApi::getContext(const nlohmann::json & response) {
    auto lines = contextOutput(response, consoleOutput_.width());
    consoleOutput_.pushLines(lines);
}

void ConsoleUiApi::saveGame(const nlohmann::json & response, const std::string & fileName) {
    std::ofstream s;
    s.open("Save/" + fileName);
    s << response;
    s.close();
    consoleOutput_.pushString("Game saved");
}

void ConsoleUiApi::loadGame() {
    consoleOutput_.pushString("Game loaded");
}

void ConsoleUiApi::pushString(const std::string & string) {
    consoleOutput_.pushString(string);
}

Table & ConsoleUiApi::getTable() {
    return table_;
}
