#include "CardSyncLayout.h"
#include "ResponseOutput.h"

namespace {

size_t indentationSize = 2;

} // namespace

std::vector<std::string> cardSyncOutput(const nlohmann::json & cardSyncResponse, size_t screenWidth) {
    CardSyncLayout cardSyncLayout(cardSyncResponse);
    return cardSyncLayout.generateOutput(screenWidth);
}

std::string CardLayout::generateOutput(size_t idWidth) {
    if (id < 0)
        return name;
    std::string idString = std::to_string(id);
    size_t padding = idWidth > idString.size() ? idWidth - idString.size() : 0;
    return std::string(padding, ' ') + idString + std::string(1 + indentation, ' ') + name;
}

void CardColumn::insertString(const std::string & string) {
    if (width < string.size())
        width = string.size();
    CardLayout text{ 0, -1, string };
    cards.emplace_back(std::move(text));
}

void CardColumn::insertCard(CardID id, const CardData & cardData, size_t indentation = 0) {
    std::string name = cardData.cardName(id);
    size_t idSize = std::to_string(id).size();
    if (idWidth < idSize)
        idWidth = idSize;
    size_t size = indentation + name.size();
    if (width < size)
        width = size;
    CardLayout card{ indentation, id, name };
    cards.emplace_back(std::move(card));
}

void CardColumn::insertDeck(const nlohmann::json & jsonDeck, const CardData & cardData) {
    std::map<CardID, PossessionNode> possessions;
    std::vector<CardID> nonPossessions;
    for (CardID card : jsonDeck) {
        auto node = possessions.try_emplace(card, std::make_shared<Possessions>(card)).first->second;
        const auto & jsonCard = cardData.cards.at(card);
        auto it = jsonCard.find("parent");
        if (it == jsonCard.end())
            insertPossessions(0, possessions[card], cardData);
    }
}

void CardColumn::insertPossessions(size_t indentation, PossessionNode possessions, const CardData & cardData) {
    insertCard(possessions->id, cardData, indentation);
    for (PossessionNode possessionNode : possessions->possessions)
        insertPossessions(indentation + indentationSize, possessionNode, cardData);
}

void CardColumn::generateOutput(StringList & lines) {
    for (auto & card : cards)
        lines.push_back(card.generateOutput());
    lines.push_back("");
}

size_t CardColumn::getWidth() {
    return idWidth + 1 + width;
}

CardColumn & CardColumns::addColumn() {
    columns.emplace_back();
    return columns.back();
}

void CardColumns::addDeck(const nlohmann::json & jsonDeck, const CardData & cardData) {
    CardColumn & column = addColumn();
    std::string name = jsonDeck["deck"];
    name[0] = toupper(name[0]);
    column.insertString(name + ":");
    column.insertDeck(jsonDeck["cards"], cardData);
}

void CardColumns::generateOutput(StringList & lines, size_t screenWidth) {
    size_t columnWidth = 0;
    for (CardColumn & column : columns) {
        size_t width = column.getWidth();
        if (columnWidth < width)
            columnWidth = width;
    }
    const size_t columnsPerRow = (screenWidth + 2) / (columnWidth + 2);
    size_t columnsLeft = columns.size();
    std::vector<CardColumn>::iterator startIt, endIt;
    startIt = columns.begin();
    while (columnsLeft) {
        if (columnsLeft < columnsPerRow) {
            endIt = startIt + columnsLeft;
            columnsLeft = 0;
        }
        else {
            endIt = startIt + columnsPerRow;
            columnsLeft -= columnsPerRow;
        }

        size_t rowHeight = 0;
        for (auto it = startIt; it != endIt; ++it) {
            size_t height = it->cards.size();
            if (rowHeight < height)
                rowHeight = height;
        }
        for (int n = rowHeight - 1; n >= 0; n--) {
            lines.push_back("");
            std::string & line = lines.back();
            for (auto it = startIt; it != endIt; ++it) {
                if (n < int(it->cards.size())) {
                    auto card = it->cards[n].generateOutput(it->idWidth);
                    line += card + std::string(columnWidth - card.size(), ' ');
                } else {
                    line += std::string(columnWidth, ' ');
                }
                if (it <= endIt) {
                    line += "  ";
                }
            }
        }
        lines.push_back("");
        startIt = endIt;
    }
}

CompanyLayout::CompanyLayout(const nlohmann::json & jsonCompany, const CardData & cardData) {
    CompanyID companyId = jsonCompany["id"];
    companyTitle = "Company " + std::to_string(companyId) + ':';
    CardID source = jsonCompany["currentLocation"];
    CardID destination = jsonCompany["newLocation"];
    CardLayout sourceCard{ 0, source, cardData.cardName(source) };
    if (source == destination)
        location = sourceCard.generateOutput();
    else {
        CardLayout destinationCard{ 0, destination, cardData.cardName(destination) };
        location = sourceCard.generateOutput() + "  ->  " + destinationCard.generateOutput();
    }

    const auto & jsonOnGuard = jsonCompany["onGuard"];
    for (CardID id : jsonOnGuard)
        onGuard.push_back(id);

    permanentEvents.insertString("Permanent events:");
    permanentEvents.insertDeck(jsonCompany["permanentEvents"], cardData);

    const auto & jsonCharacters = jsonCompany["characters"];
    for (auto & jsonCharacter : jsonCharacters) {
        CardID id = jsonCharacter["id"];
        if (!cardData.cards.at(id).contains("parent")) {
            CardColumn & characterColumn = characters.addColumn();
            characterColumn.insertPossessions(0, cardData.possessions.at(id), cardData);
            auto & jsonFollowers = jsonCharacter["followers"];
            for (CardID follower : jsonFollowers)
                characterColumn.insertPossessions(0, cardData.possessions.at(follower), cardData);
        }
    }
}

void CompanyLayout::generateOutput(StringList & lines, size_t width) {
    std::string onGuardMessage;
    if (!onGuard.empty()) {
        onGuardMessage = "  (On guard:";
        for (CardID card : onGuard)
            onGuardMessage += ' ' + std::to_string(card);
        onGuardMessage += ")";
    }
    lines.push_back(companyTitle + onGuardMessage);
    lines.push_back(location);
    lines.push_back("");

    if (permanentEvents.cards.size() > 1)
        permanentEvents.generateOutput(lines);
    characters.generateOutput(lines, width);
}

SideLayout::SideLayout(const nlohmann::json & jsonSide, const CardData & cardData) {
    player = "Player " + std::to_string(jsonSide["player"].get<int>());
    {
        auto it = jsonSide.find("companies");
        if (it != jsonSide.end()) {
            const auto & jsonCompanies = *it;
            for (const auto & jsonCompany : jsonCompanies)
                companies.emplace_back(jsonCompany, cardData);
        }
    }
    {
        auto it = jsonSide.find("decks");
        if (it != jsonSide.end()) {
            const auto & jsonDecks = *it;
            for (const auto & jsonDeck : jsonDecks) {
                decks.addDeck(jsonDeck, cardData);
            }
        }
    }
}

void SideLayout::generateOutput(StringList & lines, size_t width) {
    lines.push_back("");
    lines.push_back("");
    lines.push_back(player);
    lines.push_back("");
    for (CompanyLayout & company : companies)
        company.generateOutput(lines, width);
    decks.generateOutput(lines, width);
}

CardSyncLayout::CardSyncLayout(const nlohmann::json & cardSyncResponse) {
    CardData cardData(cardSyncResponse["cardData"]);
    const auto & jsonSides = cardSyncResponse["sides"];
    for (const auto & jsonSide : jsonSides)
        sides.emplace_back(jsonSide, cardData);
}

StringList CardSyncLayout::generateOutput(size_t width) {
    StringList lines;
    for (auto & side : sides)
        side.generateOutput(lines, width);
    return lines;
}
