#include "CommandApi.h"
#include "JsonConverters.h"
#include "ReceiveHandler.h"
#include "RequestTypes.h"

#include <memory>

#include <nlohmann/json.hpp>

namespace {

int nextRequestId = 0;

void send(ServerApi & serverApi, std::string && msg) {
    serverApi.send(
        std::move(msg),
        [](const std::error_code & ec) {
        (void)ec;
    }
    );
}

void sendAuthentication(BoostServerApi & serverApi, const ConnectionRequest & connectionRequest) {
    nlohmann::json authentication = {
        {"type", "authenticate"},
        {"userName", connectionRequest.userName},
    };
    std::string msg = authentication.dump();

    send(serverApi, std::move(msg));
}

} // namespace

CommandApi::CommandApi(UiApi & uiApi, ServerApi & serverApi)
    : uiApi_(uiApi)
    , serverApi_(serverApi)
{ ; }

void CommandApi::registerCustomHandler(int requestId, CustomHandler && customHandler) {
    customHandlers_.try_emplace(requestId, customHandler);
}

void CommandApi::connect(const ConnectionRequest & connectionRequest) {
    uiApi_.connecting();
    serverApi_.connect(connectionRequest.serverHost, connectionRequest.port,
        [this, connectionRequest](const std::error_code & ec) {
            if (!ec) {
                uiApi_.connectSucceeded();
                serverApi_.startReceiveLoop(ReceiveHandler(uiApi_, serverApi_, customHandlers_));
                sendAuthentication(serverApi_, connectionRequest);
            } else {
                uiApi_.connectFailed(ec.message());
            }
        }
    );
}

void CommandApi::disconnect() {
    serverApi_.disconnect();
}

void CommandApi::listTables() {
    nlohmann::json jsonRequest = {
        {"type", "listTables"},
    };

    std::string msg = jsonRequest.dump();
    send(serverApi_, std::move(msg));
}

void CommandApi::createTable(int players) {
    int requestId = nextRequestId++;

    nlohmann::json jsonRequest = {
        {"requestId", requestId},
        {"type", "createTable"},
    };
    if (1 < players && players < 5)
        jsonRequest["players"] = players;

    registerCustomHandler(requestId, [this](UiApi & uiApi, nlohmann::json & jsonResponse) {
        if (jsonResponse["type"] == "error")
            return uiApi.serverError(jsonResponse["message"]);
        int tableId = jsonResponse["tableId"];
        int nPlayers = jsonResponse["nPlayers"];
        uiApi.createTable(tableId, nPlayers);
        sit(tableId, -1);
    });

    std::string msg = jsonRequest.dump();
    send(serverApi_, std::move(msg));
}

void CommandApi::sit(int tableId, int seat) {
    nlohmann::json jsonRequest = {
        {"type", "sit"},
        {"tableId", tableId },
    };

    if (seat >= 0)
        jsonRequest["seat"] = seat;

    std::string msg = jsonRequest.dump();
    send(serverApi_, std::move(msg));
}

void CommandApi::leave(int tableId) {
    nlohmann::json jsonRequest = {
        {"type", "leaveTable"},
        {"tableId", tableId },
    };

    std::string msg = jsonRequest.dump();
    send(serverApi_, std::move(msg));
}

void CommandApi::syncCards(Table & table, const CardSyncRequest & request) {
    nlohmann::json jsonRequest = {
        {"requestId", nextRequestId},
        {"stateId", table.stateId},
        {"tableId", table.tableId},
        {"type", "syncCards"},
    };

    toJson(request, jsonRequest);

    std::string msg = jsonRequest.dump();
    send(serverApi_, std::move(msg));
}

void CommandApi::context(Table & table, int index) {
    nlohmann::json jsonRequest = {
        {"requestId", nextRequestId},
        {"stateId", table.stateId},
        {"tableId", table.tableId},
        {"type", "context"},
        {"index", index},
    };

    std::string msg = jsonRequest.dump();
    send(serverApi_, std::move(msg));
}

void CommandApi::save(Table & table, const std::string & fileName, bool compress) {
    nlohmann::json jsonRequest = {
        {"requestId", nextRequestId},
        {"stateId", table.stateId},
        {"tableId", table.tableId},
        {"type", "save"},
        {"fileName", fileName},
        {"compress", compress},
    };

    std::string msg = jsonRequest.dump();
    send(serverApi_, std::move(msg));
}

void CommandApi::load(Table & table, const nlohmann::json & jsonGame) {
    nlohmann::json jsonRequest = {
        {"requestId", nextRequestId},
        {"stateId", table.stateId},
        {"tableId", table.tableId},
        {"type", "load"},
        {"game", jsonGame},
    };

    std::string msg = jsonRequest.dump();
    send(serverApi_, std::move(msg));
}
