#pragma once
#include "nlohmann/json.hpp"

#include <string>
#include <vector>

struct Column {
    Column(const std::string & title) {
        insert(title);
    }
    void insert(const std::string & string);
    void appendEntry(std::string & string, size_t index);

    std::vector<std::string> entries;
    size_t width = 0;
    size_t padding = 2;
};
