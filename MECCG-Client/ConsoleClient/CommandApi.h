#pragma once
#include "../../ServerApi/BoostServerApi.h"
//class ServerApi : public BoostServerApi {};
using ServerApi = BoostServerApi;
//
#include "ConsoleUiApi.h"
//class UiApi : public ConsoleUiApi {};
using UiApi = ConsoleUiApi;

#include "../../ServerApi/RequestTypes.h"

#include <nlohmann/json.hpp>

#include <string>
#include <variant>
#include <cinttypes>

using CustomHandler = std::function<void(UiApi & uiApi, nlohmann::json & jsonResponse)>;

class CommandApi {
public:
    CommandApi(UiApi & uiApi, ServerApi & serverApi);
    void registerCustomHandler(int requestId, CustomHandler && customHandler);
    void connect(const ConnectionRequest & connectionRequest);
    void disconnect();
    void listTables();
    void createTable(int players);
    void sit(int tableId, int seat);
    void leave(int tableId);
    void syncCards(Table & table, const CardSyncRequest & request);
    void context(Table & table, int index = -1); // negative = count from back of array
    void save(Table & table, const std::string & fileName, bool compress);
    void load(Table & table, const nlohmann::json & jsonGame);

private:
    UiApi & uiApi_;
    ServerApi & serverApi_;
    std::unordered_map<int, CustomHandler> customHandlers_;
};
