#pragma once
#include "nlohmann/json.hpp"

std::vector<std::string> listTablesOutput(const nlohmann::json & listTablesResponse, size_t screenWidth);
std::vector<std::string> cardSyncOutput(const nlohmann::json & cardSyncResponse, size_t screenWidth);
std::vector<std::string> contextOutput(const nlohmann::json & contextResponse, size_t screenWidth);
