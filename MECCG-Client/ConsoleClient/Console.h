#pragma once

#include <curses.h>
#include <array>
#include <functional>
#include <string>

class InputString {
public:
    void insertChar(char c);
    void backspace();
    void left();
    void right();
    void ctrlBackspace();
    void ctrlLeft();
    void ctrlRight();
    void clear();
    bool isEditingStringEnd();
    const std::string & getInput() const;
    uint32_t getInputPos() const;
    void setInput(const std::string & string);

private:
    std::string currentInput_;
    uint32_t inputPos_ = 0;
};

class History {
public:
    void commit(const std::string & string);
    std::string getPrevious(const std::string & current);
    std::string getNext(const std::string & current);

private:
    static const std::size_t length_ = (1 << 6) - 1;
    std::array<std::string, length_ + 1> history_;
    std::size_t front_ = 0;
    std::size_t back_ = length_;
    std::size_t current_ = 0;
};

struct Rect {
    int x = 0;
    int y = 0;
    size_t width = 0;
    size_t height = 0;
};

class ConsoleInputField {
public:
    ConsoleInputField(size_t h, size_t w, int y, int x);
    ~ConsoleInputField();

    void processInput(int keyCode);
    void submit();
    void setSubmitHandler(std::function<void(const std::string &)> && handler);

    WINDOW * getWindow();
    const std::string & getInput() const;

private:
    void insertChar(chtype c);
    void insertString(const std::string & string);
    void removeChar();
    void removeCharSequence();
    void removeLeftoverCharacters(uint32_t originalLength);
    void redrawTail(uint32_t tailBegin);
    void updateCursor();

    WINDOW * win_;
    const Rect boundingRect_;
    InputString inputString_;
    History history_;
    std::function<void(const std::string &)> handler_;
};


class ConsoleOutputWindow {
public:
    ConsoleOutputWindow(size_t h, size_t w, int y, int x);
    ~ConsoleOutputWindow();

    size_t width();
    void pushString(const std::string & string);
    void pushLines(const std::vector<std::string> & lines);
    void pushNumber(int number);

private:
    bool more();

    WINDOW * win_;
    const Rect boundingRect_;
};

