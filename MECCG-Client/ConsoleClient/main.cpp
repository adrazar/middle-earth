#include "../../ServerApi/BoostServerApi.h"

#include "Console.h"
#include "CommandApi.h"
#include "ConsoleUiApi.h"
#include "InputParser.h"

#include <chrono>
#include <thread>
#include <vector>

class SubmitHandler {
public:
    SubmitHandler(ConsoleOutputWindow & outputWindow)
        : outputWindow_(outputWindow)
    {}
    void operator()(const std::string & string) {
        outputWindow_.pushString(string);
    }

private:
    ConsoleOutputWindow & outputWindow_;
};

int main() {
    using namespace std::chrono_literals;
    initscr();
    noecho();
    keypad(stdscr, true);
    scrollok(stdscr, true);
    start_color();
    init_pair(1, COLOR_RED, COLOR_BLACK);

    boost::asio::io_context ioContext;

    {
        int verticalSplit = LINES - 2;
        ConsoleOutputWindow outputWindow(verticalSplit, COLS, 0, 0);
        ConsoleInputField inputField(1, COLS, verticalSplit + 1, 0);
        //SubmitHandler submitHandler(outputWindow);

        ConsoleUiApi uiApi(outputWindow);
        BoostServerApi serverApi(ioContext);
        CommandApi commandApi(uiApi, serverApi);
        InputParser inputParser(commandApi);
 
        auto submitHandler = [&uiApi, &inputParser](const std::string & line) {
            uiApi.pushString(line);
            std::string error = inputParser.parseInputLine(line, uiApi.getTable());
            if (!error.empty())
                uiApi.pushString(error);
        };

        inputField.setSubmitHandler(submitHandler);

        mvhline(verticalSplit, 0, '-', COLS);
        refresh();

        int c = 0;
        do {
            std::this_thread::sleep_for(1ms);
            c = wgetch(inputField.getWindow());
            if (c != -1) {
                inputField.processInput(c);
            }
            ioContext.poll();
        } while (c != KEY_F(1));
    }
    endwin();
    return 0;
}