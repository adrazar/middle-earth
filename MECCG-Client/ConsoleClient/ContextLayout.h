#pragma once
#include "CardData.h"

struct ContextLayout {
public:
    ContextLayout(const nlohmann::json & contextResponse);
    std::string contextHeader() const;
    std::string cardName(CardID card) const;
    StringList generateOutput(size_t width) const;

    const nlohmann::json & context;
    const int contextIndex;

private:
    CardData cardData;
};
