#include "ReceiveHandler.h"
#include <nlohmann/json.hpp>
#include <stdexcept>
#include <string>

namespace {

using CommandFunction = void(*)(ConsoleUiApi & uiApi, const nlohmann::json & input);
using MessageTypeHandlerMap = std::unordered_map<std::string, CommandFunction>;

void listTables(ConsoleUiApi & uiApi, const nlohmann::json & response) {
    uiApi.listTables(response);
}

void createTable(ConsoleUiApi & uiApi, const nlohmann::json & response) {
    int tableId = response["tableId"];
    int nPlayers = response["nPlayers"];
    uiApi.createTable(tableId, nPlayers);
}

void sit(ConsoleUiApi & uiApi, const nlohmann::json & response) {
    int tableId = response["tableId"];
    int player = response["position"];
    uiApi.sit(tableId, player);
}

void leave(ConsoleUiApi & uiApi, const nlohmann::json & response) {
    int tableId = response["tableId"];
    uiApi.leave(tableId);
}

void syncCards(ConsoleUiApi & uiApi, const nlohmann::json & response) {
    uiApi.syncCards(response);
}

void getContext(ConsoleUiApi & uiApi, const nlohmann::json & response) {
    uiApi.getContext(response);
}

void saveGame(ConsoleUiApi & uiApi, const nlohmann::json & response) {
    const std::string & fileName = response["fileName"];
    uiApi.saveGame(response["game"], fileName);
}

void loadGame(ConsoleUiApi & uiApi, const nlohmann::json & response) {
    uiApi.loadGame();
}

void error(ConsoleUiApi & uiApi, const nlohmann::json & response) {
    uiApi.serverError(response["message"]);
}

MessageTypeHandlerMap handlerMap = {
    { "listTables", listTables },
    { "createTable", createTable },
    { "sit", sit },
    { "leaveTable", leave },
    { "syncCards", syncCards },
    { "context", getContext },
    { "saveGame", saveGame },
    { "loadGame", loadGame },
    { "error", error },
};

void parse(ConsoleUiApi & uiApi, const nlohmann::json & input) {
    if (!input.contains("type"))
        throw std::exception("\"type\" missing in server output");

    auto found = handlerMap.find(input["type"]);
    if (found == handlerMap.end()) {
        std::string errorMessage("\"" + std::string(input["type"]) + "\": undefined message type");
        throw std::exception(errorMessage.data());
    }
    found->second(uiApi, input);
}

}  // namespace

ReceiveHandler::ReceiveHandler(ConsoleUiApi & uiApi, BoostServerApi & serverApi, CustomHandlerMap & customHandlers)
    : uiApi_(uiApi)
    , serverApi_(serverApi)
    , customHandlers_(customHandlers)
{ ; }

void ReceiveHandler::operator()(const std::error_code & ec, const std::string & msg) {
    if (!ec) {
        try {
            auto jsonResponse = nlohmann::json::parse(msg);

            int requestId = -1;
            {
                auto it = jsonResponse.find("requestId");
                if (it != jsonResponse.end())
                    requestId = it->get<int>();
            }

            auto it = customHandlers_.find(requestId);
            if (it == customHandlers_.end())
                parse(uiApi_, jsonResponse);
            else {
                it->second(uiApi_, jsonResponse);
                customHandlers_.erase(it);
            }
        }
        catch (std::exception & ex) {
            uiApi_.unexpectedError("ReceiveHandler exception: " + std::string(ex.what()));
        }
    } else if (serverApi_.isConnectionLostError(ec)) {
        uiApi_.connectionLost();
        serverApi_.disconnect();
    } else if (serverApi_.isShuttingDownError(ec)) {
        uiApi_.shuttingDownSocket();
    } else {
        uiApi_.unexpectedError("ReceiveHandler error: " + ec.message());
    }
}
