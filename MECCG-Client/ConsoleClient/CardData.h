#pragma once
#include "nlohmann/json.hpp"

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

struct Possessions;

using CardID = short;
using CompanyID = short;
using StringList = std::vector<std::string>;
using PossessionNode = std::shared_ptr<Possessions>;

struct Possessions {
    Possessions(CardID id)
        : id(id)
    { ; }

    CardID id;
    std::vector<PossessionNode> possessions;
};

struct CardData {
    CardData(const nlohmann::json & jsonCardData);
    std::string cardName(CardID id) const;

    std::unordered_map<CardID, nlohmann::json> cards;
    std::map<CardID, PossessionNode> possessions;
};
