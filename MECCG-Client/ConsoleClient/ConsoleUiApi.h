#pragma once
#include "../../Middle-Earth/CardTopology.h"

#include "nlohmann/json.hpp"

#include <string>
#include <unordered_map>

class ConsoleOutputWindow;

struct Table {
    int tableId = -1;
    int nPlayers = 2;
    int player = -1;

    int stateId = 0;
};

class ConsoleUiApi {
public:
    ConsoleUiApi(ConsoleOutputWindow & consoleOutput);

    void connectSucceeded();
    void connecting();
    void connectFailed(const std::string & error);
    void connectionLost();
    void shuttingDownSocket();
    void serverError(const std::string & error);
    void unexpectedError(const std::string & error);

    void listTables(const nlohmann::json & response);
    void createTable(int tableId, int nPlayers);
    void sit(int tableId, int player);
    void leave(int tableId);

    void syncCards(const nlohmann::json & response);
    void getContext(const nlohmann::json & response);

    void saveGame(const nlohmann::json & response, const std::string & fileName);
    void loadGame();

    void pushString(const std::string & string);
    Table & getTable();

private:
    Table table_;
    ConsoleOutputWindow & consoleOutput_;
};
