#include "Console.h"


#if defined(_WIN32)
#undef KEY_BACKSPACE
#undef KEY_ENTER
#define KEY_BACKSPACE 0x8
#define KEY_ENTER     0xa
#define KEY_ESC       0x1b
#define CTL_BACKSPACE 0x7f
#endif

const chtype blankCh(' ');
const std::string morePrompt = "More...";

void InputString::insertChar(char c)
{
    currentInput_.insert(inputPos_++, 1, c);
}
void InputString::backspace()
{
    if (inputPos_ > 0)
        currentInput_.erase(--inputPos_, 1);
}
void InputString::left()
{
    if (inputPos_ > 0)
        inputPos_--;
}
void InputString::right()
{
    if (inputPos_ < currentInput_.size())
        inputPos_++;
}
void InputString::ctrlBackspace()
{
    bool eraseLeadingWhitespaces = inputPos_ > 0 && currentInput_[inputPos_] == ' ' && currentInput_[inputPos_ - 1] != ' ';
    while (inputPos_ > 0 && currentInput_[inputPos_ - 1] == ' ')
        currentInput_.erase(--inputPos_, 1);
    while (inputPos_ > 0 && currentInput_[inputPos_ - 1] != ' ')
        currentInput_.erase(--inputPos_, 1);
    if (eraseLeadingWhitespaces)
       while (inputPos_ > 0 && currentInput_[inputPos_ - 1] == ' ')
            currentInput_.erase(--inputPos_, 1);
}
void InputString::ctrlLeft()
{
    while (inputPos_ > 0 && currentInput_[inputPos_ - 1] == ' ')
        inputPos_--;
    while (inputPos_ > 0 && currentInput_[inputPos_ - 1] != ' ')
        inputPos_--;
}
void InputString::ctrlRight()
{
    const uint32_t upperLimit = currentInput_.size();
    while (inputPos_ < upperLimit && currentInput_[inputPos_] != ' ')
        inputPos_++;
    while (inputPos_ < upperLimit && currentInput_[inputPos_] == ' ')
        inputPos_++;
}
void InputString::clear()
{
    currentInput_.clear();
    inputPos_ = 0;
}
bool InputString::isEditingStringEnd()
{
    return inputPos_ == currentInput_.size();
}
const std::string & InputString::getInput() const
{
    return currentInput_;
}
uint32_t InputString::getInputPos() const
{
    return inputPos_;
}
void InputString::setInput(const std::string & string) {
    currentInput_ = string;
    inputPos_ = currentInput_.length();
}

void History::commit(const std::string & string) {
    current_ = front_;
    std::size_t previous = (current_ - 1) & length_;
    if (previous != back_ && history_[previous] == string)
        return;
    history_[front_] = string;
    front_ = (front_ + 1) & length_;
    if (front_ == back_)
        back_ = (back_ + 1) & length_;
    current_ = front_;
    history_[front_].clear();
}
std::string History::getPrevious(const std::string & current) {
    std::size_t previous = (current_ - 1) & length_;
    if (previous == back_)
        return current;
    history_[current_] = current;
    current_ = previous;
    return history_[current_];
}
std::string History::getNext(const std::string & current) {
    if (current_ == front_)
        return current;
    history_[current_] = current;
    current_ = (current_ + 1) & length_;
    return history_[current_];
}

ConsoleInputField::ConsoleInputField(size_t h, size_t w, int y, int x)
    : win_(newwin(h, w, y, x))
    , boundingRect_{ x, y, w, h }
    , handler_(nullptr)
{
    keypad(win_, true);
    wtimeout(win_, 0);
}
ConsoleInputField::~ConsoleInputField() {
    delwin(win_);
}
void ConsoleInputField::processInput(int keyCode) {
    if (keyCode < 256 && isprint(keyCode)) {
        insertChar(keyCode);
    } else if (keyCode == KEY_BACKSPACE) {
        removeChar();
    } else if (keyCode == KEY_LEFT) {
        inputString_.left();
        updateCursor();
    } else if (keyCode == KEY_RIGHT) {
        inputString_.right();
        updateCursor();
    } else if (keyCode == KEY_UP) {
        insertString(history_.getPrevious(inputString_.getInput()));
    } else if (keyCode == KEY_DOWN) {
        insertString(history_.getNext(inputString_.getInput()));
    } else if (keyCode == CTL_BACKSPACE) {
        removeCharSequence();
        updateCursor();
    } else if (keyCode == CTL_LEFT) {
        inputString_.ctrlLeft();
        updateCursor();
    } else if (keyCode == CTL_RIGHT) {
        inputString_.ctrlRight();
        updateCursor();
    } else if (keyCode == KEY_ENTER) {
        submit();
    }
}
void ConsoleInputField::submit()
{
    int inputSize(inputString_.getInput().size());
    if (inputSize == 0)
        return;

    wmove(win_, 0, 0);
    for (int i = 0; i < inputSize; ++i)
        waddch(win_, blankCh);
    wmove(win_, 0, 0);
    wrefresh(win_);

    history_.commit(inputString_.getInput());
    handler_(inputString_.getInput());
    inputString_.clear();
}
void ConsoleInputField::setSubmitHandler(std::function<void(const std::string &)> && handler)
{
    handler_ = handler;
}
WINDOW * ConsoleInputField::getWindow()
{
    return win_;
}
const std::string & ConsoleInputField::getInput() const
{
    return inputString_.getInput();
}
void ConsoleInputField::insertChar(chtype c)
{
    inputString_.insertChar(char(c));
    if (inputString_.isEditingStringEnd())
        waddch(win_, c);
    else {
        redrawTail(inputString_.getInputPos() - 1);
        updateCursor();
    }
    wrefresh(win_);
}
void ConsoleInputField::insertString(const std::string & string)
{
    uint32_t originalLength = inputString_.getInput().size();
    inputString_.setInput(string);
    redrawTail(0);
    removeLeftoverCharacters(originalLength);
}
void ConsoleInputField::removeChar()
{
    inputString_.backspace();
    if (inputString_.isEditingStringEnd())
        updateCursor();
    else
        redrawTail(inputString_.getInputPos());
    waddch(win_, blankCh);
    updateCursor();
    wrefresh(win_);
}
void ConsoleInputField::removeCharSequence()
{
    uint32_t originalLength = inputString_.getInput().size();
    inputString_.ctrlBackspace();
    removeLeftoverCharacters(originalLength);
}
void ConsoleInputField::removeLeftoverCharacters(uint32_t originalLength) {
    int charactersRemoved(originalLength - inputString_.getInput().size());
    if (charactersRemoved <= 0)
        return;
    redrawTail(inputString_.getInputPos());
    for (; charactersRemoved; --charactersRemoved)
        waddch(win_, blankCh);
    updateCursor();
    wrefresh(win_);
}

void ConsoleInputField::redrawTail(uint32_t tailBegin)
{
    const std::string & s = inputString_.getInput();
    wmove(win_, 0, tailBegin);
    waddnstr(win_, s.data() + tailBegin, s.size() - tailBegin);
}
void ConsoleInputField::updateCursor()
{
    wmove(win_, 0, boundingRect_.x + inputString_.getInputPos());
}

ConsoleOutputWindow::ConsoleOutputWindow(size_t h, size_t w, int y, int x)
    : win_(newwin(h, w, y, x))
    , boundingRect_{ x, y, w, h }
{
    scrollok(win_, true);
    wmove(win_, h - 1, 0);

    wattron(win_, COLOR_PAIR(1));
    wattron(win_, A_BOLD);
}
ConsoleOutputWindow::~ConsoleOutputWindow()
{
    delwin(win_);
}
size_t ConsoleOutputWindow::width() {
    return boundingRect_.width;
}
void ConsoleOutputWindow::pushString(const std::string & string)
{
    wprintw(win_, "\n%s", string.data());
    wrefresh(win_);
}
void ConsoleOutputWindow::pushLines(const std::vector<std::string> & lines)
{
    size_t linesPerPause = boundingRect_.height;
    size_t nLines = 0;
    bool proceed = true;
    for (auto it = lines.begin(); it != lines.end(); ++it) {
        if (++nLines == linesPerPause && it + 1 != lines.end()) {
            proceed = more();
            nLines = 1;
            wprintw(win_, "%s", it->data());
            if (!proceed)
                break;
        } else {
            wprintw(win_, "\n%s", it->data());
        }
    }
    wrefresh(win_);
}
void ConsoleOutputWindow::pushNumber(int number)
{
    wprintw(win_, "\n%d", number);
    wrefresh(win_);
}
bool ConsoleOutputWindow::more() {
    pushString(morePrompt);
    curs_set(0);
    wrefresh(win_);
    int result = getch();
    wmove(win_, boundingRect_.height - 1, 0);
    for (size_t i = 0; i < morePrompt.size(); ++i)
        waddch(win_, blankCh);
    wmove(win_, boundingRect_.height - 1, 0);
    curs_set(1);
    return result != KEY_ESC;
}
