#include "InputParser.h"

#include "RequestTypes.h"

#include <fstream>
#include <stdexcept>
#include <set>

namespace {

class UnknownCommandException : public std::runtime_error {
public:
    UnknownCommandException(const std::string & message) : std::runtime_error(message) { ; }
};

class RedoFromStartException : public std::exception {};

class MissingArgumentException : public std::exception {};

void readNumber(std::istream & stream, int & val) {
    if (stream.eof())
        throw MissingArgumentException();
    stream >> val;
    if (stream.fail())
        throw RedoFromStartException();
}

void readString(std::istream & stream, std::string & s) {
    if (stream.eof())
        throw MissingArgumentException();
    stream >> s;
}

CardSyncRequest createCardSyncRequest(const std::string & what, int player, CompanyID id, Table & table) {
    CardSyncRequest request;
    int endPlayer = player + 1;
    if (player < 0) {
        player = 0;
        endPlayer = table.nPlayers;
    }
    if (what == "all") {

    } else if (what == "companies") {

        for (; player < endPlayer; player++) {
            request.sides.push_back(player);
            auto & side = request.sides.back();
            if (id >= 0)
                side.companies = std::vector<CompanyID>{ id };
            else
                side.companies = "all";
        }
    } else if (what == "hand") {
        request.sides.push_back(player);
        auto & side = request.sides.back();
        side.decks = { Hand };
    }
    return request;
}

void command_connect(std::istream & stream, CommandApi & commandApi, Table & table) {
    std::string userName = "Tony";
    std::string serverHost = "127.0.0.1";
    int port = 8289;
    if (!stream.eof())
        readString(stream, userName);
    if (!stream.eof())
        readString(stream, serverHost);
    if (!stream.eof())
        readNumber(stream, port);
    if (port > int(std::numeric_limits<uint16_t>::max) || port < 0)
        throw std::invalid_argument("Port needs to be in range 0-65535");

    commandApi.connect({ userName, serverHost, uint16_t(port) });
}

void command_disconnect(std::istream & stream, CommandApi & commandApi, Table & table) {
    commandApi.disconnect();
}

void command_list_tables(std::istream & stream, CommandApi & commandApi, Table & table) {
    commandApi.listTables();
}

void command_create_table(std::istream & stream, CommandApi & commandApi, Table & table) {
    if (table.tableId >= 0)
        throw std::runtime_error("Leave current table before creating a new one");
    int players = -1;
    if (!stream.eof())
        readNumber(stream, players);
    commandApi.createTable(players);
}

void command_sit(std::istream & stream, CommandApi & commandApi, Table & table) {
    int tableId;
    int seat = -1;
    readNumber(stream, tableId);
    if (table.tableId >= 0 && tableId != table.tableId)
        throw std::runtime_error("Already present at another table");
    if (!stream.eof())
        readNumber(stream, seat);
    commandApi.sit(tableId, seat);
}

void command_leave(std::istream & stream, CommandApi & commandApi, Table & table) {
    if (table.tableId < 0)
        throw std::runtime_error("Not sitting at any table");
    commandApi.leave(table.tableId);
}

const std::set<std::string> possibleCardSets = { "all", "hand", "companies"};

void command_list(std::istream & stream, CommandApi & commandApi, Table & table) {
    std::string what = "companies";
    int player = -1;
    int companyID = -1;
    if (!stream.eof())
        readString(stream, what);
    if (!stream.eof())
        readNumber(stream, player);
    if (!stream.eof())
        readNumber(stream, companyID);
    if (possibleCardSets.find(what) == possibleCardSets.end())
        throw std::invalid_argument("Unknown card selector '" + what + "'");
    CardSyncRequest cardSyncRequest(createCardSyncRequest(what, player, CompanyID(companyID), table));
    commandApi.syncCards(table, cardSyncRequest);
}

void command_context(std::istream & stream, CommandApi & commandApi, Table & table) {
    int index = -1;
    if (!stream.eof())
        readNumber(stream, index);
    commandApi.context(table, index);
}

void command_save(std::istream & stream, CommandApi & commandApi, Table & table) {
    std::string fileName = "save.json";
    int compress = 1;
    if (!stream.eof())
        readString(stream, fileName);
    if (!stream.eof())
        readNumber(stream, compress);
    commandApi.save(table, fileName, compress > 0);
}

void command_load(std::istream & stream, CommandApi & commandApi, Table & table) {
    std::string fileName = "save.json";
    if (!stream.eof())
        readString(stream, fileName);

    std::ifstream fstream;
    fstream.open("Save/" + fileName);
    if (fstream.fail())
        throw std::invalid_argument("File '" + fileName + "' not found");

    nlohmann::json jsonGame;
    fstream >> jsonGame;
    commandApi.load(table, jsonGame);
}

}  // namespace


InputParser::CommandMap InputParser::commandMap_ = {
    { "connect", command_connect },
    { "disconnect", command_disconnect },
    { "tables", command_list_tables },
    { "create", command_create_table },
    { "sit", command_sit },
    { "leave", command_leave },
    { "list", command_list },
    { "context", command_context },
    { "save", command_save },
    { "load", command_load },
};

InputParser::InputParser(CommandApi & commandApi)
    : commandApi_(commandApi)
{ ; }

std::string InputParser::parseInputLine(const std::string & line, Table & table) {
    std::string command;
    try {
        std::istringstream iss(line);
        if (iss.eof())
            throw RedoFromStartException();
        iss >> command;
        auto found = commandMap_.find(command);
        if (found == commandMap_.end())
            throw UnknownCommandException(command);
        found->second(iss, commandApi_, table);
    } catch (const UnknownCommandException & ex) {
        return std::string("Unknown command: ") + ex.what();
    } catch (const MissingArgumentException &) {
        return "Missing argument";
    } catch (const RedoFromStartException &) {
        return "Please redo from start";
    } catch (const std::invalid_argument & ex) {
        return ex.what();
    } catch (const std::out_of_range & ex) {
        return ex.what();
    } catch (const std::runtime_error & ex) {
        return ex.what();
    }
    return "";
}
