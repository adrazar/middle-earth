#pragma once
#include "../Middle-Earth/CardTopology.h"
#include <nlohmann/json.hpp>
#include <string>
#include <variant>
#include <vector>

struct ConnectionRequest {
    const std::string userName;
    const std::string serverHost;
    uint16_t port;
};

struct CardSyncRequest {
    struct Side {
        Side(int player)
            : player(player)
        { ; }

        int player;
        std::vector<DeckName> decks;
        std::variant<std::vector<CompanyID>, std::string> companies;
    };

    std::vector<Side> sides;
};
