#pragma once
#include <vector>
#include <string>

std::vector<std::string> phaseNames = {
    "Untap phase", "Organization phase", "Long-event phase", "Movement/hazard phase", "Site phase", "End-of-turn phase", "Next turn",
};

std::vector<std::string> cardCategoryNames = {
    "Resource", "Hazard", "Character", "Site", "Region",
};

std::vector<std::string> itemTypeNames = {
    "minor", "major", "greater", "gold ring", "special", "information",
};

std::vector<std::string> raceNames = {
    "Man", "Dunadan", "Dwarf", "Elf", "Hobbit", "Wizard", "Orc", "Troll", "Ringwraith", "Balrog",
    "Dragon", "Drake", "Animal", "Spider", "Wolf", "Undead", "Nazgul", "Gas", "Unspecified",
};

std::vector<std::string> pathSymbolNames = {
    "CoastalSeas", "FreeDomain", "BorderLands", "Wilderness", "ShadowLands", "DarkDomain",
};

std::vector<std::string> siteTypeNames = {
    "Haven", "FreeHold", "BorderHold", "RuinsLairs", "ShadowHold", "DarkHold", "Darkhaven", "SiteInRegion",
};

std::vector<std::string> regionNames = {
    "Lindon", "Numeriador", "Forochel", "Arthedain", "TheShire", "Cardolan", "Angmar", "Rhudaur", "Hollin",
    "Dunland", "Enedhwaith", "OldPukelLand", "Gundabad", "HighPass", "RedhornGate", "GapOfIsen", "AnduinVales",
    "WoldAndFoothills", "Fangorn", "Rohan", "GreyMountainNarrows", "WoodlandRealm", "WesternMirkwood", "HeartOfMirkwood",
    "SouthernMirkwood", "BrownLands", "Dagorlad", "WitheredHeath", "NorthernRhovanion", "SouthernRhovanion", "IronHills",
    "Dorwinion", "HorsePlains", "OldPukelGap", "Andrast", "Anfalas", "Lamedon", "Belfalas", "Lebennin", "Anorien", "Ithilien",
    "Harandor", "Khand", "ImladMorgul", "Udun", "Gorgoroth", "Nurn", "ElvenShores", "EriadoranCoast", "AndrastCoast",
   "BayOfBelfalas", "MouthsOfTheAnduin", "RegionEnd",
};

std::vector<std::string> deckNames{
    "hand", "play deck", "discard pile", "location deck", "sideboard", "out of play", "eliminated", "kills",
    "stored", "factions", "permanent events", "long events", "pending", "creatures",
    "InCompany", "characters", "company permanent events", "on guard", "possessions", "sites",
};
