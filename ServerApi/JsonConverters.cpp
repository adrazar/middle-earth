#include "JsonConverters.h"

void toJson(const CardSyncRequest & request, nlohmann::json & jsonRequest) {
    auto & jsonCards = jsonRequest["sides"];
    jsonCards = nlohmann::json::array();
    for (const CardSyncRequest::Side & side : request.sides) {
        jsonCards.push_back(nlohmann::json());
        auto & jsonSide = jsonCards.back();
        jsonSide["player"] = side.player;

        auto & jsonDecks = jsonSide["decks"];
        jsonDecks = nlohmann::json::array();
        for (DeckName deckName : side.decks)
            jsonDecks.push_back(deckName);

        auto & jsonCompanies = jsonSide["companies"];
        if (std::holds_alternative<std::string>(side.companies)) {
            jsonCompanies = std::get<std::string>(side.companies);
        }
        else {
            auto & companies = std::get<std::vector<CompanyID>>(side.companies);
            if (!companies.empty()) {
                jsonCompanies = nlohmann::json::array();
                for (CompanyID id : companies)
                    jsonCompanies.push_back(id);
            }
        }
    }
}
