#include "ErrorCodes.h"
#include "TestUtilities.h"
#include <iostream>


// StrikeSimulator

StrikeSimulator::operator testing::AssertionResult() const {
    return assertionResult;
}

StrikeSimulator & StrikeSimulator::selectStrike(CardID target) {
    if (!assertionResult)
        return *this;
    currentFunction = "resolveStrike " + cardName(target) + ": ";
    ContextType & context = metaGame.gameStates.back().getContext();
    if (std::holds_alternative<Strike>(context) && std::get<Strike>(context).target == target) {
        strikeContext = getGameStrikeContext();
        return *this;
    }
    int ec = metaGame.resolveStrike(player, target);
    if (ec != Success) {
        assertionResult = testing::AssertionFailure() << currentFunction;
        if (ec == WrongContext)
            assertionResult << "Not in an AssignStrike context";
        else if (ec == WrongState)
            assertionResult << "Strikes not yet assigned";
        else if (ec == InvalidTarget)
            assertionResult << "Not facing a strike";
    } else {
        strikeContext = getGameStrikeContext();
    }
    return *this;
}

StrikeSimulator & StrikeSimulator::taps() {
    if (!assertionResult)
        return *this;
    currentFunction = "taps";
    Game & game = metaGame.gameStates.back();
    ContextType & context = game.getContext();
    if (!std::holds_alternative<Strike>(context))
        return expectedContext("Strike");
    Strike & strike = std::get<Strike>(context);
    if (!skipToState(strike, Strike::TapCharacter))
        return assertionFail(strike.target, "too late for tapping");
    int ec = metaGame.tap(player, strike.target);
    if (ec != Success) {
        assertionResult = testing::AssertionFailure() << currentFunction
            << " " << cardName(strike.target);
        if (ec == NotUntapped)
            assertionResult << ": card not untapped";
    }
    return *this;
}

StrikeSimulator & StrikeSimulator::staysInState(CardState state) {
    if (!assertionResult)
        return *this;
    currentFunction = "staysInState";
    Game & game = metaGame.gameStates.back();
    ContextType & context = game.getContext();
    if (!std::holds_alternative<Strike>(context))
        return expectedContext("Strike");
    Strike & strike = std::get<Strike>(context);
    if (game.getCardState(strike.target) != state)
        return assertionFail(strike.target, "not " + cardStateName(state));
    skipToState(strike, Strike::PlayResources);
    return *this;
}

StrikeSimulator & StrikeSimulator::support(std::vector<CardID> cards) {
    if (!assertionResult)
        return *this;
    currentFunction = "support";
    Game & game = metaGame.gameStates.back();
    ContextType & context = game.getContext();
    if (!std::holds_alternative<Strike>(context))
        return expectedContext("Strike");
    Strike & strike = std::get<Strike>(context);
    if (!skipToState(strike, Strike::PlayResources))
        return assertionFail(strike.target, "too late for tapping in support");
    for (CardID target : cards) {
        int ec = metaGame.tap(player, target);
        if (ec != Success) {
            assertionResult = testing::AssertionFailure()
                << "support " << cardName(target) << ": ";
            if (ec == NotUntapped)
                assertionResult << "card not untapped";
            else if (ec == InvalidTarget)
                assertionResult << "not a character or ally";
            else if (ec == IllegalOperation)
                assertionResult << "already tapped in support";
            else if (ec == CardNotInCompany)
                assertionResult << "not in attacked company";
        }
    }
    return *this;
}

StrikeSimulator & StrikeSimulator::playCard(TestPlayer testPlayer, CardID card, int option, CardID selectedCard) {
    if (!assertionResult)
        return *this;
    currentFunction = "playCard";
    Game & game = metaGame.gameStates.back();
    ContextType & context = game.getContext();
    if (!std::holds_alternative<Strike>(context))
        return expectedContext("Strike");
    Strike & strike = std::get<Strike>(context);
    if (testPlayer == game.hazardPlayer)
        if (strike.state != Strike::PlayHazards)
            return assertionFail(card, "too late for playing hazard cards");
    else
        if (!skipToState(strike, Strike::PlayResources))
            return assertionFail(card, "too late for playing resource cards");
    metaGame.selectCard(testPlayer, selectedCard);
    auto actions = metaGame.getActions(testPlayer, card);
    if (option < 0 || option >= (int)actions.size())
        return assertionFail(card, "the selected option is unavailable");
    int ec = metaGame.useAction(testPlayer, actions[option].id);
    if (ec != Success) {
        // This branch should never occur with the current implementation of metaGame.useAction()
        assertionResult = testing::AssertionFailure()
            << "playCard " << cardName(card) << ": ";
        if (ec == InvalidActionID)
            assertionResult << "invalid actionID";
        else if (ec == IllegalAction)
            assertionResult << "the conditions for playing this card are not met";
    } else {
        metaGame.done(testPlayer);
    }
    return *this;
}

StrikeSimulator & StrikeSimulator::attackRoll(int result) {
    if (!assertionResult)
        return *this;
    currentFunction = "attackRoll";
    Game & game = metaGame.gameStates.back();
    ContextType & context = game.getContext();
    if (std::holds_alternative<Strike>(context)) {
        Strike & strike = std::get<Strike>(context);
        bool noRoll = getCurrentScene(game).strikeResult(strike) + 2 > 0;
        skipToState(strike, Strike::ResolveStrike);
        if (noRoll) {
            metaGame.done(player);
            return *this;
        }
    }
    if (!std::holds_alternative<RollDice>(game.getContext()))
        expectedContext("RollDice");
    std::shared_ptr<Context> parentContextExtendedLifetime = game.contextStack[game.contextStack.back()->parent];
    ContextType & parentContext = parentContextExtendedLifetime->context;
    if (!std::holds_alternative<Strike>(parentContext))
        return assertionFail(-1, "Expected parent: Strike, Current parent: " + getContextName(parentContext));
    int ec = metaGame.roll(game.currentPlayer, result);
    if (ec != Success) {
        assertionResult = testing::AssertionFailure() << currentFunction << ": ";
        if (ec == WrongPlayer)
            assertionResult << "not the resource player's roll";
    } else {
        metaGame.done(player);
        metaGame.done(player);
    }
    return *this;
}

StrikeSimulator & StrikeSimulator::defendBodyCheck(int result) {
    if (!assertionResult)
        return *this;
    currentFunction = "defendBodyCheck";
    Game & game = metaGame.gameStates.back();
    ContextType & context = game.getContext();
    if (!std::holds_alternative<RollDice>(context))
        return expectedContext("RollDice");
    ContextType & parentContext = game.contextStack[game.contextStack.back()->parent]->context;
    if (!std::holds_alternative<BodyCheck>(parentContext))
        return assertionFail(-1, "Expected parent: BodyCheck, Current parent: " + getContextName(parentContext));
    int ec = metaGame.roll(game.hazardPlayer, result);
    if (ec != Success) {
        assertionResult = testing::AssertionFailure() << currentFunction << ": ";
        if (ec == WrongPlayer)
            assertionResult << "not the hazard player's roll";
    } else {
        metaGame.done(game.currentPlayer);
    }
    return *this;
}

StrikeSimulator & StrikeSimulator::verifyTargetProwess(int prowess) {
    if (!assertionResult)
        return *this;
    currentFunction = "verifyTargetProwess";
    Game & game = metaGame.gameStates.back();
    ContextType & context = game.getContext();
    if (!std::holds_alternative<Strike>(context))
        return expectedContext("Strike");
    Strike & strike = std::get<Strike>(context);
    Scene scene = getCurrentScene(game);
    int targetProwess = scene.getTargetProwess(strike);
    if (targetProwess != prowess)
        assertionResult = testing::AssertionFailure()
        << currentFunction
        << ": expected prowess: " << prowess
        << ", actual prowess: " << targetProwess;
    return *this;
}

StrikeSimulator & StrikeSimulator::verifyTargetBody(int body) {
    if (!assertionResult)
        return *this;
    currentFunction = "verifyTargetBody";
    Game & game = metaGame.gameStates.back();
    ContextType * context = &game.getContext();
    Scene scene = getCurrentScene(game);
    if (std::holds_alternative<RollDice>(*context))
        context = &scene.parent().context->context;
    int targetBody;
    if (std::holds_alternative<Strike>(*context)) {
        Strike & strike = std::get<Strike>(*context);
        targetBody = getCurrentScene(game).getTargetBody(strike);
    } else if (std::holds_alternative<BodyCheck>(*context)) {
        BodyCheck & bodyCheck = std::get<BodyCheck>(*context);
        CompanyID id = bodyCheck.defendingCompany;
        targetBody = getCurrentScene(game).getBody(bodyCheck.target, Unspecified, id);
        targetBody += bodyCheck.modifier;
    } else {
        return expectedContext("Strike or BodyCheck");
    }
    if (targetBody != body)
        assertionResult = testing::AssertionFailure()
        << currentFunction
        << ": expected body: " << body
        << ", actual body: " << targetBody;
    return *this;
}

StrikeSimulator & StrikeSimulator::verifyCreatureProwess(int prowess) {
    if (!assertionResult)
        return *this;
    currentFunction = "verifyCreatureProwess";
    Game & game = metaGame.gameStates.back();
    ContextType & context = game.getContext();
    if (!std::holds_alternative<Strike>(context))
        return expectedContext("Strike");
    Strike & strike = std::get<Strike>(context);
    Scene scene = getCurrentScene(game);
    int creatureProwess = scene.getCreatureStat(Prowess, strike.card, strike.creatureType, strike.creatureBaseProwess);
    if (creatureProwess != prowess)
        assertionResult = testing::AssertionFailure()
        << currentFunction
        << ": expected prowess: " << prowess
        << ", actual prowess: " << creatureProwess;
    return *this;
}

StrikeSimulator & StrikeSimulator::verifyStrikeFail() {
    if (!assertionResult)
        return *this;
    if (outcome() != Failed)
        assertionResult = testing::AssertionFailure()
        << "verifyStrikeFail: currently registered outcome is "
        << outcomeName(outcome());
    return *this;
}

StrikeSimulator & StrikeSimulator::verifyStrikeSuccess() {
    if (!assertionResult)
        return *this;
    if (outcome() != Successful)
        assertionResult = testing::AssertionFailure()
        << "verifyStrikeSuccess: currently registered outcome is "
        << outcomeName(outcome());
    return *this;
}

StrikeSimulator & StrikeSimulator::verifyStrikeIneffectual() {
    if (!assertionResult)
        return *this;
    if (outcome() != Ineffectual)
        assertionResult = testing::AssertionFailure()
        << "verifyStrikeIneffectual: currently registered outcome is "
        << outcomeName(outcome());
    return *this;
}

bool StrikeSimulator::skipToState(Strike & strike, Strike::States state) {
    Game & game = metaGame.gameStates.back();
    if (strike.state == Strike::PlayHazards)
        metaGame.done(game.hazardPlayer);
    if (state == Strike::ExcessStrikes)
        return strike.state == state;
    if (strike.state == Strike::ExcessStrikes)
        metaGame.done(game.hazardPlayer);
    if (state == Strike::TapCharacter)
        return strike.state == state;
    if (strike.state == Strike::TapCharacter)
        metaGame.done(game.currentPlayer);
    if (state == Strike::PlayResources)
        return strike.state == state;
    if (strike.state == Strike::PlayResources)
        metaGame.done(game.currentPlayer);
    return strike.state == state;
}

std::string StrikeSimulator::cardName(CardID card) {
    if (card < 0)
        return "";
    return getName(metaGame.gameStates.back().getCardType(card)).front();
}

std::string StrikeSimulator::outcomeName(Outcome outcome) {
    switch (outcome) {
    case Unresolved:
        return "Unresolved";
    case Cancelled:
        return "Cancelled";
    case Failed:
        return "Failed";
    case Successful:
        return "Successful";
    case Ineffectual:
        return "Ineffectual";
    }
    return "";
}

std::string StrikeSimulator::cardStateName(CardState state) {
    switch (state) {
    case Untapped:
        return "Untapped";
    case Tapped:
        return "Tapped";
    case Wounded:
        return "Wounded";
    }
    return "";
}

std::shared_ptr<Context> StrikeSimulator::getGameStrikeContext() {
    std::shared_ptr<Context> context = metaGame.gameStates.back().contextStack.back();
    if (std::holds_alternative<Strike>(context->context))
        return context;
    return nullptr;
}

Outcome StrikeSimulator::outcome() {
    if (!strikeContext)
        return Unresolved;
    return std::get<Strike>(strikeContext->context).outcome;
}

StrikeSimulator & StrikeSimulator::expectedContext(const std::string & requiredContext) {
    assertionResult = testing::AssertionFailure()
        << currentFunction << ": "
        << "Required context: " << requiredContext
        << "Current context: " << getContextName(metaGame.gameStates.back().getContext());
    return *this;
}

StrikeSimulator & StrikeSimulator::assertionFail(CardID card, const std::string & message) {
    assertionResult = testing::AssertionFailure() << currentFunction;
    if (card != -1)
        assertionResult << " " << cardName(card);
    assertionResult << ": " << message;
    return *this;
}

StrikeSimulator resolveStrike(MetaGame & metaGame, CardID target) {
    StrikeSimulator sim(metaGame);
    return sim.selectStrike(target);
}


// RegionSelector

RegionSelector::RegionSelector(MetaGame & metaGame, int player, CompanyID id)
    : metaGame(metaGame)
    , player(player)
    , company(*metaGame.gameStates.back().getSide(player).findCompany(id))
    , assertionResult(testing::AssertionSuccess())
    , regions{}
    , adjacentRegions{}
{
    Game & game = metaGame.gameStates.back();
    Location & location = game.getLocation(company.currentLocation);
    regions.push_back(location.region);
}

RegionSelector RegionSelector::verify(int code, int expected) {
    if (code != expected)
        assertionResult = testing::AssertionFailure()
        << "verify: got " << code << ", expected " << expected << std::endl;
    return *this;
}

RegionSelector RegionSelector::selectDestination(CardID destination, int expected) {
    if (!assertionResult)
        return *this;
    return (regions.size() < 2)
        ? verify(metaGame.selectDestination(player, company.id, destination), expected)
        : verify(metaGame.selectDestination(player, company.id, destination, regions), expected);
}

RegionSelector RegionSelector::selectDestination(const std::string & destinationName, int expected) {
    if (!assertionResult)
        return *this;
    GameSettings & settings = metaGame.settings;
    auto it = settings.cardNameToDefinitionIndex.find(destinationName);
    if (it == settings.cardNameToDefinitionIndex.end())
        return verify(NoCardOfSuchName, expected);
    if (!std::holds_alternative<Location>(settings.cardDefinitions[it->second]->type))
        return verify(NotALocation, expected);
    Game & game = metaGame.gameStates.back();
    CardID destination = game.findSiteID(game.sides[player], it->second);
    if (destination < 0)
        return verify(LocationNotAvailable, expected);
    return selectDestination(destination, expected);
}

RegionSelector RegionSelector::resetDestination() {
    if (!assertionResult)
        return *this;
    while (regions.size() > 1)
        regions.pop_back();
    return verify(metaGame.resetDestination(player, company.id));
}

RegionSelector RegionSelector::insertRegion(RegionNumber region, int expected) {
    if (!assertionResult)
        return *this;
    Scene scene = metaGame.currentScene();
    int regionLimit = scene.getRegionLimit(player, company.id);
    if (regions.size() == regionLimit)
        return verify(RegionLimitReached, expected);
    metaGame.getAdjacentRegions(player, regions, adjacentRegions);
    if (adjacentRegions.find(region) == adjacentRegions.end())
        return verify(RegionNotAdjacent, expected);
    regions.push_back(region);
    return verify(Success, expected);
}

RegionSelector RegionSelector::popRegion() {
    if (!assertionResult)
        return *this;
    if (regions.size() > 1)
        regions.pop_back();
    return *this;
}

std::ostream & operator<<(std::ostream & out, const std::unordered_set<RegionNumber> & regions) {
    for (RegionNumber region : regions)
        out << region << " ";
    return out << std::endl;
}

RegionSelector RegionSelector::verifyAdjacentRegions(const std::unordered_set<RegionNumber> & expectedResult) {
    if (!assertionResult)
        return *this;
    metaGame.getAdjacentRegions(player, regions, adjacentRegions);
    if (adjacentRegions != expectedResult)
        assertionResult = testing::AssertionFailure()
        << "verifyAdjacentRegions: "
        << "actual: " << adjacentRegions
        << "expected: " << expectedResult;
    return *this;
}


void resetContextStack(Game & game) {
    while (!std::holds_alternative<BottomContext>(game.getContext()))
        game.contextStack.pop_back();
}

bool isPlayable(MetaGame & metaGame, CardID card, CardID location, CardID recipient) {
    metaGame.cloneState();
    Game & game = metaGame.gameStates.back();
    int player = game.currentPlayer;
    resetContextStack(game);
    game.turnSequence.setPhase(SitePhase, Beginning);
    GameConfigurer configurer(metaGame);
    PlayerConfigurer pc(configurer, player);
    CompanyID company = game.getCompanyID(recipient);
    pc.setCurrentLocation(company, location);
    metaGame.done(player);
    Explore & explore = std::get<Explore>(game.getContext());
    explore.state = Explore::ExploreSite;
    metaGame.selectCard(player, recipient);
    bool playable = !metaGame.getActions(player, card).empty();
    metaGame.gameStates.pop_back();
    return playable;
}

bool isTargetable(MetaGame & metaGame, CardID target, KeyedToType key, CardID creature) {
    metaGame.cloneState();
    Game & game = metaGame.gameStates.back();
    int player = game.hazardPlayer;
    metaGame.setKeyedTo(player, key);
    metaGame.useAction(player, metaGame.getActions(player, creature)[0].id);
    metaGame.done(player);
    metaGame.done(player);
    bool targetable = getCurrentScene(game).allowedOperation(target, Target);
    metaGame.gameStates.pop_back();
    return targetable;
}

bool useAction(MetaGame & metaGame, CardID card, int option, int player) {
    Game & game = metaGame.gameStates.back();
    if (player < 0)
        player = game.cards[card].owner;
    std::vector<ActionInfo> actions = metaGame.getActions(player, card);
    if (actions.empty())
        return false;
    metaGame.useAction(player, actions[option].id);
    metaGame.done(player);
    return true;
}

void discardFromHand(MetaGame & metaGame, int player, int handSize) {
    Game & game = metaGame.gameStates.back();
    std::vector<CardID> & hand = game.sides[player].hand;
    int discards = hand.size() - handSize;
    if (discards > 0) {
        std::vector<CardID> cards;
        std::copy(hand.cend() - discards, hand.cend(), std::back_inserter(cards));
        metaGame.discard(player, cards);
    } else {
        metaGame.done(player);
    }
}

void skipReturnToHandSize(MetaGame & metaGame) {
    // Discards the last drawn cards
    Game & game = metaGame.gameStates.back();
    if (std::holds_alternative<ReturnToHandSize>(game.getContext())) {
        ReturnToHandSize & context = std::get<ReturnToHandSize>(game.getContext());
        discardFromHand(metaGame, game.currentPlayer, context.resourcePlayerHandSize);
        if (std::holds_alternative<ReturnToHandSize>(game.getContext()))
            discardFromHand(metaGame, game.hazardPlayer, context.hazardPlayerHandSize);
    }
}

void skipDrawCards(MetaGame & metaGame) {
    Game & game = metaGame.gameStates.back();
    if (std::holds_alternative<DrawCards>(game.getContext())) {
        metaGame.drawCard(game.currentPlayer);
        metaGame.done(game.currentPlayer);
        metaGame.drawCard(game.hazardPlayer);
        metaGame.done(game.hazardPlayer);
    }
}

void skipToMovementHazardPhase(MetaGame & metaGame, CompanyID id) {
    Game & game = metaGame.gameStates.back();
    resetContextStack(game);
    game.turnSequence.setPhase(MovementHazardPhase, Beginning);
    int player = game.currentPlayer;
    metaGame.done(player);
    if (std::holds_alternative<SelectCompany>(game.getContext())) {
        if (id < 0)
            id = std::get<SelectCompany>(game.getContext()).remainingCompanies.front();
        metaGame.chooseCompany(player, id);
    }
    skipDrawCards(metaGame);
}

void skipToSitePhase(MetaGame & metaGame, CompanyID id) {
    Game & game = metaGame.gameStates.back();
    resetContextStack(game);
    Side & side = game.getCurrentSide();
    for (Company & company : side.companies)
        company.currentLocation = company.newLocation;
    game.turnSequence.setPhase(SitePhase, Beginning);
    int player = game.currentPlayer;
    metaGame.done(player);
    if (std::holds_alternative<SelectCompany>(game.getContext())) {
        if (id < 0)
            id = std::get<SelectCompany>(game.getContext()).remainingCompanies.front();
        metaGame.chooseCompany(player, id);
    }
}

void skipAutomaticAttack(MetaGame & metaGame) {
    Game & game = metaGame.gameStates.back();
    ContextType & context = game.getContext();
    if (std::holds_alternative<Explore>(context))
        std::get<Explore>(context).state = Explore::ExploreSite;
}

void skipTurn(MetaGame & metaGame) {
    Game & game = metaGame.gameStates.back();
    resetContextStack(game);
    game.turnSequence.setPhase(EndOfTurnPhase, End);
    metaGame.done(game.currentPlayer);
}

struct CardTraits {
    CardTraits(Game & game) : game(game) {}
    std::string name(CardID card) {
        return getName(game.getCardType(card)).front();
    }
    std::vector<CardID> possessions(CardID card) {
        return game.cards[card].possessions;
    }
    CardState state(CardID card) {
        return game.cards[card].state;
    }
    Game & game;
};

std::string indentString(int indent) {
    return std::string(3 * indent, ' ');
}

void printCardState(CardState state) {
    switch (state) {
    case Tapped:
        std::cout << " (T)";
        break;
    case Wounded:
        std::cout << " (W)";
    default:
        break;
    }
}

void printCard(Game & game, CardID card, int indent, bool printState) {
    CardTraits cardTraits(game);
    std::cout << indentString(indent) << cardTraits.name(card);
    if (printState)
        printCardState(cardTraits.state(card));
    std::cout << std::endl;
}

void printCards(Game & game, const std::vector<CardID> & cards, int indent, bool printState) {
    for (CardID card : cards) {
        printCard(game, card, indent, printState);
    }
}

void printCharacter(Game & game, CardID character, int indent = 0) {
    CardTraits cardTraits(game);
    printCard(game, character, indent, true);
    printCards(game, cardTraits.possessions(character), indent + 1, true);
}

void printCompany(Game & game, const Company & company) {
    //getName(metaGame.settings.cardDefinitions[game.cards[company.currentLocation].dataIndex]->type);
    CardTraits cardTraits(game);

    std::cout << cardTraits.name(company.currentLocation);
    if (company.newLocation != company.currentLocation)
        std::cout << " -> " << cardTraits.name(company.newLocation);
    std::cout << std::endl;
    printCards(game, company.permanentEvents, 0, false);
    std::map<CardID, std::vector<CardID>> followerMap;
    for (const CardID character : company.characters) {
        const Card & cardData = game.cards[character];
        if (cardData.parent >= 0) {
            followerMap[cardData.parent].push_back(character);
        } else {
            followerMap.insert({character, {}});
        }
    }
    for (const auto &[parent, followers] : followerMap) {
        printCharacter(game, parent);
        for (CardID follower : followers) {
            printCharacter(game, follower, 1);
        }
    }
}


void printState(Game & game) {
    std::cout << "Current player: " << game.currentPlayer << " Hazard player: " << game.hazardPlayer << std::endl;
    for (const Side & side : game.sides) {
        std::cout << "Player " << side.player << std::endl;
        printCards(game, side.permanentEvents, 0, false);
        printCards(game, side.longEvents, 0, false);
        for (const Company & company : side.companies) {
            printCompany(game, company);
        }
        std::cout << std::endl;
    }
}

int TestPlayer::selectDestination(CompanyID id, CardID destination)
{
    return metaGame.selectDestination(player, id, destination);
}

int TestPlayer::transferCharacter(CardID target, CompanyID destination)
{
    return metaGame.transferCharacter(player, target, destination);
}

int TestPlayer::takeFollower(CardID leader, CardID follower) {
    return metaGame.takeFollower(player, leader, follower);
}

int TestPlayer::dropFollower(CardID follower) {
    return metaGame.dropFollower(player, follower);
}

int TestPlayer::chooseCompany(CompanyID id)
{
    return metaGame.chooseCompany(player, id);
}

int TestPlayer::discard(const std::vector<CardID> & cards)
{
    return metaGame.discard(player, cards);
}

int TestPlayer::enterSite()
{
    return metaGame.enterSite(player);
}

int TestPlayer::selectCompany(CompanyID id)
{
    return metaGame.selectCompany(player, id);
}

int TestPlayer::selectCard(CardID card)
{
    return metaGame.selectCard(player, card);
}

int TestPlayer::setKey(KeyedToType key)
{
    return metaGame.setKeyedTo(player, key);
}

int TestPlayer::useAction(CardID card, size_t option)
{
    std::vector<ActionInfo> actions = metaGame.getActions(player, card);
    if (option >= actions.size())
        return ActionNotAvailable;
    int result = metaGame.useAction(player, actions[option].id);
    if (result == Success)
        metaGame.done(player);
    return result;
}

int TestPlayer::useActionOnCard(CardID card, CardID target, size_t option)
{
    int result = metaGame.selectCard(player, target);
    return (result == Success) ? useAction(card, option) : result;
}

int TestPlayer::useActionOnCards(CardID card, const std::vector<CardID> & cards, size_t option)
{
    int result = metaGame.selectCards(player, cards);
    return (result == Success) ? useAction(card, option) : result;
}

int TestPlayer::useActionOnCompany(CardID card, CompanyID id, size_t option)
{
    int result = metaGame.selectCompany(player, id);
    return (result == Success) ? useAction(card, option) : result;
}

int TestPlayer::assignStrike(CardID target)
{
    return metaGame.assignStrike(player, target);
}

int TestPlayer::tap(CardID card) {
    return metaGame.tap(player, card);
}

int TestPlayer::resolveCorruptionCheck(CardID id) {
    return metaGame.resolveCorruptionCheck(player, id);
}

int TestPlayer::roll(int result) {
    result = metaGame.roll(player, result);
    if (result != Success)
        return result;
    return metaGame.done(player);
}

int TestPlayer::done()
{
    return metaGame.done(player);
}

int TestPlayer::drawCard()
{
    return metaGame.drawCard(player);
}
