#include "ContextInterface.h"
#include "ErrorCodes.h"
#include "Fixtures.h"
#include "GameConfigurer.h"
#include "GameInterface.h"
#include "TestUtilities.h"
#include <gtest/gtest.h>


TEST(DwarfDeck, StartingConfiguration) {
    MetaGame metaGame;
    GameSettings & settings = metaGame.settings;
    loadCardSet(metaGame.settings, CardSet::CoreCards);
    loadCardSet(metaGame.settings, CardSet::DwarfDeck);

    GameConfigurer configurer(metaGame);
    PlayerConfigurer pc = configurer.addPlayer();

    auto decks = loadDeckDefinitions();
    pc.loadDeck(decks["Dwarven Quest"]);
}

TEST_F(StartFromLorien, BuhrWidu) {
    Scene scene = getCurrentScene(game);
    pc.setCurrentLocation(0, buhrWidu);
    scene.tap(buhrWidu);
    ASSERT_EQ(metaGame.selectDestination(0, 0, lorien), Success);
    skipToMovementHazardPhase(metaGame);
    Company & company = game.getCurrentSide().companies.front();
    EXPECT_EQ(company.currentLocation, buhrWidu);
    EXPECT_EQ(game.getDeck(buhrWidu), LocationDeck);
    EXPECT_EQ(game.getCardState(buhrWidu), Tapped);
    metaGame.done(1);
    EXPECT_TRUE(std::holds_alternative<ReturnToHandSize>(game.getContext()));
    EXPECT_EQ(metaGame.discard(0, { balin }), Success);
    EXPECT_EQ(metaGame.discard(1, { caveWorm }), Success);
    EXPECT_TRUE(std::holds_alternative<Explore>(game.getContext()));
    EXPECT_EQ(company.currentLocation, lorien);
    EXPECT_EQ(company.newLocation, lorien);
    EXPECT_EQ(game.getDeck(buhrWidu), LocationDeck);
    EXPECT_EQ(game.getCardState(buhrWidu), Untapped);
}

TEST_F(StartFromLorien, Quickbeam) {
    EXPECT_FALSE(isPlayable(metaGame, quickbeam, lorien, thorin));
    EXPECT_TRUE(isPlayable(metaGame, quickbeam, wellinghall, thorin));
    EXPECT_FALSE(isPlayable(metaGame, quickbeam, cavesOfUlund, thorin));
    configurer.addPossession(thorin, quickbeam);
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(metaGame.selectDestination(0, 0, moria), Success);
        skipToMovementHazardPhase(metaGame);
        metaGame.setKeyedTo(1, "Moria");
        EXPECT_TRUE(useAction(metaGame, watcher));
        metaGame.done(0);
        ASSERT_TRUE(std::holds_alternative<AssignStrikes>(game.getContext()));
        AssignStrikes & assign = std::get<AssignStrikes>(game.getContext());
        EXPECT_EQ(assign.assignments.size(), 5);
        metaGame.gameStates.pop_back();
    }
    ASSERT_EQ(metaGame.selectDestination(0, 0, sarnGoriwing), Success);
    skipToMovementHazardPhase(metaGame);
    EXPECT_TRUE(isTargetable(metaGame, quickbeam, Wilderness, giantSpiders));
    EXPECT_FALSE(isTargetable(metaGame, quickbeam, ShadowHold, giantSpiders));
    EXPECT_TRUE(isTargetable(metaGame, quickbeam, HeartOfMirkwood, giantSpiders));
    // Automatic attack
    resetContextStack(game);
    pc.setCurrentLocation(0, sarnGoriwing);
    game.turnSequence.setPhase(SitePhase, Beginning);
    metaGame.done(0);
    ASSERT_EQ(metaGame.enterSite(0), Success);
    metaGame.done(0);
    ASSERT_TRUE(std::holds_alternative<AssignStrikes>(game.getContext()));
    EXPECT_EQ(metaGame.assignStrike(0, thorin), Success);
    EXPECT_EQ(metaGame.assignStrike(0, quickbeam), InvalidTarget);
}

TEST_F(StartFromLorien, BookOfMazarbul) {
    EXPECT_FALSE(isPlayable(metaGame, bookOfMazarbul, cavesOfUlund, gloin));
    EXPECT_TRUE(isPlayable(metaGame, bookOfMazarbul, moria, gloin));
    EXPECT_FALSE(isPlayable(metaGame, bookOfMazarbul, goblinGate, gloin));
    pc.addCharacterToCompany(0, pallando);
    configurer.addPossession(gloin, bookOfMazarbul);
    EXPECT_TRUE(metaGame.getActions(0, bookOfMazarbul).empty());
    Scene scene = getCurrentScene(game);
    scene.gainPossession(pallando, bookOfMazarbul);
    ASSERT_EQ(game.getParent(bookOfMazarbul), pallando);
    std::vector<ActionInfo> actions = metaGame.getActions(0, bookOfMazarbul);
    EXPECT_FALSE(actions.empty());
    EXPECT_EQ(scene.getHandSize(0), 9);
    ASSERT_EQ(metaGame.useAction(0, actions[0].id), Success);
    metaGame.done(0);
    EXPECT_EQ(scene.getHandSize(0), 10);
    EXPECT_EQ(game.getCardState(bookOfMazarbul), Tapped);
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(metaGame.store(0, bookOfMazarbul), Success);
        metaGame.done(0);
        metaGame.roll(0, 3);
        metaGame.done(0);
        EXPECT_EQ(game.getDeck(bookOfMazarbul), Stored);
        EXPECT_EQ(game.getCardState(bookOfMazarbul), Untapped);
        EXPECT_EQ(scene.getMP(bookOfMazarbul, 0), 1);
        metaGame.gameStates.pop_back();
    }
    pc.setCurrentLocation(0, blueMountainDh);
    EXPECT_TRUE(scene.allowedOperation(bookOfMazarbul, Store, false));
    pc.setCurrentLocation(0, wellinghall);
    EXPECT_FALSE(scene.allowedOperation(bookOfMazarbul, Store, false));
    pc.setCurrentLocation(0, ironHillDh);
    ASSERT_EQ(metaGame.store(0, bookOfMazarbul), Success);
    metaGame.done(0);
    metaGame.roll(0, 3);
    metaGame.done(0);
    EXPECT_EQ(game.getDeck(bookOfMazarbul), Stored);
    EXPECT_EQ(scene.getMP(bookOfMazarbul, 0), 5);
    // The increased hand size effect lasts until player's next untap phase
    EXPECT_EQ(scene.getHandSize(0), 10);
    resetContextStack(game);
    game.turnSequence.setPhase(EndOfTurnPhase, End);
    metaGame.done(0);
    EXPECT_EQ(scene.getHandSize(0), 10);
    resetContextStack(game);
    game.turnSequence.setPhase(EndOfTurnPhase, End);
    metaGame.done(0);
    EXPECT_EQ(scene.getHandSize(0), 9);
}

TEST_F(StartFromLorien, MagicalHarp) {
    CardID magicalHarp = getCardID(game, 0, "Magical Harp");
    configurer.drawCard(magicalHarp);
    EXPECT_FALSE(isPlayable(metaGame, magicalHarp, buhrWidu, thorin));
    EXPECT_TRUE(isPlayable(metaGame, magicalHarp, cavesOfUlund, thorin));
    EXPECT_FALSE(isPlayable(metaGame, magicalHarp, moria, thorin));
    CardID fellowship = getCardID(game, 0, "Fellowship");
    configurer.drawCard(fellowship);
    metaGame.selectCompany(0, 0);
    EXPECT_TRUE(useAction(metaGame, fellowship));
    EXPECT_EQ(game.getDeck(fellowship), CompanyPermanentEvents);
    configurer.addPossession(thorin, magicalHarp);
    EXPECT_TRUE(useAction(metaGame, magicalHarp));
    ASSERT_TRUE(std::holds_alternative<CorruptionCheck>(game.getContext()));
    CorruptionCheck & corruption = std::get<CorruptionCheck>(game.getContext());
    EXPECT_EQ(corruption.targetCharacter, thorin);
    EXPECT_EQ(corruption.base, 6);
    EXPECT_EQ(corruption.modifier, 1);
    metaGame.done(0);
    ASSERT_TRUE(std::holds_alternative<RollDice>(game.getContext()));
    ASSERT_EQ(metaGame.roll(0, 4), Success);
    metaGame.done(0);
    ASSERT_TRUE(std::holds_alternative<OrganizeCompanies>(game.getContext()));
    EXPECT_EQ(game.getDeck(thorin), Characters);
    EXPECT_EQ(game.getCardState(thorin), Untapped);
    EXPECT_EQ(game.getDeck(magicalHarp), Possessions);
    EXPECT_EQ(game.getCardState(magicalHarp), Tapped);
    EXPECT_EQ(game.getDeck(fellowship), CompanyPermanentEvents);
    game.turnSequence.phase = EndOfTurnPhase;
    removeEffects(game);
    EXPECT_TRUE(game.duplicateEffect(magicalHarp));
    metaGame.done(0);
    ASSERT_TRUE(std::holds_alternative<OrganizeCompanies>(game.getContext()));
    removeEffects(game);
    EXPECT_FALSE(game.duplicateEffect(magicalHarp));
    // Tap during opponent's Site phase
    CardID boromir = getCardID(game, 1, "Boromir II");
    opc.setCurrentLocation(0, opc.addCardToDeck("Lórien", LocationDeck));
    game.cards[magicalHarp].state = Untapped;
    EXPECT_TRUE(metaGame.getActions(0, magicalHarp).empty());
    game.turnSequence.setPhase(SitePhase, Beginning);
    game.turnSequence.round = 2;
    EXPECT_FALSE(metaGame.getActions(0, magicalHarp).empty());
    metaGame.done(1);
    ASSERT_EQ(metaGame.enterSite(1), Success);
    metaGame.cloneState(); {
        EXPECT_TRUE(useAction(metaGame, magicalHarp));
        metaGame.done(0);
        ASSERT_EQ(metaGame.roll(0, 4), Success);
        metaGame.done(0);
        ASSERT_EQ(metaGame.influenceOpponent(1, boromir, oin), InvalidTarget);
        ASSERT_EQ(metaGame.influenceOpponent(1, boromir, throrsMap), Success);
        metaGame.gameStates.pop_back();
    }
    ASSERT_EQ(metaGame.influenceOpponent(1, boromir, oin), Success);
    EXPECT_TRUE(useAction(metaGame, magicalHarp));
    metaGame.done(0);
    ASSERT_EQ(metaGame.roll(0, 4), Success);
    metaGame.done(0);
    ASSERT_TRUE(std::holds_alternative<InfluenceAttempt>(game.getContext()));
    metaGame.done(1);
    EXPECT_TRUE(std::holds_alternative<Explore>(game.getContext()));
}

TEST_F(StartFromLorien, Fellowship) {
    CardID fellowship[2];
    for (int i = 0; i < 2; i++) {
        fellowship[i] = getCardID(game, 0, "Fellowship", Hand);
        configurer.drawCard(fellowship[i]);
    }
    configurer.drawCard(balin);
    metaGame.selectCompany(0, 0);
    EXPECT_FALSE(metaGame.getActions(0, fellowship[0]).empty());
    ASSERT_EQ(metaGame.transferCharacter(0, gloin), Success);
    EXPECT_TRUE(metaGame.getActions(0, fellowship[0]).empty());
    configurer.addPossession(thorin, quickbeam);
    EXPECT_FALSE(metaGame.getActions(0, fellowship[0]).empty());
    game.turnSequence.phase = SitePhase;
    EXPECT_TRUE(metaGame.getActions(0, fellowship[0]).empty());
    game.turnSequence.phase = OrganizationPhase;
    pc.setCurrentLocation(0, ironHillDh);
    metaGame.selectCompany(0, 0);
    EXPECT_TRUE(metaGame.getActions(0, fellowship[0]).empty());
    pc.setCurrentLocation(0, lorien);
    EXPECT_EQ(metaGame.transferCharacter(0, gloin, 0), Success);
    for (int i = 0; i < 2; i++) {
        metaGame.selectCompany(0, 0);
        EXPECT_TRUE(useAction(metaGame, fellowship[i]));
        ASSERT_EQ(game.getDeck(fellowship[i]), CompanyPermanentEvents);
    }
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        EXPECT_EQ(metaGame.store(0, durinsAxe), Success);
        ASSERT_TRUE(std::holds_alternative<CorruptionCheck>(game.getContext()));
        CorruptionCheck & corruptionCheck = std::get<CorruptionCheck>(game.getContext());
        EXPECT_EQ(corruptionCheck.base, 4);
        EXPECT_EQ(corruptionCheck.modifier, 2);
        EXPECT_EQ(metaGame.tap(0, dori), Success);
        metaGame.done(0);
        EXPECT_EQ(game.getDeck(durinsAxe), Stored);
        EXPECT_EQ(game.getDeck(fellowship[0]), CompanyPermanentEvents);
        metaGame.gameStates.pop_back();
    }
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        EXPECT_EQ(metaGame.transferCharacter(0, kili), Success);
        EXPECT_EQ(game.getCurrentSide().companies.front().characters.size(), 4);
        EXPECT_EQ(game.getDeck(fellowship[0]), DiscardPile);
        metaGame.gameStates.pop_back();
    }
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        metaGame.selectCompany(0, 0);
        EXPECT_TRUE(useAction(metaGame, balin));
        EXPECT_EQ(game.getCurrentSide().companies.front().characters.size(), 6);
        EXPECT_EQ(game.getDeck(fellowship[0]), DiscardPile);
        metaGame.gameStates.pop_back();
    }
    pc.addCharacterToCompany(pc.addCompany(moria), balin);
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        EXPECT_EQ(metaGame.transferCharacter(0, balin), Success);
        EXPECT_EQ(game.getCurrentSide().companies.back().id, 3);
        EXPECT_EQ(game.getDeck(fellowship[0]), CompanyPermanentEvents);
        EXPECT_EQ(metaGame.selectDestination(0, 0, moria), Success);
        skipToMovementHazardPhase(metaGame, 0);
        metaGame.setKeyedTo(1, RedhornGate);
        EXPECT_TRUE(useAction(metaGame, caveWorm));
        metaGame.done(0);
        metaGame.assignStrike(0, quickbeam);
        metaGame.done(0);
        ASSERT_TRUE(resolveStrike(metaGame, quickbeam)
            .verifyTargetProwess(8)
            .taps()
        );
        metaGame.cloneState(); {
            Game & game = metaGame.gameStates.back();
            ASSERT_TRUE(StrikeSimulator(metaGame)
                .attackRoll(5)
                .defendBodyCheck(11)
            );
            EXPECT_EQ(game.getDeck(quickbeam), Eliminated);
            EXPECT_EQ(game.getDeck(fellowship[0]), DiscardPile);
            metaGame.gameStates.pop_back();
        }
        ASSERT_TRUE(StrikeSimulator(metaGame)
            .attackRoll(9)
            .verifyStrikeFail()
        );
        EXPECT_EQ(game.getDeck(caveWorm), Kills);
        EXPECT_EQ(game.getDeck(fellowship[0]), CompanyPermanentEvents);
        metaGame.done(1);
        for (int i = 0; i < 2; i++) {
            metaGame.done(1);
            metaGame.done(0);
        }
        EXPECT_EQ(game.getCurrentSide().companies.front().characters.size(), 6);
        EXPECT_EQ(game.getDeck(fellowship[0]), DiscardPile);
        metaGame.gameStates.pop_back();
    }
    pc.setCurrentLocation(0, goblinGate);
    game.turnSequence.setPhase(SitePhase, Beginning);
    metaGame.done(0);
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        metaGame.chooseCompany(0, game.getCurrentSide().companies.back().id);
        ASSERT_TRUE(std::holds_alternative<Explore>(game.getContext()));
        std::get<Explore>(game.getContext()).state = Explore::ExploreSite;
        metaGame.selectCard(0, balin);
        EXPECT_TRUE(useAction(metaGame, gollum));
        EXPECT_EQ(game.getDeck(fellowship[0]), CompanyPermanentEvents);
        metaGame.gameStates.pop_back();
    }
    metaGame.chooseCompany(0, 0);
    ASSERT_TRUE(std::holds_alternative<Explore>(game.getContext()));
    std::get<Explore>(game.getContext()).state = Explore::ExploreSite;
    metaGame.selectCard(0, thorin);
    EXPECT_TRUE(useAction(metaGame, gollum));
    EXPECT_EQ(game.getDeck(fellowship[0]), DiscardPile);
}

TEST_F(StartFromLorien, GreatRoad) {
    CardID greatRoad = getCardID(game, 0, "Great-road");
    configurer.drawCard(greatRoad);

    mainPlayer.selectCompany(mainCompany);
    EXPECT_FALSE(useAction(metaGame, greatRoad));
    mainPlayer.transferCharacter(gloin);
    int gloinCompany = 1;
    pc.setCurrentLocation(mainCompany, moria);
    mainPlayer.selectDestination(mainCompany, lorien);
    mainPlayer.selectDestination(gloinCompany, wellinghall);
    mainPlayer.selectCompany(gloinCompany);
    EXPECT_FALSE(useAction(metaGame, greatRoad));
    mainPlayer.done();
    EXPECT_EQ(game.turnSequence, TurnSequence(OrganizationPhase, End));
    mainPlayer.selectCompany(mainCompany);
    EXPECT_TRUE(metaGame.getActions(0, greatRoad).empty());
    mainPlayer.selectCompany(gloinCompany);
    EXPECT_TRUE(useAction(metaGame, greatRoad));
    mainPlayer.done();
    // Long-event phase
    mainPlayer.done();
    EXPECT_TRUE(std::holds_alternative<SelectCompany>(game.getContext()));
    EXPECT_EQ(mainPlayer.chooseCompany(gloinCompany), Success);
    ASSERT_TRUE(std::holds_alternative<DrawCards>(game.getContext()));
    {
        DrawCards & drawCards = std::get<DrawCards>(game.getContext());
        EXPECT_EQ(drawCards.playerLimit, 1);
        EXPECT_EQ(drawCards.opponentLimit, 2);
        EXPECT_EQ(opponentPlayer.drawCard(), Success);
        EXPECT_EQ(opponentPlayer.drawCard(), Success);
        EXPECT_EQ(opponentPlayer.drawCard(), CardLimitReached);
        EXPECT_EQ(mainPlayer.drawCard(), Success);
        EXPECT_EQ(mainPlayer.drawCard(), CardLimitReached);
        mainPlayer.done();
    }
    EXPECT_TRUE(std::holds_alternative<Movement>(game.getContext()));
    opponentPlayer.done();
    skipReturnToHandSize(metaGame);
    skipDrawCards(metaGame);
    opponentPlayer.done();
    skipReturnToHandSize(metaGame);
    ASSERT_TRUE(std::holds_alternative<SelectCompany>(game.getContext()));
    EXPECT_EQ(mainPlayer.chooseCompany(gloinCompany), Success);
    EXPECT_EQ(metaGame.enterSite(mainPlayer), Success);
    mainPlayer.selectCard(gloin);
    EXPECT_TRUE(useAction(metaGame, quickbeam));
    EXPECT_EQ(game.cards[gloin].possessions.back(), quickbeam);
    EXPECT_EQ(game.getCardState(wellinghall), Tapped);
    mainPlayer.done();
    mainPlayer.done();
    ASSERT_TRUE(std::holds_alternative<ReturnToHandSize>(game.getContext()));
    skipReturnToHandSize(metaGame);
    EXPECT_EQ(game.turnSequence, TurnSequence(EndOfTurnPhase, Middle));
    Side & side = game.getCurrentSide();
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        mainPlayer.done();
        EXPECT_TRUE(std::holds_alternative<OrganizeCompanies>(game.getContext()));
        EXPECT_EQ(side.companies.back().currentLocation, wellinghall);
        metaGame.gameStates.pop_back();
    }
    EXPECT_TRUE(useAction(metaGame, greatRoad));
    mainPlayer.done();
    EXPECT_TRUE(std::holds_alternative<OrganizeCompanies>(game.getContext()));
    EXPECT_EQ(side.companies.back().currentLocation, lorien);
}

TEST_F(StartFromLorien, SmokeRings) {
    CardID smokeRings = getCardID(game, 0, "Smoke Rings");
    configurer.drawCard(smokeRings);
    Side & side = game.getCurrentSide();
    CardID hundredsOfButterflies = getCardID(game, 0, "Hundreds of Butterflies");
    moveCardFast(game, hundredsOfButterflies, DiscardPile, 0);
    EXPECT_EQ(game.cards[hundredsOfButterflies].deck, DiscardPile);
    EXPECT_TRUE(useAction(metaGame, smokeRings));
    ASSERT_TRUE(std::holds_alternative<TransferCards>(game.getContext()));
    TransferCards & transfer = std::get<TransferCards>(game.getContext());
    ASSERT_EQ(transfer.transactions[0][0].sourceDeck, Sideboard);
    ASSERT_EQ(transfer.transactions[0][1].sourceDeck, DiscardPile);
    EXPECT_EQ(metaGame.transferCards(0, { oin, thorin }), WrongNumberOfCards);
    EXPECT_EQ(metaGame.transferCards(0, { hundredsOfButterflies }, 0), WrongDeck);
    EXPECT_EQ(metaGame.transferCards(0, { getCardID(game, 0, "Ghosts") }, 0), WrongCardCategory);
    std::vector<CardID> playDeckCopy = side.playDeck;
    EXPECT_EQ(metaGame.transferCards(0, { hundredsOfButterflies }, 1), Success);
    EXPECT_TRUE(std::holds_alternative<OrganizeCompanies>(game.getContext()));
    EXPECT_EQ(side.playDeck.size(), playDeckCopy.size() + 1);
    playDeckCopy.push_back(hundredsOfButterflies);
    EXPECT_NE(side.playDeck, playDeckCopy);  // Check that the deck gets reshuffeled
    EXPECT_EQ(game.cards[hundredsOfButterflies].deck, PlayDeck);
}

TEST_F(StartFromLorien, FoolishWords) {
    CardID foolishWords = getCardID(game, 1, "Foolish Words");
    CardID boromir = getCardID(game, 1, "Boromir II");
    pc.addCharacterToCompany(0, pallando);
    pc.setCurrentLocation(0, getLocationID(game, 0, "Rivendell"));
    configurer.drawCard(foolishWords);
    moveCardFast(game, fullOfFrothAndRage, DiscardPile, 1);
    metaGame.selectCard(0, pallando);
    EXPECT_TRUE(useAction(metaGame, balin));
    configurer.addPossession(balin, getCardID(game, 1, "Foolish Words", Hand));
    metaGame.selectCard(0, pallando);
    ASSERT_FALSE(useAction(metaGame, foolishWords));
    skipToMovementHazardPhase(metaGame);
    game.turnSequence.round = 2;
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        metaGame.selectCard(1, balin);
        ASSERT_FALSE(useAction(metaGame, foolishWords)); // Cannot be duplicated on a given character
        metaGame.selectCard(1, pallando);
        ASSERT_TRUE(useAction(metaGame, foolishWords));
        metaGame.done(1);
        skipReturnToHandSize(metaGame);
        ASSERT_EQ(metaGame.enterSite(0), Success);
        ASSERT_EQ(metaGame.influenceOpponent(0, pallando, boromir), Success);
        ASSERT_TRUE(std::holds_alternative<InfluenceAttempt>(game.getContext()));
        InfluenceAttempt & influence = std::get<InfluenceAttempt>(game.getContext());
        EXPECT_EQ(influence.requiredInfluence, 4);
        EXPECT_EQ(influence.modifier, 5 - 4 - 16);
        skipTurn(metaGame);
        EXPECT_FALSE(useAction(metaGame, foolishWords, 0, 0));
        skipTurn(metaGame);
        metaGame.done(1);  // Opponent may transfer hazards during Untap phase since Pallando is in play
        EXPECT_TRUE(useAction(metaGame, foolishWords, 0, 0));
        ContextType & type = (*(++game.contextStack.rbegin()))->context;
        ASSERT_TRUE(std::holds_alternative<RemoveCorruptionCard>(type));
        RemoveCorruptionCard & context = std::get<RemoveCorruptionCard>(type);
        EXPECT_EQ(game.getCardState(pallando), Tapped);
        EXPECT_EQ(context.requiredNumber, 7);
        EXPECT_EQ(context.modifier, 0);
        metaGame.cloneState(); {
            Game & game = metaGame.gameStates.back();
            metaGame.roll(0, 7);
            metaGame.done(0);
            EXPECT_TRUE(std::holds_alternative<OrganizeCompanies>(game.getContext()));
            EXPECT_EQ(game.cards[pallando].possessions.back(), foolishWords);
            metaGame.gameStates.pop_back();
        }
        metaGame.roll(0, 8);
        metaGame.done(0);
        EXPECT_TRUE(std::holds_alternative<OrganizeCompanies>(game.getContext()));
        EXPECT_EQ(game.getDeck(foolishWords), DiscardPile);
        EXPECT_TRUE(game.cards[pallando].possessions.empty());
        metaGame.gameStates.pop_back();
    }
    // On-guard
    EXPECT_EQ(metaGame.placeOnGuard(1, foolishWords), Success);
    metaGame.done(1);
    skipReturnToHandSize(metaGame);
    ASSERT_EQ(metaGame.enterSite(0), Success);
    metaGame.selectCard(1, pallando);
    EXPECT_FALSE(useAction(metaGame, foolishWords));
    metaGame.done(1);
    ASSERT_EQ(metaGame.influenceOpponent(0, pallando, boromir), Success);
    ASSERT_TRUE(std::holds_alternative<InfluenceAttempt>(game.getContext()));
    InfluenceAttempt & influence = std::get<InfluenceAttempt>(game.getContext());
    EXPECT_EQ(influence.requiredInfluence, 4);
    EXPECT_EQ(influence.modifier, 5 - 16);
    metaGame.clearSelection(1);
    EXPECT_FALSE(useAction(metaGame, foolishWords));
    metaGame.selectCard(1, pallando);
    EXPECT_TRUE(useAction(metaGame, foolishWords));
    EXPECT_EQ(influence.modifier, 5 - 4 - 16);
}

TEST_F(StartFromLorien, MusterDisperses) {
    CardID ironHillDwarves = getCardID(game, 0, "Iron Hill Dwarves");
    PlayerConfigurer pc = configurer.currentPlayerConfiguerer();
    pc.addFaction(ironHillDwarves);
    metaGame.selectCard(1, ironHillDwarves);
    EXPECT_FALSE(useAction(metaGame, musterDisperses));
    skipToMovementHazardPhase(metaGame);
    metaGame.selectCard(1, oin);
    EXPECT_FALSE(useAction(metaGame, musterDisperses));
    metaGame.selectCard(1, ironHillDwarves);
    EXPECT_TRUE(useAction(metaGame, musterDisperses));
    ContextType & type = (*(++game.contextStack.rbegin()))->context;
    ASSERT_TRUE(std::holds_alternative<SpecialTest>(type));
    SpecialTest & specialTest = std::get<SpecialTest>(type);
    EXPECT_EQ(specialTest.host, musterDisperses);
    EXPECT_EQ(specialTest.target, ironHillDwarves);
    EXPECT_EQ(specialTest.modifier, 7);
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        metaGame.roll(0, 4);
        metaGame.done(0);
        EXPECT_TRUE(std::holds_alternative<Movement>(game.getContext()));
        EXPECT_EQ(game.getDeck(ironHillDwarves), Factions);
        EXPECT_EQ(game.getDeck(musterDisperses), DiscardPile);
        metaGame.gameStates.pop_back();
    }
    metaGame.roll(0, 3);
    metaGame.done(0);
    EXPECT_TRUE(std::holds_alternative<Movement>(game.getContext()));
    EXPECT_EQ(game.getDeck(ironHillDwarves), DiscardPile);
    EXPECT_EQ(game.getDeck(musterDisperses), DiscardPile);
}

TEST_F(StartFromLorien, River) {
    CompanyID pallandoCompany = pc.addCompany(moria);
    pc.addCharacterToCompany(pallandoCompany, pallando);

    ASSERT_EQ(mainPlayer.selectDestination(mainCompany, moria), Success);
    skipToMovementHazardPhase(metaGame, mainCompany);
    CardID river = getCardID(game, 1, "River");
    configurer.drawCard(river);
    ASSERT_TRUE(useAction(metaGame, river));
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        ASSERT_NE(mainPlayer.useActionOnCard(river, thorin), Success);
        ASSERT_NE(mainPlayer.useActionOnCard(river, pallando), Success);
        ASSERT_EQ(mainPlayer.useActionOnCard(river, oin), Success);
        // Check that the site can be entered
        ASSERT_EQ(opponentPlayer.done(), Success);
        skipReturnToHandSize(metaGame);
        ASSERT_EQ(opponentPlayer.done(), Success);
        skipReturnToHandSize(metaGame);
        EXPECT_EQ(game.getCurrentSide().companies.size(), 1);
        ASSERT_TRUE(std::holds_alternative<Explore>(game.getContext()));
        EXPECT_EQ(metaGame.enterSite(mainPlayer), Success);
        metaGame.gameStates.pop_back();
    }
    ASSERT_EQ(opponentPlayer.done(), Success);
    skipReturnToHandSize(metaGame);
    ASSERT_NE(mainPlayer.useActionOnCard(river, pallando), Success);
    ASSERT_EQ(opponentPlayer.done(), Success);
    skipReturnToHandSize(metaGame);
    ASSERT_EQ(metaGame.enterSite(mainPlayer), SiteMayNotBeEntered);
    ASSERT_EQ(mainPlayer.useActionOnCard(river, pallando), Success);
    ASSERT_EQ(metaGame.enterSite(mainPlayer), Success);
}

TEST_F(StartFromLorien, RiverNoArrivingCompanies) {
    mainPlayer.transferCharacter(oin);
    CompanyID mainCompany = 0;
    CompanyID oinCompany = game.getCompanyID(oin);
    pc.setCurrentLocation(mainCompany, cavesOfUlund);
    ASSERT_EQ(mainPlayer.selectDestination(oinCompany, cavesOfUlund), Success);
    PlayerConfigurer hazardPc(configurer, opponentPlayer);
    CardID river = hazardPc.drawCard("River");
    CardID seizedByTerror = hazardPc.drawCard("Seized by Terror");
    skipToMovementHazardPhase(metaGame, oinCompany);
    ASSERT_EQ(opponentPlayer.useAction(river), Success);
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(opponentPlayer.done(), Success);
        skipReturnToHandSize(metaGame);
        ASSERT_EQ(opponentPlayer.done(), Success);
        skipReturnToHandSize(metaGame);
        EXPECT_EQ(game.getCurrentSide().companies.size(), 1);
        ASSERT_TRUE(std::holds_alternative<Explore>(game.getContext()));
        EXPECT_EQ(metaGame.enterSite(mainPlayer), SiteMayNotBeEntered);
        ASSERT_EQ(mainPlayer.useActionOnCard(river, oin), Success);
        EXPECT_EQ(metaGame.enterSite(mainPlayer), Success);
        metaGame.gameStates.pop_back();
    }
    ASSERT_EQ(opponentPlayer.useActionOnCard(seizedByTerror, oin), Success);
    ASSERT_EQ(mainPlayer.roll(2), Success);
    ASSERT_EQ(opponentPlayer.done(), Success);
    skipReturnToHandSize(metaGame);
    ASSERT_EQ(opponentPlayer.done(), Success);
    skipReturnToHandSize(metaGame);
    ASSERT_EQ(mainPlayer.chooseCompany(mainCompany), Success);
    // May enter without tapping a ranger because the company didn't move
    // to the site and because it wasn't joined by a moving company.
    EXPECT_EQ(mainPlayer.enterSite(), Success);
}

TEST_F(StartFromLorien, GiantSpiders) {
    pc.addCharacterToCompany(mainCompany, pallando);

    mainPlayer.selectDestination(mainCompany, sarnGoriwing);
    skipToMovementHazardPhase(metaGame);
    opponentPlayer.setKey(ShadowHold);
    ASSERT_EQ(opponentPlayer.useAction(giantSpiders), Success);
    ASSERT_EQ(mainPlayer.done(), Success);
    ASSERT_EQ(mainPlayer.assignStrike(thorin), Success);

    metaGame.cloneState(); {
        // Do not discard wizard characters
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(mainPlayer.assignStrike(pallando), Success);
        ASSERT_EQ(mainPlayer.done(), Success);
        ASSERT_TRUE(resolveStrike(metaGame, pallando)
            .taps()
            .verifyTargetProwess(6)
            .verifyCreatureProwess(10)
            .attackRoll(3)
            .verifyStrikeSuccess()
            .defendBodyCheck(9)
        );
        ASSERT_EQ(game.getDeck(pallando), Characters);
        ASSERT_EQ(game.getCardState(pallando), Wounded);
        metaGame.gameStates.pop_back();
    }

    ASSERT_EQ(mainPlayer.assignStrike(gloin), Success);
    ASSERT_EQ(mainPlayer.done(), Success);
    ASSERT_TRUE(resolveStrike(metaGame, thorin)
        .taps()
        .verifyTargetProwess(9)
        .verifyCreatureProwess(10)
        .attackRoll(2)
        .verifyStrikeFail()
    );
    ASSERT_TRUE(resolveStrike(metaGame, gloin)
        .taps()
        .support({ dori })
        .verifyTargetProwess(6)
        .verifyTargetBody(7)
        .verifyCreatureProwess(10)
        .attackRoll(3)
        .verifyStrikeSuccess()
    );
    metaGame.cloneState(); {
        // Discard character if body check result equals the character's body
        Game & game = metaGame.gameStates.back();
        EXPECT_EQ(game.getParent(oin), gloin);
        ASSERT_TRUE(StrikeSimulator(metaGame).defendBodyCheck(7));
        EXPECT_EQ(game.getDeck(gloin), DiscardPile);
        EXPECT_EQ(game.getParent(oin), HadParent);
        metaGame.gameStates.pop_back();
    }
    metaGame.cloneState(); {
        Game& game = metaGame.gameStates.back();
        ASSERT_TRUE(StrikeSimulator(metaGame).defendBodyCheck(6));
        EXPECT_EQ(game.getDeck(gloin), Characters);
        EXPECT_EQ(game.getCardState(gloin), Wounded);
        metaGame.gameStates.pop_back();
    }
    ASSERT_TRUE(StrikeSimulator(metaGame).defendBodyCheck(8));
    EXPECT_EQ(game.getDeck(gloin), Eliminated);
}

TEST_F(StartFromLorien, NeekerBreekers) {
    PlayerConfigurer pc(configurer, mainPlayer);
    pc.addCharacterToCompany(mainCompany, pallando);
    mainPlayer.selectDestination(mainCompany, sarnGoriwing);
    skipToMovementHazardPhase(metaGame);
    opponentPlayer.setKey(Wilderness);
    ASSERT_EQ(opponentPlayer.useAction(neekerBreekers), Success);
    ASSERT_EQ(mainPlayer.done(), Success);
    ASSERT_TRUE(std::holds_alternative<AssignStrikes>(game.getContext()));
    AssignStrikes & strikes = std::get<AssignStrikes>(game.getContext());
    EXPECT_EQ(strikes.unresolvedStrikes.size(), 5);

    ASSERT_TRUE(resolveStrike(metaGame, thorin)
        .staysInState(Untapped)
        .verifyTargetProwess(5)
        .verifyCreatureProwess(7)
        .attackRoll(6)
        .verifyStrikeFail()
    );

    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        ASSERT_TRUE(resolveStrike(metaGame, gloin)
            .staysInState(Untapped)
            .verifyTargetProwess(2)
            .verifyCreatureProwess(7)
            .attackRoll(4)
            .verifyStrikeSuccess()
        );
        ASSERT_EQ(game.getCardState(gloin), Tapped);
        metaGame.gameStates.pop_back();
    }
    ASSERT_TRUE(resolveStrike(metaGame, gloin)
        .attackRoll(6)
        .verifyStrikeFail()
    );

    ASSERT_TRUE(resolveStrike(metaGame, dori)
        .taps()
        .verifyTargetProwess(1)
        .verifyCreatureProwess(7)
        .attackRoll(7)
        .verifyStrikeFail()
    );

    ASSERT_TRUE(resolveStrike(metaGame, oin)
        .staysInState(Untapped)
        .verifyTargetProwess(0)
        .verifyCreatureProwess(7)
        .attackRoll(8)
        .verifyStrikeFail()
    );

    metaGame.cloneState(); {
        ASSERT_TRUE(resolveStrike(metaGame, kili)
            .taps()
            .verifyTargetProwess(3)
            .verifyCreatureProwess(7)
            .attackRoll(6)
            .verifyStrikeFail()
        );
        ASSERT_EQ(metaGame.gameStates.back().getDeck(neekerBreekers), Kills);
        metaGame.gameStates.pop_back();
    }

    ASSERT_TRUE(resolveStrike(metaGame, kili)
        .taps()
        .verifyTargetProwess(3)
        .verifyCreatureProwess(7)
        .attackRoll(3)
        .verifyStrikeSuccess()
    );
    ASSERT_EQ(game.getCardState(kili), Tapped);
    ASSERT_EQ(game.getDeck(neekerBreekers), DiscardPile);
}

TEST_F(StartFromLorien, AFriendOrThree) {
    // For every character in the influencing character's company, A Friend or Three
    // gives a +1 modification to an influence check *or* to a corruption check made by a
    // character in the same company.

    CardID aFriendOrThree = getCardID(game, mainPlayer, "A Friend or Three");
    CardID blueMountainDwarves = getCardID(game, mainPlayer, "Blue Mountain Dwarves");
    configurer.drawCard(aFriendOrThree);
    configurer.drawCard(blueMountainDwarves);

    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        metaGame.store(mainPlayer, durinsAxe);
        ASSERT_TRUE(std::holds_alternative<CorruptionCheck>(game.getContext()));
        auto & corruptionCheck = std::get<CorruptionCheck>(game.getContext());
        EXPECT_EQ(corruptionCheck.base, 4);
        EXPECT_EQ(corruptionCheck.modifier, 0);

        ASSERT_EQ(mainPlayer.useAction(aFriendOrThree), Success);

        EXPECT_EQ(corruptionCheck.base, 4);
        EXPECT_EQ(corruptionCheck.modifier, 5);
        metaGame.gameStates.pop_back();
    }

    PlayerConfigurer pc = configurer.currentPlayerConfiguerer();
    pc.setCurrentLocation(mainCompany, blueMountainDh);

    game.turnSequence.setPhase(SitePhase, Beginning);
    mainPlayer.done();

    ASSERT_TRUE(std::holds_alternative<Explore>(game.getContext()));
    Explore & explore = std::get<Explore>(game.getContext());
    ASSERT_EQ(game.getCardState(explore.site), Untapped);
    explore.state = Explore::ExploreSite;
    mainPlayer.selectCard(thorin);

    ASSERT_TRUE(useAction(metaGame, blueMountainDwarves));

    ASSERT_TRUE(std::holds_alternative<InfluenceAttempt>(game.getContext()));
    auto & influence = std::get<InfluenceAttempt>(game.getContext());
    EXPECT_EQ(game.getCardState(thorin), Tapped);
    EXPECT_EQ(influence.influencingCharacter, thorin);
    EXPECT_EQ(influence.target, blueMountainDwarves);
    EXPECT_EQ(influence.requiredInfluence, 9);
    EXPECT_EQ(influence.modifier, 4);
    EXPECT_FALSE(influence.influenceOpponent);

    ASSERT_TRUE(useAction(metaGame, aFriendOrThree));

    EXPECT_EQ(influence.modifier, 9);
}

TEST_F(StartFromLorien, BountyOfTheHoard) {
    // *Playable during the site phase.* One minor or major item may be played at a tapped
    // site that contains a hoard.

    CardID bountyOfTheHoard = pc.drawCard("Bounty of the Hoard");
    CardID theLonelyMountain = pc.drawCard("The Lonely Mountain");
    CardID enrunedShield = pc.drawCard("Enruned Shield");
    CardID wormsbane = pc.drawCard("Wormsbane");
    CardID bowOfDragonHorn = pc.drawCard("Bow of Dragon-horn");

    pc.setCurrentLocation(mainCompany, theLonelyMountain);

    skipToSitePhase(metaGame);
    skipAutomaticAttack(metaGame);

    ASSERT_NE(mainPlayer.useAction(bountyOfTheHoard), Success);

    ASSERT_EQ(mainPlayer.useActionOnCard(enrunedShield, gloin), Success);
    ASSERT_NE(mainPlayer.useActionOnCard(bowOfDragonHorn, oin), Success);

    ASSERT_EQ(mainPlayer.useAction(bountyOfTheHoard), Success);
    ASSERT_TRUE(std::holds_alternative<Explore>(game.getContext()));
    Explore & explore = std::get<Explore>(game.getContext());
    ASSERT_TRUE(explore.isPlayable(Minor));
    ASSERT_TRUE(explore.isPlayable(Major));

    // Try ending the turn without playing another item
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        mainPlayer.done();
        ASSERT_TRUE(game.turnSequence.phase == EndOfTurnPhase);
        ASSERT_FALSE(game.errors.empty());
        ASSERT_EQ(game.errors.back(), bountyOfTheHoard);
        metaGame.gameStates.pop_back();
    }

    ASSERT_EQ(mainPlayer.useActionOnCard(bowOfDragonHorn, oin), Success);
}

/*
TEST_F(StartFromLorien, GatesOfMorning) {
    // Environment. When Gates of Morning is played, all environment hazard cards in
    // play are immediately discarded, and all hazard environment effect are canceled.
    //   Cannot be duplicated.
}
*/

TEST_F(StartFromLorien, HundredsOfButterflies) {
    // *Playable on a moving character during his movement/hazard phase.* Untap the
    // character and increase the hazard limit against his company by one.

    CardID hundredsOfButterflies = pc.drawCard("Hundreds of Butterflies");

    configurer.setCardState(thorin, Tapped);

    ASSERT_NE(mainPlayer.useActionOnCard(hundredsOfButterflies, thorin), Success);

    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        skipToMovementHazardPhase(metaGame);
        ASSERT_NE(mainPlayer.useActionOnCard(hundredsOfButterflies, thorin), Success);
        metaGame.gameStates.pop_back();
    }

    mainPlayer.selectDestination(mainCompany, moria);
    skipToMovementHazardPhase(metaGame);

    auto & context = game.getContext();
    ASSERT_TRUE(std::holds_alternative<Movement>(context));
    auto & movement = std::get<Movement>(context);
    ASSERT_EQ(movement.hazardLimit, 5);

    ASSERT_NE(mainPlayer.useActionOnCard(hundredsOfButterflies, gloin), Success);
    ASSERT_EQ(mainPlayer.useActionOnCard(hundredsOfButterflies, thorin), Success);
    ASSERT_EQ(movement.hazardLimit, 6);
}

TEST_F(StartFromLorien, LuckySearch) {
    // *Scout only.* During the site phase, tap a scout at a tapped or untapped Shadow-hold or Dark-hold. Turn over
    // cards from your play deck one at a time unil: you reveal a non-special item (it cannot be a unique item already in play)
    // *or* reach the end (does not exhaust the play deck). If you reveal such an item, the scout takes control of it.
    // In any case, the scout faces a single strike attack with prowess equal to 3 plus the number of cards revealed.
    // This attack/strike cannot be canceled. Discard the item if the scout is wounded. Reshuffle all revealed cards except the
    // item back into the play deck.

    CompanyID opponentCompany = 0;

    CardID opponentBoromir = getCardID(game, opponentPlayer, "Boromir II");
    CardID opponentBowOfDragonHorn = opc.addStartingItem("Bow of Dragon-horn", opponentBoromir);
    configurer.addPossession(opponentBoromir, opponentBowOfDragonHorn); // Give opponent a unique item

    CardID luckySearch = pc.drawCard("Lucky Search");

    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        pc.setCurrentLocation(mainCompany, moria);
        ASSERT_NE(mainPlayer.useActionOnCard(luckySearch, thorin), Success); // Wrong phase
        pc.setCurrentLocation(mainCompany, cavesOfUlund);
        skipToSitePhase(metaGame);
        skipAutomaticAttack(metaGame);
        ASSERT_NE(mainPlayer.useActionOnCard(luckySearch, thorin), Success); // Wrong site type
        metaGame.gameStates.pop_back();
    }

    pc.setCurrentLocation(mainCompany, goblinGate);

    skipToSitePhase(metaGame);
    skipAutomaticAttack(metaGame);

    CardID enrunedShield = pc.putOnTopOfPlayDeck("Enruned Shield");
    CardID giantSpiders = pc.putOnTopOfPlayDeck("Giant Spiders");
    CardID bowOfDragonHorn = pc.putOnTopOfPlayDeck("Bow of Dragon-horn");
    CardID pallando = pc.putOnTopOfPlayDeck("Pallando");
    CardID bookOfMazarbul = pc.putOnTopOfPlayDeck("Book of Mazarbul");
    CardID gollum = pc.putOnTopOfPlayDeck("Gollum");
    CardID fellowship = pc.putOnTopOfPlayDeck("Fellowship");
    CardID twilight = pc.putOnTopOfPlayDeck("Twilight");

    configurer.setCardState(thorin, Tapped);
    ASSERT_NE(mainPlayer.useActionOnCard(luckySearch, thorin), Success); // scout must be untapped
    configurer.setCardState(thorin, Untapped);


    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        auto pc = PlayerConfigurer(configurer, mainPlayer);

        CardID wormsbane = pc.putOnTopOfPlayDeck("Wormsbane");
        ASSERT_EQ(game.getParent(wormsbane), NoParent);
        ASSERT_EQ(mainPlayer.useActionOnCard(luckySearch, kili), Success);
        ASSERT_EQ(game.getParent(wormsbane), kili);
        ASSERT_EQ(game.getCardState(kili), Tapped);

        // The strike fails, and Kili keeps the card
        ASSERT_TRUE(resolveStrike(metaGame, kili)
            .staysInState(Tapped)
            .verifyTargetProwess(4)
            .verifyCreatureProwess(4)
            .attackRoll(7)
            .verifyStrikeFail()
        );
        ASSERT_EQ(game.getParent(wormsbane), kili);
        metaGame.gameStates.pop_back();
    }

    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();

        configurer.setCardState(goblinGate, Tapped); // May be played on tapped sites
        ASSERT_EQ(mainPlayer.useActionOnCard(luckySearch, kili), Success);
        ASSERT_EQ(game.getParent(enrunedShield), kili);
        ASSERT_EQ(game.getCardState(kili), Tapped);

        // The strike succeeds, and Kili loses the card
        ASSERT_TRUE(resolveStrike(metaGame, kili)
            .staysInState(Tapped)
            .verifyTargetProwess(2)
            .verifyCreatureProwess(11)
            .verifyTargetBody(10)
            .attackRoll(4)
            .verifyStrikeSuccess()
            .verifyTargetBody(8)
        );

        ASSERT_EQ(game.getParent(enrunedShield), NoParent);

        metaGame.gameStates.pop_back();
    }

    metaGame.cloneState(); {
        // May not be played on scout allies
        Game & game = metaGame.gameStates.back();
        configurer.addPossession(oin, gollum);
        ASSERT_EQ(game.getParent(gollum), oin);
        ASSERT_NE(mainPlayer.useActionOnCard(luckySearch, gollum), Success);
        metaGame.gameStates.pop_back();
    }

    // Add Kili to Boromir's company, and test end of deck test
    CardID opponentLuckySearch = opc.addCardToHand("Lucky Search");
    CardID opponentKili = opc.addCharacterToCompany("Kíli");
    CardID opponentMoria = opc.addCardToDeck("Moria", LocationDeck);
    moveCardFast(game, ghosts, DiscardPile, 1);
    ASSERT_NE(mainPlayer.useActionOnCard(luckySearch, opponentKili), Success);
    ASSERT_NE(opponentPlayer.useActionOnCard(opponentLuckySearch, opponentKili), Success); // Wrong turn
    skipTurn(metaGame);
    opc.setCurrentLocation(opponentCompany, opponentMoria);
    skipToSitePhase(metaGame);
    skipAutomaticAttack(metaGame);
    ASSERT_EQ(game.getCardState(opponentBoromir), Untapped);
    ASSERT_NE(opponentPlayer.useActionOnCard(opponentLuckySearch, opponentBoromir), Success); // Not a scout

    ASSERT_EQ(game.sides[opponentPlayer].playDeck.size(), 31);
    ASSERT_EQ(opponentPlayer.useActionOnCard(opponentLuckySearch, opponentKili), Success);
    ASSERT_EQ(game.sides[opponentPlayer].playDeck.size(), 31);
    ASSERT_TRUE(resolveStrike(metaGame, opponentKili)
        .verifyCreatureProwess(34)
    );
}

template <class Container>
bool contains(const Container & container, typename Container::value_type const & value) {
    return std::find(container.begin(), container.end(), value) != container.end();
}

TEST_F(StartFromLorien, MarvelsTold) {
    // Ritual. Tap a sage to force the discard of a hazard non-environment permanent-event
    // or long-event. Sage makes a corruption check modified by -2.

    CompanyID opponentCompany = 0;
    CardID fellowship = pc.drawCard("Fellowship");
    CardID marvelsTold = pc.drawCard("Marvels Told");
    CardID wakeOfWar = opc.drawCard("Wake of War");

    ASSERT_EQ(mainPlayer.useActionOnCompany(balin, mainCompany), Success);

    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(mainPlayer.useActionOnCompany(fellowship, mainCompany), Success);
        ASSERT_EQ(mainPlayer.selectDestination(mainCompany, moria), Success);
        skipToMovementHazardPhase(metaGame);
        ASSERT_EQ(mainPlayer.useActionOnCards(marvelsTold, { balin, fellowship }), ActionNotAvailable);
        ASSERT_EQ(opponentPlayer.useAction(fullOfFrothAndRage), Success);
        ASSERT_TRUE(contains(game.sides[opponentPlayer].permanentEvents, fullOfFrothAndRage));

        ASSERT_EQ(mainPlayer.useActionOnCards(marvelsTold, { balin, fullOfFrothAndRage }), Success);
        ASSERT_EQ(game.getCardState(balin), Tapped);
        ASSERT_FALSE(contains(game.sides[opponentPlayer].permanentEvents, fullOfFrothAndRage));
        auto & context = game.getContext();
        ASSERT_TRUE(std::holds_alternative<CorruptionCheck>(context));
        auto & check = std::get<CorruptionCheck>(context);
        EXPECT_EQ(check.targetCharacter, balin);
        EXPECT_EQ(check.modifier, -1); // Would be -2 without fellowship
        metaGame.gameStates.pop_back();
    }

    ASSERT_EQ(mainPlayer.selectDestination(mainCompany, moria), Success);
    skipToMovementHazardPhase(metaGame);

    ASSERT_EQ(opponentPlayer.useAction(wakeOfWar), Success);
    ASSERT_TRUE(contains(game.sides[opponentPlayer].longEvents, wakeOfWar));

    ASSERT_EQ(mainPlayer.useActionOnCards(marvelsTold, { balin, wakeOfWar }), Success);
    ASSERT_EQ(game.getCardState(balin), Tapped);
    ASSERT_FALSE(contains(game.sides[opponentPlayer].permanentEvents, wakeOfWar));
    auto & context = game.getContext();
    ASSERT_TRUE(std::holds_alternative<CorruptionCheck>(context));
    auto & check = std::get<CorruptionCheck>(context);
    EXPECT_EQ(check.targetCharacter, balin);
    EXPECT_EQ(check.modifier, -2);
}

TEST_F(StartFromLorien, NotAtHome) {
    // Cancels one Dragon, Drake, or Troll attack. This attack must be either an automatic-attack
    // or keyed to a site.
    //   Alternatively, if Gates of Morning is in play, reduce the number of strikes of any
    // automatic-attack by 2 (to a minimum of 1).

    CardID notAtHome = pc.drawCard("Not at Home");
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(mainPlayer.selectDestination(mainCompany, cavesOfUlund), Success);
        skipToMovementHazardPhase(metaGame);
        metaGame.cloneState(); {
            // Cannot cancel non-automatic drake attacks keyed to regions
            Game & game = metaGame.gameStates.back();
            ASSERT_EQ(opponentPlayer.setKey(WitheredHeath), Success);
            ASSERT_EQ(opponentPlayer.useAction(caveWorm), Success);
            ASSERT_NE(mainPlayer.useActionOnCard(notAtHome, caveWorm), Success);
            metaGame.gameStates.pop_back();
        }

        // Cancel automatic attack
        opponentPlayer.done();
        mainPlayer.discard({ concealment, gollum });
        opponentPlayer.discard({ ghosts });
        mainPlayer.enterSite();
        ASSERT_EQ(mainPlayer.useAction(notAtHome), Success);
        auto & context = game.getContext();
        ASSERT_TRUE(std::holds_alternative<Explore>(context));
        EXPECT_EQ(std::get<Explore>(context).state, Explore::ExploreSite);
        metaGame.gameStates.pop_back();
    }

    // Reduce number of strikes
    CardID gatesOfMorning = pc.drawCard("Gates of Morning");
    pc.setCurrentLocation(mainCompany, sarnGoriwing);
    skipToSitePhase(metaGame);
    mainPlayer.enterSite();
    auto & context = game.getContext();
    ASSERT_TRUE(std::holds_alternative<Attack>(context));
    Attack & attack = std::get<Attack>(context);
    EXPECT_EQ(attack.strikes, 3);
    ASSERT_NE(mainPlayer.useAction(notAtHome), Success);
    ASSERT_EQ(mainPlayer.useAction(gatesOfMorning), Success);
    ASSERT_EQ(mainPlayer.useAction(notAtHome), Success);
    EXPECT_EQ(attack.strikes, 1);
}

// TEST_F(StartFromLorien, PromptingsOfWisdom) {
//     // Light Enchantment. *Playable during the organization phase on a ranger.* Target
//     // ranger may tap to cancel all hazard effects for the rest of the turn that: force his
//     // company to return to its site of origin *or* that tap his company's current or new site. If so
//     // tapped, target ranger makes a corruption check. Cannot be duplicated in a given company.
// }

TEST_F(StartFromLorien, RiskyBlow) {
    // *Warrior only against one strike.* +3 to prowess and -1 to body.

    CardID riskyBlow = pc.addCardToHand("Risky Blow");
    configurer.addPossession(thorin, gollum);

    ASSERT_EQ(mainPlayer.selectDestination(mainCompany, moria), Success);

    skipToMovementHazardPhase(metaGame);

    ASSERT_EQ(opponentPlayer.setKey(Wilderness), Success);
    ASSERT_EQ(opponentPlayer.useAction(giantSpiders), Success);
    ASSERT_EQ(mainPlayer.done(), Success);
    ASSERT_EQ(mainPlayer.assignStrike(thorin), Success);
    ASSERT_EQ(mainPlayer.assignStrike(gollum), Success);
    ASSERT_EQ(mainPlayer.done(), Success);

    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();

        StrikeSimulator thorinStrike = resolveStrike(metaGame, thorin);
        ASSERT_TRUE(thorinStrike.taps());
        ASSERT_EQ(mainPlayer.useAction(riskyBlow), Success);

        metaGame.gameStates.pop_back();
    }

    StrikeSimulator gollumStrike = resolveStrike(metaGame, gollum);
    ASSERT_TRUE(gollumStrike.taps());
    ASSERT_EQ(mainPlayer.useAction(riskyBlow), ActionNotAvailable);
}

TEST_F(StartFromLorien, TheDwarvesAreUponYou) {
    // *Playable on a company containing Dwarves facing an attack.* All Dwarves in the
    // company receive +2 prowess and -1 body against the attack. Cannot be duplicated
    // against a given attack.

    configurer.addPossession(thorin, gollum);
    CardID gildor = getCardID(game, mainPlayer, "Gildor Inglorion");
    pc.addCharacterToCompany(mainCompany, gildor);

    ASSERT_EQ(mainPlayer.selectDestination(mainCompany, moria), Success);

    skipToMovementHazardPhase(metaGame);
    CardID lesserSpiders = opc.drawCard("Lesser Spiders");

    ASSERT_EQ(opponentPlayer.setKey(Wilderness), Success);
    ASSERT_EQ(opponentPlayer.useAction(lesserSpiders), Success);

    metaGame.cloneState(); {
        // Test without "The Dwarves Are upon You!"
        ASSERT_EQ(mainPlayer.done(), Success);
        ASSERT_EQ(mainPlayer.useAction(theDwarvesAreUponYou), ActionNotAvailable);

        ASSERT_EQ(mainPlayer.assignStrike(thorin), Success);
        ASSERT_EQ(mainPlayer.assignStrike(gollum), Success);
        ASSERT_EQ(mainPlayer.assignStrike(gloin), Success);
        ASSERT_EQ(mainPlayer.assignStrike(gildor), Success);
        ASSERT_EQ(mainPlayer.done(), Success);

        ASSERT_EQ(mainPlayer.useAction(theDwarvesAreUponYou), ActionNotAvailable);

        metaGame.cloneState(); {
            // Try and fail to use "The Dwarves Are upon You!" as a strike modifier
            Game & game = metaGame.gameStates.back();

            StrikeSimulator thorinStrike = resolveStrike(metaGame, thorin);
            ASSERT_TRUE(thorinStrike.taps());
            ASSERT_EQ(mainPlayer.useAction(theDwarvesAreUponYou), ActionNotAvailable);
            ASSERT_TRUE(thorinStrike.verifyTargetProwess(9));

            metaGame.gameStates.pop_back();
        }

        Game & game = metaGame.gameStates.back();
        ASSERT_TRUE(resolveStrike(metaGame, thorin)
            .taps()
            .verifyTargetProwess(9)
            .attackRoll(12)
            .verifyStrikeFail()
        );
        ASSERT_EQ(mainPlayer.useAction(theDwarvesAreUponYou), ActionNotAvailable);
        ASSERT_TRUE(resolveStrike(metaGame, gollum)
            .taps()
            .verifyTargetProwess(2)
            .attackRoll(12)
            .verifyStrikeFail()
        );
        ASSERT_TRUE(resolveStrike(metaGame, gloin)
            .taps()
            .verifyTargetProwess(5)
            .attackRoll(12)
            .verifyStrikeFail()
        );
        ASSERT_TRUE(resolveStrike(metaGame, gildor)
            .taps()
            .verifyTargetProwess(5)
            .attackRoll(12)
            .verifyStrikeFail()
        );
        metaGame.gameStates.pop_back();
    }

    ASSERT_EQ(mainPlayer.useAction(theDwarvesAreUponYou), Success);
    ASSERT_EQ(mainPlayer.done(), Success);

    ASSERT_EQ(mainPlayer.assignStrike(thorin), Success);
    ASSERT_EQ(mainPlayer.assignStrike(gollum), Success);
    ASSERT_EQ(mainPlayer.assignStrike(gloin), Success);
    ASSERT_EQ(mainPlayer.assignStrike(gildor), Success);
    ASSERT_EQ(mainPlayer.done(), Success);

    ASSERT_TRUE(resolveStrike(metaGame, thorin)
        .taps()
        .verifyTargetProwess(11)
        .attackRoll(12)
        .verifyStrikeFail()
    );
    ASSERT_TRUE(resolveStrike(metaGame, gollum)
        .taps()
        .verifyTargetProwess(2)
        .attackRoll(12)
        .verifyStrikeFail()
    );
    ASSERT_TRUE(resolveStrike(metaGame, gloin)
        .taps()
        .verifyTargetProwess(7)
        .attackRoll(12)
        .verifyStrikeFail()
    );
    ASSERT_TRUE(resolveStrike(metaGame, gildor)
        .taps()
        .verifyTargetProwess(5)
        .attackRoll(12)
        .verifyStrikeFail()
    );
}

TEST_F(StartFromLorien, Gollum) {
    // *Unique. Playable at Goblin-gate or Moria.* If his company's size is two or less, tap
    // Gollum to cancel one attack against his company keyed to Wilderness or
    // Shadow-lands. [Ignore the part concerning The One Ring]

    // Playability
    EXPECT_FALSE(isPlayable(metaGame, gollum, lorien, thorin));
    EXPECT_TRUE(isPlayable(metaGame, gollum, moria, thorin));
    EXPECT_FALSE(isPlayable(metaGame, gollum, cavesOfUlund, thorin));
    EXPECT_TRUE(isPlayable(metaGame, gollum, goblinGate, thorin));
    configurer.addPossession(thorin, gollum);
    ASSERT_EQ(mainPlayer.selectDestination(mainCompany, cavesOfUlund), Success);

    // Cannot cancel if company size is greater than two
    metaGame.cloneState(); {
        // Cannot cancel non-automatic drake attacks keyed to regions
        Game & game = metaGame.gameStates.back();
        skipToMovementHazardPhase(metaGame);
        ASSERT_EQ(opponentPlayer.setKey(Wilderness), Success);
        ASSERT_EQ(opponentPlayer.useAction(neekerBreekers), Success);
        ASSERT_NE(mainPlayer.useAction(gollum), Success);
        metaGame.gameStates.pop_back();
    }

    // Cancel attack in company with two characters
    ASSERT_EQ(mainPlayer.transferCharacter(gloin), Success);
    int secondaryCompany = game.getCurrentSide().companies.back().id;
    ASSERT_EQ(mainPlayer.transferCharacter(dori, secondaryCompany), Success);
    ASSERT_EQ(game.getCurrentSide().findCompany(mainCompany)->characters.size(), 2);
    skipToMovementHazardPhase(metaGame);

    metaGame.cloneState(); {
        // Cancel attack keyed to wilderness
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(opponentPlayer.setKey(Wilderness), Success);
        ASSERT_EQ(opponentPlayer.useAction(giantSpiders), Success);
        configurer.setCardState(gollum, Tapped);
        ASSERT_NE(mainPlayer.useAction(gollum), Success);
        configurer.setCardState(gollum, Untapped);
        ASSERT_EQ(mainPlayer.useAction(gollum), Success);
        EXPECT_TRUE(std::holds_alternative<Movement>(game.getContext()));
        metaGame.gameStates.pop_back();
    }

    metaGame.cloneState(); {
        // Cancel attack keyed to shadow-lands
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(opponentPlayer.setKey(ShadowLands), Success);
        ASSERT_EQ(opponentPlayer.useAction(neekerBreekers), Success);
        ASSERT_EQ(mainPlayer.useAction(gollum), Success);
        EXPECT_TRUE(std::holds_alternative<Movement>(game.getContext()));
        metaGame.gameStates.pop_back();
    }

    // Cannot cancel attacks keyed to border-lands
    ASSERT_EQ(opponentPlayer.setKey(BorderLands), Success);
    ASSERT_EQ(opponentPlayer.useAction(neekerBreekers), Success);
    ASSERT_NE(mainPlayer.useAction(gollum), Success);
    EXPECT_FALSE(std::holds_alternative<Movement>(game.getContext()));
}

 TEST_F(StartFromLorien, ThrorsMap) {
     // Unique. Discard Thrór's Map to untap a site with a Dragon automatic-attack.

     CardID theLonelyMountain = pc.drawCard("The Lonely Mountain");
     metaGame.cloneState(); {
         Game & game = metaGame.gameStates.back();
         pc.setCurrentLocation(mainCompany, theLonelyMountain);
         ASSERT_NE(mainPlayer.useAction(throrsMap), Success);
         configurer.setCardState(theLonelyMountain, Tapped);

         metaGame.cloneState(); {
             // Untap dragon's lair during organisation phase
             Game & game = metaGame.gameStates.back();
             ASSERT_EQ(mainPlayer.useAction(throrsMap), Success);
             ASSERT_EQ(game.getCardState(theLonelyMountain), Untapped);
             metaGame.gameStates.pop_back();
         }

         // Untap dragon's lair during site phase
         skipToSitePhase(metaGame);
         mainPlayer.enterSite();
         ASSERT_EQ(mainPlayer.done(), Success);
         ASSERT_EQ(mainPlayer.assignStrike(thorin), Success);
         ASSERT_EQ(mainPlayer.done(), Success);
         ASSERT_TRUE(resolveStrike(metaGame, thorin)
             .taps()
             .verifyTargetProwess(9)
             .verifyCreatureProwess(14)
             .attackRoll(7)
             .verifyStrikeFail()
         );
         ASSERT_EQ(mainPlayer.useAction(throrsMap), Success);
         ASSERT_EQ(game.getCardState(theLonelyMountain), Untapped);
         metaGame.gameStates.pop_back();
     }

     // Can't untap Moria
     pc.setCurrentLocation(mainCompany, moria);
     ASSERT_NE(mainPlayer.useAction(throrsMap), Success);
     configurer.setCardState(moria, Tapped);
     ASSERT_NE(mainPlayer.useAction(throrsMap), Success);
     ASSERT_EQ(game.getCardState(moria), Tapped);
 }

TEST_F(StartFromLorien, BowOfDragonHorn) {
    // *Unique.* Hoard item. Warrior only: tap bow to reduce the number of strikes from one
    // hazard creature attack not keyed to a site by one (to a minimum of one).

    CardID bowOfDragonHorn = pc.drawCard("Bow of Dragon-horn");
    configurer.addPossession(gloin, bowOfDragonHorn);

    ASSERT_EQ(mainPlayer.selectDestination(mainCompany, moria), Success);

    skipToMovementHazardPhase(metaGame);

    ASSERT_EQ(opponentPlayer.setKey(Wilderness), Success);
    ASSERT_EQ(opponentPlayer.useAction(giantSpiders), Success);

    metaGame.cloneState(); {
        // Don't use "Bow of Dragon-horn", and try to use it too late
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(mainPlayer.done(), Success);

        ASSERT_TRUE(std::holds_alternative<AssignStrikes>(game.getContext()));
        AssignStrikes & assignStrikes = std::get<AssignStrikes>(game.getContext());
        ASSERT_EQ(assignStrikes.unassignedStrikes, 2);

        ASSERT_EQ(mainPlayer.useAction(bowOfDragonHorn), ActionNotAvailable);
        metaGame.gameStates.pop_back();
    }

    ASSERT_EQ(mainPlayer.useAction(bowOfDragonHorn), Success);
    EXPECT_EQ(game.getCardState(bowOfDragonHorn), Tapped);
    ASSERT_EQ(mainPlayer.useAction(bowOfDragonHorn), ActionNotAvailable);
    ASSERT_EQ(mainPlayer.done(), Success);

    ASSERT_TRUE(std::holds_alternative<AssignStrikes>(game.getContext()));
    AssignStrikes & assignStrikes = std::get<AssignStrikes>(game.getContext());
    ASSERT_EQ(assignStrikes.unassignedStrikes, 1);
}

TEST_F(StartFromLorien, EnrunedShield) {
    // *Unique.* Hoard item. Shield. +3 to body to a maximum of 10. Warrior only: tap
    // Enruned Shield to cause one strike against bearer to be ineffectual (i.e, it doesn't fail
    // and it is not successful.

    CardID enrunedShield = pc.drawCard("Enruned Shield");
    configurer.addPossession(gloin, enrunedShield);

    ASSERT_EQ(mainPlayer.selectDestination(mainCompany, moria), Success);

    skipToMovementHazardPhase(metaGame);

    ASSERT_EQ(opponentPlayer.setKey(Wilderness), Success);
    ASSERT_EQ(opponentPlayer.useAction(giantSpiders), Success);
    ASSERT_EQ(mainPlayer.done(), Success);

    ASSERT_EQ(mainPlayer.assignStrike(thorin), Success);
    ASSERT_EQ(mainPlayer.assignStrike(gloin), Success);
    ASSERT_EQ(mainPlayer.done(), Success);

    StrikeSimulator gloinStrike = resolveStrike(metaGame, gloin);
    ASSERT_EQ(mainPlayer.useAction(enrunedShield), Success);
    ASSERT_TRUE(gloinStrike.verifyStrikeIneffectual());
}

TEST_F(StartFromLorien, DurinsAxe) {
    // Unique. Weapon. +2 prowess (+4 if held by a Dwarf) to a maximum of 9.
    // If held by a Dwarf, 4 marshalling points and 3 corruption points.
    // MP: 2(4), Prowess: +2(+4), Corruption: 2(3)

    Scene scene = getCurrentScene(game);
    pc.addCharacterToCompany(mainCompany, pallando);

    // MP Thorin
    int beforeTransferMP = scene.getTotalMP(mainPlayer).sum();
    EXPECT_EQ(scene.getMP(durinsAxe, mainPlayer), 4);

    metaGame.cloneState(); {
        // Prowess Thorin
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(mainPlayer.selectDestination(mainCompany, moria), Success);

        skipToMovementHazardPhase(metaGame);

        ASSERT_EQ(opponentPlayer.setKey(RedhornGate), Success);
        ASSERT_EQ(opponentPlayer.useAction(caveWorm), Success);
        ASSERT_EQ(mainPlayer.done(), Success);

        ASSERT_EQ(mainPlayer.assignStrike(thorin), Success);
        ASSERT_EQ(mainPlayer.done(), Success);

        ASSERT_TRUE(resolveStrike(metaGame, thorin)
            .taps()
            .verifyTargetProwess(9)
        );

        metaGame.gameStates.pop_back();
    }

    {
        // Corruption Thorin
        ASSERT_EQ(metaGame.transferItem(mainPlayer, durinsAxe, pallando), Success);
        ASSERT_TRUE(std::holds_alternative<CorruptionCheck>(game.getContext()));
        CorruptionCheck & corruptionCheck = std::get<CorruptionCheck>(game.getContext());
        EXPECT_EQ(corruptionCheck.base, 4); // Cram + Durin's Axe
        ASSERT_EQ(mainPlayer.done(), Success);
        ASSERT_EQ(mainPlayer.roll(12), Success);

        // MP Pallando
        EXPECT_EQ(scene.getMP(durinsAxe, mainPlayer), 2);
        EXPECT_EQ(scene.getTotalMP(mainPlayer).sum(), beforeTransferMP - 2);
    }

    metaGame.cloneState(); {
        // Prowess Pallando
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(mainPlayer.selectDestination(mainCompany, moria), Success);

        skipToMovementHazardPhase(metaGame);

        ASSERT_EQ(opponentPlayer.setKey(RedhornGate), Success);
        ASSERT_EQ(opponentPlayer.useAction(caveWorm), Success);
        ASSERT_EQ(mainPlayer.done(), Success);

        ASSERT_EQ(mainPlayer.assignStrike(pallando), Success);
        ASSERT_EQ(mainPlayer.done(), Success);

        ASSERT_TRUE(resolveStrike(metaGame, pallando)
            .taps()
            .verifyTargetProwess(8)
        );

        metaGame.gameStates.pop_back();
    }

    {
        // Corruption Pallando
        ASSERT_EQ(metaGame.transferItem(mainPlayer, durinsAxe, dori), Success);
        ASSERT_TRUE(std::holds_alternative<CorruptionCheck>(game.getContext()));
        CorruptionCheck & corruptionCheck = std::get<CorruptionCheck>(game.getContext());
        EXPECT_EQ(corruptionCheck.base, 2);
        ASSERT_EQ(mainPlayer.done(), Success);
        ASSERT_EQ(mainPlayer.roll(12), Success);
    }
}

TEST_F(StartFromLorien, Wormsbane) {
    // *Unique.* Weapon. +2 to prowess to a maximum of 9 (+4 to prowess to a
    // maximum of 12 and -2 to strike's body against a Dragon or Drake strike).

    CardID wormsbane = pc.drawCard("Wormsbane");
    configurer.addPossession(gloin, wormsbane);

    ASSERT_EQ(mainPlayer.selectDestination(mainCompany, cavesOfUlund), Success);

    skipToMovementHazardPhase(metaGame);

    metaGame.cloneState(); {
        // Prowess non-dragon attack
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(opponentPlayer.setKey(Wilderness), Success);
        ASSERT_EQ(opponentPlayer.useAction(giantSpiders), Success);
        ASSERT_EQ(mainPlayer.done(), Success);

        ASSERT_EQ(mainPlayer.assignStrike(thorin), Success);
        ASSERT_EQ(mainPlayer.assignStrike(gloin), Success);
        ASSERT_EQ(mainPlayer.done(), Success);

        resolveStrike(metaGame, gloin)
            .taps()
            .verifyTargetProwess(7);

        metaGame.gameStates.pop_back();
    }

    skipToSitePhase(metaGame);
    ASSERT_EQ(mainPlayer.enterSite(), Success);
    mainPlayer.done();

    ASSERT_EQ(mainPlayer.assignStrike(gloin), Success);
    ASSERT_EQ(mainPlayer.done(), Success);

    resolveStrike(metaGame, gloin)
        .taps()
        .verifyTargetProwess(9);
}

TEST_F(OrcVsDwarf, CovetousThougths) {
    // Corruption. *Playable only on a minion.* At the end of each of his turns, target minion makes a
    // corruption check for each item his company bears that he does not bear. For each check, modify the
    // roll by subtracting the corruption of that item.
    // During his organization phase, the minion may tap to attempt to remove this card. Opponent
    // makes a roll (draws a #) - if the result is greater than 5, discard this card. Cannot be
    // duplicated on a given minion.

    CardID hoarmurath = getCardID(game, orcPlayer, "Hoarmûrath the Ringwraith*");
    CardID covetousThoughts = dwarfConfigurer.drawCard("Covetous Thoughts");
    orcConfigurer.addCharacterToCompany(mainCompany, hoarmurath);
    skipToMovementHazardPhase(metaGame);
    ASSERT_NE(dwarfPlayer.useActionOnCard(covetousThoughts, hoarmurath), Success);
    ASSERT_EQ(dwarfPlayer.useActionOnCard(covetousThoughts, ufthak), Success);
    skipToSitePhase(metaGame);
    ASSERT_EQ(orcPlayer.done(), Success);
    ASSERT_EQ(orcPlayer.done(), Success);
    ASSERT_EQ(dwarfPlayer.done(), Success);
    EXPECT_EQ(game.turnSequence.phase, EndOfTurnPhase);
    {
        ASSERT_EQ(orcPlayer.resolveCorruptionCheck(blazonOfTheEye), Success);
        CorruptionCheck & corruptionCheck = std::get<CorruptionCheck>(game.getContext());
        EXPECT_EQ(corruptionCheck.base, 2); // Strange Rations + Covetous Thoughts
        EXPECT_EQ(corruptionCheck.modifier, -1);
        ASSERT_EQ(orcPlayer.done(), Success);
        ASSERT_EQ(orcPlayer.roll(2), Success);
        EXPECT_EQ(game.getCardState(ufthak), Tapped);
    }
    {
        Card & card = game.cards[ufthak];
        ASSERT_EQ(orcPlayer.resolveCorruptionCheck(scrollOfIsildur), Success);
        CorruptionCheck & corruptionCheck = std::get<CorruptionCheck>(game.getContext());
        EXPECT_EQ(corruptionCheck.base, 2);
        EXPECT_EQ(corruptionCheck.modifier, -4);
        ASSERT_EQ(orcPlayer.done(), Success);
        ASSERT_EQ(orcPlayer.roll(12), Success);
    }
    skipTurn(metaGame);
    ASSERT_EQ(game.turnSequence.phase, UntapPhase); // Ringwraith in play
    ASSERT_EQ(orcPlayer.done(), Success);
    ASSERT_EQ(orcPlayer.useAction(covetousThoughts), Success);
    auto context = game.contextStack.back();
    ASSERT_GT(context->parent, 0);
    auto parentContext = game.contextStack[context->parent]->context;
    ASSERT_TRUE(std::holds_alternative<RemoveCorruptionCard>(parentContext));
    RemoveCorruptionCard & remove = std::get<RemoveCorruptionCard>(parentContext);
    EXPECT_EQ(remove.requiredNumber, 5);
}

TEST_F(StartFromLorien, DespairOfTheHeart) {
    // Corruption. *Playable on a non-Hobbit, non-Wizard, non-Ringwraith character.* Target
    // character receives 2 corruption points and makes a corruption check each time a character in his
    // company becomes wounded.
    //   During his organization phase, the character may tap to attempt to remove this card. Opponent
    // makes a roll (draws a #)--if the result is greater than 4, discard this card. Cannot be
    // duplicated on a given character.

    pc.addCharacterToCompany(mainCompany, pallando);
    ASSERT_EQ(mainPlayer.selectDestination(mainCompany, moria), Success);
    skipToMovementHazardPhase(metaGame);
    ASSERT_NE(opponentPlayer.useActionOnCard(despairOfTheHeart, pallando), Success);
    ASSERT_EQ(opponentPlayer.useActionOnCard(despairOfTheHeart, thorin), Success);
    ASSERT_EQ(opponentPlayer.setKey(Wilderness), Success);
    ASSERT_EQ(opponentPlayer.useAction(giantSpiders), Success);
    ASSERT_EQ(mainPlayer.done(), Success);
    ASSERT_EQ(mainPlayer.assignStrike(thorin), Success);
    ASSERT_EQ(mainPlayer.assignStrike(gloin), Success);
    ASSERT_EQ(mainPlayer.done(), Success);

    // Gloin is discarded by Giant Spiders body check
    ASSERT_TRUE(resolveStrike(metaGame, gloin)
        .taps()
        .attackRoll(2)
        .verifyStrikeSuccess()
        .verifyTargetBody(7)
        .defendBodyCheck(7)
    );
    ASSERT_EQ(game.getDeck(gloin), DiscardPile);

    // Resolve corruption check
    ASSERT_EQ(mainPlayer.resolveCorruptionCheck(despairOfTheHeart), Success);
    ASSERT_TRUE(std::holds_alternative<CorruptionCheck>(game.getContext()));
    CorruptionCheck & corruptionCheck = std::get<CorruptionCheck>(game.getContext());
    ASSERT_EQ(corruptionCheck.base, 6); // Cram + Durin's Axe + Despair of the Heart
    ASSERT_EQ(mainPlayer.tap(dori), Success);
    ASSERT_EQ(corruptionCheck.modifier, 1);
    ASSERT_EQ(mainPlayer.done(), Success);
    ASSERT_EQ(mainPlayer.roll(6), Success);

    metaGame.cloneState(); {
        // Thorin fails
        Game & game = metaGame.gameStates.back();
        ASSERT_TRUE(resolveStrike(metaGame, thorin)
            .staysInState(Untapped)
            .attackRoll(2)
            .verifyStrikeSuccess()
            .verifyTargetBody(8)
        );
        StrikeSimulator thorinStrike(metaGame);

        metaGame.cloneState(); {
            // Thorin is wounded normally
            Game & game = metaGame.gameStates.back();
            ASSERT_TRUE(thorinStrike.defendBodyCheck(7));
            EXPECT_TRUE(std::holds_alternative<ResolveCorruptionChecks>(game.getContext()));
            metaGame.gameStates.pop_back();
        }

        // Thorin is discarded
        ASSERT_TRUE(thorinStrike.defendBodyCheck(8));
        EXPECT_TRUE(std::holds_alternative<Movement>(game.getContext()));
        metaGame.gameStates.pop_back();
    }

    // Thorin succeeds
    ASSERT_TRUE(resolveStrike(metaGame, thorin)
        .staysInState(Untapped)
        .attackRoll(7)
        .verifyStrikeFail()
    );

    metaGame.cloneState(); {
        // No corruption check when a Neeker-breekers strike succeeds
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(opponentPlayer.useAction(neekerBreekers), Success);
        ASSERT_EQ(mainPlayer.done(), Success);
        ASSERT_TRUE(resolveStrike(metaGame, kili)
            .staysInState(Untapped)
            .verifyTargetProwess(0)
            .verifyCreatureProwess(7)
            .attackRoll(5)
            .verifyStrikeSuccess()
        );
        EXPECT_TRUE(std::holds_alternative<AssignStrikes>(game.getContext()));
        metaGame.gameStates.pop_back();
    }

    // Attempt to remove Despair of the Heart
    ASSERT_NE(mainPlayer.useAction(despairOfTheHeart), Success);
    skipTurn(metaGame);
    skipTurn(metaGame);
    ASSERT_EQ(mainPlayer.done(), Success);
    ASSERT_EQ(game.turnSequence.phase, OrganizationPhase);
    ASSERT_EQ(mainPlayer.useAction(despairOfTheHeart), Success);

    metaGame.cloneState(); {
        // Attempt fails
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(mainPlayer.roll(4), Success);
        EXPECT_EQ(game.getDeck(despairOfTheHeart), Possessions);
        metaGame.gameStates.pop_back();
    }
    // Attempt succeeds
    ASSERT_EQ(mainPlayer.roll(5), Success);
    EXPECT_EQ(game.getDeck(despairOfTheHeart), DiscardPile);
}

TEST_F(StartFromLorien, FullOfFrothAndRage) {
    // All Spider and Animal attacks receive +2 prowess. Discard if a Spider or Animal
    // attack is defeated. Cannot be duplicated.

    ASSERT_EQ(mainPlayer.selectDestination(mainCompany, cavesOfUlund), Success);
    skipToMovementHazardPhase(metaGame);
    ASSERT_EQ(opponentPlayer.useAction(fullOfFrothAndRage), Success);

    metaGame.cloneState(); {
        // Animal attack
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(opponentPlayer.setKey(Wilderness), Success);
        ASSERT_EQ(opponentPlayer.useAction(neekerBreekers), Success);
        ASSERT_EQ(mainPlayer.done(), Success);
        ASSERT_TRUE(resolveStrike(metaGame, thorin)
            .staysInState(Untapped)
            .verifyCreatureProwess(9)
            .verifyTargetProwess(5)
            .attackRoll(3)
        );
        EXPECT_EQ(game.getCardState(thorin), Tapped);
        metaGame.gameStates.pop_back();
    }

    metaGame.cloneState(); {
        // Spider attack
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(opponentPlayer.setKey(Wilderness), Success);
        ASSERT_EQ(opponentPlayer.useAction(giantSpiders), Success);
        ASSERT_EQ(mainPlayer.done(), Success);
        ASSERT_EQ(mainPlayer.assignStrike(thorin), Success);
        ASSERT_EQ(mainPlayer.assignStrike(kili), Success);
        ASSERT_EQ(mainPlayer.done(), Success);
        ASSERT_TRUE(resolveStrike(metaGame, thorin).verifyCreatureProwess(12));
        metaGame.gameStates.pop_back();
    }

    metaGame.cloneState(); {
        // Drake attack (no bonus)
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(opponentPlayer.setKey(WitheredHeath), Success);
        ASSERT_EQ(opponentPlayer.useAction(caveWorm), Success);
        ASSERT_EQ(mainPlayer.done(), Success);
        ASSERT_EQ(mainPlayer.assignStrike(thorin), Success);
        ASSERT_EQ(mainPlayer.done(), Success);
        ASSERT_TRUE(resolveStrike(metaGame, thorin).verifyCreatureProwess(16));
        metaGame.gameStates.pop_back();
    }
}

TEST_F(StartFromLorien, LostInFreeDomains) {
    // *Playable on a company moving with a Free-domain in its site path.*
    // The company must do nothing during its site phase.

    CardID lostInFreeDomains = getCardID(game, opponentPlayer, "Lost in Free-domains");
    CardID blueMountainDwarves = getCardID(game, mainPlayer, "Blue Mountain Dwarves");
    CardID greyHavens = getCardID(game, mainPlayer, "Grey Havens");
    opc.drawCard(lostInFreeDomains);
    pc.drawCard(blueMountainDwarves);

    metaGame.cloneState(); {
        // No free-domains in site path
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(mainPlayer.selectDestination(mainCompany, moria), Success);
        skipToMovementHazardPhase(metaGame);
        ASSERT_NE(opponentPlayer.useAction(lostInFreeDomains), Success);
        metaGame.gameStates.pop_back();
    }

    // Move to Blue Mountain Dwarf-hold
    pc.setCurrentLocation(mainCompany, greyHavens);
    ASSERT_EQ(mainPlayer.selectDestination(mainCompany, blueMountainDh), Success);
    skipToMovementHazardPhase(metaGame);
    ASSERT_EQ(opponentPlayer.useAction(lostInFreeDomains), Success);
    ASSERT_EQ(opponentPlayer.done(), Success);
    ASSERT_EQ(mainPlayer.discard({ balin, gollum }), Success);
    ASSERT_EQ(opponentPlayer.discard({ caveWorm }), Success);
    ASSERT_EQ(mainPlayer.enterSite(), SiteMayNotBeEntered);
    ASSERT_NE(mainPlayer.useActionOnCard(blueMountainDwarves, thorin), Success);
}

TEST_F(StartFromLorien, SeizedByTerror) {
    // *Playable on a non-Wizard, non-Ringwraith character moving in a Shadow-land or Dark-domain.*
    // Target character makes a roll (draws a #) and adds his mind. If the result is less
    // than 12, that character splits off into a different company. This new company immediately returns
    // to his original company's site of origin.

    CardID seizedByTerror = getCardID(game, opponentPlayer, "Seized by Terror");
    opc.drawCard(seizedByTerror);
    pc.addCharacterToCompany(mainCompany, pallando);

    metaGame.cloneState(); {
        // No shadow-lands in site path
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(mainPlayer.selectDestination(mainCompany, moria), Success);
        skipToMovementHazardPhase(metaGame);
        ASSERT_NE(opponentPlayer.useActionOnCard(seizedByTerror, kili), Success);
        metaGame.gameStates.pop_back();
    }

    ASSERT_EQ(mainPlayer.selectDestination(mainCompany, cavesOfUlund), Success);
    skipToMovementHazardPhase(metaGame);
    ASSERT_NE(opponentPlayer.useActionOnCard(seizedByTerror, pallando), Success);
    ASSERT_EQ(opponentPlayer.useActionOnCard(seizedByTerror, gloin), Success);

    metaGame.cloneState(); {
        // 5 + 7 >= 12  ->  test is successful
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(mainPlayer.roll(7), Success);
        EXPECT_TRUE(game.characterInCompany(kili, mainCompany));
        metaGame.gameStates.pop_back();
    }

    // 5 + 6 < 12  ->  test fails
    ASSERT_EQ(mainPlayer.roll(6), Success);
    ASSERT_FALSE(game.characterInCompany(gloin, mainCompany));
    Company & gloinCompany = game.getCurrentSide().companies.back();
    ASSERT_EQ(gloinCompany.characters.size(), 1);
    ASSERT_EQ(gloinCompany.characters.back(), gloin);
    EXPECT_EQ(gloinCompany.currentLocation, lorien);
    EXPECT_EQ(gloinCompany.newLocation, lorien);

    // Verify that it's still mainCompany's movement/hazard phase
    ContextType & context = game.getContext();
    ASSERT_TRUE(std::holds_alternative<Movement>(context));
    ASSERT_EQ(std::get<Movement>(context).company, mainCompany);
    ASSERT_NE(mainCompany, gloinCompany.id);
    ASSERT_NE(opponentPlayer.useActionOnCard(despairOfTheHeart, gloin), Success);
    ASSERT_EQ(opponentPlayer.useActionOnCard(despairOfTheHeart, oin), Success);

    // Glóin must NOT face a second movement/hazard phase
    ASSERT_EQ(opponentPlayer.done(), Success);
    ASSERT_EQ(mainPlayer.done(), Success);     // Pallando increased the hand size
    EXPECT_EQ(game.turnSequence.phase, SitePhase);
}

TEST_F(StartFromLorien, Twilight) {
    // Environment. One environment card (in play or declared earlier in the same chain of
    // effects) is canceled and discarded. Twilight may also be played as a resource, and may
    // be played at any point during any player's turn. Note Twilight is a hazard otherwise.

    loadCardSet(metaGame.settings, CardSet::OrcDeck);
    CardID doorsOfNight = opc.addCardToHand("Doors of Night");
    CardID gatesOfMorning, twilight, opponentTwilight;
    configurer.drawCard(gatesOfMorning = getCardID(game, mainPlayer, "Gates of Morning"));
    configurer.drawCard(twilight = getCardID(game, mainPlayer, "Twilight"));
    configurer.drawCard(opponentTwilight = getCardID(game, opponentPlayer, "Twilight"));

    ASSERT_EQ(mainPlayer.useAction(gatesOfMorning), Success);
    ASSERT_EQ(game.getDeck(gatesOfMorning), PermanentEvents);

    metaGame.cloneState(); {
        // Play Twilight during opponent's organization phase
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(opponentPlayer.useActionOnCard(opponentTwilight, gatesOfMorning), Success);
        EXPECT_EQ(game.getDeck(gatesOfMorning), DiscardPile);
        metaGame.gameStates.pop_back();
    }

    metaGame.cloneState(); {
        // Play Twilight on your own resource
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(mainPlayer.useActionOnCard(twilight, gatesOfMorning), Success);
        EXPECT_EQ(game.getDeck(gatesOfMorning), DiscardPile);
        metaGame.gameStates.pop_back();
    }

    // Proceed to movment/hazard phase
    ASSERT_EQ(mainPlayer.transferCharacter(gloin), Success);
    int secondaryCompany = game.getCurrentSide().companies.back().id;
    mainPlayer.selectDestination(secondaryCompany, cavesOfUlund);
    skipToMovementHazardPhase(metaGame, secondaryCompany);

    // Try targeting a non-environment card
    ASSERT_EQ(opponentPlayer.useAction(fullOfFrothAndRage), Success);
    ASSERT_NE(mainPlayer.useActionOnCard(twilight, fullOfFrothAndRage), Success);
    ContextType & context = game.getContext();
    ASSERT_TRUE(std::holds_alternative<Movement>(context));
    Movement & movement = std::get<Movement>(context);
    EXPECT_EQ(movement.hazardsPlayed, 1);

    // Play Doors of Night without resolving chain
    std::vector<ActionInfo> actions = metaGame.getActions(opponentPlayer, doorsOfNight);
    ASSERT_FALSE(actions.empty());
    ASSERT_EQ(metaGame.useAction(opponentPlayer, actions[0].id), Success);
    EXPECT_EQ(movement.hazardsPlayed, 2);

    metaGame.cloneState(); {
        // Resolve Doors of Night without interfering
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(mainPlayer.done(), Success);
        EXPECT_EQ(game.getDeck(gatesOfMorning), DiscardPile);
        EXPECT_EQ(game.getDeck(doorsOfNight), PermanentEvents);
        metaGame.gameStates.pop_back();
    }

    metaGame.cloneState(); {
        // Counter Doors of Night with Twilight
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(mainPlayer.useActionOnCard(twilight, doorsOfNight), Success);
        EXPECT_EQ(game.getDeck(gatesOfMorning), PermanentEvents);
        EXPECT_EQ(game.getDeck(doorsOfNight), DiscardPile);
        EXPECT_EQ(movement.hazardsPlayed, 2);
        metaGame.gameStates.pop_back();
    }

    metaGame.cloneState(); {
        // Counter Twilight with Twilight
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(mainPlayer.selectCard(doorsOfNight), Success);
        std::vector<ActionInfo> actions = metaGame.getActions(mainPlayer, twilight);
        ASSERT_FALSE(actions.empty());
        ASSERT_EQ(metaGame.useAction(mainPlayer, actions[0].id), Success);
        ASSERT_EQ(opponentPlayer.useActionOnCard(opponentTwilight, twilight), Success);
        EXPECT_EQ(game.getDeck(gatesOfMorning), DiscardPile);
        EXPECT_EQ(game.getDeck(doorsOfNight), PermanentEvents);
        metaGame.gameStates.pop_back();
    }
}

TEST_F(StartFromLorien, WakeOfWar) {
    // The number of strikes and prowess of each Wolf, Spider, and Animal attack are
    // increased by one (by two for Wolves if *Doors of Night* is in play).
    // Cannot be duplicated.

    loadCardSet(metaGame.settings, CardSet::OrcDeck);
    CardID doorsOfNight = opc.addCardToHand("Doors of Night");
    CardID wargs = getCardID(game, opponentPlayer, "Wargs");
    configurer.drawCard(wargs);
    CardID wakeOfWar = getCardID(game, opponentPlayer, "Wake of War");
    configurer.drawCard(wakeOfWar);

    mainPlayer.selectDestination(mainCompany, cavesOfUlund);
    skipToMovementHazardPhase(metaGame);

    metaGame.cloneState(); {
        // Play Wargs WITHOUT Doors of Night
        Game & game = metaGame.gameStates.back();
        ASSERT_EQ(opponentPlayer.useAction(wakeOfWar), Success);
        opponentPlayer.setKey(Wilderness);
        ASSERT_EQ(opponentPlayer.useAction(wargs), Success);
        ASSERT_EQ(mainPlayer.done(), Success);

        ContextType & context = game.getContext();
        ASSERT_TRUE(std::holds_alternative<AssignStrikes>(context));
        EXPECT_EQ(std::get<AssignStrikes>(context).unassignedStrikes, 3);

        ASSERT_EQ(mainPlayer.assignStrike(thorin), Success);
        ASSERT_EQ(mainPlayer.assignStrike(gloin), Success);
        ASSERT_EQ(mainPlayer.assignStrike(dori), Success);
        ASSERT_EQ(mainPlayer.done(), Success);
        ASSERT_TRUE(resolveStrike(metaGame, thorin).verifyCreatureProwess(10));

        metaGame.gameStates.pop_back();
    }

    ASSERT_EQ(opponentPlayer.useAction(doorsOfNight), Success);
    ASSERT_EQ(opponentPlayer.useAction(wakeOfWar), Success);

    metaGame.cloneState(); {
        // Play Wargs WITH Doors of Night
        Game & game = metaGame.gameStates.back();
        opponentPlayer.setKey(Wilderness);
        ASSERT_EQ(opponentPlayer.useAction(wargs), Success);
        ASSERT_EQ(mainPlayer.done(), Success);

        ContextType & context = game.getContext();
        ASSERT_TRUE(std::holds_alternative<AssignStrikes>(context));
        EXPECT_EQ(std::get<AssignStrikes>(context).unassignedStrikes, 4);

        ASSERT_EQ(mainPlayer.assignStrike(thorin), Success);
        ASSERT_EQ(mainPlayer.assignStrike(gloin), Success);
        ASSERT_EQ(mainPlayer.assignStrike(dori), Success);
        ASSERT_EQ(mainPlayer.assignStrike(kili), Success);
        ASSERT_EQ(mainPlayer.done(), Success);
        ASSERT_TRUE(resolveStrike(metaGame, thorin).verifyCreatureProwess(11));

        metaGame.gameStates.pop_back();
    }

    metaGame.cloneState(); {
        // Play Giant Spiders (with Doors of Night)
        Game & game = metaGame.gameStates.back();
        opponentPlayer.setKey(Wilderness);
        ASSERT_EQ(opponentPlayer.useAction(giantSpiders), Success);
        ASSERT_EQ(mainPlayer.done(), Success);

        ContextType & context = game.getContext();
        ASSERT_TRUE(std::holds_alternative<AssignStrikes>(context));
        EXPECT_EQ(std::get<AssignStrikes>(context).unassignedStrikes, 3);

        metaGame.gameStates.pop_back();
    }

    metaGame.cloneState(); {
        // No bonus for drakes
        Game & game = metaGame.gameStates.back();
        opponentPlayer.setKey(WitheredHeath);
        ASSERT_EQ(opponentPlayer.useAction(caveWorm), Success);
        ASSERT_EQ(mainPlayer.done(), Success);

        ContextType & context = game.getContext();
        ASSERT_TRUE(std::holds_alternative<AssignStrikes>(context));
        EXPECT_EQ(std::get<AssignStrikes>(context).unassignedStrikes, 1);

        metaGame.gameStates.pop_back();
    }

    metaGame.cloneState(); {
        // Should not affect attacks where each character must face a strike
        Game & game = metaGame.gameStates.back();
        opponentPlayer.setKey(BorderLands);
        ASSERT_EQ(opponentPlayer.useAction(neekerBreekers), Success);
        ASSERT_EQ(mainPlayer.done(), Success);

        ContextType & context = game.getContext();
        ASSERT_TRUE(std::holds_alternative<AssignStrikes>(context));
        EXPECT_EQ(std::get<AssignStrikes>(context).unassignedStrikes, 0);

        metaGame.gameStates.pop_back();
    }
}

TEST_F(StartFromLorien, Ghosts) {
    // Undead. Three strikes. After attack, each character wounded by Ghosts makes a
    // corruption check modified by -1.

    CardID ovirHollow = getCardID(game, mainPlayer, "Ovir Hollow");
    ASSERT_EQ(mainPlayer.selectDestination(mainCompany, ovirHollow), Success);
    skipToMovementHazardPhase(metaGame);
    ASSERT_EQ(opponentPlayer.setKey(ShadowLands), Success);
    ASSERT_EQ(opponentPlayer.useAction(ghosts), Success);
    ASSERT_EQ(mainPlayer.done(), Success);

    ASSERT_EQ(mainPlayer.assignStrike(thorin), Success);
    ASSERT_EQ(mainPlayer.assignStrike(gloin), Success);
    ASSERT_EQ(mainPlayer.assignStrike(dori), Success);
    ASSERT_EQ(mainPlayer.done(), Success);

    metaGame.cloneState(); {
        // No corruption check if no strike is successful
        Game & game = metaGame.gameStates.back();
        ASSERT_TRUE(resolveStrike(metaGame, thorin)
            .taps()
            .verifyTargetProwess(9)
            .verifyCreatureProwess(9)
            .attackRoll(4)
        );
        ASSERT_TRUE(resolveStrike(metaGame, gloin)
            .taps()
            .verifyTargetProwess(5)
            .attackRoll(7)
        );
        ASSERT_TRUE(resolveStrike(metaGame, dori)
            .taps()
            .verifyTargetProwess(3)
            .attackRoll(7)
        );
        ASSERT_EQ(game.getDeck(ghosts), Kills);
        ASSERT_TRUE(std::holds_alternative<Movement>(game.getContext()));

        metaGame.gameStates.pop_back();
    }

    // Thorin and Dori fails their strikes
    ASSERT_TRUE(resolveStrike(metaGame, thorin)
        .staysInState(Untapped)
        .verifyTargetProwess(6)
        .verifyCreatureProwess(9)
        .attackRoll(2)
        .defendBodyCheck(2)
    );
    ASSERT_TRUE(resolveStrike(metaGame, gloin)
        .taps()
        .verifyTargetProwess(5)
        .attackRoll(4)
        .verifyStrikeIneffectual()
    );
    ASSERT_TRUE(resolveStrike(metaGame, dori)
        .taps()
        .verifyTargetProwess(3)
        .attackRoll(4)
        .defendBodyCheck(2)
    );

    ContextType & context = game.getContext();
    ASSERT_TRUE(std::holds_alternative<ResolveCorruptionChecks>(context));
    ResolveCorruptionChecks & corruptionChecks = std::get<ResolveCorruptionChecks>(context);

    // Thorin and Dori makes a corruption check modified by -1
    ASSERT_EQ(mainPlayer.resolveCorruptionCheck(dori), Success);
    ASSERT_EQ(mainPlayer.done(), Success);
    ASSERT_EQ(mainPlayer.roll(12), Success);
    ASSERT_EQ(mainPlayer.resolveCorruptionCheck(thorin), Success);
    ASSERT_TRUE(std::holds_alternative<CorruptionCheck>(game.getContext()));
    CorruptionCheck & check = std::get<CorruptionCheck>(game.getContext());
    EXPECT_EQ(check.targetCharacter, thorin);
    EXPECT_EQ(check.modifier, -1);
    ASSERT_EQ(mainPlayer.done(), Success);
    ASSERT_EQ(mainPlayer.roll(12), Success);
    EXPECT_TRUE(std::holds_alternative<Movement>(game.getContext()));
}
