#pragma once
#include "CardSetLoader.h"
#include "DeckDefinition.h"
#include "GameConfigurer.h"
#include "GameInterface.h"
#include "TestUtilities.h"
#include <gtest/gtest.h>

class DwarfDeckFixture : public ::testing::Test {
public:
    DwarfDeckFixture()
        : configurer(metaGame)
        , decks(loadDeckDefinitions())
    {
        loadCardSet(metaGame.settings, CardSet::CoreCards);
        loadCardSet(metaGame.settings, CardSet::DwarfDeck);
        loadDwarfPlayer();

        Game & game = metaGame.gameStates.back();
        thorin = getCardID(game, 0, "Thorin II");
        gloin = getCardID(game, 0, "Glóin");
        oin = getCardID(game, 0, "Óin");
        kili = getCardID(game, 0, "Kíli");
        dori = getCardID(game, 0, "Dori");
    }

    void loadDwarfPlayer() {
        PlayerConfigurer pc = configurer.addPlayer();
        pc.loadDeck(decks["Dwarven Quest"]);
        pc.insertLocationEffects();
    }

    void loadHazardPlayer() {
        PlayerConfigurer pc = configurer.addPlayer();
        pc.addCardsToDeck(PlayDeck, {
            {"Covetous Thoughts", 2},
            {"Despair of the Heart", 3},
            {"Foolish Words", 2},
            {"Full of Froth and Rage", 3},
            {"Lost in Free-domains"},
            {"Muster Disperses"},
            {"River", 2},
            {"Seized by Terror", 2},
            {"Twilight", 2},
            {"Wake of War", 2},
            {"Cave Worm", 3},
            {"Ghosts", 3},
            {"Giant Spiders", 3},
            {"Lesser Spiders", 3},
            {"Neeker-breekers", 2},
            {"Wargs", 2},
            {"Watcher in the Water", 3},
        });

        pc.addCompany("Rivendell");
        pc.addCharacterToCompany("Boromir II");
    }

    CardID thorin, gloin, oin, kili, dori;
    MetaGame metaGame;
    GameConfigurer configurer;
    std::unordered_map<std::string, DeckDefinition> decks;
};

class OrcDeckFixture : public DwarfDeckFixture {
public:
    OrcDeckFixture() : DwarfDeckFixture() {
        loadCardSet(metaGame.settings, CardSet::OrcDeck);
        loadOrcPlayer();

        Game & game = metaGame.gameStates.back();
        gorbag = getCardID(game, 1, "Gorbag*");
        lugdush = getCardID(game, 1, "Lugdush*");
        ufthak = getCardID(game, 1, "Ufthak*");
        grishnakh = getCardID(game, 1, "Grishnákh*");
        illFavouredFellow = getCardID(game, 1, "Ill-favoured Fellow*");
    }

    void loadOrcPlayer() {
        PlayerConfigurer pc = configurer.addPlayer();
        pc.loadDeck(decks["Stealthy Tribe"]);
        pc.insertLocationEffects();
    }

    CardID gorbag, lugdush, ufthak, grishnakh, illFavouredFellow;
};

class EliminateCharacterFixture : public DwarfDeckFixture {
public:
    EliminateCharacterFixture() : DwarfDeckFixture() {
        Game & game = metaGame.gameStates.back();
        configurer.addPossession(oin, enrunedShield = getCardID(game, 0, "Enruned Shield"));
        configurer.addPossession(oin, magicalHarp = getCardID(game, 0, "Magical Harp"));
        configurer.addPossession(oin, durinsAxe = getCardID(game, 0, "Durin's Axe"));
        game.cards[dori].state = Wounded;
    }
    CardID enrunedShield, magicalHarp, durinsAxe;
};

class PlayHazardCreatureFixture : public DwarfDeckFixture {
public:
    PlayHazardCreatureFixture() {
        Game & game = metaGame.gameStates.back();
        loadHazardPlayer();
        configurer.drawCard(caveWorm = getCardID(game, 1, "Cave Worm"));
        configurer.drawCard(giantSpiders = getCardID(game, 1, "Giant Spiders"));
        configurer.drawCard(lesserSpiders = getCardID(game, 1, "Lesser Spiders"));
        configurer.drawCard(neekerBreekers = getCardID(game, 1, "Neeker-breekers"));
        configurer.drawCard(wargs = getCardID(game, 1, "Wargs"));
        configurer.drawCard(watcher = getCardID(game, 1, "Watcher in the Water"));
        game.currentPlayer = 0;
    }
    CardID caveWorm, giantSpiders, lesserSpiders, neekerBreekers, wargs, watcher;
};

class InfluenceOpponentFixture : public DwarfDeckFixture {
public:
    InfluenceOpponentFixture() : DwarfDeckFixture() {
        Game & game = metaGame.gameStates.back();
        PlayerConfigurer pc = configurer.currentPlayerConfiguerer();
        pc.addCharacterToCompany(0, pallando = getCardID(game, 0, "Pallando"));
        pc.addCharacterToCompany(0, celeborn = getCardID(game, 0, "Celeborn"));
        configurer.addPossession(pallando, quickbeam = getCardID(game, 0, "Quickbeam"));
        configurer.addPossession(pallando, bookOfMazarbul = getCardID(game, 0, "Book of Mazarbul"));
        configurer.addPossession(thorin, durinsAxe = getCardID(game, 0, "Durin's Axe"));
        game.cards[oin].parent = thorin;
        game.cards[kili].parent = gloin;
        game.cards[celeborn].parent = pallando;
        CompanyID id = pc.addCompany(getLocationID(game, 0, "Blue Mountain Dwarf-hold"));
        pc.addCharacterToCompany(id, fili = getCardID(game, 0, "Fíli"));
        pc.addFaction(blueMountainDwarves = getCardID(game, 0, "Blue Mountain Dwarves"));

        PlayerConfigurer opc = configurer.addPlayer();
        opc.addCompany("Rivendell");
        balin = opc.addCharacterToCompany("Balin");
        boromir = opc.addCharacterToCompany("Boromir II");
        configurer.game().currentPlayer = 1;
        configurer.game().hazardPlayer = 0;
        configurer.game().turnSequence.setPhase(SitePhase, Beginning);
        configurer.game().turnSequence.round = 2;
        metaGame.done(1);
    }
    CardID pallando, quickbeam, durinsAxe, bookOfMazarbul, blueMountainDwarves, celeborn, fili, balin, boromir;
};

class StartFromLorien : public DwarfDeckFixture {
public:
    StartFromLorien()
        : DwarfDeckFixture()
        , game(metaGame.gameStates.back())
        , mainPlayer(metaGame, 0)
        , opponentPlayer(metaGame, 1)
        , pc(configurer, mainPlayer)
        , opc(configurer, opponentPlayer)
    {
        // Starting company (starts with Durin's Axe and Thrór's Map)
        game.cards[kili].parent = thorin;
        game.cards[dori].parent = thorin;
        game.cards[oin].parent = gloin;
        PlayerConfigurer pc = configurer.currentPlayerConfiguerer();
        pc.setCurrentLocation(0, lorien = getLocationID(game, 0, "Lórien"));
        configurer.addPossession(thorin, durinsAxe = getCardID(game, 0, "Durin's Axe"));
        configurer.addPossession(kili, throrsMap = getCardID(game, 0, "Thrór's Map"));

        // Player's default-hand
        configurer.drawCard(pallando = getCardID(game, 0, "Pallando"));
        configurer.drawCard(balin = getCardID(game, 0, "Balin"));
        configurer.drawCard(gollum = getCardID(game, 0, "Gollum"));
        configurer.drawCard(quickbeam = getCardID(game, 0, "Quickbeam"));
        configurer.drawCard(bookOfMazarbul = getCardID(game, 0, "Book of Mazarbul"));
        configurer.drawCard(aFriendOrThree = getCardID(game, 0, "A Friend or Three"));
        configurer.drawCard(concealment = getCardID(game, 0, "Concealment"));
        configurer.drawCard(theDwarvesAreUponYou = getCardID(game, 0, "The Dwarves Are upon You!"));

        // Opponent's default-hand
        loadHazardPlayer();
        configurer.drawCard(giantSpiders = getCardID(game, 1, "Giant Spiders"));
        configurer.drawCard(neekerBreekers = getCardID(game, 1, "Neeker-breekers"));
        configurer.drawCard(ghosts = getCardID(game, 1, "Ghosts"));
        configurer.drawCard(watcher = getCardID(game, 1, "Watcher in the Water"));
        configurer.drawCard(caveWorm = getCardID(game, 1, "Cave Worm"));
        configurer.drawCard(despairOfTheHeart = getCardID(game, 1, "Despair of the Heart"));
        configurer.drawCard(fullOfFrothAndRage = getCardID(game, 1, "Full of Froth and Rage"));
        configurer.drawCard(musterDisperses = getCardID(game, 1, "Muster Disperses"));

        // Most relevant sites
        moria = getLocationID(game, 0, "Moria");
        goblinGate = getLocationID(game, 0, "Goblin-gate");
        buhrWidu = getLocationID(game, 0, "Buhr Widu");
        cavesOfUlund = getLocationID(game, 0, "Caves of Ûlund");
        sarnGoriwing = getLocationID(game, 0, "Sarn Goriwing");
        ironHillDh = getLocationID(game, 0, "Iron Hill Dwarf-hold");
        blueMountainDh = getLocationID(game, 0, "Blue Mountain Dwarf-hold");
        wellinghall = getLocationID(game, 0, "Wellinghall");

        configurer.game().currentPlayer = 0;
        configurer.game().hazardPlayer = 1;
        configurer.game().turnSequence.setPhase(OrganizationPhase, Beginning);
        metaGame.done(0);
    }
    CardID lorien, moria, goblinGate, buhrWidu, cavesOfUlund, sarnGoriwing, ironHillDh, blueMountainDh, wellinghall;
    CardID durinsAxe, throrsMap;
    CardID pallando, balin, gollum, quickbeam, bookOfMazarbul, aFriendOrThree, concealment, theDwarvesAreUponYou;
    CardID giantSpiders, neekerBreekers, ghosts, watcher, caveWorm, despairOfTheHeart, fullOfFrothAndRage, musterDisperses;

    Game & game;
    TestPlayer mainPlayer, opponentPlayer;
    PlayerConfigurer pc, opc;
    CompanyID mainCompany = 0;
};

class OrcVsDwarf : public OrcDeckFixture {
public:
    OrcVsDwarf()
        : game(metaGame.gameStates.back())
        , dwarfPlayer(metaGame, 0)
        , orcPlayer(metaGame, 1)
        , dwarfConfigurer(configurer, dwarfPlayer)
        , orcConfigurer(configurer, orcPlayer)
    {
        configurer.game().currentPlayer = orcPlayer;
        configurer.game().hazardPlayer = dwarfPlayer;
        configurer.game().turnSequence.setPhase(OrganizationPhase, Beginning);
        configurer.game().turnSequence.currentPlayerIndex = orcPlayer;
        configurer.game().turnSequence.hazardPlayerIndex = dwarfPlayer;
        metaGame.done(1);

        game.cards[grishnakh].parent = gorbag;
        metaGame.transferCharacter(orcPlayer, illFavouredFellow);
        orcConfigurer.setCurrentLocation(mainCompany, dolGuldur = getCardID(game, orcPlayer, "Dol Guldur*"));

        blazonOfTheEye = getCardID(game, orcPlayer, "Blazon of the Eye*");
        strangeRations = getCardID(game, orcPlayer, "Strange Rations*");
        configurer.addPossession(lugdush, scrollOfIsildur = getCardID(game, orcPlayer, "Scroll of Isildur*"));
    }
    CardID dolGuldur;
    CardID blazonOfTheEye, strangeRations, scrollOfIsildur;

    Game & game;
    TestPlayer dwarfPlayer, orcPlayer;
    PlayerConfigurer dwarfConfigurer, orcConfigurer;
    CompanyID mainCompany = 0;
};
