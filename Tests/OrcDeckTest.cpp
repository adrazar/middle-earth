#include "ContextInterface.h"
#include "ErrorCodes.h"
#include "Fixtures.h"
#include "GameConfigurer.h"
#include "GameInterface.h"
#include "TestUtilities.h"
#include <gtest/gtest.h>

TEST(OrcDeck, StartingConfiguration) {
    MetaGame metaGame;
    GameSettings & settings = metaGame.settings;
    loadCardSet(metaGame.settings, CardSet::CoreCards);
    loadCardSet(metaGame.settings, CardSet::OrcDeck);

    GameConfigurer configurer(metaGame);
    PlayerConfigurer pc = configurer.addPlayer();

    auto decks = loadDeckDefinitions();
    pc.loadDeck(decks["Stealthy Tribe"]);
}

TEST_F(OrcVsDwarf, HoarmurathTheRingwraith) {
    // *Unique. Manifestation of Hoarmûrath of Dír.* Can use sorcery. +1 direct influence
    // in Heralded lord mode. +2 prowess in Fell Rider mode. As your Ringwraith, if
    // at a Darkhaven, you may keep one more card than normal in your hand.
}

TEST_F(OrcVsDwarf, BadeToRule) {
    // *Playable at a Darkhaven during the organization phase on your Ringwraith.* -2 to his
    // direct influence, +5 general influence. You may discard this card during any of your organization
    // phases. Discard this card if your Ringwraith moves.
    // Alternatively, *playable if your Ringwraith is now in play.* +5 general influence. Place this card with
    // your Ringwraith when he comes into play.
    // Cannot be duplicated by a given player.
}

TEST_F(OrcVsDwarf, CreptAlongCleverly) {
    // *Ranger only.* Cancels a Wolf, Animal, Spider, Dragon, Drake or Undead attack against a
    // ranger's company.
}

TEST_F(OrcVsDwarf, OrcQuarrels) {
    // *Playable on an Orc, Troll, or Man attack.* The attack is canceled.
    //   Alternatively, *playable on any attack if Skies of Fire is in play.* The number of
    // strikes from the attack is reduced to half of its original number (rounded up).
}

TEST_F(OrcVsDwarf, Sneakin) {
    // *Scout only. Playable during the organization phase on an untapped scout
    // in a company with a company size less than 3.* Tap the scout. No creature hazards
    // may be played on his company this turn.
}

TEST_F(OrcVsDwarf, GreatLordOfGoblinGate) {
    // *Unique. Playable at Goblin-gate. Orc. Manifestation of The Great Goblin.*
    // Its controlling character's company is overt. Tap to give +2 prowess to all Orcs in
    // its company: against one attack *or* in company versus company combat.
}

TEST_F(OrcVsDwarf, WarWolf) {
    // *Playable at any tapped or untapped Ruins & Lairs with a Wolf
    // automatic-attack or at any tapped or untapped Shadow-hold with an Orc
    // automatic-attack.
}

TEST_F(OrcVsDwarf, SnagaHai) {
    // *Playable at any tapped or untapped Shadow-hold* if the influence check is greater than 9.
    // Once in play, the number required to influence this faction is zero.
}

TEST_F(OrcVsDwarf, BlazonOfTheEye) {
    // +2 direct influence against factions. Cannot be duplicated on a given character.
}

TEST_F(OrcVsDwarf, RecordsUnread) {
    // Cannot be included with a starting company. Discard: to untap a Shadow-hold *or*
    // to make Information playable at any Shadow-hold. Cannot be duplicated
    // in a given company.
}

TEST_F(OrcVsDwarf, BlastingFire) {
    // *Technology. Playable at a tapped or untapped Shadow-hold, Dark-hold, or a site
    // with a Dwarf automatic-attack.* Discard to cancel all automatic-attacks at a site against
    // the bearer's company, any influence attempts against factions at the site this turn are
    // modified by +2.
}

TEST_F(OrcVsDwarf, VileFumes) {
    // *Technology. Playable at a tapped or untapped Shadow-hold, Dark-hold or a site wih a
    // Dwarf automatic-attack.* Discard during the site phase at a Border-hold or Shadow-hold to
    // make all versions of the site Ruins & Lairs. Its normal automatic-attacks are replaced with:
    // Gas - each character faces 1 strike with 7 prowess (cannot be canceled). Keep *Vile Fumes* with the
    // site until the site is discarded or returned to its location deck.
}

TEST_F(OrcVsDwarf, HighHelm) {
    // *Unique. Helmet.* +2 to direct influence. +1 to body to a maximum of 9.
    // *Warrior only:* +1 to prowess to a maximum of 8.
}

TEST_F(OrcVsDwarf, SableShield) {
    // *Unique. Shield.* If a strike against the bearer is successful, he is not wounded. Instead,
    // the attacker makes a roll - if this result is greater than 6, discard *Sable Shield*.
}

TEST_F(OrcVsDwarf, TheArkenstone) {
    // *Unique.* +5 to bearer's direct influece against Dwarves and Dwarf factions. Each Dwarf in
    // play has +1 mind. If the bearer of this item is at the same site as a Dwarf character, you may
    // discard this item to force the discard of the Dwarf (and all [errata: non-follower] cards he controls).
}

TEST_F(OrcVsDwarf, ScrollOfIsildur) {
    // *Unique.* When a Gold Ring is tested in a company bearing the *Scroll of Isildur*, the
    // random value obtained is modified by +1. May be stored at Barad-dûr for 5 marshalling points.
}

TEST_F(OrcVsDwarf, AnUnexpectedOutpost) {
    // Bring one hazard from your sideboard or discard pile into your play deck and
    // shuffle. If *Doors of Night* is in play, you may do this twice.
}

TEST_F(OrcVsDwarf, ChokingShadows) {
    // *Environment.* Modify the prowess of one automatic-attack at a Ruins & Lairs
    // site by +2.
    //   Alternatively, if *Doors of Night* is in play, treat one Wilderness as a Shadow-land
    // *or* one Ruins & Lairs as a Shadow-hold until the end of the turn.
    // Cannot be duplicated.
}

TEST_F(OrcVsDwarf, DoorsOfNight) {
    // *Environment.* When *Doors of Night* is played, all resource environment cards
    // in play are immediately discarded and all resource environment effects are cancelled.
    // Cannot be duplicated.
}

TEST_F(OrcVsDwarf, ExhalationOfDecay) {
    // *Playable on an Undead hazard creature in your discard pile.* If target Undead can attack,
    // bring it into play as a creature that attacs immediately (not counting against the hazard
    // limit). The attack's prowess is modified by -1.
}

TEST_F(OrcVsDwarf, PlagueOfWights) {
    // The prowess of all Undead attacks is increased by one.
    //   Additionally, if *Doors of Night* is in play, the number of strikes for each Undead
    // attack is doubled.
    //   Cannot be duplicated.
}

TEST_F(OrcVsDwarf, PowerBuiltByWaiting) {
    // Tap during a company's movement/hazard phase to increase the hazard limit against
    // that company by one. This card does not untap during your untap phase. You may use
    // two against a company's hazard limit to untap this card.
}

TEST_F(OrcVsDwarf, TheMoonIsDead) {
    // All Undead attacks receive +1 strike and +1 prowess. All Undead automatic-attacks are
    // duplicated (i.e., each must be faced twice, including all modifications). Discard this
    // card when an Undead attack is defeated. Cannot be duplicated.
}

TEST_F(OrcVsDwarf, BarrowWight) {
    // *Undead.* One strike. After the attack, each character wounded by *Barrow-wight* makes
    // a corruption check modified by -2.
}

TEST_F(OrcVsDwarf, ChillDouser) {
    // *Undead.* Three strikes. Unless *Chill Douser's* attack is canceled, all other Undead attacks
    // against the company for the rest of the turn receive +1 strike and +1 prowess.
}

TEST_F(OrcVsDwarf, WispOfPaleSheen) {
    // *Undead.* One strike. Attacker chooses defending characters. Any character facing
    // a strike whose mind is equal to or lower than the strike's prowess must tap if
    // untapped following the strike (unless the strike is cancelled).
}

TEST_F(OrcVsDwarf, DeadMarshes) {
    // Automatic-attacks: Undead - 2 strikes with 8 prowess, each character wounded must
    // make a corruption check modified by -2.
    // Special: Non-Nazgûl creatures played at this site attack normally, not as detainment.
}

TEST_F(OrcVsDwarf, MountGundabad) {
    // Automatic-attacks: Orcs - each character faces 1 strike with 7 prowess
    // (detainment against overt company).
}

TEST_F(OrcVsDwarf, SuddenCall) {
    // You may play this card as a resource or a hazard according to The Audience of
    // Sauron Rules. This card may not ba played as a hazard against a Wizard
    // player, and may be included as a hazard in a Wizard's deck. You may reshuffle this
    // card into your play deck at any time that it is in your hand (show opponent).
}

TEST_F(OrcVsDwarf, FellRider) {
    // *Fell Rider mode. Playable at a Darkhave during the organization phase on your
    // Ringwraith's own company.* +2 prowess, -3 direct influence to your Ringwraith. Discard all allies
    // and Ringwraith followers in the company; none may join the company. Your Ringwraith may
    // move to a non-Darkhaven site. Discard this card during any of your following organization phases
    // your Ringwraith is at a Darkhave. Cannot be duplicated on a given company.
}

TEST_F(OrcVsDwarf, BlackMace) {
    // *Weapon. Warrior only:* +3 prowess to a maximum of 10
    // (+4 to a maximum of 10 against Elves).
}

TEST_F(OrcVsDwarf, StenchOfMordor) {
    // *Environment.* At the start of its site phase, each company at a site in a Dark-domain
    // (or Shadow-land, if *Doors of Night* is in play) must tap one untapped character if
    // available. Discard when any play deck is exhausted. Cannot be duplicated.
}
