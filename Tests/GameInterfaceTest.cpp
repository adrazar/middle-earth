#include "ContextInterface.h"
#include "ErrorCodes.h"
#include "Fixtures.h"
#include "GameConfigurer.h"
#include "GameInterface.h"
#include "TestUtilities.h"
#include <gtest/gtest.h>
#include <deque>

TEST_F(OrcVsDwarf, VerifyCardAliasTable) {
    Game & game = metaGame.gameStates.back();
    int players(game.sides.size());
    EXPECT_EQ(players, 2);
    int cards(game.cards.size());
    for (int id = 0; id < cards; id++) {
        Card & card = game.cards[id];
        DeckName deck = card.deck;
        for (int player = 0; player < players; player++) {
            CardAliasTable table = *game.sides[player].aliasTable;
            bool inBlackbox = (card.owner == player)
                ? deck == PlayDeck
                : deck == PlayDeck || deck == Hand || deck == LocationDeck || deck == Sideboard;
            auto it = table.toAlias.find(id);
            ASSERT_EQ(inBlackbox, it == table.toAlias.end());
        }
    }
}

TEST_F(DwarfDeckFixture, StarterMovementFromHaven) {
    Scene scene = metaGame.currentScene();
    Game & game = scene.game;
    game.turnSequence.setPhase(OrganizationPhase, Middle);

    CardID goblinGate = getCardID(game, 0, "Goblin-gate");
    ASSERT_NE(goblinGate, -1);
    RegionSelector regionSelector = RegionSelector(metaGame, game.currentPlayer, 0)
        .selectDestination("Bur Widu", NoCardOfSuchName)
        .selectDestination("Buhr Widu", HavenMismatch)
        .selectDestination(goblinGate, Success)
        .selectDestination("Grey Havens", Success);
    moveCardFast(game, goblinGate, DiscardPile, 0);
    regionSelector.selectDestination(goblinGate, LocationNotAvailable)
        .selectDestination("The Dwarves Are upon You!", NotALocation);
}

TEST_F(DwarfDeckFixture, RegionMovement) {
    Game & game = metaGame.gameStates.back();
    game.turnSequence.setPhase(OrganizationPhase, Beginning);
    metaGame.done(0);
    Company & company = game.sides[0].companies.back();

    // Insert some regions and check that the adjacent regions lists are correct
    RegionSelector(metaGame, game.currentPlayer, 0)
        .verifyAdjacentRegions({ Angmar, Arthedain, HighPass, Cardolan, Hollin })
        .insertRegion(Arthedain)
        .verifyAdjacentRegions({ Angmar, Cardolan, Forochel, Lindon, Numeriador, TheShire })
        .insertRegion(Rhudaur, RegionNotAdjacent)
        .insertRegion(Cardolan)
        .verifyAdjacentRegions({ Dunland, Enedhwaith, EriadoranCoast, Hollin, TheShire })
        .insertRegion(EriadoranCoast)
        .verifyAdjacentRegions({ Andrast, AndrastCoast, ElvenShores, Enedhwaith, OldPukelLand })
        // Test the limit of four regions per movement
        .insertRegion(Andrast, RegionLimitReached)
        // A case with no adjacent regions
        .popRegion()
        .insertRegion(TheShire)
        .verifyAdjacentRegions({});

    // Region movement to a site with different nearest haven
    RegionSelector(metaGame, game.currentPlayer, 0)
        .insertRegion(RedhornGate, RegionNotAdjacent)
        .insertRegion(Hollin)
        .verifyAdjacentRegions({ Cardolan, Dunland, RedhornGate })
        .selectDestination("Moria", RegionMismatch)
        .insertRegion(RedhornGate)
        .verifyAdjacentRegions({ WoldAndFoothills })
        .selectDestination("Moria");

    ASSERT_TRUE(std::holds_alternative<OrganizeCompanies>(game.getContext()));
    CardID moria = getCardID(game, 0, "Moria");
    CardID goblinGate = getCardID(game, 0, "Goblin-gate");
    EXPECT_EQ(company.newLocation, moria);
    std::vector<RegionNumber> expectedRegions = { Rhudaur, Hollin, RedhornGate };
    EXPECT_EQ(company.regions, expectedRegions);

    // Change to new destination with starter movement
    ASSERT_EQ(metaGame.selectDestination(0, 0, goblinGate), Success);
    expectedRegions.clear();
    EXPECT_EQ(company.regions, expectedRegions);

    // Select same destination using region movement instead
    RegionSelector(metaGame, game.currentPlayer, company.id)
        .insertRegion(HighPass)
        .selectDestination(goblinGate);
    expectedRegions = { Rhudaur, HighPass };
    EXPECT_EQ(company.regions, expectedRegions);

    // Reset destination to site of origin
    RegionSelector(metaGame, game.currentPlayer, company.id).resetDestination();
    expectedRegions.clear();
    EXPECT_EQ(company.regions, expectedRegions);
    EXPECT_EQ(company.newLocation, getLocationID(game, 0, "Rivendell"));
}

TEST_F(DwarfDeckFixture, ParallelMovement) {
    Game & game = metaGame.gameStates.back();
    game.turnSequence.setPhase(OrganizationPhase, Beginning);
    metaGame.done(0);
    Scene scene = getCurrentScene(game);
    Side & side = game.getCurrentSide();
    CardID goblinGate = getCardID(game, 0, "Goblin-gate");
    Card & siteCard = game.cards[goblinGate];
    EXPECT_EQ(siteCard.deck, LocationDeck);
    ASSERT_EQ(metaGame.transferCharacter(0, thorin, -1), Success);
    ASSERT_EQ(metaGame.selectDestination(0, 1, goblinGate), Success);
    EXPECT_EQ(siteCard.deck, Sites);
    RegionSelector(metaGame, game.currentPlayer, 0)
        .selectDestination(goblinGate, ParallelMovement)
        .insertRegion(HighPass)
        .selectDestination(goblinGate, ParallelMovement)
        .insertRegion(AnduinVales)
        .insertRegion(WoldAndFoothills)
        .selectDestination(getCardID(game, 0, "Lórien"));
    EXPECT_EQ(siteCard.deck, Sites);
    ASSERT_EQ(metaGame.resetDestination(0, 1), Success);
    EXPECT_EQ(siteCard.deck, LocationDeck);
}

TEST_F(DwarfDeckFixture, CramTest) {
    Game & game = metaGame.gameStates.back();
    game.turnSequence.setPhase(OrganizationPhase, Beginning);
    metaGame.done(0);
    Company & company = game.sides[0].companies.back();
    CardID cram = getCardID(game, 0, "Cram");
    std::vector<ActionInfo> actions;
    getCurrentScene(game).tap(thorin);
    actions = metaGame.getActions(0, cram);
    ASSERT_EQ(actions[0].description, "Untap");
    ASSERT_EQ(actions[1].description, "Movement");
    EXPECT_EQ(game.sides[0].actions.size(), 4);
    ASSERT_EQ(metaGame.useAction(0, 7), InvalidActionID);

    // Use cram to increase region limit to 5
    ASSERT_EQ(metaGame.useAction(0, actions[1].id), Success);
    EXPECT_FALSE(game.cards[thorin].possessions.empty());
    ASSERT_TRUE(std::holds_alternative<ChainOfEffects>(game.getContext()));
    ASSERT_EQ(std::get<ChainOfEffects>(game.getContext()).chainOfEffects.size(), 1);
    EXPECT_EQ(game.sides[0].actions.size(), 3);
    metaGame.done(0);
    EXPECT_TRUE(game.cards[thorin].possessions.empty());
    RegionSelector(metaGame, game.currentPlayer, company.id)
        .insertRegion(HighPass)
        .insertRegion(AnduinVales)
        .insertRegion(WesternMirkwood)
        .insertRegion(HeartOfMirkwood)
        .insertRegion(SouthernRhovanion, RegionLimitReached)
        .selectDestination("Sarn Goriwing");

    // Move to another site through 5 regions
    RegionSelector(metaGame, game.currentPlayer, company.id)
        .insertRegion(Cardolan)
        .insertRegion(TheShire)
        .insertRegion(Arthedain)
        .insertRegion(Numeriador)
        .selectDestination("Blue Mountain Dwarf-hold");

    // Use 2nd cram to increase to 6 regions
    cram = game.cards[oin].possessions.back();
    actions = metaGame.getActions(0, cram);
    ASSERT_FALSE(actions.empty());
    ASSERT_EQ(actions[0].description, "Movement");
    ASSERT_EQ(metaGame.useAction(0, actions[0].id), Success);
    metaGame.done(0);
    RegionSelector(metaGame, game.currentPlayer, company.id)
        .insertRegion(HighPass)
        .insertRegion(AnduinVales)
        .insertRegion(WesternMirkwood)
        .insertRegion(HeartOfMirkwood)
        .insertRegion(NorthernRhovanion)
        .insertRegion(Dorwinion, RegionLimitReached)
        .selectDestination("The Lonely Mountain");

    // Use cram to untap
    actions = metaGame.getActions(0, cram);
    EXPECT_TRUE(actions.empty());
    configurer.addPossession(thorin, cram);
    EXPECT_FALSE(game.cards[thorin].possessions.empty());
    EXPECT_EQ(game.cards[cram].deck, Possessions);
    actions = metaGame.getActions(0, cram);
    ASSERT_EQ(actions[0].description, "Untap");
    ASSERT_EQ(game.cards[thorin].state, Tapped);
    metaGame.useAction(0, actions[0].id);
    metaGame.done(0);
    ASSERT_EQ(game.cards[thorin].state, Untapped);
    EXPECT_TRUE(game.cards[thorin].possessions.empty());
    EXPECT_EQ(game.cards[cram].deck, DiscardPile);

    // Try using cram during opponent's turn
    loadHazardPlayer();
    CardID boromir = getCardID(game, 1, "Boromir II");
    configurer.addPossession(boromir, cram);
    getCurrentScene(game).tap(boromir);
    EXPECT_TRUE(metaGame.getActions(1, cram).empty());
}

TEST_F(DwarfDeckFixture, StoreItem) {
    Game & game = metaGame.gameStates.back();
    game.turnSequence.setPhase(OrganizationPhase, Beginning);
    metaGame.done(0);
    Company & company = game.sides[0].companies.back();
    CardID enrunedShield = getCardID(game, 0, "Enruned Shield");
    configurer.addPossession(dori, enrunedShield);
    metaGame.store(0, enrunedShield);
    auto it = game.contextStack.rbegin();
    ASSERT_TRUE(std::holds_alternative<CorruptionCheck>((*it)->context));
    CorruptionCheck & corruptionCheck = std::get<CorruptionCheck>((*(it++))->context);
    ASSERT_EQ(corruptionCheck.targetCharacter, dori);
    ASSERT_EQ(corruptionCheck.base, 2);
    ASSERT_EQ(corruptionCheck.modifier, -1);
    ASSERT_TRUE(std::holds_alternative<ResolveCorruptionChecks>((*it)->context));
    ResolveCorruptionChecks & resolve = std::get<ResolveCorruptionChecks>((*(it++))->context);
    ASSERT_TRUE(resolve.unresolvedChecks.empty());
    ASSERT_TRUE(std::holds_alternative<StoreCard>((*it)->context));
    ASSERT_EQ(std::get<StoreCard>((*(it++))->context).target, enrunedShield);
    ASSERT_TRUE(std::holds_alternative<OrganizeCompanies>((*it)->context));
    metaGame.done(0);
    ASSERT_EQ(metaGame.roll(0, 4), Success);
    ASSERT_TRUE(std::holds_alternative<RollDice>(game.getContext()));
    RollDice & rollDice = std::get<RollDice>(game.getContext());
    ASSERT_EQ(rollDice.rolls[rollDice.selectedRoll], 4);
    metaGame.done(0);
    EXPECT_TRUE(std::holds_alternative<OrganizeCompanies>(game.getContext()));
    EXPECT_TRUE(game.cards[dori].possessions.empty());
    ASSERT_EQ(game.sides[0].stored.size(), 1);
    EXPECT_EQ(game.sides[0].stored.front(), enrunedShield);
}

TEST_F(DwarfDeckFixture, AutomaticAttack) {
    Game & game = metaGame.gameStates.back();
    game.turnSequence.setPhase(SitePhase, Beginning);
    CardID cram = game.cards[thorin].possessions.front();
    configurer.addPossession(gloin, cram);
    EXPECT_TRUE(game.cards[thorin].possessions.empty());
    getCurrentScene(game).tap(kili);
    CardID sarnGoriwing = getLocationID(game, 0, "Sarn Goriwing");
    PlayerConfigurer pc = configurer.currentPlayerConfiguerer();
    pc.setCurrentLocation(0, sarnGoriwing);
    metaGame.done(0);
    ASSERT_TRUE(std::holds_alternative<Explore>(game.getContext()));
    // Face automatic attack
    ASSERT_EQ(metaGame.enterSite(0), Success);
    ASSERT_TRUE(std::holds_alternative<Attack>(game.getContext()));
    Attack & attack = std::get<Attack>(game.getContext());
    ASSERT_TRUE(std::holds_alternative<std::string>(attack.keyedTo));
    EXPECT_EQ(std::get<std::string>(attack.keyedTo), "Sarn Goriwing");
    EXPECT_EQ(attack.statType, Prowess);
    EXPECT_EQ(attack.creatureType, Orc);
    EXPECT_EQ(attack.strikes, 3);
    EXPECT_EQ(attack.prowess, 5);
    EXPECT_EQ(attack.body, 0);
    EXPECT_TRUE(attack.automaticAttack);
    EXPECT_EQ(metaGame.assignStrike(0, thorin), WrongContext);
    metaGame.done(0);
    // Player assigns strikes
    ASSERT_TRUE(std::holds_alternative<AssignStrikes>(game.getContext()));
    AssignStrikes & assign = std::get<AssignStrikes>(game.getContext());
    EXPECT_EQ(assign.state, AssignStrikes::PlayerAssign);
    EXPECT_EQ(metaGame.assignStrike(0, thorin), Success);
    EXPECT_EQ(metaGame.assignStrike(1, kili), WrongPlayer);
    EXPECT_EQ(metaGame.assignStrike(0, kili), InvalidTarget);
    EXPECT_EQ(metaGame.assignStrike(0, thorin), InvalidTarget);
    EXPECT_EQ(metaGame.assignStrike(0, oin), Success);
    metaGame.done(1);
    ASSERT_EQ(assign.state, AssignStrikes::PlayerAssign);
    metaGame.done(0);
    // Opponent assigns strikes
    EXPECT_EQ(assign.state, AssignStrikes::OpponentAssign);
    EXPECT_EQ(metaGame.assignStrike(1, kili), Success);
    EXPECT_EQ(metaGame.assignStrike(1, gloin), OutOfStrikes);
    EXPECT_EQ(metaGame.unassignStrike(1, gloin), InvalidTarget);
    EXPECT_EQ(metaGame.unassignStrike(1, thorin), WrongPlayer);
    EXPECT_EQ(metaGame.unassignStrike(1, kili), Success);
    metaGame.done(1);
    ASSERT_EQ(assign.state, AssignStrikes::OpponentAssign);
    EXPECT_EQ(metaGame.assignStrike(1, dori), Success);
    metaGame.done(0);
    ASSERT_EQ(assign.state, AssignStrikes::OpponentAssign);
    metaGame.done(1);
    EXPECT_EQ(assign.state, AssignStrikes::ResolveStrikes);
    ASSERT_TRUE(std::holds_alternative<AssignStrikes>(game.getContext()));
    // Resolve Thorin's strike
    {
        EXPECT_EQ(metaGame.resolveStrike(0, thorin), Success);
        ASSERT_TRUE(std::holds_alternative<Strike>(game.getContext()));
        Strike & strike = std::get<Strike>(game.getContext());
        Scene scene = getCurrentScene(game);
        EXPECT_EQ(scene.getTargetProwess(strike), 8);
        EXPECT_EQ(scene.getTargetBody(strike), 8);
        EXPECT_EQ(strike.excessStrikes, 0);
        EXPECT_EQ(strike.finalTarget, false);
        EXPECT_EQ(strike.detainment, false);
        ASSERT_EQ(strike.state, Strike::TapCharacter);
        // Do not tap against the strike
        metaGame.done(1);
        ASSERT_EQ(strike.state, Strike::TapCharacter);
        EXPECT_EQ(scene.getTargetProwess(strike), 8);
        metaGame.done(0);
        ASSERT_EQ(strike.state, Strike::PlayResources);
        EXPECT_EQ(scene.getTargetProwess(strike), 5);
        metaGame.done(1);
        ASSERT_EQ(strike.state, Strike::PlayResources);
        metaGame.done(0);
        ASSERT_EQ(strike.state, Strike::CloseStrike);
        metaGame.done(0);
        ASSERT_TRUE(std::holds_alternative<AssignStrikes>(game.getContext()));
        EXPECT_EQ(assign.undefeatedStrikes, 0);
    }
    // Resolve Dori's strike
    {
        EXPECT_EQ(metaGame.resolveStrike(1, dori), WrongPlayer);
        EXPECT_EQ(metaGame.resolveStrike(0, gloin), InvalidTarget);
        EXPECT_EQ(metaGame.resolveStrike(0, dori), Success);
        ASSERT_TRUE(std::holds_alternative<Strike>(game.getContext()));
        Strike & strike = std::get<Strike>(game.getContext());
        Scene scene = getCurrentScene(game);
        EXPECT_EQ(scene.getTargetProwess(strike), 4);
        EXPECT_EQ(scene.getTargetBody(strike), 6);
        // Tap Dori
        ASSERT_EQ(strike.state, Strike::TapCharacter);
        EXPECT_EQ(metaGame.tap(0, gloin), WrongState);
        EXPECT_EQ(metaGame.tap(0, dori), Success);
        ASSERT_EQ(strike.state, Strike::PlayResources);
        EXPECT_EQ(scene.getTargetProwess(strike), 4);
        metaGame.done(0);
        ASSERT_EQ(strike.state, Strike::CloseStrike);
        metaGame.done(0);
        EXPECT_EQ(assign.undefeatedStrikes, 0);
    }
    // Resolve Oin's strike
    {
        Strike & strike = std::get<Strike>(game.getContext());
        Scene scene = getCurrentScene(game);
        EXPECT_EQ(scene.getTargetProwess(strike), 4);
        EXPECT_EQ(scene.getTargetBody(strike), 8);
        metaGame.done(0);
        EXPECT_EQ(scene.getTargetProwess(strike), 1);
        // Tap Gloin in support
        EXPECT_EQ(metaGame.tap(0, thorin), IllegalOperation);
        EXPECT_EQ(metaGame.tap(0, kili), NotUntapped);
        EXPECT_EQ(metaGame.tap(0, gloin), Success);
        EXPECT_EQ(scene.getTargetProwess(strike), 2);
        // Untap Gloin with cram, and try to support a second time
        std::vector<ActionInfo> actions = metaGame.getActions(0, cram);
        ASSERT_EQ(actions[0].description, "Untap");
        metaGame.useAction(0, actions[0].id);
        metaGame.done(0);
        ASSERT_EQ(strike.state, Strike::PlayResources);
        EXPECT_EQ(game.getCardState(gloin), Untapped);
        EXPECT_EQ(metaGame.tap(0, gloin), IllegalOperation);
        // Make the strike succeed
        metaGame.done(0);
        ASSERT_EQ(metaGame.roll(0, 2), Success);
        metaGame.done(0);
        ASSERT_TRUE(std::holds_alternative<RollDice>(game.getContext()));
        ASSERT_EQ(metaGame.roll(1, 8), Success); // Opponent must make the roll
        metaGame.done(0);
        EXPECT_EQ(game.getCardState(oin), Wounded);
        EXPECT_TRUE(std::holds_alternative<Explore>(game.getContext()));
    }
}

TEST_F(EliminateCharacterFixture, TransferItem) {
    Game & game = metaGame.gameStates.back();
    game.turnSequence.setPhase(OrganizationPhase, Beginning);
    metaGame.done(0);
    CardID greyHavens = getLocationID(game, 0, "Grey Havens");
    CardID balin = getCardID(game, 0, "Balin");
    PlayerConfigurer pc = configurer.currentPlayerConfiguerer();
    CompanyID id = pc.addCompany(greyHavens);
    pc.addCharacterToCompany(id, balin);
    EXPECT_EQ(metaGame.transferItem(0, durinsAxe, balin), WrongLocation);
    EXPECT_EQ(metaGame.transferItem(0, durinsAxe, oin), AlreadyInPossession);
    EXPECT_EQ(metaGame.transferItem(0, durinsAxe, thorin), Success);
    {
        Scene scene = getCurrentScene(game);
        ASSERT_TRUE(std::holds_alternative<CorruptionCheck>(scene.context->context));
        CorruptionCheck corruption = std::get<CorruptionCheck>(scene.context->context);
        EXPECT_EQ(corruption.base, 8);
        EXPECT_EQ(corruption.modifier, -1);
    }
    metaGame.done(0);
    metaGame.cloneState(); {
        metaGame.roll(0, 10);
        metaGame.done(0);
        Scene scene = metaGame.currentScene();
        EXPECT_TRUE(std::holds_alternative<OrganizeCompanies>(scene.context->context));
        EXPECT_EQ(scene.game.cards[thorin].possessions.back(), durinsAxe);
        metaGame.gameStates.pop_back();
    }
    metaGame.cloneState(); {
        metaGame.roll(0, 8);
        metaGame.done(0);
        Scene scene = metaGame.currentScene();
        EXPECT_TRUE(std::holds_alternative<OrganizeCompanies>(scene.context->context));
        EXPECT_EQ(scene.game.getDeck(durinsAxe), DiscardPile);
        EXPECT_EQ(scene.game.getDeck(oin), DiscardPile);
        metaGame.gameStates.pop_back();
    }
    metaGame.roll(0, 7);
    metaGame.done(0);
    EXPECT_TRUE(std::holds_alternative<OrganizeCompanies>(game.getContext()));
    EXPECT_EQ(game.getDeck(durinsAxe), DiscardPile);
    EXPECT_EQ(game.getDeck(oin), Eliminated);
}

TEST_F(EliminateCharacterFixture, CorruptionElimination) {
    Game & game = metaGame.gameStates.back();
    game.turnSequence.setPhase(OrganizationPhase, Beginning);
    metaGame.done(0);
    EXPECT_EQ(metaGame.store(0, durinsAxe), Success);
    ASSERT_TRUE(std::holds_alternative<CorruptionCheck>(game.getContext()));
    CorruptionCheck & corruption = std::get<CorruptionCheck>(game.getContext());
    EXPECT_EQ(corruption.base, 8);
    EXPECT_EQ(corruption.modifier, -1);
    // Try double-supporting with Thorin
    EXPECT_EQ(metaGame.tap(0, thorin), Success);
    EXPECT_EQ(corruption.modifier, 0);
    CardID cram = game.cards[thorin].possessions.front();
    std::vector<ActionInfo> actions = metaGame.getActions(0, cram);
    ASSERT_EQ(actions[0].description, "Untap");
    metaGame.useAction(0, actions[0].id);
    metaGame.done(0);
    ASSERT_TRUE(std::holds_alternative<CorruptionCheck>(game.getContext()));
    EXPECT_EQ(game.getCardState(thorin), Untapped);
    EXPECT_EQ(metaGame.tap(0, thorin), InvalidTarget);
    EXPECT_EQ(corruption.modifier, 0);
    EXPECT_EQ(metaGame.tap(0, kili), Success);
    EXPECT_EQ(corruption.modifier, 1);
    metaGame.done(0);
    metaGame.roll(0, 5);
    ASSERT_TRUE(std::holds_alternative<RollDice>(game.getContext()));
    metaGame.done(1);
    EXPECT_TRUE(std::holds_alternative<OrganizeCompanies>(game.getContext()));
    Side & side = game.sides[0];
    EXPECT_EQ(side.eliminated.back(), oin);
    EXPECT_EQ(side.companies.back().characters.size(), 4);
    EXPECT_EQ(game.cards[enrunedShield].deck, DiscardPile);
    EXPECT_EQ(game.cards[magicalHarp].deck, DiscardPile);
    EXPECT_EQ(game.cards[durinsAxe].deck, DiscardPile);
}

TEST_F(EliminateCharacterFixture, BodyCheckElimination) {
    Game & game = metaGame.gameStates.back();
    game.turnSequence.setPhase(SitePhase, Beginning);
    getCurrentScene(game).tap(kili);
    CardID cram = game.cards[oin].possessions.front();
    CardID theLonelyMountain = getLocationID(game, 0, "The Lonely Mountain");
    PlayerConfigurer pc = configurer.currentPlayerConfiguerer();
    pc.setCurrentLocation(0, theLonelyMountain);
    metaGame.done(0);
    ASSERT_TRUE(std::holds_alternative<Explore>(game.getContext()));
    EXPECT_TRUE(std::get<Explore>(game.getContext()).containsHoard);
    ASSERT_EQ(metaGame.enterSite(0), Success);
    ASSERT_TRUE(std::holds_alternative<Attack>(game.getContext()));
    Attack & attack = std::get<Attack>(game.getContext());
    EXPECT_EQ(attack.statType, Prowess);
    EXPECT_EQ(attack.creatureType, Dragon);
    EXPECT_EQ(attack.strikes, 1);
    EXPECT_EQ(attack.prowess, 14);
    EXPECT_EQ(attack.body, 0);
    metaGame.done(0);
    EXPECT_EQ(metaGame.assignStrike(0, oin), Success);
    metaGame.done(0);
    ASSERT_TRUE(std::holds_alternative<Strike>(game.getContext()));
    Strike & strike = std::get<Strike>(game.getContext());
    Scene scene = getCurrentScene(game);
    EXPECT_EQ(scene.getTargetProwess(strike), 7);
    EXPECT_EQ(scene.getTargetBody(strike), 10);
    EXPECT_EQ(metaGame.tap(0, oin), Success);
    EXPECT_EQ(metaGame.tap(0, thorin), Success);
    EXPECT_EQ(metaGame.tap(0, gloin), Success);
    EXPECT_EQ(scene.getTargetProwess(strike), 9);
    // Strike succeeds, body check fails
    Side & side = game.getCurrentSide();
    metaGame.getActions(0, magicalHarp);
    removeEffects(game);
    size_t actionCount = side.actions.size();
    size_t effectCount = side.effects.size();
    metaGame.done(0);
    metaGame.roll(0, 3);
    metaGame.done(0);
    metaGame.roll(1, 12);
    metaGame.done(0);
    ASSERT_TRUE(std::holds_alternative<TransferItems>(game.getContext()));
    // Distribute the items previously held by the eliminated character
    TransferItems & transfer = std::get<TransferItems>(game.getContext());
    EXPECT_EQ(metaGame.selectItemRecipient(0, enrunedShield, gloin), Success);
    EXPECT_EQ(metaGame.selectItemRecipient(0, enrunedShield, dori), InvalidTarget);
    EXPECT_EQ(metaGame.selectItemRecipient(0, durinsAxe, kili), Success);
    EXPECT_EQ(metaGame.selectItemRecipient(0, magicalHarp, thorin), Success);
    EXPECT_EQ(metaGame.selectItemRecipient(0, durinsAxe, thorin), Success);    // Replaces Magical Harp
    EXPECT_EQ(metaGame.selectItemRecipient(0, cram, kili), Success);
    metaGame.done(1);
    ASSERT_TRUE(std::holds_alternative<TransferItems>(game.getContext()));
    metaGame.done(0);
    ASSERT_TRUE(std::holds_alternative<Explore>(game.getContext()));
    EXPECT_EQ(game.sides[0].eliminated.back(), oin);
    EXPECT_EQ(game.cards[gloin].possessions.back(), enrunedShield);
    EXPECT_EQ(game.cards[thorin].possessions.back(), durinsAxe);
    EXPECT_EQ(game.cards[kili].possessions.back(), cram);
    EXPECT_EQ(game.cards[magicalHarp].deck, DiscardPile);
    metaGame.getActions(0, magicalHarp);
    removeEffects(game);
    EXPECT_EQ(side.actions.size(), actionCount - 1); // Magical Harp action gone
    EXPECT_EQ(side.effects.size(), effectCount - 2); // Two effects from Óin gone
}

TEST_F(DwarfDeckFixture, PlayItem) {
    Game & game = metaGame.gameStates.back();
    game.turnSequence.setPhase(SitePhase, Beginning);
    getCurrentScene(game).tap(kili);
    CardID magicalHarp = getCardID(game, 0, "Magical Harp");
    CardID wormsbane = getCardID(game, 0, "Wormsbane");
    CardID hauberk = getCardID(game, 0, "Hauberk of Bright Mail");
    CardID throrsMap = getCardID(game, 0, "Thrór's Map");
    configurer.drawCard(magicalHarp);
    configurer.drawCard(wormsbane);
    configurer.drawCard(hauberk);
    configurer.drawCard(throrsMap);
    CardID cram = game.cards[oin].possessions.front();
    PlayerConfigurer pc = configurer.currentPlayerConfiguerer();
    pc.setCurrentLocation(0, getLocationID(game, 0, "Buhr Widu"));
    metaGame.done(0);
    ASSERT_TRUE(std::holds_alternative<Explore>(game.getContext()));
    Explore & explore = std::get<Explore>(game.getContext());
    EXPECT_EQ(game.getCardState(explore.site), Untapped);
    explore.state = Explore::ExploreSite;
    std::list<ItemType> playable = { Minor, Major };
    EXPECT_EQ(explore.playable, playable);
    metaGame.selectCard(0, gloin);
    EXPECT_TRUE(metaGame.getActions(0, wormsbane).empty());
    EXPECT_TRUE(metaGame.getActions(0, magicalHarp).empty());   // Can't play hoard items at Buhr Widu
    std::vector<ActionInfo> actions = metaGame.getActions(0, hauberk);
    ASSERT_EQ(actions.size(), 1);
    EXPECT_EQ(actions[0].description, "Play");
    // Play major item with Gloin
    ASSERT_EQ(metaGame.useAction(0, actions[0].id), Success);
    metaGame.done(0);
    EXPECT_EQ(game.getCardState(gloin), Tapped);
    EXPECT_EQ(game.cards[gloin].possessions.back(), hauberk);
    EXPECT_EQ(game.getCardState(explore.site), Tapped);
    playable = { Minor };
    EXPECT_EQ(explore.playable, playable);
    // Play minor item with Thorin
    metaGame.selectCard(0, kili);
    EXPECT_TRUE(metaGame.getActions(0, throrsMap).empty());
    metaGame.selectCard(0, thorin);
    actions = metaGame.getActions(0, throrsMap);
    ASSERT_EQ(actions.size(), 1);
    metaGame.useAction(0, actions[0].id);
    metaGame.done(0);
    EXPECT_EQ(game.getCardState(thorin), Tapped);
    EXPECT_EQ(game.cards[thorin].possessions.back(), throrsMap);
    EXPECT_EQ(game.getCardState(explore.site), Tapped);
    EXPECT_TRUE(explore.playable.empty());
    EXPECT_TRUE(metaGame.getActions(0, throrsMap).empty());
}

TEST_F(DwarfDeckFixture, PlayabilityAtTappedSite) {
    Game & game = metaGame.gameStates.back();
    game.turnSequence.setPhase(SitePhase, Beginning);
    CardID buhrWidu = getLocationID(game, 0, "Buhr Widu");
    PlayerConfigurer pc = configurer.currentPlayerConfiguerer();
    pc.setCurrentLocation(0, buhrWidu);
    getCurrentScene(game).tap(buhrWidu);
    metaGame.done(0);
    ASSERT_TRUE(std::holds_alternative<Explore>(game.getContext()));
    Explore & explore = std::get<Explore>(game.getContext());
    EXPECT_EQ(game.getCardState(explore.site), Tapped);
    EXPECT_TRUE(explore.playable.empty());
}

TEST_F(DwarfDeckFixture, PlayFaction) {
    Game & game = metaGame.gameStates.back();
    game.turnSequence.setPhase(OrganizationPhase, Beginning);
    metaGame.done(0);
    EXPECT_EQ(metaGame.takeFollower(0, thorin, oin), Success);
    resetContextStack(game);

    game.turnSequence.setPhase(SitePhase, Beginning);
    getCurrentScene(game).tap(kili);
    CardID faction = getCardID(game, 0, "Blue Mountain Dwarves");
    configurer.drawCard(faction);
    CardID cram = game.cards[oin].possessions.front();
    PlayerConfigurer pc = configurer.currentPlayerConfiguerer();
    pc.setCurrentLocation(0, getLocationID(game, 0, "Blue Mountain Dwarf-hold"));
    metaGame.done(0);

    ASSERT_TRUE(std::holds_alternative<Explore>(game.getContext()));
    Explore & explore = std::get<Explore>(game.getContext());
    EXPECT_EQ(game.getCardState(explore.site), Untapped);
    explore.state = Explore::ExploreSite;
    metaGame.selectCard(0, thorin);

    std::vector<ActionInfo> actions = metaGame.getActions(0, faction);
    ASSERT_EQ(actions.size(), 1);
    EXPECT_EQ(actions[0].description, "Play");
    ASSERT_EQ(metaGame.useAction(0, actions[0].id), Success);
    EXPECT_EQ(game.getDeck(faction), Pending);
    metaGame.done(0);

    ASSERT_TRUE(std::holds_alternative<InfluenceAttempt>(game.getContext()));
    InfluenceAttempt & influence = std::get<InfluenceAttempt>(game.getContext());
    EXPECT_EQ(game.getCardState(thorin), Tapped);
    EXPECT_EQ(influence.influencingCharacter, thorin);
    EXPECT_EQ(influence.target, faction);
    EXPECT_EQ(influence.requiredInfluence, 9);
    EXPECT_EQ(influence.modifier, 5);
    EXPECT_FALSE(influence.influenceOpponent);
    metaGame.cloneState(); {
        // Influence attempt fails
        Scene scene = metaGame.currentScene();
        Game & game = scene.game;
        metaGame.done(1);
        ASSERT_TRUE(std::holds_alternative<InfluenceAttempt>(game.getContext()));
        metaGame.done(0);
        ASSERT_TRUE(std::holds_alternative<RollDice>(game.getContext()));
        metaGame.roll(0, 4);
        metaGame.done(0);
        EXPECT_EQ(game.getDeck(faction), DiscardPile);
        EXPECT_EQ(game.getCardState(explore.site), Untapped);
        EXPECT_TRUE(std::holds_alternative<Explore>(game.getContext()));
    }
    metaGame.gameStates.pop_back(); {
        // Influce attempt succeeds
        Scene scene = metaGame.currentScene();
        Game & game = scene.game;
        metaGame.done(0);
        ASSERT_TRUE(std::holds_alternative<RollDice>(game.getContext()));
        metaGame.roll(0, 5);
        metaGame.done(0);
        EXPECT_EQ(game.getDeck(faction), Factions);
        EXPECT_EQ(game.getCardState(explore.site), Tapped);
        EXPECT_TRUE(std::holds_alternative<Explore>(game.getContext()));
    }
}

TEST_F(DwarfDeckFixture, ManageFollowers) {
    Game & game = metaGame.gameStates.back();
    game.turnSequence.setPhase(OrganizationPhase, Beginning);
    metaGame.done(0);
    Scene scene = getCurrentScene(game);
    EXPECT_EQ(scene.getUnusedGI(0), 0);
    EXPECT_EQ(scene.getUnusedDI(thorin, 0, Dwarf), 4);
    EXPECT_EQ(scene.getUnusedDI(gloin, 0, Dwarf), 3);
    EXPECT_EQ(scene.getMind(oin, 0), 3);
    EXPECT_EQ(scene.getMind(dori, 0), 1);
    EXPECT_EQ(scene.getMind(kili, 0), 3);
    EXPECT_EQ(metaGame.takeFollower(0, thorin, oin), Success);
    EXPECT_EQ(metaGame.takeFollower(0, thorin, kili), InsufficientInfluence);
    EXPECT_EQ(scene.getUnusedGI(0), 3);
    EXPECT_EQ(scene.getUnusedDI(thorin, 0, Dwarf), 1);
    EXPECT_EQ(metaGame.takeFollower(0, thorin, dori), Success);
    // Transfer Oin to Gloin
    EXPECT_EQ(metaGame.takeFollower(0, gloin, oin), Success);
    EXPECT_EQ(scene.getUnusedDI(thorin, 0, Dwarf), 3);
    EXPECT_EQ(metaGame.dropFollower(0, oin), Success);
    EXPECT_EQ(scene.getUnusedDI(gloin, 0, Dwarf), 3);
    EXPECT_EQ(metaGame.takeFollower(0, gloin, kili), Success);
    EXPECT_EQ(metaGame.takeFollower(0, thorin, oin), Success);
    EXPECT_EQ(scene.getUnusedGI(0), 7);
}

TEST_F(DwarfDeckFixture, SplitCompany) {
    Game & game = metaGame.gameStates.back();
    game.turnSequence.setPhase(OrganizationPhase, Beginning);
    metaGame.done(0);
    Scene scene = getCurrentScene(game);
    Side & side = game.getCurrentSide();
    EXPECT_EQ(metaGame.takeFollower(0, thorin, oin), Success);
    // Split Thorin and Óin into a new company
    EXPECT_EQ(metaGame.transferCharacter(0, thorin, -1), Success);
    EXPECT_EQ(side.companies.size(), 2);
    EXPECT_EQ(side.companies.front().characters.size(), 3);
    EXPECT_EQ(side.companies.back().characters.size(), 2);
    EXPECT_EQ(game.cards[oin].parent, thorin);
    // Move Óin back to the previous company
    EXPECT_EQ(metaGame.transferCharacter(0, oin, 0), Success);
    EXPECT_EQ(side.companies.front().characters.size(), 4);
    EXPECT_EQ(side.companies.back().characters.size(), 1);
    EXPECT_EQ(game.cards[oin].parent, HadParent);
    // Abandon the new company altogether
    EXPECT_EQ(metaGame.transferCharacter(0, thorin, 0), Success);
    EXPECT_EQ(side.companies.size(), 1);
}

TEST_F(DwarfDeckFixture, PlayCharacter) {
    Game & game = metaGame.gameStates.back();
    game.turnSequence.setPhase(OrganizationPhase, Beginning);
    metaGame.done(0);
    CardID balin = getCardID(game, 0, "Balin");
    CardID fili = getCardID(game, 0, "Fíli");
    CardID celeborn = getCardID(game, 0, "Celeborn");
    CardID pallando = getCardID(game, 0, "Pallando");
    CardID blueMountain = getLocationID(game, 0, "Blue Mountain Dwarf-hold");
    CardID rivendell = getLocationID(game, 0, "Rivendell");
    CardID lorien = getLocationID(game, 0, "Lórien");
    CardID greyHavens = getLocationID(game, 0, "Grey Havens");
    CardID wellinghall = getLocationID(game, 0, "Wellinghall");
    configurer.drawCard(balin);
    configurer.drawCard(fili);
    configurer.drawCard(celeborn);
    configurer.drawCard(pallando);
    // Wizard not in play
    EXPECT_EQ(metaGame.takeFollower(0, thorin, oin), Success);
    EXPECT_EQ(metaGame.takeFollower(0, thorin, dori), Success);
    EXPECT_EQ(metaGame.takeFollower(0, gloin, kili), Success);
    EXPECT_TRUE(metaGame.getActions(0, balin).empty());
    metaGame.selectCard(0, blueMountain);
    EXPECT_FALSE(metaGame.getActions(0, balin).empty());
    EXPECT_TRUE(metaGame.getActions(0, celeborn).empty());
    EXPECT_EQ(metaGame.dropFollower(0, kili), Success);
    EXPECT_FALSE(metaGame.getActions(0, balin).empty());
    EXPECT_FALSE(metaGame.getActions(0, fili).empty());
    EXPECT_TRUE(metaGame.getActions(0, pallando).empty());
    metaGame.selectCard(0, gloin);
    EXPECT_FALSE(metaGame.getActions(0, fili).empty());
    EXPECT_TRUE(metaGame.getActions(0, balin).empty());
    EXPECT_EQ(metaGame.takeFollower(0, gloin, kili), Success);
    metaGame.selectCard(0, lorien);
    EXPECT_FALSE(metaGame.getActions(0, balin).empty());
    EXPECT_FALSE(metaGame.getActions(0, celeborn).empty());
    EXPECT_TRUE(metaGame.getActions(0, pallando).empty());
    metaGame.selectCard(0, greyHavens);
    EXPECT_FALSE(metaGame.getActions(0, balin).empty());
    EXPECT_FALSE(metaGame.getActions(0, celeborn).empty());
    EXPECT_FALSE(metaGame.getActions(0, pallando).empty());
    metaGame.selectCard(0, rivendell);
    EXPECT_FALSE(metaGame.getActions(0, celeborn).empty());
    EXPECT_FALSE(metaGame.getActions(0, pallando).empty());
    metaGame.selectCompany(0, 0);
    EXPECT_FALSE(metaGame.getActions(0, celeborn).empty());
    EXPECT_FALSE(metaGame.getActions(0, pallando).empty());
    metaGame.selectCard(0, greyHavens);
    EXPECT_FALSE(metaGame.getActions(0, celeborn).empty());
    // Wizard in play
    Side & side = game.getCurrentSide();
    std::vector<ActionInfo> actions;
    actions = metaGame.getActions(0, pallando);
    EXPECT_FALSE(actions.empty());
    ASSERT_EQ(metaGame.useAction(0, actions[0].id), Success);
    metaGame.done(0);
    EXPECT_EQ(game.characterBroughtIntoPlay, pallando);
    EXPECT_EQ(side.companies.size(), 2);
    metaGame.selectCard(0, greyHavens);
    EXPECT_TRUE(metaGame.getActions(0, celeborn).empty());
    metaGame.selectCard(0, pallando);
    EXPECT_TRUE(metaGame.getActions(0, celeborn).empty());
    ASSERT_EQ(metaGame.dropFollower(0, kili), Success);
    metaGame.selectCard(0, gloin);
    EXPECT_TRUE(metaGame.getActions(0, fili).empty());
    game.characterBroughtIntoPlay = -1;
    EXPECT_FALSE(metaGame.getActions(0, fili).empty());
    metaGame.selectCard(0, rivendell);
    EXPECT_TRUE(metaGame.getActions(0, fili).empty());
    PlayerConfigurer pc = configurer.currentPlayerConfiguerer();
    pc.setCurrentLocation(0, blueMountain);
    metaGame.selectCompany(0, 0);
    EXPECT_TRUE(metaGame.getActions(0, fili).empty());
    metaGame.selectCard(0, gloin);
    EXPECT_FALSE(metaGame.getActions(0, fili).empty());
    pc.setCurrentLocation(0, wellinghall);
    EXPECT_EQ(side.selectedCards.front(), gloin);
    EXPECT_TRUE(metaGame.getActions(0, fili).empty());
    // Try playing Fíli different ways
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        metaGame.selectCard(0, greyHavens);
        EXPECT_FALSE((actions = metaGame.getActions(0, fili)).empty());
        EXPECT_EQ(metaGame.useAction(0, actions[0].id), Success);
        metaGame.done(0);
        EXPECT_EQ(game.characterBroughtIntoPlay, fili);
        EXPECT_EQ(game.getCurrentSide().companies.size(), 3);
        metaGame.gameStates.pop_back();
    }
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        metaGame.selectCompany(0, 1);
        EXPECT_FALSE((actions = metaGame.getActions(0, fili)).empty());
        EXPECT_EQ(metaGame.useAction(0, actions[0].id), Success);
        metaGame.done(0);
        EXPECT_EQ(game.characterBroughtIntoPlay, fili);
        EXPECT_EQ(game.getCurrentSide().companies.size(), 2);
        EXPECT_EQ(game.cards[fili].parent, NoParent);
        metaGame.gameStates.pop_back();
    }
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        metaGame.selectCard(0, pallando);
        EXPECT_FALSE((actions = metaGame.getActions(0, fili)).empty());
        EXPECT_EQ(metaGame.useAction(0, actions[0].id), Success);
        metaGame.done(0);
        EXPECT_EQ(game.characterBroughtIntoPlay, fili);
        EXPECT_EQ(game.getCurrentSide().companies.size(), 2);
        std::vector<CardID> & characters = game.getCurrentSide().companies.back().characters;
        EXPECT_EQ(characters.size(), 2);
        EXPECT_EQ(characters.back(), fili);
        EXPECT_EQ(game.cards[fili].parent, pallando);
        metaGame.gameStates.pop_back();
    }
    // Test uniqueness
    metaGame.selectCard(0, pallando);
    EXPECT_FALSE(metaGame.getActions(0, balin).empty());
    pc.addCompany("Wellinghall");
    CardID balin2 = pc.addCharacterToCompany("Balin");
    EXPECT_NE(balin, balin2);
    game.currentPlayer = 0;
    EXPECT_EQ(game.getCurrentSide().selectedCards.back(), pallando);
    EXPECT_FALSE(metaGame.getActions(0, fili).empty());
    EXPECT_FALSE(metaGame.getActions(0, celeborn).empty());
    EXPECT_TRUE(metaGame.getActions(0, balin).empty());
}

void setDestination(MetaGame & metaGame, const std::vector<CardID> & havens, const std::string & locationName) {
    metaGame.gameStates.pop_back();
    metaGame.cloneState();
    Game & game = metaGame.gameStates.back();
    Company & company = game.sides[0].companies.back();
    CardID destination = getLocationID(game, game.currentPlayer, locationName);
    Location & location = game.getLocation(destination);
    company.newLocation = destination;
    for (CardID haven : havens)
        if (game.cards[haven].dataIndex == location.sitePath.front().nearestHaven)
            company.currentLocation = haven;
    metaGame.done(0);
    ASSERT_TRUE(std::holds_alternative<DrawCards>(game.getContext()));
    for (int i = 0; i < 2; i++) {
        metaGame.drawCard(i);
        metaGame.done(i);
    }
    ASSERT_TRUE(std::holds_alternative<Movement>(game.getContext()));
}

class TestKeyTypes {
public:
    TestKeyTypes(MetaGame & metaGame) : metaGame(metaGame) {}

    std::string operator()(const PathSymbol & pathSymbol) {
        std::string names[] = { "Coastal Sea", "Free-domain", "Border-land", "Wilderness", "Shadow-land", "Dark-domain" };
        return names[pathSymbol];
    }

    std::string operator()(const SiteType & siteType) {
        std::string names[] = { "Haven", "Free-hold", "Border-hold", "Ruins & Lairs"
                              , "Shadow-hold", "Dark-hold", "Site in region" };
        return names[siteType];
    }

    std::string operator()(const RegionNumber & regionNumber) {
        return metaGame.gameStates.back().getRegion(regionNumber).name;
    }

    std::string operator()(const std::string & name) {
        return name;
    }

    void testCreatures(const std::vector<CardID> & creatures, const std::deque<std::vector<KeyedToType>> & keys) {
        ASSERT_EQ(keys.size(), 6);
        auto it = keys.begin();
        for (CardID creature : creatures)
            testKeyTypes(creature, *(it++));
    }

private:
    void testKeyTypes(CardID creature, const std::vector<KeyedToType> & expectedResult) {
        Game & game = metaGame.gameStates.back();
        auto it = expectedResult.cbegin();
        end = expectedResult.cend();
        for (int i = 0; i < 6; i++)
            testKeyType(creature, PathSymbol(i), it);
        for (int i = 1; i < 7; i++)
            testKeyType(creature, SiteType(i), it);
        Movement & movement = std::get<Movement>(game.getContext());
        for (KeyedToType key : movement.regions)
            testKeyType(creature, key, it);
        testKeyType(creature, game.getLocation(movement.destination).name, it);
        EXPECT_TRUE(it == expectedResult.end());
    }

    void testKeyType(CardID creature, KeyedToType key, std::vector<KeyedToType>::const_iterator & it) {
        metaGame.setKeyedTo(1, key);
        if (!metaGame.getActions(1, creature).empty()) {
            ASSERT_TRUE(it != end);
            ASSERT_EQ(std::visit(*this, key), std::visit(*this, *(it++)));
        }
    }

    MetaGame & metaGame;
    std::vector<KeyedToType>::const_iterator end;
};

TEST_F(PlayHazardCreatureFixture, KeyCreature) {
    Game & game = metaGame.gameStates.back();
    game.turnSequence.setPhase(MovementHazardPhase, Beginning);
    std::vector<CardID> havens = { getLocationID(game, 0, "Rivendell"),
                                   getLocationID(game, 0, "Lórien"),
                                   getLocationID(game, 0, "Grey Havens") };
    metaGame.cloneState();
    const std::vector<CardID> creatures = { caveWorm, giantSpiders, lesserSpiders, neekerBreekers, wargs, watcher };
    TestKeyTypes tester(metaGame);
    setDestination(metaGame, havens, "Blue Mountain Dwarf-hold");
    tester.testCreatures(creatures, { { Numeriador }, {}, { Wilderness }, { Wilderness }, { Wilderness }, {} });
    setDestination(metaGame, havens, "Buhr Widu");
    tester.testCreatures(creatures, { {}, { Wilderness }, { Wilderness, RuinsLairs }, { BorderLands, Wilderness, RuinsLairs }
                                    , { BorderLands, Wilderness }, { Wilderness } });
    setDestination(metaGame, havens, "Caves of Ûlund");
    tester.testCreatures(creatures, { { WitheredHeath }, { Wilderness }, { Wilderness, ShadowLands, RuinsLairs }
                                    , { BorderLands, Wilderness, ShadowLands, RuinsLairs }
                                    , { BorderLands, Wilderness, ShadowLands }, { Wilderness } });
    setDestination(metaGame, havens, "Dead Marshes");
    tester.testCreatures(creatures, { {}, {}, { Wilderness, ShadowLands }, { Wilderness, ShadowLands }
                                    , { Wilderness, ShadowLands }, {} });
    setDestination(metaGame, havens, "Goblin-gate");
    tester.testCreatures(creatures, { { HighPass }, { Wilderness }, { Wilderness }, { Wilderness }, { Wilderness }
                                    , { Wilderness } });
    setDestination(metaGame, havens, "Iron Hill Dwarf-hold");
    tester.testCreatures(creatures, { { IronHills }, { Wilderness }, { Wilderness }, { BorderLands, Wilderness }
                                    , { BorderLands, Wilderness }, { Wilderness } });
    setDestination(metaGame, havens, "Moria");
    tester.testCreatures(creatures, { { RedhornGate }, { Wilderness }, { Wilderness }, { Wilderness }, { Wilderness }
                                    , { Wilderness, "Moria" } });
    setDestination(metaGame, havens, "Ovir Hollow");
    tester.testCreatures(creatures, { { GreyMountainNarrows }, {}, { Wilderness, ShadowLands, RuinsLairs }
                                    , { BorderLands, Wilderness, ShadowLands, RuinsLairs }
                                    , { BorderLands, Wilderness, ShadowLands }, {} });
    setDestination(metaGame, havens, "Sarn Goriwing");
    tester.testCreatures(creatures, { {}, { Wilderness, ShadowHold, HeartOfMirkwood }, { Wilderness }
                                    , { BorderLands, Wilderness }, { BorderLands, Wilderness }, { Wilderness } });
    setDestination(metaGame, havens, "Shrel-Kain");
    tester.testCreatures(creatures, { {}, { Wilderness }, { Wilderness }, { BorderLands, Wilderness }
                                    , { BorderLands, Wilderness }, { Wilderness } });
    setDestination(metaGame, havens, "The Lonely Mountain");
    tester.testCreatures(creatures, { {}, { Wilderness }, { Wilderness, RuinsLairs }, { BorderLands, Wilderness, RuinsLairs }
                                    , { BorderLands, Wilderness }, { Wilderness } });
    setDestination(metaGame, havens, "Wellinghall");
    tester.testCreatures(creatures, { {}, { Wilderness }, { Wilderness }, { Wilderness }, { Wilderness }, { Wilderness } });
}

TEST_F(PlayHazardCreatureFixture, CreatureOnGuard) {
    Game & game = metaGame.gameStates.back();
    game.turnSequence.setPhase(OrganizationPhase, Beginning);
    metaGame.done(0);
    Company & company = game.sides[0].companies.back();
    company.currentLocation = getLocationID(game, 0, "Lórien");
    CardID concealment = getCardID(game, 0, "Concealment");
    configurer.drawCard(concealment);
    ASSERT_EQ(metaGame.selectDestination(0, 0, getCardID(game, 0, "Ovir Hollow")), Success);
    metaGame.done(0);
    // End of Organization phase
    metaGame.done(0);
    // Long-event phase
    metaGame.done(0);
    // Movement/hazard phase
    ASSERT_TRUE(std::holds_alternative<DrawCards>(game.getContext()));
    EXPECT_EQ(metaGame.drawCard(0), Success);
    EXPECT_EQ(metaGame.drawCard(0), Success);
    EXPECT_EQ(metaGame.drawCard(0), CardLimitReached);
    EXPECT_EQ(metaGame.drawCard(1), Success);
    EXPECT_EQ(metaGame.drawCard(1), Success);
    EXPECT_EQ(metaGame.drawCard(1), CardLimitReached);
    metaGame.done(1);
    ASSERT_TRUE(std::holds_alternative<Movement>(game.getContext()));
    EXPECT_EQ(metaGame.placeOnGuard(0, lesserSpiders), WrongPlayer);
    ASSERT_EQ(metaGame.placeOnGuard(1, lesserSpiders), Success);
    metaGame.done(1);
    ASSERT_TRUE(std::holds_alternative<ReturnToHandSize>(game.getContext()));
    metaGame.done(0);
    metaGame.done(1);
    // Site phase
    ASSERT_TRUE(std::holds_alternative<Explore>(game.getContext()));
    ASSERT_EQ(metaGame.enterSite(0), Success);
    ASSERT_EQ(std::get<Explore>(game.getContext()).state, Explore::RevealOnGuard);
    auto actions = metaGame.getActions(1, lesserSpiders);
    ASSERT_FALSE(actions.empty());
    ASSERT_EQ(metaGame.useAction(1, actions[0].id), Success);
    metaGame.done(0);
    ASSERT_TRUE(std::holds_alternative<Attack>(game.getContext())); {
        Attack & attack = std::get<Attack>(game.getContext());
        EXPECT_EQ(attack.creatureType, Dragon);
        EXPECT_EQ(attack.strikes, 1);
        EXPECT_EQ(attack.prowess, 12);
        ASSERT_EQ(metaGame.selectCard(0, kili), Success);
        ASSERT_EQ(metaGame.useAction(0, metaGame.getActions(0, concealment)[0].id), Success);
        metaGame.done(0);
    }
    ASSERT_TRUE(std::holds_alternative<Attack>(game.getContext())); {
        Attack & attack = std::get<Attack>(game.getContext());
        EXPECT_EQ(attack.creatureType, Spider);
        EXPECT_EQ(attack.strikes, 4);
        EXPECT_EQ(attack.prowess, 7);
        metaGame.done(0);
        CardID fightOrder[] = { dori, oin, thorin, gloin };
        for (CardID target : fightOrder)
            ASSERT_EQ(metaGame.assignStrike(0, target), Success);
        metaGame.done(0);
        for (CardID target : fightOrder) {
            ASSERT_TRUE(resolveStrike(metaGame, target)
                .taps()
                .attackRoll(5)
                .verifyStrikeFail()
            );
        }
    }
    ASSERT_TRUE(std::holds_alternative<Explore>(game.getContext()));
    EXPECT_EQ(std::get<Explore>(game.getContext()).state, Explore::ExploreSite);
    EXPECT_EQ(game.getCurrentSide().kills.back(), lesserSpiders);
}

TEST_F(PlayHazardCreatureFixture, ReturnToHandSize) {
    Game & game = metaGame.gameStates.back();
    game.turnSequence.setPhase(EndOfTurnPhase, Beginning);
    auto & playerHand = game.getCurrentSide().hand;
    auto & hazardHand = game.sides[game.hazardPlayer].hand;
    {
        Scene scene = getCurrentScene(game);
        while (playerHand.size() < 7)
            scene.drawCard(0);
        while (hazardHand.size() < 10)
            scene.drawCard(1);
    }
    metaGame.done(0);
    Scene scene = getCurrentScene(game);
    ASSERT_TRUE(std::holds_alternative<ReturnToHandSize>(scene.context->context));
    ReturnToHandSize & context = std::get<ReturnToHandSize>(scene.context->context);
    EXPECT_EQ(context.resourcePlayerHandSize, 8);
    EXPECT_EQ(context.hazardPlayerHandSize, 8);
    EXPECT_TRUE(context.resourcePlayerMayDiscard);
    EXPECT_FALSE(context.hazardPlayerMayDiscard);
    EXPECT_EQ(playerHand.size(), 7);
    EXPECT_EQ(hazardHand.size(), 10);
    EXPECT_EQ(metaGame.discard(0, { playerHand[4], playerHand[5] }), WrongNumberOfCards);
    EXPECT_EQ(metaGame.discard(1, { hazardHand.back(), hazardHand.back() }), Success);
    EXPECT_EQ(hazardHand.size(), 9);
    EXPECT_EQ(metaGame.discard(0, { playerHand[4] }), Success);
    EXPECT_EQ(playerHand.size(), 8);
    EXPECT_EQ(metaGame.discard(1, { hazardHand.front() }), Success);
    EXPECT_TRUE(std::holds_alternative<OrganizeCompanies>(game.getContext()));
    EXPECT_EQ(game.turnSequence, TurnSequence(OrganizationPhase, Middle));
    EXPECT_EQ(game.currentPlayer, 1);
    EXPECT_EQ(game.hazardPlayer, 0);
}

TEST_F(DwarfDeckFixture, UntapPhaseTest) {
    Game & game = metaGame.gameStates.back();
    game.turnSequence.setPhase(OrganizationPhase, Beginning);
    metaGame.done(0);
    ASSERT_EQ(metaGame.takeFollower(0, gloin, kili), Success);
    ASSERT_EQ(metaGame.transferCharacter(0, gloin, -1), Success);
    PlayerConfigurer pc = configurer.currentPlayerConfiguerer();
    pc.setCurrentLocation(1, getLocationID(game, 0, "Blue Mountain Dwarf-hold"));
    {
        Scene scene = getCurrentScene(game);
        scene.tap(gloin);
        scene.wound(kili);
        scene.wound(thorin);
        scene.tap(dori);
    }
    resetContextStack(game);
    game.turnSequence.setPhase(UntapPhase, Beginning);
    EXPECT_EQ(game.getCardState(thorin), Wounded);
    EXPECT_EQ(game.getCardState(oin), Untapped);
    EXPECT_EQ(game.getCardState(dori), Tapped);
    EXPECT_EQ(game.getCardState(gloin), Tapped);
    EXPECT_EQ(game.getCardState(kili), Wounded);
    metaGame.done(0);
    EXPECT_EQ(game.getCardState(thorin), Tapped);
    EXPECT_EQ(game.getCardState(oin), Untapped);
    EXPECT_EQ(game.getCardState(dori), Untapped);
    EXPECT_EQ(game.getCardState(gloin), Untapped);
    EXPECT_EQ(game.getCardState(kili), Wounded);
}

TEST_F(DwarfDeckFixture, EndOrganizationPhase) {
    Game & game = metaGame.gameStates.back();
    game.turnSequence.setPhase(OrganizationPhase, Beginning);
    metaGame.done(0);
    CardID balin = getCardID(game, 0, "Balin");
    configurer.drawCard(balin);
    ASSERT_EQ(metaGame.takeFollower(0, gloin, dori), Success);
    metaGame.selectCompany(0, 0);
    std::vector<ActionInfo> actions = metaGame.getActions(0, balin);
    ASSERT_EQ(actions.size(), 1);
    ASSERT_EQ(metaGame.useAction(0, actions[0].id), Success);
    metaGame.done(0);
    Scene scene = getCurrentScene(game);
    EXPECT_EQ(scene.getUnusedGI(0), -4);
    ASSERT_EQ(metaGame.takeFollower(0, balin, oin), Success);
    EXPECT_TRUE(game.cards[balin].possessions.empty());
    CardID cram = game.cards[thorin].possessions.back();
    ASSERT_EQ(metaGame.transferItem(0, cram, balin), Success);
    metaGame.done(0);
    EXPECT_TRUE(game.cards[thorin].possessions.empty());
    ASSERT_FALSE(game.cards[balin].possessions.empty());
    EXPECT_EQ(game.cards[balin].possessions.back(), cram);
    EXPECT_EQ(scene.getUnusedGI(0), -1);
    metaGame.done(0);
    ASSERT_TRUE(std::holds_alternative<BottomContext>(game.getContext()));
    EXPECT_EQ(game.turnSequence, TurnSequence(OrganizationPhase, End));
    Side & side = game.getCurrentSide();
    ASSERT_FALSE(side.discardPile.empty());
    EXPECT_EQ(side.discardPile.back(), cram);
    EXPECT_EQ(side.hand.back(), balin);
    EXPECT_EQ(game.getDeck(oin), Characters);
    EXPECT_EQ(game.getParent(oin), -1);
    EXPECT_EQ(scene.getUnusedGI(0), 1);
    game.turnSequence.setPhase(OrganizationPhase, Beginning);
    game.characterBroughtIntoPlay = -1;
    metaGame.done(0);
    metaGame.selectCompany(0, 0);
    EXPECT_EQ(metaGame.getActions(0, balin).size(), 1);
}

TEST_F(DwarfDeckFixture, RemoveCorruptionCardTest) {
    Game & game = metaGame.gameStates.back();
    game.turnSequence.setPhase(OrganizationPhase, Beginning);
    metaGame.done(0);
    CardID despair = getCardID(game, 0, "Despair of the Heart");
    configurer.addPossession(thorin, despair);
    std::vector<ActionInfo> actions = metaGame.getActions(0, despair);
    ASSERT_EQ(actions.size(), 1);
    ASSERT_EQ(metaGame.useAction(0, actions[0].id), Success);
    metaGame.done(0);
    ASSERT_TRUE(std::holds_alternative<RollDice>(game.getContext()));
    Scene scene = getCurrentScene(game).parent();
    ASSERT_TRUE(std::holds_alternative<RemoveCorruptionCard>(scene.context->context));
    RemoveCorruptionCard removeCorruptionCard = std::get<RemoveCorruptionCard>(scene.context->context);
    EXPECT_EQ(game.getCardState(thorin), Tapped);
    EXPECT_EQ(removeCorruptionCard.target, despair);
    EXPECT_EQ(removeCorruptionCard.requiredNumber, 4);
    EXPECT_EQ(removeCorruptionCard.modifier, 0);
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        metaGame.roll(0, 4);
        metaGame.done(0);
        EXPECT_TRUE(std::holds_alternative<OrganizeCompanies>(game.getContext()));
        EXPECT_EQ(game.getDeck(despair), Possessions);
        metaGame.gameStates.pop_back();
    }
    metaGame.roll(0, 5);
    metaGame.done(0);
    EXPECT_TRUE(std::holds_alternative<OrganizeCompanies>(game.getContext()));
    EXPECT_EQ(game.getDeck(despair), DiscardPile);
}

TEST_F(InfluenceOpponentFixture, InfluenceCharacter) {
    Game & game = metaGame.gameStates.back();
    PlayerConfigurer pc = configurer.currentPlayerConfiguerer();
    CardID gloin2 = pc.addCardToDeck("Glóin", Hand);
    CardID oin2 = pc.addCardToDeck("Óin", Hand);
    EXPECT_EQ(metaGame.influenceOpponent(1, balin, gloin), WrongState);
    EXPECT_EQ(metaGame.enterSite(1), Success);
    EXPECT_EQ(metaGame.influenceOpponent(1, balin, boromir), InvalidTarget);
    EXPECT_EQ(metaGame.influenceOpponent(1, balin, fili), WrongLocation);
    EXPECT_EQ(metaGame.influenceOpponent(1, balin, gloin), Success);
    EXPECT_EQ(game.getCardState(balin), Tapped);
    ASSERT_TRUE(std::holds_alternative<InfluenceAttempt>(game.getContext()));
    InfluenceAttempt & influence = std::get<InfluenceAttempt>(game.getContext());
    EXPECT_EQ(influence.requiredInfluence, 5);
    EXPECT_EQ(influence.modifier, 3 - 4);
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        metaGame.done(1);
        metaGame.roll(1, 9);
        metaGame.done(1);
        metaGame.cloneState(); {
            Game & game = metaGame.gameStates.back();
            metaGame.roll(0, 3);
            metaGame.done(0);
            EXPECT_TRUE(std::holds_alternative<Explore>(game.getContext()));
            EXPECT_EQ(game.getDeck(gloin), Characters);
            metaGame.gameStates.pop_back();
        }
        metaGame.roll(0, 2);
        metaGame.done(0);
        EXPECT_TRUE(std::holds_alternative<Explore>(game.getContext()));
        EXPECT_EQ(game.getDeck(gloin), DiscardPile);
        EXPECT_EQ(game.getDeck(kili), Characters);
        EXPECT_EQ(game.getParent(kili), HadParent);
        EXPECT_EQ(getCurrentScene(game).getUnusedGI(0), 9);
        EXPECT_EQ(metaGame.influenceOpponent(1, boromir, oin), AlreadyInfluencedOpponent);
        metaGame.gameStates.pop_back();
    }
    EXPECT_EQ(metaGame.revealIdenticalCard(1, oin2), NotAManifestation);
    EXPECT_EQ(metaGame.revealIdenticalCard(1, gloin2), Success);
    EXPECT_EQ(influence.requiredInfluence, 0);
    EXPECT_EQ(influence.modifier, -1);
    metaGame.done(1);
    metaGame.roll(1, 4);
    metaGame.done(1);
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        metaGame.roll(0, 3);
        metaGame.done(0);
        EXPECT_TRUE(std::holds_alternative<Explore>(game.getContext()));
        EXPECT_EQ(game.getDeck(gloin), Characters);
        EXPECT_EQ(game.getDeck(gloin2), DiscardPile);
        metaGame.gameStates.pop_back();
    }
    metaGame.roll(0, 2);
    metaGame.done(0);
    ASSERT_TRUE(std::holds_alternative<InfluenceAttempt>(game.getContext()));
    ASSERT_EQ(influence.state, InfluenceAttempt::Take);
    EXPECT_EQ(game.getDeck(gloin), DiscardPile);
    EXPECT_EQ(game.getDeck(gloin2), Pending);
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        metaGame.done(1);
        EXPECT_TRUE(std::holds_alternative<Explore>(game.getContext()));
        EXPECT_EQ(game.getDeck(gloin2), DiscardPile);
        metaGame.gameStates.pop_back();
    }
    EXPECT_EQ(metaGame.takeInfluencedCard(1, balin), InsufficientInfluence);
    EXPECT_EQ(metaGame.takeInfluencedCard(1), Success);
    EXPECT_EQ(game.getDeck(gloin2), Characters);
}

TEST_F(InfluenceOpponentFixture, InfluenceFollower) {
    Game & game = metaGame.gameStates.back();
    PlayerConfigurer pc = configurer.currentPlayerConfiguerer();
    CardID oin2 = pc.addCardToDeck("Óin", Hand);
    EXPECT_EQ(metaGame.enterSite(1), Success);
    EXPECT_EQ(metaGame.influenceOpponent(1, balin, oin), Success);
    ASSERT_TRUE(std::holds_alternative<InfluenceAttempt>(game.getContext()));
    InfluenceAttempt & influence = std::get<InfluenceAttempt>(game.getContext());
    EXPECT_EQ(influence.requiredInfluence, 3);
    EXPECT_EQ(influence.modifier, 3 - 4 - 1); // DI - unused GI - unused DI of controlling character
    EXPECT_EQ(metaGame.revealIdenticalCard(1, oin2), Success);
    EXPECT_EQ(influence.requiredInfluence, 0);
}

TEST_F(InfluenceOpponentFixture, InfluenceAlly) {
    Game & game = metaGame.gameStates.back();
    PlayerConfigurer pc = configurer.currentPlayerConfiguerer();
    CardID quickbeam2 = pc.addCardToDeck("Quickbeam", Hand);
    EXPECT_EQ(metaGame.enterSite(1), Success);
    EXPECT_EQ(metaGame.influenceOpponent(1, balin, quickbeam), Success);
    ASSERT_TRUE(std::holds_alternative<InfluenceAttempt>(game.getContext()));
    InfluenceAttempt & influence = std::get<InfluenceAttempt>(game.getContext());
    EXPECT_EQ(influence.requiredInfluence, 3);
    EXPECT_EQ(influence.modifier, 2 - 4 - 4);
    EXPECT_EQ(metaGame.revealIdenticalCard(1, quickbeam2), Success);
    EXPECT_EQ(influence.requiredInfluence, 0);
    metaGame.done(1);
    metaGame.roll(1, 10);
    metaGame.done(1);
    metaGame.roll(0, 3);
    metaGame.done(0);
    ASSERT_TRUE(std::holds_alternative<InfluenceAttempt>(game.getContext()));
    ASSERT_EQ(influence.state, InfluenceAttempt::Take);
    EXPECT_EQ(game.getDeck(quickbeam), DiscardPile);
    EXPECT_EQ(game.getDeck(quickbeam2), Pending);
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        metaGame.done(1);
        EXPECT_TRUE(std::holds_alternative<Explore>(game.getContext()));
        EXPECT_EQ(game.getDeck(quickbeam2), DiscardPile);
        metaGame.gameStates.pop_back();
    }
    EXPECT_EQ(metaGame.takeInfluencedCard(1), Success);
    EXPECT_TRUE(std::holds_alternative<Explore>(game.getContext()));
    Card & cardData = game.cards[quickbeam2];
    EXPECT_EQ(cardData.deck, Possessions);
    EXPECT_EQ(cardData.parent, balin);
    EXPECT_TRUE(cardData.faceUp);
    EXPECT_EQ(game.cards[balin].possessions.back(), quickbeam2);
}

TEST_F(InfluenceOpponentFixture, InfluenceItem) {
    Game & game = metaGame.gameStates.back();
    PlayerConfigurer pc = configurer.currentPlayerConfiguerer();
    CardID durinsAxe2 = pc.addCardToDeck("Durin's Axe", Hand);
    CardID book2 = pc.addCardToDeck("Book of Mazarbul", Hand);
    EXPECT_EQ(metaGame.enterSite(1), Success);
    EXPECT_EQ(metaGame.influenceOpponent(1, balin, bookOfMazarbul), InvalidTarget);
    EXPECT_EQ(metaGame.influenceOpponent(1, balin, durinsAxe), Success);
    ASSERT_TRUE(std::holds_alternative<InfluenceAttempt>(game.getContext()));
    InfluenceAttempt & influence = std::get<InfluenceAttempt>(game.getContext());
    EXPECT_EQ(influence.requiredInfluence, 8);
    EXPECT_EQ(influence.modifier, 3 - 4 - 1);
    EXPECT_EQ(metaGame.revealIdenticalCard(1, book2), NotAManifestation);
    EXPECT_EQ(metaGame.revealIdenticalCard(1, durinsAxe2), Success);
    EXPECT_EQ(influence.requiredInfluence, 0);
    metaGame.done(1);
    metaGame.roll(1, 6);
    metaGame.done(1);
    metaGame.roll(0, 3);
    metaGame.done(0);
    EXPECT_EQ(metaGame.takeInfluencedCard(1), Success);
    EXPECT_TRUE(std::holds_alternative<Explore>(game.getContext()));
    Card & cardData = game.cards[durinsAxe2];
    EXPECT_EQ(cardData.deck, Possessions);
    EXPECT_EQ(cardData.parent, balin);
    EXPECT_TRUE(cardData.faceUp);
    EXPECT_EQ(game.cards[balin].possessions.back(), durinsAxe2);
    EXPECT_EQ(getCurrentScene(game).getProwess(balin, Orc, 0), 10);
}

TEST_F(InfluenceOpponentFixture, InfluenceFaction) {
    Game & game = metaGame.gameStates.back();
    PlayerConfigurer pc = configurer.currentPlayerConfiguerer();
    CardID blueMountainDwarves2 = pc.addCardToDeck("Blue Mountain Dwarves", Hand);
    EXPECT_EQ(metaGame.enterSite(1), Success);
    EXPECT_EQ(metaGame.influenceOpponent(1, balin, blueMountainDwarves), WrongLocation);
    pc.setCurrentLocation(0, pc.addCardToDeck("Blue Mountain Dwarf-hold", LocationDeck));
    EXPECT_EQ(metaGame.influenceOpponent(1, balin, blueMountainDwarves), Success);
    ASSERT_TRUE(std::holds_alternative<InfluenceAttempt>(game.getContext()));
    InfluenceAttempt & influence = std::get<InfluenceAttempt>(game.getContext());
    EXPECT_EQ(influence.requiredInfluence, 9);
    EXPECT_EQ(influence.modifier, 5 - 4);
    EXPECT_EQ(metaGame.revealIdenticalCard(1, blueMountainDwarves2), Success);
    metaGame.done(1);
    metaGame.roll(1, 2);
    metaGame.done(1);
    metaGame.cloneState(); {
        Game & game = metaGame.gameStates.back();
        metaGame.roll(0, 3);
        metaGame.done(0);
        EXPECT_TRUE(std::holds_alternative<Explore>(game.getContext()));
        EXPECT_EQ(game.getDeck(blueMountainDwarves), Factions);
        EXPECT_EQ(game.getDeck(blueMountainDwarves2), DiscardPile);
        EXPECT_EQ(metaGame.influenceOpponent(1, boromir, blueMountainDwarves), AlreadyInfluencedOpponent);
        metaGame.gameStates.pop_back();
    }
    metaGame.roll(0, 2);
    metaGame.done(0);
    EXPECT_TRUE(std::holds_alternative<Explore>(game.getContext()));
    EXPECT_EQ(game.getDeck(blueMountainDwarves), DiscardPile);
    EXPECT_EQ(game.getDeck(blueMountainDwarves2), Factions);
}
