#pragma once
#include "../Middle-Earth/ContextInterface.h"
#include "../Middle-Earth/ErrorCodes.h"
#include "../Middle-Earth/GameConfigurer.h"
#include "../Middle-Earth/GameInterface.h"
#include <gtest/gtest.h>
#include <iterator>
#include <unordered_set>

class TestPlayer {
public:
    TestPlayer(MetaGame & metaGame, int player)
        : metaGame(metaGame)
        , player(player)
    { ; }

    int selectDestination(CompanyID id, CardID destination);
    int transferCharacter(CardID target, CompanyID destination = -1);
    int takeFollower(CardID leader, CardID follower);
    int dropFollower(CardID follower);
    int chooseCompany(CompanyID id);
    int discard(const std::vector<CardID> & cards);
    int enterSite();
    int selectCompany(CompanyID id);
    int selectCard(CardID card);
    int setKey(KeyedToType key);
    int useAction(CardID card, size_t option = 0);
    int useActionOnCard(CardID card, CardID selectedCard, size_t option = 0);
    int useActionOnCards(CardID card, const std::vector<CardID> & cards, size_t option = 0);
    int useActionOnCompany(CardID card, CompanyID id, size_t option = 0);

    int resolveCorruptionCheck(CardID id);
    int assignStrike(CardID target);
    int tap(CardID card);

    int roll(int result = 0);
    int done();
    int drawCard();

    operator int() { return player; }

private:
    MetaGame & metaGame;
    int player;
};

//ASSERT_TRUE(resolveStrike(metagame, thorin)
//    .taps()
//    .support({ kili, quickbeam })
//    .playCard(mainPlayer, luckyStrike)
//    .attackRoll(2)
//    .attackRoll(3)
//    .defendBodyCheck(12)
//    .verifyStrikeFail()
//);
class StrikeSimulator {
public:
    StrikeSimulator(MetaGame & metaGame)
        : metaGame(metaGame)
        , assertionResult(testing::AssertionSuccess())
        , strikeContext(getGameStrikeContext())
        , player(metaGame, metaGame.gameStates.back().currentPlayer)
    { ; }

    operator testing::AssertionResult() const;
    StrikeSimulator & selectStrike(CardID target);
    StrikeSimulator & taps();
    StrikeSimulator & staysInState(CardState state);
    StrikeSimulator & support(std::vector<CardID> cards);
    StrikeSimulator & playCard(TestPlayer player, CardID card, int option = 0, CardID selectedCard = -1);
    StrikeSimulator & attackRoll(int result);
    StrikeSimulator & defendBodyCheck(int result);
    StrikeSimulator & verifyTargetProwess(int prowess);
    StrikeSimulator & verifyTargetBody(int body);
    StrikeSimulator & verifyCreatureProwess(int prowess);
    StrikeSimulator & verifyStrikeFail();
    StrikeSimulator & verifyStrikeSuccess();
    StrikeSimulator & verifyStrikeIneffectual();

private:
    bool skipToState(Strike & strike, Strike::States state);
    std::string cardName(CardID card);
    std::string outcomeName(Outcome outcome);
    std::string cardStateName(CardState state);
    std::shared_ptr<Context> getGameStrikeContext();
    Outcome outcome();
    StrikeSimulator & expectedContext(const std::string & requiredContext);
    StrikeSimulator & assertionFail(CardID card, const std::string & message);

    MetaGame & metaGame;
    testing::AssertionResult assertionResult;
    std::string currentFunction;
    std::shared_ptr<Context> strikeContext;
    TestPlayer player;
};

StrikeSimulator resolveStrike(MetaGame & metaGame, CardID target);

class RegionSelector {
public:
    RegionSelector(MetaGame & metaGame, int player, CompanyID id);

    RegionSelector selectDestination(CardID destination, int expected = Success);
    RegionSelector selectDestination(const std::string & destinationName, int expected = Success);
    RegionSelector resetDestination();
    RegionSelector insertRegion(RegionNumber region, int expected = Success);
    RegionSelector popRegion();
    RegionSelector verifyAdjacentRegions(const std::unordered_set<RegionNumber> & expectedResult);

private:
    RegionSelector verify(int code, int expected = Success);

    MetaGame & metaGame;
    const int player;
    Company & company;
    testing::AssertionResult assertionResult;
    std::vector<RegionNumber> regions;
    std::unordered_set<RegionNumber> adjacentRegions;
};

void resetContextStack(Game & game);
bool isPlayable(MetaGame & metaGame, CardID card, CardID location, CardID recipient);
bool isTargetable(MetaGame & metaGame, CardID target, KeyedToType key, CardID creature);
bool useAction(MetaGame & metaGame, CardID card, int option = 0, int player = -1);
void skipReturnToHandSize(MetaGame & metaGame);
void skipDrawCards(MetaGame & metaGame);
void skipToMovementHazardPhase(MetaGame & metaGame, CompanyID id = -1);
void skipToSitePhase(MetaGame & metaGame, CompanyID id = -1);
void skipAutomaticAttack(MetaGame & metaGame);
void skipTurn(MetaGame & metaGame);
void printState(Game & game);
