#pragma once
#include "nlohmann/json.hpp"
#include <unordered_map>

enum DeckType { HeroDeck, MinionDeck, BalrogDeck };

struct StartingCharacter {
    StartingCharacter() = default;
    StartingCharacter(std::string character, std::string item = "")
        : character(character)
        , startingItem(item)
    {}
    std::string character;
    std::string startingItem = "";

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(StartingCharacter, character, startingItem)
};

struct StartingPermanentEvent {
    std::string permanentEvent;
    bool inCompany;

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(StartingPermanentEvent, permanentEvent, inCompany)
};

struct StartingCompany {
    std::string startingLocation;
    std::vector<StartingCharacter> startingCharacters;
    std::vector<StartingPermanentEvent> startingPermanentEvents;

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(StartingCompany, startingLocation, startingCharacters, startingPermanentEvents)
};

struct NameNumberParam {
    NameNumberParam() = default;
    NameNumberParam(std::string name, int n = 1) : name(name), n(n) { ; }
    std::string name;
    int n = 1;

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(NameNumberParam, name, n)
};

using DeckDescription = std::vector<NameNumberParam>;

struct DeckDefinition {
    DeckType deckType;
    DeckDescription playDeck;
    DeckDescription locationDeck;
    DeckDescription sideboard;
    StartingCompany startingCompany;

    NLOHMANN_DEFINE_TYPE_INTRUSIVE(DeckDefinition, deckType, playDeck, locationDeck, sideboard, startingCompany)
};

std::unordered_map<std::string, DeckDefinition> loadDeckDefinitions();
