#pragma once

enum class CardSet { CoreCards, DwarfDeck, OrcDeck };
