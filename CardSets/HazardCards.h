#pragma once
#include "../Middle-Earth/BaseClasses.h"

/* Foolish Words */
struct FoolishWordsEffect : public EquipmentEffect {
    FoolishWordsEffect(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        // Affects only influence attempts at the moment
        if (!std::holds_alternative<Modifier>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<InfluenceAttempt>(context))
            return false;
        InfluenceAttempt & influenceAttempt = std::get<InfluenceAttempt>(context);
        if (influenceAttempt.influencingCharacter != batch.scene.game.getParent(host))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<Modifier>(batch.et).modifier -= 4;
    }
};
struct PlayFoolishWords : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.hazardConditions(host))
            return false;
        if (args.empty())
            return false;
        CardID target = args.front();
        Game & game = scene.game;
        if (game.cards[target].owner != game.currentPlayer)
            return false;
        CardType & type = game.getCardType(target);
        if (!std::holds_alternative<Character>(type))
            return false;
        if (game.duplicatePossession(target, host))
            return false;
        if (game.turnSequence.phase == SitePhase) {
            // On-guard
            ContextType & context = scene.context->context;
            if (!std::holds_alternative<InfluenceAttempt>(context))
                return false;
            InfluenceAttempt & influence = std::get<InfluenceAttempt>(context);
            if (influence.state != InfluenceAttempt::Start)
                return false;
            if (influence.influencingCharacter != target)
                return false;
        }
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Side & side = scene.game.getCurrentSide();
        scene.gainPossession(args.front(), host);
        insertPatches(scene.game);
        ContextType & context = scene.context->context;
        if (std::holds_alternative<InfluenceAttempt>(context))
            std::get<InfluenceAttempt>(context).modifier -= 4;
    }
    void insertPatches(Game & game) const {
        Side & side = game.getSide(game.getParent(host));
        side.effects.push_back(std::make_shared<FoolishWordsEffect>(host));
        side.actions.add(std::make_shared<RemoveCorruptionCardAction>(7), host);
    }
};
struct FoolishWords : public CardDefinition {
    FoolishWords() : CardDefinition(HazardEvent()) {
        HazardEvent & def = std::get<HazardEvent>(type);
        def.name.push_back("Foolish Words");
        def.type = PermanentEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayFoolishWords>(), host);
    }
};

/* Twilight */
struct TwilightHazardLimitEffect : public PlayCardEffect {
    TwilightHazardLimitEffect(CardID host) : PlayCardEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        // Twilight never counts against the hazard limit (MELE, p.11, top)
        if (!std::holds_alternative<HazardLimitCost>(batch.et))
            return false;
        return std::get<HazardLimitCost>(batch.et).target == host;
    }
    void activate(EffectBatch & batch) const {
        std::get<HazardLimitCost>(batch.et).cost = 0;
    }
};
struct PlayTwilight : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (args.empty())
            return false;
        CardID target = args.front();
        if (!scene.hasKeyword(target, Environment))
            return false;
        Card & card = scene.game.cards[target];
        if (card.deck <= OutOfPlay || !card.faceUp)   // Card must be in play
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.discard(args.front());
        scene.discard(host);
    }
};
struct Twilight : public CardDefinition {
    Twilight() : CardDefinition(HazardEvent()) {
        HazardEvent & def = std::get<HazardEvent>(type);
        def.name.push_back("Twilight");
        def.type = ShortEvent;
        def.keywords = { Environment };
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayTwilight>(), host);
    }
    void getEffects(EffectList & effects, CardID host) {
        effects.push_back(std::make_shared<TwilightHazardLimitEffect>(host));
    }
};

/* Ghosts */
struct GhostsEffect : public Effect {
    GhostsEffect(CardID host)
        : Effect(host), done(false) {}
    bool conditions(EffectBatch & batch) const {
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<CloseContext>(batch.et))
            return false;
        if (std::holds_alternative<BodyCheck>(context)) {
            BodyCheck & bodyCheck = std::get<BodyCheck>(context);
            if (bodyCheck.target == host)
                return false;
            if (bodyCheck.roll + bodyCheck.modifier > bodyCheck.body)
                return false;
            return true;
        }
        else if (std::holds_alternative<Attack>(context)) {
            Attack & attack = std::get<Attack>(context);
            if (attack.card != host)
                return false;
            return true;
        }
        return false;
    }
    void activate(EffectBatch & batch) const {
        auto effect = std::make_shared<GhostsEffect>(*this);
        batch.effectQueue.front()->effect = effect;
        if (std::holds_alternative<BodyCheck>(batch.getContext()))
            effect->wounded.push_back(std::get<BodyCheck>(batch.getContext()).target);
        else {
            effect->done = true;
            ResolveCorruptionChecks resolve;
            if (wounded.empty())
                return;
            for (CardID target : wounded)
                resolve.unresolvedChecks.emplace_back(target, target, -1);
            batch.scene.game.contextStack.push_back(std::make_shared<Context>(resolve));
        }
    }
    bool remove(EffectBatch & batch) const {
        return done;
    }
    std::vector<CardID> wounded;
    bool done;
};
struct PlayGhosts : public PlayHazardCreature {
    void insertEffects(Scene scene, const std::list<CardID> & args) const {
        scene.game.effects.push_back(std::make_shared<GhostsEffect>(host));
    }
};
struct Ghosts : public CardDefinition {
    Ghosts() : CardDefinition(HazardCreature()) {
        HazardCreature & def = std::get<HazardCreature>(type);
        def.name.push_back("Ghosts");
        def.playable = { ShadowLands, DarkDomain, ShadowHold, DarkHold };
        def.race = Undead;
        def.strikes = 3;
        def.prowess = 9;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayGhosts>(), host);
    }
};
