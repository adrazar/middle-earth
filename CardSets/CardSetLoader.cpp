#include "CardSetLoader.h"
#include "CoreCards.h"
#include "DwarfDeck.h"
#include "HazardCards.h"
#include "OrcDeck.h"

void loadCardSet(GameSettings & settings, CardSet cardSet) {
    for (CardSet & loadedCardSet : settings.loadedCardSets)
        if (loadedCardSet == cardSet)
            return;
    switch (cardSet) {
    case CardSet::CoreCards:
        addCoreCardDefinitions(settings);
        break;
    case CardSet::DwarfDeck:
        addDwarfDeckDefinitions(settings);
        break;
    case CardSet::OrcDeck:
        addOrcDeckDefinitions(settings);
        break;
    default:
        return;
    }
    settings.loadedCardSets.push_back(cardSet);
}
