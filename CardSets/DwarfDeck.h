#pragma once
#include "DeckDefinition.h"
#include "../Middle-Earth/GameSettings.h"

void addDwarfDeckDefinitions(GameSettings & settings);
void addDwarfDeck(DeckDefinition & deck);
