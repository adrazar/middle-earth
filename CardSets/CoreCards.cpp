#include "CoreCards.h"
#include "../Middle-Earth/BaseClasses.h"
#include "../Middle-Earth/Middle-Earth.h"

/*
 *  The first elements of Game::cardDefintions are assumed to contain the following
 *  card definitions in fixed order:
 *
 *   0 - 51  ->  Region cards
 *  52 - 55  ->  Havens
 *  56 - 59  ->  Darkhavens
 */

struct LindonRegion : RegionDefinition {
    LindonRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Lindon";
        region.number = Lindon;
        region.type = FreeDomain;
        region.adjacentRegions = { Arthedain, ElvenShores, Numeriador };
    }
};
struct NumeriadorRegion : RegionDefinition {
    NumeriadorRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Númeriador";
        region.number = Numeriador;
        region.type = Wilderness;
        region.adjacentRegions = { Arthedain, Forochel, Lindon };
    }
};
struct ForochelRegion : RegionDefinition {
    ForochelRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Forochel";
        region.number = Forochel;
        region.type = Wilderness;
        region.adjacentRegions = { Angmar, Arthedain, Numeriador };
    }
};
struct ArthedainRegion : RegionDefinition {
    ArthedainRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Arthedain";
        region.number = Arthedain;
        region.type = Wilderness;
        region.adjacentRegions = { Angmar, Cardolan, Forochel, Lindon, Numeriador, Rhudaur, TheShire };
    }
};
struct TheShireRegion : RegionDefinition {
    TheShireRegion() {
        Region & region = std::get<Region>(type);
        region.name = "The Shire";
        region.number = TheShire;
        region.type = FreeDomain;
        region.adjacentRegions = { Arthedain, Cardolan };
    }
};
struct CardolanRegion : RegionDefinition {
    CardolanRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Cardolan";
        region.number = Cardolan;
        region.type = Wilderness;
        region.adjacentRegions = { Arthedain, Dunland, Enedhwaith, EriadoranCoast, Hollin, Rhudaur, TheShire };
    }
};
struct AngmarRegion : RegionDefinition {
    AngmarRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Angmar";
        region.number = Angmar;
        region.type = ShadowLands;
        region.adjacentRegions = { Arthedain, Forochel, Gundabad, Rhudaur };
    }
};
struct RhudaurRegion : RegionDefinition {
    RhudaurRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Rhudaur";
        region.number = Rhudaur;
        region.type = Wilderness;
        region.adjacentRegions = { Angmar, Arthedain, Cardolan, HighPass, Hollin };
    }
};
struct HollinRegion : RegionDefinition {
    HollinRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Hollin";
        region.number = Hollin;
        region.type = Wilderness;
        region.adjacentRegions = { Cardolan, Dunland, RedhornGate, Rhudaur };
    }
};
struct DunlandRegion : RegionDefinition {
    DunlandRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Dunland";
        region.number = Dunland;
        region.type = Wilderness;
        region.adjacentRegions = { Cardolan, Enedhwaith, Hollin };
    }
};
struct EnedhwaithRegion : RegionDefinition {
    EnedhwaithRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Enedhwaith";
        region.number = Enedhwaith;
        region.type = Wilderness;
        region.adjacentRegions = { Cardolan, Dunland, EriadoranCoast, GapOfIsen, OldPukelLand };
    }
};
struct OldPukelLandRegion : RegionDefinition {
    OldPukelLandRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Old Pûkel-land";
        region.number = OldPukelLand;
        region.type = Wilderness;
        region.adjacentRegions = { Andrast, Enedhwaith, EriadoranCoast, GapOfIsen, OldPukelGap };
    }
};
struct GundabadRegion : RegionDefinition {
    GundabadRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Gundabad";
        region.number = Gundabad;
        region.type = DarkDomain;
        region.adjacentRegions = { AnduinVales, Angmar };
    }
};
struct HighPassRegion : RegionDefinition {
    HighPassRegion() {
        Region & region = std::get<Region>(type);
        region.name = "High Pass";
        region.number = HighPass;
        region.type = Wilderness;
        region.adjacentRegions = { AnduinVales, Rhudaur };
    }
};
struct RedhornGateRegion : RegionDefinition {
    RedhornGateRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Redhorn Gate";
        region.number = RedhornGate;
        region.type = Wilderness;
        region.adjacentRegions = { Hollin, WoldAndFoothills };
    }
};
struct GapOfIsenRegion : RegionDefinition {
    GapOfIsenRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Gap of Isen";
        region.number = GapOfIsen;
        region.type = BorderLands;
        region.adjacentRegions = { Enedhwaith, Fangorn, OldPukelLand, Rohan };
    }
};
struct AnduinValesRegion : RegionDefinition {
    AnduinValesRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Anduin Vales";
        region.number = AnduinVales;
        region.type = BorderLands;
        region.adjacentRegions = { BrownLands, GreyMountainNarrows, Gundabad, HighPass, SouthernMirkwood,
            WesternMirkwood, WoldAndFoothills, WoodlandRealm };
    }
};
struct WoldAndFoothillsRegion : RegionDefinition {
    WoldAndFoothillsRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Wold & Foothills";
        region.number = WoldAndFoothills;
        region.type = Wilderness;
        region.adjacentRegions = { AnduinVales, BrownLands, Fangorn, RedhornGate, Rohan };
    }
};
struct FangornRegion : RegionDefinition {
    FangornRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Fangorn";
        region.number = Fangorn;
        region.type = Wilderness;
        region.adjacentRegions = { GapOfIsen, Rohan, WoldAndFoothills };
    }
};
struct RohanRegion : RegionDefinition {
    RohanRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Rohan";
        region.number = Rohan;
        region.type = BorderLands;
        region.adjacentRegions = { Anorien, Fangorn, GapOfIsen, WoldAndFoothills };
    }
};
struct GreyMountainNarrowsRegion : RegionDefinition {
    GreyMountainNarrowsRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Grey Mountain Narrows";
        region.number = GreyMountainNarrows;
        region.type = ShadowLands;
        region.adjacentRegions = { AnduinVales, NorthernRhovanion, WitheredHeath, WoodlandRealm };
    }
};
struct WoodlandRealmRegion : RegionDefinition {
    WoodlandRealmRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Woodland Realm";
        region.number = WoodlandRealm;
        region.type = BorderLands;
        region.adjacentRegions = { AnduinVales, HeartOfMirkwood, NorthernRhovanion, WesternMirkwood, GreyMountainNarrows };
    }
};
struct WesternMirkwoodRegion : RegionDefinition {
    WesternMirkwoodRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Western Mirkwood";
        region.number = WesternMirkwood;
        region.type = Wilderness;
        region.adjacentRegions = { AnduinVales, HeartOfMirkwood, SouthernMirkwood, WoodlandRealm };
    }
};
struct HeartOfMirkwoodRegion : RegionDefinition {
    HeartOfMirkwoodRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Heart of Mirkwood";
        region.number = HeartOfMirkwood;
        region.type = Wilderness;
        region.adjacentRegions = { NorthernRhovanion, SouthernMirkwood, SouthernRhovanion, WesternMirkwood, WoodlandRealm };
    }
};
struct SouthernMirkwoodRegion : RegionDefinition {
    SouthernMirkwoodRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Southern Mirkwood";
        region.number = SouthernMirkwood;
        region.type = DarkDomain;
        region.adjacentRegions = { AnduinVales, BrownLands, Dagorlad, HeartOfMirkwood, SouthernRhovanion, WesternMirkwood };
    }
};
struct BrownLandsRegion : RegionDefinition {
    BrownLandsRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Brown Lands";
        region.number = BrownLands;
        region.type = ShadowLands;
        region.adjacentRegions = { AnduinVales, Dagorlad, SouthernMirkwood, WoldAndFoothills };
    }
};
struct DagorladRegion : RegionDefinition {
    DagorladRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Dagorlad";
        region.number = Dagorlad;
        region.type = ShadowLands;
        region.adjacentRegions = { BrownLands, HorsePlains, Ithilien, SouthernMirkwood, SouthernRhovanion };
    }
};
struct WitheredHeathRegion : RegionDefinition {
    WitheredHeathRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Withered Heath";
        region.number = WitheredHeath;
        region.type = Wilderness;
        region.adjacentRegions = { IronHills, NorthernRhovanion, GreyMountainNarrows };
    }
};
struct NorthernRhovanionRegion : RegionDefinition {
    NorthernRhovanionRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Northern Rhovanion"; 
        region.number = NorthernRhovanion;
        region.type = Wilderness;
        region.adjacentRegions = { Dorwinion, HeartOfMirkwood, IronHills, SouthernRhovanion, WitheredHeath, WoodlandRealm, GreyMountainNarrows };
    }
};
struct SouthernRhovanionRegion : RegionDefinition {
    SouthernRhovanionRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Southern Rhovanion";
        region.number = SouthernRhovanion;
        region.type = Wilderness;
        region.adjacentRegions = { Dagorlad, Dorwinion, HeartOfMirkwood, HorsePlains, NorthernRhovanion, SouthernMirkwood };
    }
};
struct IronHillsRegion : RegionDefinition {
    IronHillsRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Iron Hills";
        region.number = IronHills;
        region.type = Wilderness;
        region.adjacentRegions = { NorthernRhovanion, WitheredHeath };
    }
};
struct DorwinionRegion : RegionDefinition {
    DorwinionRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Dorwinion";
        region.number = Dorwinion;
        region.type = BorderLands;
        region.adjacentRegions = { NorthernRhovanion, SouthernRhovanion };
    }
};
struct HorsePlainsRegion : RegionDefinition {
    HorsePlainsRegion() {
        Region & region = std::get<Region>(type);
        region.name = "HorsePlains";
        region.number = HorsePlains;
        region.type = ShadowLands;
        region.adjacentRegions = { Dagorlad, Nurn, SouthernRhovanion };
    }
};
struct OldPukelGapRegion : RegionDefinition {
    OldPukelGapRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Old Pûkel Gap";
        region.number = OldPukelGap;
        region.type = Wilderness;
        region.adjacentRegions = { Anfalas, OldPukelLand };
    }
};
struct AndrastRegion : RegionDefinition {
    AndrastRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Andrast";
        region.number = Andrast;
        region.type = Wilderness;
        region.adjacentRegions = { AndrastCoast, Anfalas, BayOfBelfalas, EriadoranCoast, OldPukelLand };
    }
};
struct AnfalasRegion : RegionDefinition {
    AnfalasRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Anfalas";
        region.number = Anfalas;
        region.type = Wilderness;
        region.adjacentRegions = { Andrast, BayOfBelfalas, Belfalas, Lamedon, OldPukelGap };
    }
};
struct LamedonRegion : RegionDefinition {
    LamedonRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Lamedon";
        region.number = Lamedon;
        region.type = BorderLands;
        region.adjacentRegions = { Anfalas, Belfalas, Lebennin };
    }
};
struct BelfalasRegion : RegionDefinition {
    BelfalasRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Belfalas";
        region.number = Belfalas;
        region.type = FreeDomain;
        region.adjacentRegions = { MouthsOfTheAnduin, Anfalas, BayOfBelfalas, Lamedon, Lebennin };
    }
};
struct LebenninRegion : RegionDefinition {
    LebenninRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Lebennin";
        region.number = Lebennin;
        region.type = FreeDomain;
        region.adjacentRegions = { MouthsOfTheAnduin, Anorien, Belfalas, Lamedon };
    }
};
struct AnorienRegion : RegionDefinition {
    AnorienRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Anórien";
        region.number = Anorien;
        region.type = FreeDomain;
        region.adjacentRegions = { Ithilien, Lebennin, Rohan };
    }
};
struct IthilienRegion : RegionDefinition {
    IthilienRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Ithilien";
        region.number = Ithilien;
        region.type = Wilderness;
        region.adjacentRegions = { Anorien, Dagorlad, Harandor, ImladMorgul };
    }
};
struct HarandorRegion : RegionDefinition {
    HarandorRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Harandor";
        region.number = Harandor;
        region.type = Wilderness;
        region.adjacentRegions = { MouthsOfTheAnduin, Ithilien, Khand };
    }
};
struct KhandRegion : RegionDefinition {
    KhandRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Khand";
        region.number = Khand;
        region.type = ShadowLands;
        region.adjacentRegions = { Harandor, Nurn };
    }
};
struct ImladMorgulRegion : RegionDefinition {
    ImladMorgulRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Imlad Morgul";
        region.number = ImladMorgul;
        region.type = ShadowLands;
        region.adjacentRegions = { Gorgoroth, Ithilien };
    }
};
struct UdunRegion : RegionDefinition {
    UdunRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Udûn";
        region.number = Udun;
        region.type = DarkDomain;
        region.adjacentRegions = { Gorgoroth };
    }
};
struct GorgorothRegion : RegionDefinition {
    GorgorothRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Gorgoroth";
        region.number = Gorgoroth;
        region.type = DarkDomain;
        region.adjacentRegions = { ImladMorgul, Nurn, Udun };
    }
};
struct NurnRegion : RegionDefinition {
    NurnRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Nurn";
        region.number = Nurn;
        region.type = DarkDomain;
        region.adjacentRegions = { Gorgoroth, HorsePlains, Khand };
    }
};
struct ElvenShoresRegion : RegionDefinition {
    ElvenShoresRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Elven Shores";
        region.number = ElvenShores;
        region.type = CoastalSeas;
        region.adjacentRegions = { EriadoranCoast, Lindon };
    }
};
struct EriadoranCoastRegion : RegionDefinition {
    EriadoranCoastRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Eriadoran Coast";
        region.number = EriadoranCoast;
        region.type = CoastalSeas;
        region.adjacentRegions = { Andrast, AndrastCoast, Cardolan, ElvenShores, Enedhwaith, OldPukelLand };
    }
};
struct AndrastCoastRegion : RegionDefinition {
    AndrastCoastRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Andrast Coast";
        region.number = AndrastCoast;
        region.type = CoastalSeas;
        region.adjacentRegions = { Andrast, BayOfBelfalas, EriadoranCoast };
    }
};
struct BayOfBelfalasRegion : RegionDefinition {
    BayOfBelfalasRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Bay of Belfalas";
        region.number = BayOfBelfalas;
        region.type = CoastalSeas;
        region.adjacentRegions = { AndrastCoast, Andrast, MouthsOfTheAnduin, Anfalas, Belfalas };
    }
};
struct MouthsOfTheAnduinRegion : RegionDefinition {
    MouthsOfTheAnduinRegion() {
        Region & region = std::get<Region>(type);
        region.name = "Mouths of the Anduin";
        region.number = MouthsOfTheAnduin;
        region.type = CoastalSeas;
        region.adjacentRegions = { BayOfBelfalas, Belfalas, Harandor, Lebennin };
    }
};

struct RivendellLocation : LocationDefinition {
    RivendellLocation() {
        Location & def = std::get<Location>(type);
        def.name = "Rivendell";
        def.type = Haven;
        def.sitePath.emplace_back(Lorien);
        def.sitePath.back().sitePath = { Wilderness, BorderLands, Wilderness, Wilderness };
        def.sitePath.emplace_back(GreyHavens);
        def.sitePath.back().sitePath = { FreeDomain, Wilderness, Wilderness };
        def.region = Rhudaur;
        def.drawCards = { 2, 2 };
    }
};
struct LorienLocation : LocationDefinition {
    LorienLocation() {
        Location & def = std::get<Location>(type);
        def.name = "Lórien";
        def.type = Haven;
        def.sitePath.emplace_back(Rivendell);
        def.sitePath.back().sitePath = { Wilderness, Wilderness, BorderLands, Wilderness };
        def.sitePath.emplace_back(Edhellond);
        def.sitePath.back().sitePath = { Wilderness, BorderLands, FreeDomain, FreeDomain, BorderLands, Wilderness };
        def.region = WoldAndFoothills;
        def.drawCards = { 2, 2 };
    }
};
struct GreyHavensLocation : LocationDefinition {
    GreyHavensLocation() {
        Location & def = std::get<Location>(type);
        def.name = "Grey Havens";
        def.type = Haven;
        def.sitePath.emplace_back(Rivendell);
        def.sitePath.back().sitePath = { Wilderness, Wilderness, FreeDomain };
        def.sitePath.emplace_back(Edhellond);
        def.sitePath.back().sitePath = { Wilderness, CoastalSeas, CoastalSeas, CoastalSeas, CoastalSeas, FreeDomain };
        def.region = Lindon;
        def.drawCards = { 2, 2 };
    }
};
struct EdhellondLocation : LocationDefinition {
    EdhellondLocation() {
        Location & def = std::get<Location>(type);
        def.name = "Edhellond";
        def.type = Haven;
        def.sitePath.emplace_back(GreyHavens);
        def.sitePath.back().sitePath = { FreeDomain, CoastalSeas, CoastalSeas, CoastalSeas, CoastalSeas, Wilderness };
        def.sitePath.emplace_back(Lorien);
        def.sitePath.back().sitePath = { Wilderness, BorderLands, FreeDomain, FreeDomain, BorderLands, Wilderness };
        def.region = Anfalas;
        def.drawCards = { 2, 2 };
    }
};
/* All Darkhavens have more than one special effects. Adding them on demand. */
struct TestStoredRing : public Effect {
    TestStoredRing(CardID host) : Effect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<MoveCard>(batch.et))
            return false;
        MoveCard & moveCard = std::get<MoveCard>(batch.et);
        if (moveCard.destination != Stored)
            return false;
        Game & game = batch.scene.game;
        CardType & type = game.getCardType(moveCard.target);
        if (!std::holds_alternative<Item>(type))
            return false;
        if (std::get<Item>(type).type != GoldRing)
            return false;
        CardID parent = game.getParent(moveCard.target);
        if (game.getCurrentSide().findCompany(parent, Characters)->currentLocation != host)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        batch.scene.setupRingTest(std::get<MoveCard>(batch.et).target, -2);
    }
    bool remove(EffectBatch & batch) const {
        return false;
    }
};
struct AllCreaturesDetainmentAgainstSite : public Effect {
    AllCreaturesDetainmentAgainstSite(CardID host) : Effect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Detainment>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<Attack>(context))
            return false;
        Attack & attack = std::get<Attack>(context);
        if (!std::holds_alternative<std::string>(attack.keyedTo))
            return false;
        Location & location = batch.scene.game.getLocation(host);
        if (std::get<std::string>(attack.keyedTo) != location.name)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<Detainment>(batch.et).detainment = true;
    }
    bool remove(EffectBatch & batch) const {
        return false;
    }
};
struct CancelAttacksAgainstCompaniesAtSite : public Effect {
    CancelAttacksAgainstCompaniesAtSite(CardID host) : Effect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<OpenContext>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<Attack>(context))
            return false;
        Game & game = batch.scene.game;
        Side & side = game.getCurrentSide();
        auto it = side.findCompany(std::get<Attack>(context).company);
        if (it == side.companies.end())
            return false;
        if (it->newLocation != host && it->newLocation != it->currentLocation)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        batch.scene.cancelAttack();
    }
    bool remove(EffectBatch & batch) const {
        return false;
    }
};
struct CarnDumPlayOrcTrollOnlyEffect : Effect {
    CarnDumPlayOrcTrollOnlyEffect(CardID host) : Effect(host) {}
    bool atCarnDum(Game & game, CardID target) const {
        if (target == host)
            return true;
        CardType & type = game.getCardType(target);
        if (!std::holds_alternative<Character>(type))
            return false;
        Side & side = game.getCurrentSide();
        auto it = side.findCompany(target, Characters);
        if (it == side.companies.end())
            return false;
        return it->currentLocation == host;
    }
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<PlayCharacter>(batch.et))
            return false;
        PlayCharacter & pc = std::get<PlayCharacter>(batch.et);
        if (!pc.allowed)
            return false;
        Game & game = batch.scene.game;
        if (!atCarnDum(game, pc.target))
            return false;
        // pc.allowed == true  =>  pc.character is a Character
        Character & character = game.getCharacter(pc.character);
        if (character.race == Orc || character.race == Troll)
            return false;
        if (batch.scene.matchingHomeSite(character.homeSite, host))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<PlayCharacter>(batch.et).allowed = false;
    }
    bool remove(EffectBatch & batch) const {
        return false;
    }
};
struct MinasMorgulLocation : public LocationDefinition {
    MinasMorgulLocation() {
        Location & def = std::get<Location>(type);
        def.minion = true;
        def.name = "Minas Morgul";
        def.type = Darkhaven;
        def.sitePath.emplace_back(DolGuldur);
        def.sitePath.back().sitePath = { DarkDomain, ShadowLands, DarkDomain, DarkDomain, ShadowLands };
        def.region = ImladMorgul;
        def.drawCards = { 2, 2 };
    }
    void getEffects(EffectList & effects, CardID host) {
        effects.push_back(std::make_shared<TestStoredRing>(host));
        effects.push_back(std::make_shared<AllCreaturesDetainmentAgainstSite>(host));
        effects.push_back(std::make_shared<CancelAttacksAgainstCompaniesAtSite>(host));
    }
};
struct DolGuldurLocation : public LocationDefinition {
    DolGuldurLocation() {
        Location & def = std::get<Location>(type);
        def.minion = true;
        def.name = "Dol Guldur";
        def.type = Darkhaven;
        def.sitePath.emplace_back(MinasMorgul);
        def.sitePath.back().sitePath = { ShadowLands, DarkDomain, DarkDomain, ShadowLands, DarkDomain };
        def.sitePath.emplace_back(CarnDum);
        def.sitePath.back().sitePath = { ShadowLands, DarkDomain, BorderLands, DarkDomain };
        def.region = SouthernMirkwood;
        def.drawCards = { 2, 2 };
    }
    void getEffects(EffectList & effects, CardID host) {
        effects.push_back(std::make_shared<TestStoredRing>(host));
        effects.push_back(std::make_shared<AllCreaturesDetainmentAgainstSite>(host));
        effects.push_back(std::make_shared<CancelAttacksAgainstCompaniesAtSite>(host));
    }
};
struct CarnDumLocation : public LocationDefinition {
    CarnDumLocation() {
        Location & def = std::get<Location>(type);
        def.minion = true;
        def.name = "Carn Dûm";
        def.type = Darkhaven;
        def.sitePath.emplace_back(DolGuldur);
        def.sitePath.back().sitePath = { DarkDomain, BorderLands, DarkDomain, ShadowLands };
        def.sitePath.emplace_back(GeannALisch);
        def.sitePath.back().sitePath = { Wilderness, Wilderness, Wilderness, Wilderness, ShadowLands };
        def.region = Angmar;
        def.drawCards = { 2, 2 };
    }
    void getEffects(EffectList & effects, CardID host) {
        effects.push_back(std::make_shared<CarnDumPlayOrcTrollOnlyEffect>(host));
        effects.push_back(std::make_shared<TestStoredRing>(host));
        effects.push_back(std::make_shared<AllCreaturesDetainmentAgainstSite>(host));
        effects.push_back(std::make_shared<CancelAttacksAgainstCompaniesAtSite>(host));
    }
};
struct GeannALischLocation : public LocationDefinition {
    GeannALischLocation() {
        Location & def = std::get<Location>(type);
        def.minion = true;
        def.name = "Geann a-Lisch";
        def.type = Darkhaven;
        def.sitePath.emplace_back(CarnDum);
        def.sitePath.back().sitePath = { ShadowLands, Wilderness, Wilderness, Wilderness, Wilderness };
        def.region = OldPukelLand;
        def.drawCards = { 2, 2 };
    }
};


void addCoreCardDefinitions(GameSettings & settings) {
    settings.addCardDefinition(std::make_unique<LindonRegion>());
    settings.addCardDefinition(std::make_unique<NumeriadorRegion>());
    settings.addCardDefinition(std::make_unique<ForochelRegion>());
    settings.addCardDefinition(std::make_unique<ArthedainRegion>());
    settings.addCardDefinition(std::make_unique<TheShireRegion>());
    settings.addCardDefinition(std::make_unique<CardolanRegion>());
    settings.addCardDefinition(std::make_unique<AngmarRegion>());
    settings.addCardDefinition(std::make_unique<RhudaurRegion>());
    settings.addCardDefinition(std::make_unique<HollinRegion>());
    settings.addCardDefinition(std::make_unique<DunlandRegion>());
    settings.addCardDefinition(std::make_unique<EnedhwaithRegion>());
    settings.addCardDefinition(std::make_unique<OldPukelLandRegion>());
    settings.addCardDefinition(std::make_unique<GundabadRegion>());
    settings.addCardDefinition(std::make_unique<HighPassRegion>());
    settings.addCardDefinition(std::make_unique<RedhornGateRegion>());
    settings.addCardDefinition(std::make_unique<GapOfIsenRegion>());
    settings.addCardDefinition(std::make_unique<AnduinValesRegion>());
    settings.addCardDefinition(std::make_unique<WoldAndFoothillsRegion>());
    settings.addCardDefinition(std::make_unique<FangornRegion>());
    settings.addCardDefinition(std::make_unique<RohanRegion>());
    settings.addCardDefinition(std::make_unique<GreyMountainNarrowsRegion>());
    settings.addCardDefinition(std::make_unique<WoodlandRealmRegion>());
    settings.addCardDefinition(std::make_unique<WesternMirkwoodRegion>());
    settings.addCardDefinition(std::make_unique<HeartOfMirkwoodRegion>());
    settings.addCardDefinition(std::make_unique<SouthernMirkwoodRegion>());
    settings.addCardDefinition(std::make_unique<BrownLandsRegion>());
    settings.addCardDefinition(std::make_unique<DagorladRegion>());
    settings.addCardDefinition(std::make_unique<WitheredHeathRegion>());
    settings.addCardDefinition(std::make_unique<NorthernRhovanionRegion>());
    settings.addCardDefinition(std::make_unique<SouthernRhovanionRegion>());
    settings.addCardDefinition(std::make_unique<IronHillsRegion>());
    settings.addCardDefinition(std::make_unique<DorwinionRegion>());
    settings.addCardDefinition(std::make_unique<HorsePlainsRegion>());
    settings.addCardDefinition(std::make_unique<OldPukelGapRegion>());
    settings.addCardDefinition(std::make_unique<AndrastRegion>());
    settings.addCardDefinition(std::make_unique<AnfalasRegion>());
    settings.addCardDefinition(std::make_unique<LamedonRegion>());
    settings.addCardDefinition(std::make_unique<BelfalasRegion>());
    settings.addCardDefinition(std::make_unique<LebenninRegion>());
    settings.addCardDefinition(std::make_unique<AnorienRegion>());
    settings.addCardDefinition(std::make_unique<IthilienRegion>());
    settings.addCardDefinition(std::make_unique<HarandorRegion>());
    settings.addCardDefinition(std::make_unique<KhandRegion>());
    settings.addCardDefinition(std::make_unique<ImladMorgulRegion>());
    settings.addCardDefinition(std::make_unique<UdunRegion>());
    settings.addCardDefinition(std::make_unique<GorgorothRegion>());
    settings.addCardDefinition(std::make_unique<NurnRegion>());
    settings.addCardDefinition(std::make_unique<ElvenShoresRegion>());
    settings.addCardDefinition(std::make_unique<EriadoranCoastRegion>());
    settings.addCardDefinition(std::make_unique<AndrastCoastRegion>());
    settings.addCardDefinition(std::make_unique<BayOfBelfalasRegion>());
    settings.addCardDefinition(std::make_unique<MouthsOfTheAnduinRegion>());
    settings.addCardDefinition(std::make_unique<RivendellLocation>());
    settings.addCardDefinition(std::make_unique<LorienLocation>());
    settings.addCardDefinition(std::make_unique<GreyHavensLocation>());
    settings.addCardDefinition(std::make_unique<EdhellondLocation>());
    settings.addCardDefinition(std::make_unique<MinasMorgulLocation>());
    settings.addCardDefinition(std::make_unique<DolGuldurLocation>());
    settings.addCardDefinition(std::make_unique<CarnDumLocation>());
    settings.addCardDefinition(std::make_unique<GeannALischLocation>());
}
