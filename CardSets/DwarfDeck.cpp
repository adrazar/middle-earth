#include "DeckDefinition.h"
#include "DwarfDeck.h"
#include "HazardCards.h"
#include "../Middle-Earth/BaseClasses.h"

/* Blue Mountain Dwarf-hold */
struct BlueMountainDwarfHold : public LocationDefinition {
    BlueMountainDwarfHold() {
        Location & def = std::get<Location>(type);
        def.name = "Blue Mountain Dwarf-hold";
        def.type = FreeHold;
        def.sitePath.emplace_back(GreyHavens);
        def.sitePath.back().sitePath = { FreeDomain, Wilderness };
        def.region = Numeriador;
        def.drawCards = { 1, 1 };
    }
};

/* Buhr Widu */
struct BuhrWiduEffect : public Effect {
    BuhrWiduEffect(CardID host) : Effect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<MoveCard>(batch.et))
            return false;
        MoveCard & moveCard = std::get<MoveCard>(batch.et);
        if (moveCard.target != host)
            return false;
        if (moveCard.destination != DiscardPile)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<MoveCard>(batch.et).destination = LocationDeck;
    }
    bool remove(EffectBatch & batch) const {
        // This card is never put into the discard pile, so the Effect
        // will not become duplicated.
        return false;
    }
};
struct BuhrWidu : public LocationDefinition {
    BuhrWidu() {
        Location & def = std::get<Location>(type);
        def.name = "Buhr Widu";
        def.type = RuinsLairs;
        def.sitePath.emplace_back(Lorien);
        def.sitePath.back().sitePath = { Wilderness, BorderLands, Wilderness, Wilderness, Wilderness };
        def.region = SouthernRhovanion;
        def.playable = { Minor, Major };
        def.automaticAttack.add(Troll, 1, 10);
        def.drawCards = { 3, 3 };
    }
    void getEffects(EffectList & effects, CardID host) {
        /*/ For Locations, this function should be called when locations
            are returned from the discard pile, and during setup. */
        effects.push_back(std::make_shared<BuhrWiduEffect>(host));
    }
};

/* Caves of Ûlund */
struct CavesOfUlund : public LocationDefinition {
    CavesOfUlund() {
        Location & def = std::get<Location>(type);
        def.name = "Caves of Ûlund";
        def.type = RuinsLairs;
        def.sitePath.emplace_back(Lorien);
        def.sitePath.back().sitePath = { Wilderness, BorderLands, ShadowLands, Wilderness };
        def.region = WitheredHeath;
        def.playable = { Minor, Major, Greater, GoldRing };
        def.automaticAttack.add(Dragon, 1, 13);
        def.drawCards = { 3, 3 };
        def.containsHoard = true;
    }
};

/* Dead Marshes */
struct DeadMarshes : public LocationDefinition {
    DeadMarshes() {
        Location & def = std::get<Location>(type);
        def.name = "Dead Marshes";
        def.type = ShadowHold;
        def.sitePath.emplace_back(Lorien);
        def.sitePath.back().sitePath = { Wilderness, ShadowLands, ShadowLands };
        def.region = Dagorlad;
        def.playable = { Minor, Major, Greater };
        def.automaticAttack.add(Undead, 2, 8);
        def.drawCards = { 2, 3 };
    }
};

/* Goblin-gate */
struct GoblinGate : public LocationDefinition {
    GoblinGate() {
        Location & def = std::get<Location>(type);
        def.name = "Goblin-gate";
        def.type = ShadowHold;
        def.sitePath.emplace_back(Rivendell);
        def.sitePath.back().sitePath = { Wilderness, Wilderness };
        def.region = HighPass;
        def.playable = { Minor, GoldRing };
        def.automaticAttack.add(Orc, 3, 6);
        def.drawCards = { 1, 2 };
    }
};

/* Iron Hill Dwarf-hold */
struct IronHillDwarfHold : public LocationDefinition {
    IronHillDwarfHold() {
        Location & def = std::get<Location>(type);
        def.name = "Iron Hill Dwarf-hold";
        def.type = FreeHold;
        def.sitePath.emplace_back(Lorien);
        def.sitePath.back().sitePath = { Wilderness, BorderLands, BorderLands, Wilderness, Wilderness };
        def.region = IronHills;
        def.drawCards = { 3, 3 };
    }
};

/* Moria */
struct Moria : public LocationDefinition {
    Moria() {
        Location & def = std::get<Location>(type);
        def.name = "Moria";
        def.type = ShadowHold;
        def.sitePath.emplace_back(Lorien);
        def.sitePath.back().sitePath = { Wilderness, Wilderness };
        def.region = RedhornGate;
        def.playable = { Minor, Major, Greater, GoldRing };
        def.automaticAttack.add(Orc, 4, 7);
        def.drawCards = { 2, 3 };
    }
};

/* Ovir Hollow */
struct OvirHollow : public LocationDefinition {
    OvirHollow() {
        Location & def = std::get<Location>(type);
        def.name = "Ovir Hollow";
        def.type = RuinsLairs;
        def.sitePath.emplace_back(Lorien);
        def.sitePath.back().sitePath = { Wilderness, BorderLands, ShadowLands };
        def.region = GreyMountainNarrows;
        def.playable = { Minor, Major };
        def.automaticAttack.add(Dragon, 1, 12);
        def.drawCards = { 2, 2 };
        def.containsHoard = true;
    }
};

/* The Lonely Mountain */
struct TheLonelyMountain : public LocationDefinition {
    TheLonelyMountain() {
        Location & def = std::get<Location>(type);
        def.name = "The Lonely Mountain";
        def.type = RuinsLairs;
        def.sitePath.emplace_back(Lorien);
        def.sitePath.back().sitePath = { Wilderness, BorderLands, BorderLands, Wilderness };
        def.region = NorthernRhovanion;
        def.playable = { Minor, Major, Greater, GoldRing };
        def.automaticAttack.add(Dragon, 1, 14);
        def.drawCards = { 2, 2 };
        def.containsHoard = true;
    }
};

/* Sarn Goriwing */
struct SarnGoriwing : public LocationDefinition {
    SarnGoriwing() {
        Location & def = std::get<Location>(type);
        def.name = "Sarn Goriwing";
        def.type = ShadowHold;
        def.sitePath.emplace_back(Lorien);
        def.sitePath.back().sitePath = { Wilderness, BorderLands, Wilderness, Wilderness };
        def.region = HeartOfMirkwood;
        def.playable = { Minor, Major };
        def.automaticAttack.add(Orc, 3, 5);
        def.drawCards = { 2, 2 };
    }
};

/* Shrel-Kain */
struct ShrelKain : public LocationDefinition {
    ShrelKain() {
        Location & def = std::get<Location>(type);
        def.name = "Shrel-Kain";
        def.type = BorderHold;
        def.sitePath.emplace_back(Lorien);
        def.sitePath.back().sitePath = { Wilderness, BorderLands, BorderLands, Wilderness, BorderLands };
        def.region = Dorwinion;
        def.drawCards = { 3, 3 };
    }
};

/* Wellinghall */
struct Wellinghall : public LocationDefinition {
    Wellinghall() {
        Location & def = std::get<Location>(type);
        def.name = "Wellinghall";
        def.type = FreeHold;
        def.sitePath.emplace_back(Lorien);
        def.sitePath.back().sitePath = { Wilderness, Wilderness };
        def.region = Fangorn;
        def.drawCards = { 1, 1 };
    }
};

/* Pallando */
struct PallandoFaceUpEffect : public CharacterEffect {
    PallandoFaceUpEffect(CardID host) : CharacterEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<MoveCard>(batch.et))
            return false;
        MoveCard & moveCard = std::get<MoveCard>(batch.et);
        if (moveCard.destination != DiscardPile)
            return false;
        Game & game = batch.scene.game;
        int opponent = (game.cards[host].owner + 1) % game.sides.size();
        if (moveCard.recipientPlayer != opponent)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<MoveCard>(batch.et).faceUp = true;
    }
};
struct PallandoHandsizeEffect : public CharacterEffect {
    PallandoHandsizeEffect(CardID host) : CharacterEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<HandSize>(batch.et))
            return false;
        if (std::get<HandSize>(batch.et).player != batch.scene.game.cards[host].owner)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<HandSize>(batch.et).handSize++;
    }
};
struct PlayPallando : public PlayWizardAction {
    void wizardAbilities(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<PallandoHandsizeEffect>(host));
        side.effects.push_back(std::make_shared<PallandoFaceUpEffect>(host));
    }
};
struct Pallando : public CardDefinition {
    Pallando() : CardDefinition(Character()) {
        Character & def = std::get<Character>(type);
        def.name.push_back("Pallando");
        def.skills = { Warrior, Ranger, Sage, Diplomat };
        def.race = Wizard;
        def.mp = 0;
        def.mind = 0;
        def.di = 10;
        def.prowess = 6;
        def.body = 9;
        def.homeSite = "Grey Havens";
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayPallando>(), host);
    }
};

/* Balin */
struct PlayBalin : public PlayHeroCharacter {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<ProwessBonus>(host, Orc, 2));
        side.effects.push_back(std::make_shared<InfluenceRaceBonus>(host, Dwarf, 1));
    }
};
struct Balin : public CardDefinition {
    Balin() : CardDefinition(Character()) {
        Character & def = std::get<Character>(type);
        def.name.push_back("Balin");
        def.skills = { Warrior, Sage };
        def.race = Dwarf;
        def.mp = 2;
        def.mind = 5;
        def.di = 2;
        def.prowess = 4;
        def.body = 7;
        def.homeSite = "Blue Mountain Dwarf-hold";
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayBalin>(), host);
    }
};

/* Boromir II */
struct PlayBoromirII : public PlayHeroCharacter {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<InfluenceFactionBonus>(host, "Men of Anórien", 2));
        side.effects.push_back(std::make_shared<CorruptionCheckBonus>(host, -1));
    }
};
struct BoromirII : public CardDefinition {
    BoromirII() : CardDefinition(Character()) {
        Character & def = std::get<Character>(type);
        def.name.push_back("Boromir II");
        def.skills = { Warrior };
        def.race = Dunadan;
        def.mp = 1;
        def.mind = 4;
        def.di = 1;
        def.prowess = 6;
        def.body = 7;
        def.homeSite = "Minas Tirith";
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayBoromirII>(), host);
    }
};

/* Celeborn */
struct PlayCeleborn : public PlayHeroCharacter {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<InfluenceCharacterBonus>(host, "Galadriel", 5));
    }
};
struct Celeborn : public CardDefinition {
    Celeborn() : CardDefinition(Character()) {
        Character & def = std::get<Character>(type);
        def.name.push_back("Celeborn");
        def.skills = { Warrior, Sage };
        def.race = Elf;
        def.mp = 2;
        def.mind = 6;
        def.di = 1;
        def.prowess = 6;
        def.body = 9;
        def.homeSite = "Lórien";
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayCeleborn>(), host);
    }
};

/* Dori */
struct PlayDori : public PlayHeroCharacter {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<ProwessBonus>(host, Orc, 1));
        side.effects.push_back(std::make_shared<CorruptionCheckBonus>(host, -1));
        side.effects.push_back(std::make_shared<InfluenceFactionBonus>(host, "", -1));
    }
};
struct Dori : public CardDefinition {
    Dori() : CardDefinition(Character()) {
        Character & def = std::get<Character>(type);
        def.name.push_back("Dori");
        def.skills = { Warrior };
        def.race = Dwarf;
        def.mp = 0;
        def.mind = 1;
        def.di = 0;
        def.prowess = 3;
        def.body = 6;
        def.homeSite = "Blue Mountain Dwarf-hold";
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayDori>(), host);
    }
};

/* Fili */
struct PlayFili : public PlayHeroCharacter {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<ProwessBonus>(host, Orc, 1));
        side.effects.push_back(std::make_shared<CorruptionCheckBonus>(host, -1));
        side.effects.push_back(std::make_shared<InfluenceFactionBonus>(host, "", -1));
    }
};
struct Fili : public CardDefinition {
    Fili() : CardDefinition(Character()) {
        Character & def = std::get<Character>(type);
        def.name.push_back("Fíli");
        def.skills = { Warrior, Scout };
        def.race = Dwarf;
        def.mp = 0;
        def.mind = 2;
        def.di = 0;
        def.prowess = 2;
        def.body = 8;
        def.homeSite = "Blue Mountain Dwarf-hold";
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayFili>(), host);
    }
};

/* Gildor Inglorion */
struct PlayGildorInglorion : public PlayHeroCharacter {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<ProwessBonus>(host, Orc, 2));
    }
};
struct GildorInglorion : public CardDefinition {
    GildorInglorion() : CardDefinition(Character()) {
        Character & def = std::get<Character>(type);
        def.name.push_back("Gildor Inglorion");
        def.skills = { Warrior, Ranger };
        def.race = Elf;
        def.mp = 1;
        def.mind = 4;
        def.di = 0;
        def.prowess = 5;
        def.body = 7;
        def.homeSite = "Rivendell";
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayGildorInglorion>(), host);
    }
};

/* Gloin */
struct PlayGloin : public PlayHeroCharacter {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<InfluenceFactionBonus>(host, "Blue Mountain Dwarves", 2));
        side.effects.push_back(std::make_shared<ProwessBonus>(host, Orc, 1));
        side.effects.push_back(std::make_shared<InfluenceRaceBonus>(host, Dwarf, 1));
    }
};
struct Gloin : public CardDefinition {
    Gloin() : CardDefinition(Character()) {
        Character & def = std::get<Character>(type);
        def.name.push_back("Glóin");
        def.skills = { Warrior, Diplomat };
        def.race = Dwarf;
        def.mp = 2;
        def.mind = 5;
        def.di = 2;
        def.prowess = 5;
        def.body = 7;
        def.homeSite = "Blue Mountain Dwarf-hold";
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayGloin>(), host);
    }
};

/* Kili */
struct PlayKili : public PlayHeroCharacter {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<ProwessBonus>(host, Orc, 1));
        side.effects.push_back(std::make_shared<CorruptionCheckBonus>(host, -1));
        side.effects.push_back(std::make_shared<InfluenceFactionBonus>(host, "", -1));
    }
};
struct Kili : public CardDefinition {
    Kili() : CardDefinition(Character()) {
        Character & def = std::get<Character>(type);
        def.name.push_back("Kíli");
        def.skills = { Warrior, Scout };
        def.race = Dwarf;
        def.mp = 1;
        def.mind = 3;
        def.di = 0;
        def.prowess = 3;
        def.body = 8;
        def.homeSite = "Blue Mountain Dwarf-hold";
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayKili>(), host);
    }
};

/* Oin */
struct PlayOin : public PlayHeroCharacter {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<ProwessBonus>(host, Orc, 1));
        side.effects.push_back(std::make_shared<CorruptionCheckBonus>(host, -1));
    }
};
struct Oin : public CardDefinition {
    Oin() : CardDefinition(Character()) {
        Character & def = std::get<Character>(type);
        def.name.push_back("Óin");
        def.skills = { Warrior, Ranger };
        def.race = Dwarf;
        def.mp = 1;
        def.mind = 3;
        def.di = 0;
        def.prowess = 3;
        def.body = 8;
        def.homeSite = "Blue Mountain Dwarf-hold";
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayOin>(), host);
    }
};

/* Thorin II */
struct PlayThorinII : public PlayHeroCharacter {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<InfluenceFactionBonus>(host, "Blue Mountain Dwarves", 2));
        side.effects.push_back(std::make_shared<ProwessBonus>(host, Orc, 3));
        side.effects.push_back(std::make_shared<InfluenceRaceBonus>(host, Dwarf, 2));
    }
};
struct ThorinII : public CardDefinition {
    ThorinII() : CardDefinition(Character()) {
        Character & def = std::get<Character>(type);
        def.name.push_back("Thorin II");
        def.skills = { Warrior, Scout, Diplomat };
        def.race = Dwarf;
        def.mp = 3;
        def.mind = 8;
        def.di = 2;
        def.prowess = 5;
        def.body = 8;
        def.homeSite = "Blue Mountain Dwarf-hold";
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayThorinII>(), host);
    }
};

/* Gollum */
struct GollumCancelAttack : public EquipmentAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        if (game.getCardState(host) != Untapped)
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Attack>(context))
            return false;
        Attack & attack = std::get<Attack>(context);
        if (!game.characterInCompany(game.getParent(host)), attack.company)
            return false;
        if (scene.getCompanySize(*game.getCurrentSide().findCompany(attack.company)) > 2)
            return false;
        if (!std::holds_alternative<PathSymbol>(attack.keyedTo))
            return false;
        PathSymbol pathSymbol = std::get<PathSymbol>(attack.keyedTo);
        if (pathSymbol != Wilderness && pathSymbol != ShadowLands)
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.tap(host);
        scene.cancelAttack();
    }
};
struct PlayGollum : public PlayAllyAction {
    PlayGollum() : PlayAllyAction({ "Goblin-gate", "Moria" }) {}
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.actions.add(std::make_shared<GollumCancelAttack>(), host);
    }
};
struct Gollum : public CardDefinition {
    Gollum() : CardDefinition(Ally()) {
        Ally & def = std::get<Ally>(type);
        def.name.push_back("Gollum");
        def.unique = true;
        def.skills = { Scout };
        def.mp = 2;
        def.mind = 4;
        def.prowess = 2;
        def.body = 9;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayGollum>(), host);
    }
};

/* Quickbeam */
struct QuickbeamEffect : public EquipmentEffect {
    QuickbeamEffect(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<AllowedOperation>(batch.et))
            return false;
        AllowedOperation & op = std::get<AllowedOperation>(batch.et);
        if (op.card != host)
            return false;
        if (op.operation != Target)
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<AssignStrikes>(context))
            return false;
        AssignStrikes & assign = std::get<AssignStrikes>(context);
        if (assign.automaticAttack ||
            std::holds_alternative<SiteType>(assign.keyedTo) ||
            std::holds_alternative<std::string>(assign.keyedTo))
            return true;
        return false;
    }
    void activate(EffectBatch & batch) const {
        std::get<AllowedOperation>(batch.et).allowed = false;
    }
};
struct PlayQuickbeam : public PlayAllyAction {
    PlayQuickbeam() : PlayAllyAction({ "Wellinghall" }) {}
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<QuickbeamEffect>(host));
    }
};
struct Quickbeam : public CardDefinition {
    Quickbeam() : CardDefinition(Ally()) {
        Ally & def = std::get<Ally>(type);
        def.name.push_back("Quickbeam");
        def.unique = true;
        def.mp = 2;
        def.mind = 3;
        def.prowess = 6;
        def.body = 9;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayQuickbeam>(), host);
    }
};

/* Blue Mountain Dwarves */
struct BlueMountainDwarves : public FactionDefinition {
    BlueMountainDwarves() {
        Faction & def = std::get<Faction>(type);
        def.name.push_back("Blue Mountain Dwarves");
        def.race = Dwarf;
        def.homeSite = "Blue Mountain Dwarf-hold";
        def.mp = 3;
        def.requiredInfluence = 9;
        def.standardModifications[Elf] = -2;
        def.standardModifications[Dwarf] = 2;
    }
};

/* Iron Hill Dwarves */
struct IronHillDwarves : public FactionDefinition {
    IronHillDwarves() {
        Faction & def = std::get<Faction>(type);
        def.name.push_back("Iron Hill Dwarves");
        def.race = Dwarf;
        def.homeSite = "Iron Hill Dwarf-hold";
        def.mp = 4;
        def.requiredInfluence = 8;
        def.standardModifications[Elf] = -2;
        def.standardModifications[Dwarf] = 2;
    }
};

/* Men of Dorwinion */
struct MenOfDorwinion : public FactionDefinition {
    MenOfDorwinion() {
        Faction & def = std::get<Faction>(type);
        def.name.push_back("Men of Dorwinion");
        def.race = Man;
        def.homeSite = "Shrel-Kain";
        def.mp = 3;
        def.requiredInfluence = 6;
        def.standardModifications[Man] = 1;
    }
};

/* Book of Mazarbul */
struct BookOfMazarbulMP : public Effect {
    BookOfMazarbulMP(CardID host) : Effect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<MarshallingPoints>(batch.et))
            return false;
        if (std::get<MarshallingPoints>(batch.et).target != host)
            return false;
        if (batch.scene.game.getDeck(host) != Stored)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<MarshallingPoints>(batch.et).mp = 5;
    }
    bool remove(EffectBatch & batch) const {
        return batch.scene.game.getDeck(host) != Stored;
    }
};
struct StoreBookOfMazarbulEffect : public EquipmentEffect {
    StoreBookOfMazarbulEffect(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<MoveCard>(batch.et))
            return false;
        MoveCard & moveCard = std::get<MoveCard>(batch.et);
        if (moveCard.target != host || moveCard.destination != Stored)
            return false;
        Game & game = batch.scene.game;
        Side & side = game.getCurrentSide();
        auto it = side.findCompany(game.getParent(host), Characters);
        if (it == side.companies.end())
            return false;
        Location & location = game.getLocation(it->currentLocation);
        if (location.name != "Iron Hill Dwarf-hold" && location.name != "Blue Mountain Dwarf-hold")
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        batch.scene.game.getCurrentSide().effects.push_back(std::make_shared<BookOfMazarbulMP>(host));
    }
};
struct StoreBookOfMazarbulAllowed : public EquipmentEffect {
    StoreBookOfMazarbulAllowed(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<AllowedOperation>(batch.et))
            return false;
        AllowedOperation & op = std::get<AllowedOperation>(batch.et);
        if (op.operation != Store || op.card != host)
            return false;
        if (!std::holds_alternative<OrganizeCompanies>(batch.getContext()))
            return false;
        Game & game = batch.scene.game;
        Side & side = game.getCurrentSide();
        auto it = side.findCompany(game.getParent(host), Characters);
        if (it == side.companies.end())
            return false;
        Location & location = game.getLocation(it->currentLocation);
        if (location.name != "Iron Hill Dwarf-hold" && location.name != "Blue Mountain Dwarf-hold")
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<AllowedOperation>(batch.et).allowed = true;
    }
};
struct BookOfMazarbulHandSize : public Effect {
    BookOfMazarbulHandSize(CardID host) : Effect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<HandSize>(batch.et))
            return false;
        if (std::get<HandSize>(batch.et).player != batch.scene.game.cards[host].owner)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<HandSize>(batch.et).handSize++;
    }
    bool remove(EffectBatch & batch) const {
        if (!std::holds_alternative<NewPhase>(batch.et))
            return false;
        Game & game = batch.scene.game;
        if (game.turnSequence.phase != UntapPhase)
            return false;
        if (game.cards[host].owner != game.currentPlayer)
            return false;
        return true;
    }
};
struct TapBookOfMazarbul : public EquipmentAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        Game & game = scene.game;
        if (game.turnSequence.phase != OrganizationPhase)
            return false;
        if (game.getCardState(host) != Untapped)
            return false;
        CardID parent = game.getParent(host);
        if (parent < 0)
            return false;
        if (!scene.hasSkill(parent, Sage))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.tap(host);
        scene.game.getCurrentSide().effects.push_back(std::make_shared<BookOfMazarbulHandSize>(host));
    }
};
struct PlayBookOfMazarbul : public PlayAtSiteAction {
    bool conditions(Scene scene, CardID target, Explore & explore) const {
        Game & game = scene.game;
        if (game.getCardState(explore.site) != Untapped)
            return false;
        Location & location = game.getLocation(explore.site);
        if (location.name != "Moria")
            return false;
        Item & item = game.getItem(host);
        if (!game.uniqueItem(host, item.name))
            return false;
        return true;
    }
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.actions.add(std::make_shared<TapBookOfMazarbul>(), host);
        side.effects.push_back(std::make_shared<StoreBookOfMazarbulAllowed>(host));
        side.effects.push_back(std::make_shared<StoreBookOfMazarbulEffect>(host));
    }
};
struct BookOfMazarbul : public CardDefinition {
    BookOfMazarbul() : CardDefinition(Item()) {
        Item & def = std::get<Item>(type);
        def.name.push_back("Book of Mazarbul");
        def.type = Special;
        def.unique = true;
        def.mp = 1;
        def.cp = 2;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayBookOfMazarbul>(), host);
    }
};

/* Bow of Dragon-horn */
struct BowOfDragonHornAction : public EquipmentAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        if (game.getCardState(host) != Untapped)
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Attack>(context))
            return false;
        Attack & attack = std::get<Attack>(context);
        if (attack.card < 0)
            return false;
        if (!std::holds_alternative<HazardCreature>(game.getCardType(attack.card)))
            return false;
        KeyedToType key = attack.keyedTo;
        if (std::holds_alternative<SiteType>(key) || std::holds_alternative<std::string>(key))
            return false;
        if (attack.strikes < 2)  // attack.strikes == 0 if each character faces one strike
            return false;
        CardID parent = game.getParent(host);
        if (!game.characterInCompany(parent, attack.company))
            return false;
        if (!scene.hasSkill(parent, Warrior))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.tap(host);
        Attack & attack = std::get<Attack>(scene.context->context);
        attack.strikes--;
    }
};
struct PlayBowOfDragonHorn : public PlayItemAction {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.actions.add(std::make_shared<BowOfDragonHornAction>(), host);
    }
};
struct BowOfDragonHorn : public CardDefinition {
    BowOfDragonHorn() : CardDefinition(Item()) {
        Item & def = std::get<Item>(type);
        def.name.push_back("Bow of Dragon-horn");
        def.type = Major;
        def.equipmentType = Weapon;
        def.unique = true;
        def.hoard = true;
        def.mp = 2;
        def.cp = 2;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayBowOfDragonHorn>(), host);
    }
};

/* Cram */
struct CramUntapAction : public EquipmentAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        Game & game = scene.game;
        CardID target = game.getParent(host);
        if (game.getCardState(target) != Tapped)
            return false;
        if (!scene.allowedOperation(target, Untap))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        CardID target = scene.game.getParent(host);
        scene.discard(host);
        scene.untap(target);
    }
};
struct CramRegionEffect : public Effect {
    CramRegionEffect(CardID host) : Effect(host) {}
    bool conditions(EffectBatch & batch) const {
        return std::holds_alternative<RegionLimit>(batch.et);
    }
    void activate(EffectBatch & batch) const {
        std::get<RegionLimit>(batch.et).limit++;
    }
    bool remove(EffectBatch & batch) const {
        return std::holds_alternative<NewPhase>(batch.et);
    }
};
struct CramRegionAction : public EquipmentAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        if (!std::holds_alternative<OrganizeCompanies>(scene.context->context))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        CardID parent = game.getParent(host);
        scene.discard(host);
        Side & side = game.getCurrentSide();
        Company & company = *side.findCompany(parent, Characters);
        company.effects.push_back(std::make_shared<CramRegionEffect>(host));
    }
};
struct PlayCram : public PlayItemAction {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.actions.add(std::make_shared<CramUntapAction>(), host, "Untap");
        side.actions.add(std::make_shared<CramRegionAction>(), host, "Movement");
    }
};
struct Cram : public CardDefinition {
    Cram() : CardDefinition(Item()) {
        Item & def = std::get<Item>(type);
        def.name.push_back("Cram");
        def.type = Minor;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayCram>(), host);
    }
};

/* Durin's Axe */
struct DurinsAxeProwess : public EquipmentEffect {
    DurinsAxeProwess(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Stat>(batch.et))
            return false;
        Stat & stat = std::get<Stat>(batch.et);
        if (stat.type != Prowess)
            return false;
        if (stat.target != batch.scene.game.getParent(host))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        Stat & stat = std::get<Stat>(batch.et);
        int & prowess = stat.stat;
        prowess += (batch.scene.getRace(stat.target) == Dwarf) ? 4 : 2;
        if (prowess > 9)
            prowess = 9;
    }
};
struct DurinsAxeCP : public EquipmentEffect {
    DurinsAxeCP(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<CorruptionTotal>(batch.et))
            return false;
        CorruptionTotal & cp = std::get<CorruptionTotal>(batch.et);
        Scene & scene = batch.scene;
        if (cp.target != host)
            return false;
        if (scene.getRace(scene.game.getParent(cp.target)) != Dwarf)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<CorruptionTotal>(batch.et).corruption++;
    }
};
struct DurinsAxeMP : public EquipmentEffect {
    DurinsAxeMP(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<MarshallingPoints>(batch.et))
            return false;
        if (std::get<MarshallingPoints>(batch.et).target != host)
            return false;
        Scene & scene = batch.scene;
        CardID parent = scene.game.getParent(host);
        if (parent < 0)
            return false;
        if (scene.getRace(parent) != Dwarf)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<MarshallingPoints>(batch.et).mp = 4;
    }
};
struct PlayDurinsAxe : public PlayItemAction {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<DurinsAxeProwess>(host));
        side.effects.push_back(std::make_shared<DurinsAxeCP>(host));
        side.effects.push_back(std::make_shared<DurinsAxeMP>(host));
    }
};
struct DurinsAxe : public CardDefinition {
    DurinsAxe() : CardDefinition(Item()) {
        Item & def = std::get<Item>(type);
        def.name.push_back("Durin's Axe");
        def.type = Major;
        def.equipmentType = Weapon;
        def.unique = true;
        def.mp = 2;
        def.cp = 2;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayDurinsAxe>(), host);
    }
};

/* Enruned Shield */
struct EnrunedShieldCancelStrike : public EquipmentAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        if (game.getCardState(host) != Untapped)
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Strike>(context))
            return false;
        CardID parent = game.getParent(host);
        Strike & strike = std::get<Strike>(context);
        if (strike.target != parent)
            return false;
        if (strike.state > Strike::PlayResources)
            return false;
        if (!scene.hasSkill(parent, Warrior))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.tap(host);
        Strike & strike = std::get<Strike>(scene.context->context);
        strike.outcome = Ineffectual;
        strike.state = Strike::CloseStrike;
        scene.context->autoProceed = true;
    }
};
struct PlayEnrunedShield : public PlayItemAction {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<EquipmentBodyBonus>(host, 3, 10));
        side.actions.add(std::make_shared<EnrunedShieldCancelStrike>(), host);
    }
};
struct EnrunedShield : public CardDefinition {
    EnrunedShield() : CardDefinition(Item()) {
        Item & def = std::get<Item>(type);
        def.name.push_back("Enruned Shield");
        def.type = Greater;
        def.equipmentType = Shield;
        def.unique = true;
        def.hoard = true;
        def.mp = 3;
        def.cp = 2;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayEnrunedShield>(), host);
    }
};

/* Hauberk of Bright Mail */
struct HauberkOfBrightMailBody : public EquipmentEffect {
    HauberkOfBrightMailBody(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Stat>(batch.et))
            return false;
        Stat & stat = std::get<Stat>(batch.et);
        if (stat.type != Body)
            return false;
        Scene & scene = batch.scene;
        if (stat.target != scene.game.getParent(host))
            return false;
        if (stat.stat >= 9)
            return false;
        if (!scene.hasSkill(stat.target, Warrior))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        int & body = std::get<Stat>(batch.et).stat;
        body += 2;
        if (body > 9)
            body = 9;
    }
};
struct PlayHauberkOfBrightMail : public PlayItemAction {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<HauberkOfBrightMailBody>(host));
    }
};
struct HauberkOfBrightMail : public CardDefinition {
    HauberkOfBrightMail() : CardDefinition(Item()) {
        Item & def = std::get<Item>(type);
        def.name.push_back("Hauberk of Bright Mail");
        def.type = Major;
        def.equipmentType = Armor;
        def.mp = 2;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayHauberkOfBrightMail>(), host);
    }
};

/* Magical Harp */
struct MagicalHarpEffect : public UntilEndOfTurnPossessionEffect {
    MagicalHarpEffect(CardID host) : UntilEndOfTurnPossessionEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<AllowedOperation>(batch.et))
            return false;
        AllowedOperation & op = std::get<AllowedOperation>(batch.et);
        if (op.operation != Discard)
            return false;
        Game & game = batch.scene.game;
        if (!std::holds_alternative<Character>(game.getCardType(op.card)))
            return false;
        CompanyID id = game.getCompanyID(op.card);
        if (id < 0)
            return false;
        if (game.characterInCompany(game.getParent(host), id, game.cards[op.card].owner))
            return true;
        return false;
    }
    void activate(EffectBatch & batch) const {
        std::get<AllowedOperation>(batch.et).allowed = false;
    }
};
struct MagicalHarpAction : public EquipmentAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        if (game.getCardState(host) != Untapped)
            return false;
        if (game.currentPlayer != game.cards[host].owner && game.turnSequence.phase != SitePhase)
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        // Bearer can't be discarded by the corruption check from tapping the harp (!)
        Game & game = scene.game;
        scene.tap(host);
        game.effects.push_back(std::make_shared<MagicalHarpEffect>(host));
        CardID parent = game.getParent(host);
        scene.setupResolveCorruptionChecks(parent, parent).setupCorruptionCheck(parent);
    }
};
struct PlayMagicalHarp : public PlayItemAction {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.actions.add(std::make_shared<MagicalHarpAction>(), host);
    }
};
struct MagicalHarp : public CardDefinition {
    MagicalHarp() : CardDefinition(Item()) {
        Item & def = std::get<Item>(type);
        def.name.push_back("Magical Harp");
        def.type = Major;
        def.unique = true;
        def.hoard = true;
        def.mp = 2;
        def.cp = 2;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayMagicalHarp>(), host);
    }
};

/* Thrór's Map */
struct ThrorsMapAction : public EquipmentAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        if (!scene.resourceConditions(host))
            return false;
        CompanyID id = game.getCompanyID(host);
        if (id < 0)
            return false;
        CardID locationCard = game.getCurrentSide().findCompany(id)->currentLocation;
        if (game.getCardState(locationCard) != Tapped)
            return false;
        Location & location = game.getLocation(locationCard);
        for (auto info : location.automaticAttack)
            if (info->type == Dragon)
                return true;
        return false;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        CompanyID id = game.getCompanyID(host);
        CardID locationCard = game.getCurrentSide().findCompany(id)->currentLocation;
        scene.discard(host);
        scene.untapSite(locationCard);
    }
};
struct PlayThrorsMap : public PlayItemAction {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.actions.add(std::make_shared<ThrorsMapAction>(), host);
    }
};
struct ThrorsMap : public CardDefinition {
    ThrorsMap() : CardDefinition(Item()) {
        Item & def = std::get<Item>(type);
        def.name.push_back("Thrór's Map");
        def.type = Minor;
        def.unique = true;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayThrorsMap>(), host);
    }
};

/* Wormsbane */
struct WormsbaneProwess : public EquipmentEffect {
    WormsbaneProwess(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Stat>(batch.et))
            return false;
        Stat & stat = std::get<Stat>(batch.et);
        if (stat.type != Prowess)
            return false;
        if (stat.target != batch.scene.game.getParent(host))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        Stat & stat = std::get<Stat>(batch.et);
        int & prowess = stat.stat;
        if (stat.raceOfOpponent == Dragon || stat.raceOfOpponent == Drake) {
            prowess += 4;
            if (prowess > 12)
                prowess = 12;
            return;
        }
        prowess += 2;
        if (prowess > 9)
            prowess = 9;
    }
};
struct WormsbaneBody : public EquipmentEffect {
    WormsbaneBody(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Modifier>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<BodyCheck>(context))
            return false;
        BodyCheck & body = std::get<BodyCheck>(context);
        ContextType & parentContext = batch.scene.parent().context->context;
        if (!std::holds_alternative<Strike>(parentContext))
            return false;
        Strike & strike = std::get<Strike>(parentContext);
        if (body.target != strike.card)
            return false;
        if (strike.creatureType != Dragon && strike.creatureType != Drake)
            return false;
        if (strike.target != batch.scene.game.getParent(host))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<Modifier>(batch.et).modifier -= 2;
    }
};
struct PlayWormsbane : public PlayItemAction {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<WormsbaneProwess>(host));
        side.effects.push_back(std::make_shared<WormsbaneBody>(host));
    }
};
struct Wormsbane : public CardDefinition {
    Wormsbane() : CardDefinition(Item()) {
        Item & def = std::get<Item>(type);
        def.name.push_back("Wormsbane");
        def.type = Greater;
        def.equipmentType = Weapon;
        def.unique = true;
        def.mp = 4;
        def.cp = 2;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayWormsbane>(), host);
    }
};

/* A Friend or Three */
struct PlayAFriendOrThree : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        ContextType & context = scene.context->context;
        if (std::holds_alternative<InfluenceAttempt>(context) || std::holds_alternative<CorruptionCheck>(context))
            return true;
        return false;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        ContextType & context = scene.context->context;
        Game & game = scene.game;
        if (std::holds_alternative<InfluenceAttempt>(context)) {
            Company & company = *game.getCurrentSide().findCompany(game.getActiveCompany());
            int modifier = int(company.characters.size());
            std::get<InfluenceAttempt>(context).modifier += modifier;
        } else {
            auto & corruptionCheck = std::get<CorruptionCheck>(context);
            CompanyID id = game.getCompanyID(corruptionCheck.targetCharacter);
            Company & company = *game.getCurrentSide().findCompany(id);
            int modifier = int(company.characters.size());
            corruptionCheck.modifier += modifier;
        }
        scene.discard(host);
    }
};
struct AFriendOrThree : public CardDefinition {
    AFriendOrThree() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.name.push_back("A Friend or Three");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayAFriendOrThree>(), host);
    }
};

/* Bounty of the Hoard */
struct BountyOfTheHoardErrorEffect : public Effect {
    BountyOfTheHoardErrorEffect(CardID host) : Effect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<CloseContext>(batch.et))
            return false;
        if (!std::holds_alternative<Explore>(batch.getContext()))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        batch.scene.game.errors.push_back(host);
    }
    bool remove(EffectBatch & batch) const {
        if (!std::holds_alternative<DeclareAction>(batch.et))
            return false;
        Game & game = batch.scene.game;
        CardID target = std::get<DeclareAction>(batch.et).host;
        if (game.getDeck(target) != Pending)
            return false;
        CardType & type = game.getCardType(target);
        if (!std::holds_alternative<Item>(type))
            return false;
        ItemType itemType = std::get<Item>(type).type;
        if (itemType != Major && itemType != Minor)
            return false;
        return true;
    }
};
struct PlayBountyOfTheHoard : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        Game & game = scene.game;
        if (game.turnSequence.phase != SitePhase)
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Explore>(context))
            return false;
        Explore & explore = std::get<Explore>(context);
        if (game.getCardState(explore.site) != Tapped)
            return false;
        if (explore.state != Explore::ExploreSite || !explore.containsHoard)
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Explore & explore = std::get<Explore>(scene.context->context);
        explore.playable.push_back(Minor);
        explore.playable.push_back(Major);
        Side & side = scene.game.getCurrentSide();
        side.effects.push_back(std::make_shared<BountyOfTheHoardErrorEffect>(host));
        scene.discard(host);
    }
};
struct BountyOfTheHoard : public CardDefinition {
    BountyOfTheHoard() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.name.push_back("Bounty of the Hoard");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayBountyOfTheHoard>(), host);
    }
};

/* Concealment */
struct PlayConcealment : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        if (args.empty())
            return false;
        CardID card = args.front();
        Game & game = scene.game;
        if (game.getCardState(card) != Untapped)
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Attack>(context))
            return false;
        Attack & attack = std::get<Attack>(context);
        if (attack.mayNotBeCancelled)
            return false;
        if (!game.characterInCompany(game.getRoot(card), attack.company))
            return false;
        if (!scene.hasSkill(card, Scout))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        CardID target = args.front();
        scene.tap(target);
        scene.cancelAttack();
        scene.discard(host);
    }
};
struct Concealment : public CardDefinition {
    Concealment() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.name.push_back("Concealment");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayConcealment>(), host);
    }
};

/* Fellowship */
struct FellowshipProwess : public CompanyPermanentEventEffect {
    FellowshipProwess(CardID host) : CompanyPermanentEventEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        return (std::holds_alternative<Stat>(batch.et) && std::get<Stat>(batch.et).type == Prowess);
    }
    void activate(EffectBatch & batch) const {
        std::get<Stat>(batch.et).stat++;
    }
};
struct FellowshipCorruption : public CompanyPermanentEventEffect {
    FellowshipCorruption(CardID host) : CompanyPermanentEventEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Modifier>(batch.et))
            return false;
        if (!std::holds_alternative<CorruptionCheck>(batch.getContext()))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<Modifier>(batch.et).modifier++;
    }
};
struct DiscardFellowship : public CompanyPermanentEventEffect {
    DiscardFellowship(CardID host) : CompanyPermanentEventEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        Game & game = batch.scene.game;
        Side & side = game.getSide(host);
        auto it = side.findCompany(host, CompanyPermanentEvents);
        if (it == side.companies.end())
            return false;
        CompanyID id = it->id;
        if (std::holds_alternative<MoveCard>(batch.et)) {
            MoveCard & moveCard = std::get<MoveCard>(batch.et);
            CardType & type = game.getCardType(moveCard.target);
            CompanyID sourceCompany = game.getCompanyID(moveCard.target);
            if (std::holds_alternative<Ally>(type) && sourceCompany == id)
                return true;
            if (!std::holds_alternative<Character>(type))
                return false;
            if (sourceCompany == moveCard.id)              // Purely internal movement OK
                return false;
            if (sourceCompany != id && moveCard.id != id)  // Purely external movement OK
                return false;
            return true;
        }
        else if (std::holds_alternative<GainPossession>(batch.et)) {
            GainPossession & gain = std::get<GainPossession>(batch.et);
            if (!std::holds_alternative<Ally>(game.getCardType(gain.possession)))
                return false;
            if (game.getCompanyID(gain.target) != id)
                return false;
            return true;
        }
        return false;
    }
    void activate(EffectBatch & batch) const {
        batch.scene.discard(host);
    }
};
struct PlayFellowship : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        if (args.size() < 2)
            return false;
        Game & game = scene.game;
        if (game.turnSequence.phase != OrganizationPhase)
            return false;
        auto it = args.begin();
        CardType & type = game.getCardType(*it);
        if (!std::holds_alternative<Location>(type))
            return false;
        if (std::get<Location>(type).type != Haven)
            return false;
        ++it;
        CompanyID id = *it;
        if (id < 0)            // -1 if the location is in the LocationDeck
            return false;
        Side & side = game.getCurrentSide();
        auto companyIt = side.findCompany(id);
        if (companyIt == side.companies.end())
            return false;
        std::vector<CardID> & characters = companyIt->characters;
        if (characters.size() > 3)
            return true;
        int allies = 0;
        for (CardID character : characters)
            for (CardID possession : game.cards[character].possessions)
                if (std::holds_alternative<Ally>(game.getCardType(possession)))
                    allies++;
        if (characters.size() + allies > 3)
            return true;
        return false;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        CompanyID id = *(++args.begin());
        Game & game = scene.game;
        scene.moveCard(host, CompanyPermanentEvents, game.currentPlayer, id);
        insertPatches(game);
    }
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        auto it = side.findCompany(host, CompanyPermanentEvents);
        it->effects.push_back(std::make_shared<FellowshipProwess>(host));
        it->effects.push_back(std::make_shared<FellowshipCorruption>(host));
        side.effects.push_back(std::make_shared<DiscardFellowship>(host));
    }
};
struct Fellowship : public CardDefinition {
    Fellowship() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.name.push_back("Fellowship");
        def.type = PermanentEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayFellowship>(), host);
    }
};

/* Gates of Morning */
struct PlayGatesOfMorning : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        Game & game = scene.game;
        if (game.resourceEventInPlay("Gates of Morning", PermanentEvent))
            return false;
        return true;
    }
    void removeHazardEnvironmentEffects(Scene scene, EffectList & effects) const {
        Game & game = scene.game;
        for (auto element : effects) {
            auto effect = element.effect;
            if (std::holds_alternative<HazardEvent>(game.getCardType(effect->host))
                && scene.hasKeyword(effect->host, Environment))
                effects.remove(effect->host);
        }
    }
    void discardHazardEnvironmentCards(Scene scene, std::vector<CardID> & deck, int player) const {
        Game & game = scene.game;
        for (CardID card : deck)
            if (std::holds_alternative<HazardEvent>(game.getCardType(card))
                && scene.hasKeyword(card, Environment))
                scene.discard(card, player);
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        scene.moveCard(host, PermanentEvents, game.currentPlayer);
        removeHazardEnvironmentEffects(scene, game.effects);
        for (Side & side : game.sides) {
            discardHazardEnvironmentCards(scene, side.permanentEvents, side.player);
            discardHazardEnvironmentCards(scene, side.longEvents, side.player);
            removeHazardEnvironmentEffects(scene, side.effects);
            for (Company & company : side.companies)
                removeHazardEnvironmentEffects(scene, company.effects);
        }
    }
};
struct GatesOfMorning : public CardDefinition {
    GatesOfMorning() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.name.push_back("Gates of Morning");
        def.type = PermanentEvent;
        def.keywords = { Environment };
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayGatesOfMorning>(), host);
    }
};

/* Great-road */
struct GreatRoadEffect : public Effect {
    GreatRoadEffect(CardID host) : Effect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<DrawCardsLimit>(batch.et))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        DrawCardsLimit & limit = std::get<DrawCardsLimit>(batch.et);
        limit.opponentLimit *= 2;
    }
    bool remove(EffectBatch & batch) const {
        return std::holds_alternative<OpenContext>(batch.et) &&
            std::holds_alternative<DrawCards>(batch.getContext()) ||
            std::holds_alternative<NewPhase>(batch.et) &&
            batch.scene.game.turnSequence.phase == NextTurn;
    }
};
struct GreatRoadAction : public Action {
    GreatRoadAction(CompanyID company, CardID origin)
        : Action(), id(company), origin(origin) {}
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        if (game.turnSequence != TurnSequence(EndOfTurnPhase, Middle))
            return false;
        Side & side = game.getCurrentSide();
        auto it = side.findCompany(id);
        if (it == side.companies.end())    // Company got extinct
            return false;
        if (it->currentLocation == origin) // Company already back at start
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Side & side = scene.game.getCurrentSide();
        Company & company = *side.findCompany(id);
        company.newLocation = origin;
        scene.arriveAtLocation(company);
    }
    bool remove(Scene scene) const {
        return scene.game.turnSequence > TurnSequence(EndOfTurnPhase, Middle);
    }
    const CompanyID id;
    const CardID origin;
};
struct PlayGreatRoad : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        if (args.size() < 2)  // A selected location will always be accompanied by a CompanyID
            return false;
        Game & game = scene.game;
        TurnSequence & turn = game.turnSequence;
        if (turn.phase != OrganizationPhase || turn.part != End)
            return false;
        auto it = args.begin();
        CardType & type = game.getCardType(*it);
        if (!std::holds_alternative<Location>(type))
            return false;
        if (std::get<Location>(type).type != Haven)
            return false;
        ++it;
        CompanyID id = *it;
        if (id < 0)           // -1 if the location is in the LocationDeck
            return false;
        Side & side = game.getCurrentSide();
        auto companyIt = side.findCompany(id);
        if (companyIt == side.companies.end())
            return false;
        if (companyIt->newLocation == companyIt->currentLocation)
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        Side & side = game.getCurrentSide();
        auto it = args.begin();
        CardID origin = *(it++);
        CompanyID company = *it;
        EffectList & effects = side.findCompany(company)->effects;
        effects.push_back(std::make_shared<GreatRoadEffect>(host));
        side.actions.add(std::make_shared<GreatRoadAction>(company, origin), host);
        side.effects.push_back(std::make_shared<HaltProgressEffect>(host, EndOfTurnPhase, Middle));
        scene.discard(host);
    }
};
struct GreatRoad : public CardDefinition {
    GreatRoad() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.name.push_back("Great-road");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayGreatRoad>(), host);
    }
};

/* Hundreds of Butterflies */
struct PlayHundredsOfButterflies : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        Game & game = scene.game;
        if (game.turnSequence.phase != MovementHazardPhase)
            return false;
        if (args.empty())
            return false;
        CardID target = args.front();
        CompanyID id = game.getActiveCompany();
        if (!game.characterInCompany(target, id))
            return false;
        Company & company = *game.getCurrentSide().findCompany(id);
        if (company.currentLocation == company.newLocation)
            return false;
        if (game.getCardState(target) != Tapped)
            return false;
        if (!scene.allowedOperation(target, Untap))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.untap(args.front());
        scene.game.getMovement().hazardLimit++;
        scene.discard(host);
    }
};
struct HundredsOfButterflies : public CardDefinition {
    HundredsOfButterflies() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.name.push_back("Hundreds of Butterflies");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayHundredsOfButterflies>(), host);
    }
};

/* Lucky Search */
struct LuckySearchEffect : public StrikeEffect {
    LuckySearchEffect(CardID host, CardID item)
        : StrikeEffect(host), foundItem(item) {}
    bool conditions(EffectBatch & batch) const {
        /* A Character is wounded if the strike against him succeeds,
         * and the body check follows as a result of the wound.
         * Since the item gets discarded if the Character is wounded,
         * the likliest interpretation is that it shall not aid him
         * in the resulting body check.
         */
        if (!std::holds_alternative<OpenContext>(batch.et))
            return false;
        if (!std::holds_alternative<BodyCheck>(batch.getContext()))
            return false;
        Scene & scene = batch.scene;
        ContextType & parent = scene.game.contextStack[scene.context->parent]->context;
        if (!std::holds_alternative<Strike>(parent))
            return false;
        if (std::get<Strike>(parent).card == host)
            return true;
        return false;
    }
    void activate(EffectBatch & batch) const {
        batch.scene.discard(foundItem);
    }
    const CardID foundItem;
};
struct PlayLuckySearch : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        Game & game = scene.game;
        if (game.turnSequence.phase != SitePhase)
            return false;
        if (args.empty())
            return false;
        CardID card = args.front();
        if (game.getCardState(card) != Untapped)
            return false;
        CardType & cardType = game.getCardType(card);
        if (!std::holds_alternative<Character>(cardType))
            return false;
        if (!game.characterInCompany(card))
            return false;
        if (!scene.hasSkill(card, Scout))
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Explore>(context))
            return false;
        Explore & explore = std::get<Explore>(context);
        if (explore.state != Explore::ExploreSite)
            return false;
        SiteType siteType = scene.getSiteType(explore.site);
        if (siteType == ShadowHold || siteType == DarkHold)
            return true;
        return false;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        CardID target = args.front();
        scene.tap(target);
        // Draw cards from deck
        Game & game = scene.game;
        Side & side = game.getCurrentSide();
        int revealedCards = 0;
        std::vector<CardID> & playDeck = side.playDeck;
        CardID nextCard;
        auto it = playDeck.rbegin();
        for (; it != playDeck.rend(); ++it) {
            nextCard = *it;
            revealedCards++;
            CardType & type = game.getCardType(nextCard);
            if (std::holds_alternative<Item>(type)) {
                Item & item = std::get<Item>(type);
                if (item.type != Special)
                    if (!item.unique || game.uniqueItem(nextCard, item.name))
                        break;
            }
        }
        if (it != playDeck.rend()) {
            // Take item
            scene.gainPossession(target, nextCard);
            scene.insertPatches(nextCard);
            game.effects.push_back(std::make_shared<LuckySearchEffect>(host, nextCard));
        }
        // Create strike
        Strike strike;
        CompanyID companyID = game.getCompanyID(target);
        strike.state = Strike::TapCharacter;
        strike.card = host;
        strike.keyedTo = SiteInRegion;
        strike.creatureType = Unspecified;
        strike.creatureBaseProwess = 3 + revealedCards;
        strike.creatureBaseBody = 0;
        strike.target = target;
        strike.excessStrikes = 0;
        strike.mayNotBeCancelled = true;
        game.contextStack.push_back(std::make_shared<Context>(strike));
        scene.shuffleDeck(side);
        scene.discard(host);
    }
};
struct LuckySearch : public CardDefinition {
    LuckySearch() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.name.push_back("Lucky Search");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayLuckySearch>(), host);
    }
};

/* Marvels Told */
struct PlayMarvelsTold : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        if (args.size() < 2)
            return false;
        Game & game = scene.game;
        auto it = args.begin();
        CardID character = *it;
        if (game.cards[character].owner != game.cards[host].owner)
            return false;
        if (game.getCardState(character) != Untapped)
            return false;
        if (!scene.hasSkill(character, Sage))
            return false;
        CardID target = *(++it);
        Card & card = game.cards[target];
        if (card.deck <= OutOfPlay || card.deck == Pending || card.deck == OnGuard)
            return false;
        CardType & type = game.settings.cardDefinitions[card.dataIndex]->type;
        if (!std::holds_alternative<HazardEvent>(type))
            return false;
        HazardEvent & ev = std::get<HazardEvent>(type);
        if (ev.type != PermanentEvent && ev.type != LongEvent)
            return false;
        for (EventKeyword key : ev.keywords)
            if (key == Environment)
                return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        auto it = args.begin();
        CardID character = *it;
        scene.tap(character);
        scene.discard(*(++it));
        scene.discard(host);
        Scene resolve = scene.setupResolveCorruptionChecks(host, character);
        Scene corruption = resolve.setupCorruptionCheck(host);
        std::get<CorruptionCheck>(corruption.context->context).modifier -= 2;
    }
};
struct MarvelsTold : public CardDefinition {
    MarvelsTold() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.name.push_back("Marvels Told");
        def.type = ShortEvent;
        def.keywords = { Ritual };
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayMarvelsTold>(), host);
    }
};

/* Not at Home */
struct NotAtHomeCancelAttack : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Attack>(context))
            return false;
        Attack & attack = std::get<Attack>(context);
        if (attack.mayNotBeCancelled)
            return false;
        Race race = attack.creatureType;
        if (race != Dragon && race != Drake && race != Troll)
            return false;
        if (attack.automaticAttack ||
            std::holds_alternative<SiteType>(attack.keyedTo) ||
            std::holds_alternative<std::string>(attack.keyedTo))
            return true;
        return false;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.cancelAttack();
        scene.discard(host);
    }
};
struct NotAtHomeReduceStrikes : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Attack>(context))
            return false;
        Attack & attack = std::get<Attack>(context);
        if (!attack.automaticAttack)
            return false;
        if (attack.strikes < 2)
            return false;
        if (!scene.game.resourceEventInPlay("Gates of Morning", PermanentEvent))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Attack & attack = std::get<Attack>(scene.context->context);
        attack.strikes -= (attack.strikes > 2) ? 2 : 1;
        scene.discard(host);
    }
};
struct NotAtHome : public CardDefinition {
    NotAtHome() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.name.push_back("Not at Home");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<NotAtHomeCancelAttack>(), host, "Cancel");
        actions.add(std::make_shared<NotAtHomeReduceStrikes>(), host, "Reduce strikes");
    }
};

/* Promptings of Wisdom */
struct PromptingsOfWisdomReturnToOrigin : public UntilEndOfTurnPossessionEffect {
    PromptingsOfWisdomReturnToOrigin(CardID host) : UntilEndOfTurnPossessionEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<AllowedOperation>(batch.et))
            return false;
        AllowedOperation & op = std::get<AllowedOperation>(batch.et);
        if (op.operation != ReturnToOrigin)
            return false;
        Game & game = batch.scene.game;
        if (!game.characterInCompany(game.getParent(host)))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<AllowedOperation>(batch.et).allowed = false;
    }
};
struct PromptingsOfWisdomTapSite : public UntilEndOfTurnPossessionEffect {
    PromptingsOfWisdomTapSite(CardID host) : UntilEndOfTurnPossessionEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<AllowedOperation>(batch.et))
            return false;
        AllowedOperation & op = std::get<AllowedOperation>(batch.et);
        if (op.operation != Tap)
            return false;
        Game & game = batch.scene.game;
        if (!std::holds_alternative<Location>(game.getCardType(op.card)))
            return false;
        Company & company = *game.getCurrentSide().findCompany(game.getParent(host), Characters);
        if (op.card != company.newLocation)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<AllowedOperation>(batch.et).allowed = false;
    }
};
struct PromptingsOfWisdomAction : public EquipmentAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        Game & game = scene.game;
        if (game.getCardState(game.getParent(host)) != Untapped)
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        Side & side = game.getCurrentSide();
        scene.tap(host);
        side.effects.push_back(std::make_shared<PromptingsOfWisdomTapSite>(host));
        side.effects.push_back(std::make_shared<PromptingsOfWisdomReturnToOrigin>(host));
        scene.setupResolveCorruptionChecks(host, game.getParent(host));
    }
};
struct PlayPromptingsOfWisdom : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        if (!scene.resourceConditions(host))
            return false;
        if (game.turnSequence.phase != OrganizationPhase)
            return false;
        if (args.empty())
            return false;
        CardID target = args.front();
        if (game.cards[target].owner != game.currentPlayer)
            return false;
        if (!std::holds_alternative<Character>(game.getCardType(target)))
            return false;
        if (!scene.hasSkill(target, Ranger))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.gainPossession(args.front(), host);
        insertPatches(scene.game);
    }
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.actions.add(std::make_shared<PromptingsOfWisdomAction>(), host);
        side.effects.push_back(std::make_shared<IncreaseCorruptionEffect>(host, 2));
        side.effects.push_back(std::make_shared<NoDuplicatesInCompanyEffect>(host));
    }
};
struct PromptingsOfWisdom : public CardDefinition {
    PromptingsOfWisdom() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.name.push_back("Promptings of Wisdom");
        def.type = PermanentEvent;
        def.keywords = { LightEnchantment };
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayPromptingsOfWisdom>(), host);
    }
};

/* Risky Blow */
struct PlayRiskyBlow : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Strike>(context))
            return false;
        Strike & strike = std::get<Strike>(context);
        if (strike.state != Strike::PlayResources)
            return false;
        if (strike.statType != Prowess)
            return false;
        if (strike.skillCardsUsed)
            return false;
        if (scene.hasSkill(strike.target, Warrior))
            return true;
        return false;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Strike & strike = std::get<Strike>(scene.context->context);
        strike.targetProwessModifier += 3;
        strike.targetBodyModifier -= 1;
        strike.skillCardsUsed++;
        scene.discard(host);
    }
};
struct RiskyBlow : public CardDefinition {
    RiskyBlow() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.name.push_back("Risky Blow");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayRiskyBlow>(), host);
    }
};

/* Smoke Rings */
struct PlaySmokeRings : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        Game & game = scene.game;
        Side & side = game.getCurrentSide();
        if (game.containsCardCategory(side.discardPile, ResourceCard) ||
            game.containsCardCategory(side.discardPile, CharacterCard) ||
            game.containsCardCategory(side.sideboard, ResourceCard) ||
            game.containsCardCategory(side.sideboard, CharacterCard))
            return true;
        return false;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        Side & side = game.getCurrentSide();
        TransferCards context;
        TransferCards::TransferCardsOption option;
        option.sourceDeck = Sideboard;
        option.destinationDeck = PlayDeck;
        option.maximum = option.minimum = 1;
        option.categories = { ResourceCard, CharacterCard };
        context.transactions[game.currentPlayer].push_back(option);
        option.sourceDeck = DiscardPile;
        context.transactions[game.currentPlayer].push_back(option);
        game.contextStack.push_back(std::make_shared<Context>(context));
        // The card is automatically discarded after TransferCards has resolved
    }
};
struct SmokeRings : public CardDefinition {
    SmokeRings() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.name.push_back("Smoke Rings");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlaySmokeRings>(), host);
    }
};

/* The Dwarves Are upon You! */
struct TheDwarvesAreUponYouProwess : public AttackEffect {
    TheDwarvesAreUponYouProwess(CardID host) : AttackEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Stat>(batch.et))
            return false;
        Stat & stat = std::get<Stat>(batch.et);
        if (stat.type != Prowess)
            return false;
        CardType & type = batch.scene.game.getCardType(stat.target);
        if (!std::holds_alternative<Character>(type))
            return false;
        if (std::get<Character>(type).race != Dwarf)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<Stat>(batch.et).stat += 2;
    }
};
struct TheDwarvesAreUponYouBody : public AttackEffect {
    TheDwarvesAreUponYouBody(CardID host) : AttackEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Stat>(batch.et))
            return false;
        Stat & stat = std::get<Stat>(batch.et);
        if (stat.type != Body)
            return false;
        CardType & type = batch.scene.game.getCardType(stat.target);
        if (!std::holds_alternative<Character>(type))
            return false;
        if (std::get<Character>(type).race != Dwarf)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<Stat>(batch.et).stat -= 1;
    }
};
struct PlayTheDwarvesAreUponYou : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Attack>(context))
            return false;
        Game & game = scene.game;
        if (game.duplicateEffect(host, game.currentPlayer))
            return false;
        Side & side = game.getCurrentSide();
        Company & company = *side.findCompany(game.getActiveCompany());
        return game.raceInCompany(company, Dwarf);
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        insertPatches(scene.game);
        scene.discard(host);
    }
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<TheDwarvesAreUponYouProwess>(host));
        side.effects.push_back(std::make_shared<TheDwarvesAreUponYouBody>(host));
    }
};
struct TheDwarvesAreUponYou : public CardDefinition {
    TheDwarvesAreUponYou() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.name.push_back("The Dwarves Are upon You!");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayTheDwarvesAreUponYou>(), host);
    }
};

/* The Old Thrush */
struct TheOldThrushDuplicateEffect : public AttackEffect {
    TheOldThrushDuplicateEffect(CardID host) : AttackEffect(host) {}
    bool conditions(EffectBatch & batch) const { return false; }
    void activate(EffectBatch & batch) const {}
};
struct PlayTheOldThrush : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Attack>(context))
            return false;
        Attack & attack = std::get<Attack>(context);
        if (attack.creatureType == Nazgul)
            return false;
        if (scene.game.duplicateEffect(host))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Attack & attack = std::get<Attack>(scene.context->context);
        attack.prowess -= 3;
        attack.body -= 1;
        scene.game.effects.push_back(std::make_shared<TheOldThrushDuplicateEffect>(host));
        scene.discard(host);
    }
};
struct TheOldThrush : public CardDefinition {
    TheOldThrush() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.name.push_back("The Old Thrush");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayTheOldThrush>(), host);
    }
};

/* Covetous Thoughts */
struct CovetousThoughtsEffect : public EquipmentEffect {
    CovetousThoughtsEffect(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<NewPhase>(batch.et))
            return false;
        Game & game = batch.scene.game;
        if (game.turnSequence != TurnSequence(EndOfTurnPhase, End))
            return false;
        if (game.cards[game.getParent(host)].owner != game.currentPlayer)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        ResolveCorruptionChecks resolve;
        Game & game = batch.scene.game;
        CardID target = game.getParent(host);
        Company & company = *game.getCurrentSide().findCompany(target, Characters);
        for (CardID character : company.characters)
            if (character != target) {
                std::vector<CardID> & possessions = game.cards[character].possessions;
                for (CardID possession : possessions) {
                    CardType & type = game.getCardType(possession);
                    if (std::holds_alternative<Item>(type)) {
                        resolve.unresolvedChecks.emplace_back(possession, target, -std::get<Item>(type).cp);
                    }
                }
            }
        if (!resolve.unresolvedChecks.empty())
            game.contextStack.push_back(std::make_shared<Context>(resolve));
    }
};
struct PlayCovetousThoughts : public PlayCorruptionCard {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!coreConditions(scene, args))
            return false;
        if (!scene.game.getCharacter(args.front()).minion)
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        CardID target = args.front();
        placeCard(scene, target);
        insertPatches(scene.game);
    }
    void insertPatches(Game & game) const {
        Side & side = game.getSide(game.getParent(host));
        side.effects.push_back(std::make_shared<IncreaseCorruptionEffect>(host, 1));
        side.effects.push_back(std::make_shared<CovetousThoughtsEffect>(host));
        side.actions.add(std::make_shared<RemoveCorruptionCardAction>(5), host);
    }
};
struct CovetousThoughts : public CardDefinition {
    CovetousThoughts() : CardDefinition(HazardEvent()) {
        HazardEvent & def = std::get<HazardEvent>(type);
        def.name.push_back("Covetous Thoughts");
        def.type = PermanentEvent;
        def.keywords.push_back(Corruption);
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayCovetousThoughts>(), host);
    }
};

/* Despair of the Heart */
struct DespairOfTheHeartEffect : public EquipmentEffect {
    DespairOfTheHeartEffect(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        // Corruption check happens after body check (METW, p.31, 2nd clarification)
        if (!std::holds_alternative<CloseContext>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<BodyCheck>(context))
            return false;
        // Is the wounded character in bearer's company?
        Game & game = batch.scene.game;
        BodyCheck & bodyCheck = std::get<BodyCheck>(context);
        if (bodyCheck.defendingCompany != game.getCompanyID(game.getParent(host)))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        Game & game = batch.scene.game;
        batch.scene.setupResolveCorruptionChecks(host, game.getParent(host));
    }
};
struct PlayDespairOfTheHeart : public PlayCorruptionCard {
    PlayDespairOfTheHeart() : PlayCorruptionCard({ Hobbit, Wizard, Ringwraith }) {}
    void activate(Scene scene, const std::list<CardID> & args) const {
        CardID target = args.front();
        placeCard(scene, target);
        insertPatches(scene.game);
    }
    void insertPatches(Game & game) const {
        Side & side = game.getSide(game.getParent(host));
        side.effects.push_back(std::make_shared<IncreaseCorruptionEffect>(host, 2));
        side.effects.push_back(std::make_shared<DespairOfTheHeartEffect>(host));
        side.actions.add(std::make_shared<RemoveCorruptionCardAction>(4), host);
    }
};
struct DespairOfTheHeart : public CardDefinition {
    DespairOfTheHeart() : CardDefinition(HazardEvent()) {
        HazardEvent & def = std::get<HazardEvent>(type);
        def.name.push_back("Despair of the Heart");
        def.type = PermanentEvent;
        def.keywords.push_back(Corruption);
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayDespairOfTheHeart>(), host);
    }
};

/* Full of Froth and Rage */
struct DiscardFullOfFrothAndRage : public PermanentEventEffect {
    DiscardFullOfFrothAndRage(CardID host) : PermanentEventEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<CloseContext>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<Attack>(context))
            return false;
        Attack & attack = std::get<Attack>(context);
        if (attack.detainment || attack.outcome == Successful)
            return false;
        if (attack.creatureType != Animal && attack.creatureType != Spider)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        batch.scene.discard(host);
    }
};
struct FullOfFrothAndRageEffect : public PermanentEventEffect {
    FullOfFrothAndRageEffect(CardID host) : PermanentEventEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Stat>(batch.et))
            return false;
        Stat & stat = std::get<Stat>(batch.et);
        if (stat.type != Prowess)
            return false;
        if (stat.raceOfOpponent != Animal && stat.raceOfOpponent != Spider)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<Stat>(batch.et).stat += 2;
    }
};
struct PlayFullOfFrothAndRage : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.hazardConditions(host))
            return false;
        if (scene.game.hazardEventInPlay(host))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        scene.moveCard(host, PermanentEvents, game.hazardPlayer);
        insertPatches(game);
    }
    void insertPatches(Game & game) const {
        game.effects.push_back(std::make_shared<FullOfFrothAndRageEffect>(host));
        game.effects.push_back(std::make_shared<DiscardFullOfFrothAndRage>(host));
    }
};
struct FullOfFrothAndRage : public CardDefinition {
    FullOfFrothAndRage() : CardDefinition(HazardEvent()) {
        HazardEvent & def = std::get<HazardEvent>(type);
        def.name.push_back("Full of Froth and Rage");
        def.type = PermanentEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayFullOfFrothAndRage>(), host);
    }
};

/* Lost in Free-domains */
struct PlayLostInFreeDomains : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.narrowHazardConditions(host))
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Movement>(context))
            return false;
        SitePath sitePath = scene.getSitePath();
        for (PathSymbol symbol : sitePath)
            if (symbol == FreeDomain)
                return true;
        return false;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Side & side = scene.game.getCurrentSide();
        Movement & movement = std::get<Movement>(scene.context->context);
        side.effects.push_back(std::make_shared<DoNothingDuringSitePhaseEffect>(host, movement.destination));
        scene.discard(host);
    }
};
struct LostInFreeDomains : public CardDefinition {
    LostInFreeDomains() : CardDefinition(HazardEvent()) {
        HazardEvent & def = std::get<HazardEvent>(type);
        def.name.push_back("Lost in Free-domains");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayLostInFreeDomains>(), host);
    }
};

/* Muster Disperses */
struct MusterDispersesEffect : public SpecialEffect {
    MusterDispersesEffect(CardID host) : SpecialEffect(host) {}
    bool conditions(SpecialTest & test) const {
        return test.roll + test.modifier < 11;
    }
    void activate(EffectBatch & batch) const {
        batch.scene.discard(std::get<SpecialTest>(batch.getContext()).target);
    }
};
struct PlayMusterDisperses : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.narrowHazardConditions(host))
            return false;
        if (args.empty())
            return false;
        Game & game = scene.game;
        CardID target = args.front();
        Card & card = game.cards[target];
        if (card.owner != game.currentPlayer)
            return false;
        if (card.deck != Factions)
            return false;
        if (!std::holds_alternative<Faction>(game.getCardType(target)))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        CardID target = args.front();
        Scene currentScene = scene.setupSpecialTest(host, game.currentPlayer, target);
        SpecialTest & test = std::get<SpecialTest>(currentScene.context->context);
        test.modifier = scene.getUnusedGI(game.currentPlayer);
        if (test.modifier < 0)
            test.modifier = 0;
        game.effects.push_back(std::make_shared<MusterDispersesEffect>(host));
        scene.discard(host);
    }
};
struct MusterDisperses : public CardDefinition {
    MusterDisperses() : CardDefinition(HazardEvent()) {
        HazardEvent & def = std::get<HazardEvent>(type);
        def.name.push_back("Muster Disperses");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayMusterDisperses>(), host);
    }
};


/* River */
struct RiverAction : public Action {
    RiverAction(CardID location) : location(location) {}
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (args.empty())
            return false;
        Game & game = scene.game;
        CardID card = args.front();
        if (game.getCardState(card) != Untapped)
            return false;
        Side & side = game.getCurrentSide();
        auto it = side.findCompany(card, Characters);
        if (it == side.companies.end())
            return false;
        if (it->id != game.getActiveCompany())
            return false;
        if (it->newLocation != location)
            return false;
        if (game.turnSequence.phase == MovementHazardPhase && it->newLocation == it->currentLocation)
            return false;
        if (!scene.hasSkill(card, Ranger))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.tap(args.front());
        scene.removeCardEffects(host, scene.game.currentPlayer);
    }
    bool remove(Scene scene) const {
        return !scene.game.hasEffect(host, scene.game.currentPlayer);
    }
    const CardID location;
};
struct RiverEffect : public Effect {
    // The question is then whether or not the company obtained from combining a moving and a staying
    // company is defined as a moving company. Since the companies actually join each other without
    // having to face the river first, the natural physical consequence is that the river must be faced
    // afterwards by the united company.
    RiverEffect(CardID host, CardID location)
        : Effect(host)
        , location(location)
        , mayEnter(true) {}
    bool conditions(EffectBatch & batch) const {
        if (std::holds_alternative<AllowedOperation>(batch.et)) {
            AllowedOperation & op = std::get<AllowedOperation>(batch.et);
            if (op.card == location && op.operation == EnterSite)
                return true;
        } else if (std::holds_alternative<ArriveAtLocation>(batch.et)) {
            ArriveAtLocation & arrive = std::get<ArriveAtLocation>(batch.et);
            if (arrive.location == location)
                return true;
        }
        return false;
    }
    void activate(EffectBatch & batch) const {
        if (std::holds_alternative<AllowedOperation>(batch.et))
            std::get<AllowedOperation>(batch.et).allowed = mayEnter;
        else {
            auto effect = std::make_shared<RiverEffect>(*this);
            batch.effectQueue.front()->effect = effect;
            effect->mayEnter = false;  // Set to false whenever a moving company arrives at the site
        }
    }
    bool remove(EffectBatch & batch) const {
        if (!std::holds_alternative<CloseContext>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (std::holds_alternative<Arrival>(context) && mayEnter)
            return true;
        if (!std::holds_alternative<Explore>(context))
            return false;
        if (std::get<Explore>(context).site != location)
            return false;
        return true;
    }
    const CardID location;
    bool mayEnter;
};
struct PlayRiver : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.narrowHazardConditions(host))
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Movement>(context))
            return false;
        Movement & movement = std::get<Movement>(context);
        if (movement.siteOfOrigin == movement.destination)
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Side & side = scene.game.getCurrentSide();
        Movement & movement = std::get<Movement>(scene.context->context);
        side.effects.push_back(std::make_shared<RiverEffect>(host, movement.destination));
        side.actions.add(std::make_shared<RiverAction>(movement.destination), host);
        scene.discard(host);
    }
};
struct River : public CardDefinition {
    River() : CardDefinition(HazardEvent()) {
        HazardEvent & def = std::get<HazardEvent>(type);
        def.name.push_back("River");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayRiver>(), host);
    }
};

/* Seized by Terror */
struct SeizedByTerrorEffect : public SpecialEffect {
    SeizedByTerrorEffect(CardID host) : SpecialEffect(host) {}
    bool conditions(SpecialTest & test) const {
        return test.roll + test.modifier < 12;
    }
    void activate(EffectBatch & batch) const {
        Scene & scene = batch.scene;
        CardID target = std::get<SpecialTest>(batch.getContext()).target;
        Company & company = *scene.game.getSide(target).findCompany(target, Characters);
        if (company.characters.size() > 1) {
            // Character is not removed from play, so GI may be updated (MELE, p.19, 3rd paragraph)
            scene.dropFollowers(target); // Updates GI
            scene.transferCharacter(target, scene.addCompany(company.currentLocation));
        } else {
            company.newLocation = company.currentLocation;
            scene.forceQuitCompany();
        }
    }
};
struct PlaySeizedByTerror : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.narrowHazardConditions(host))
            return false;
        if (args.empty())
            return false;
        Game & game = scene.game;
        CardID target = args.front();
        if (!game.characterInCompany(target))
            return false;
        Race race = scene.getRace(target);
        if (race == Wizard || race == Ringwraith)
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Movement>(context))
            return false;
        SitePath sitePath = scene.getSitePath();
        for (PathSymbol pathSymbol : sitePath)
            if (pathSymbol == ShadowLands || pathSymbol == DarkDomain)
                return true;
        return false;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        CardID target = args.front();
        Scene currentScene = scene.setupSpecialTest(host, game.currentPlayer, target);
        SpecialTest & test = std::get<SpecialTest>(currentScene.context->context);
        test.modifier = scene.getMind(target, game.getActiveCompany());
        game.effects.push_back(std::make_shared<SeizedByTerrorEffect>(host));
        scene.discard(host);
    }
};
struct SeizedByTerror : public CardDefinition {
    SeizedByTerror() : CardDefinition(HazardEvent()) {
        HazardEvent & def = std::get<HazardEvent>(type);
        def.name.push_back("Seized by Terror");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlaySeizedByTerror>(), host);
    }
};

/* Wake of War */
struct WakeOfWarEffect : public LongEventEffect {
    WakeOfWarEffect(CardID host, StatType type, int wolfBonus)
        : LongEventEffect(host)
        , type(type)
        , wolfBonus(wolfBonus) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Stat>(batch.et))
            return false;
        Stat & stat = std::get<Stat>(batch.et);
        if (stat.type != type)
            return false;
        Race race = stat.raceOfOpponent;
        if (race != Wolf && race != Spider && race != Animal)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        Stat & et = std::get<Stat>(batch.et);
        int & stat = et.stat;
        stat += (et.raceOfOpponent != Wolf) ? 1 : wolfBonus;
    }
    const StatType type;
    const int wolfBonus;
};
struct PlayWakeOfWar : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.hazardConditions(host))
            return false;
        if (scene.game.hazardEventInPlay(host))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.moveCard(host, LongEvents, scene.game.cards[host].owner);
        insertPatches(scene.game);
    }
    void insertPatches(Game & game) const {
        // According to (MELE p.70), the extra bonus for wolves applies iff
        // Doors of Night was in play at the moment Wake of War was resolved.
        int wolfBonus = game.hazardEventInPlay("Doors of Night", PermanentEvent) ? 2 : 1;
        game.effects.push_back(std::make_shared<WakeOfWarEffect>(host, Strikes, wolfBonus));
        game.effects.push_back(std::make_shared<WakeOfWarEffect>(host, Prowess, wolfBonus));
    }
};
struct WakeOfWar : public CardDefinition {
    WakeOfWar() : CardDefinition(HazardEvent()) {
        HazardEvent & def = std::get<HazardEvent>(type);
        def.name.push_back("Wake of War");
        def.type = LongEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayWakeOfWar>(), host);
    }
};


/* Cave Worm */
struct CaveWorm : public HazardCreatureDefinition {
    CaveWorm() {
        HazardCreature & def = std::get<HazardCreature>(type);
        def.name.push_back("Cave Worm");
        def.playable = { RedhornGate, HighPass, GapOfIsen, Angmar, Gundabad,
                      GreyMountainNarrows, WitheredHeath, Numeriador, IronHills };
        def.race = Drake;
        def.strikes = 1;
        def.prowess = 16;
    }
};

/* Giant Spiders */
struct GiantSpidersEffect : public AttackEffect {
    GiantSpidersEffect(CardID host) : AttackEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Roll>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<BodyCheck>(context))
            return false;
        BodyCheck & body = std::get<BodyCheck>(context);
        Race race = batch.scene.getRace(body.target);
        if (race == Wizard || race == Ringwraith)
            return false;
        if (std::get<Roll>(batch.et).result + body.modifier != body.body)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        CardID target = std::get<BodyCheck>(batch.getContext()).target;
        batch.scene.discard(target);
    }
};
struct PlayGiantSpiders : public PlayHazardCreature {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!PlayHazardCreature::conditions(scene, args))
            return false;
        if (!scene.allowedOperation(host, Play))
            return false;
        ContextType & context = scene.context->context;
        if (std::holds_alternative<Explore>(context)
            || std::holds_alternative<SiteType>(std::get<Movement>(context).key)) {
            Game & game = scene.game;
            RegionNumber region =
                game.getLocation(game.getCurrentSide().findCompany(game.getActiveCompany())->newLocation).region;
            return region == HeartOfMirkwood || region == SouthernMirkwood
                || region == WesternMirkwood || region == WoodlandRealm;
        }
        return true;
    }
    void insertEffects(Scene scene, const std::list<CardID> & args) const {
        scene.game.effects.push_back(std::make_shared<GiantSpidersEffect>(host));
    }
};
struct GiantSpiders : public CardDefinition {
    GiantSpiders() : CardDefinition(HazardCreature()) {
        HazardCreature & def = std::get<HazardCreature>(type);
        def.name.push_back("Giant Spiders");
        def.playable = { Wilderness, Wilderness, RuinsLairs, ShadowHold, DarkHold
                       , HeartOfMirkwood, SouthernMirkwood, WesternMirkwood, WoodlandRealm };
        def.race = Spider;
        def.strikes = 2;
        def.prowess = 10;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayGiantSpiders>(), host);
    }
};

/* Lesser Spiders */
struct LesserSpiders : public HazardCreatureDefinition {
    LesserSpiders() {
        HazardCreature & def = std::get<HazardCreature>(type);
        def.name.push_back("Lesser Spiders");
        def.playable = { Wilderness, ShadowLands, RuinsLairs };
        def.race = Spider;
        def.strikes = 4;
        def.prowess = 7;
    }
};

/* Neeker-breekers */
struct NeekerBreekerTargetFilter : public AttackEffect {
    NeekerBreekerTargetFilter(CardID host) : AttackEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<AllowedOperation>(batch.et))
            return false;
        AllowedOperation & op = std::get<AllowedOperation>(batch.et);
        if (op.operation != Target)
            return false;
        Race race = batch.scene.getRace(op.card);
        if (race != Wizard && race != Ringwraith)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        AllowedOperation & op = std::get<AllowedOperation>(batch.et);
        op.allowed = false;
    }
};
struct TapOnFailedStrike : public AttackEffect {
    TapOnFailedStrike(CardID host) : AttackEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Roll>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<Strike>(context))
            return false;
        Strike & strike = std::get<Strike>(context);
        Scene & scene = batch.scene;
        if (batch.scene.strikeResult(strike) < 0)
            return true;
        return false;
    }
    void activate(EffectBatch & batch) const {
        Strike & strike = std::get<Strike>(batch.getContext());
        strike.outcome = Successful;
        batch.scene.tap(strike.target);
        strike.state = Strike::CloseStrike;
        batch.scene.context->autoProceed = true;
    }
};
struct PlayNeekerBreekers : public PlayHazardCreature {
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        scene.moveCard(host, Creatures, game.hazardPlayer);
        game.effects.push_back(std::make_shared<NeekerBreekerTargetFilter>(host));
        game.effects.push_back(std::make_shared<TapOnFailedStrike>(host));
        KeyedToType key = std::get<Movement>(scene.context->context).key;
        scene.setupEncounter(host, game.getActiveCompany(), key);
    }
};
struct NeekerBreekers : public CardDefinition {
    NeekerBreekers() : CardDefinition(HazardCreature()) {
        HazardCreature & def = std::get<HazardCreature>(type);
        def.name.push_back("Neeker-breekers");
        def.playable = { BorderLands, Wilderness, ShadowLands, DarkDomain, RuinsLairs };
        def.race = Animal;
        def.stat = Mind;
        def.strikes = 0;
        def.prowess = 7;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayNeekerBreekers>(), host);
    }
};

/* Wargs */
struct Wargs : public HazardCreatureDefinition {
    Wargs() {
        HazardCreature & def = std::get<HazardCreature>(type);
        def.name.push_back("Wargs");
        def.playable = { BorderLands, Wilderness, ShadowLands };
        def.race = Wolf;
        def.strikes = 2;
        def.prowess = 9;
    }
};

/* Watcher in the Water */
struct WatcherInTheWater : public HazardCreatureDefinition {
    WatcherInTheWater() {
        HazardCreature & def = std::get<HazardCreature>(type);
        def.name.push_back("Watcher in the Water");
        def.playable = { Wilderness, Wilderness, CoastalSeas, "Moria" };
        def.race = Animal;
        def.strikes = 0;
        def.prowess = 8;
    }
};

struct ActionSignature {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
    }
};

struct EffectSignature {
    bool conditions(EffectBatch & batch) const {
    }
    void activate(EffectBatch & batch) const {
    }
};

void addDwarfDeckDefinitions(GameSettings & settings) {
    settings.addCardDefinition(std::make_unique<BlueMountainDwarfHold>());
    settings.addCardDefinition(std::make_unique<BuhrWidu>());
    settings.addCardDefinition(std::make_unique<CavesOfUlund>());
    settings.addCardDefinition(std::make_unique<DeadMarshes>());
    settings.addCardDefinition(std::make_unique<GoblinGate>());
    settings.addCardDefinition(std::make_unique<IronHillDwarfHold>());
    settings.addCardDefinition(std::make_unique<Moria>());
    settings.addCardDefinition(std::make_unique<OvirHollow>());
    settings.addCardDefinition(std::make_unique<TheLonelyMountain>());
    settings.addCardDefinition(std::make_unique<SarnGoriwing>());
    settings.addCardDefinition(std::make_unique<ShrelKain>());
    settings.addCardDefinition(std::make_unique<Wellinghall>());
    settings.addCardDefinition(std::make_unique<Pallando>());
    settings.addCardDefinition(std::make_unique<Balin>());
    settings.addCardDefinition(std::make_unique<BoromirII>());
    settings.addCardDefinition(std::make_unique<Celeborn>());
    settings.addCardDefinition(std::make_unique<Dori>());
    settings.addCardDefinition(std::make_unique<Fili>());
    settings.addCardDefinition(std::make_unique<Gloin>());
    settings.addCardDefinition(std::make_unique<GildorInglorion>());
    settings.addCardDefinition(std::make_unique<Kili>());
    settings.addCardDefinition(std::make_unique<Oin>());
    settings.addCardDefinition(std::make_unique<ThorinII>());
    settings.addCardDefinition(std::make_unique<Gollum>());
    settings.addCardDefinition(std::make_unique<Quickbeam>());
    settings.addCardDefinition(std::make_unique<BlueMountainDwarves>());
    settings.addCardDefinition(std::make_unique<IronHillDwarves>());
    settings.addCardDefinition(std::make_unique<MenOfDorwinion>());
    settings.addCardDefinition(std::make_unique<BowOfDragonHorn>());
    settings.addCardDefinition(std::make_unique<BookOfMazarbul>());
    settings.addCardDefinition(std::make_unique<Cram>());
    settings.addCardDefinition(std::make_unique<DurinsAxe>());
    settings.addCardDefinition(std::make_unique<EnrunedShield>());
    settings.addCardDefinition(std::make_unique<HauberkOfBrightMail>());
    settings.addCardDefinition(std::make_unique<MagicalHarp>());
    settings.addCardDefinition(std::make_unique<ThrorsMap>());
    settings.addCardDefinition(std::make_unique<Wormsbane>());
    settings.addCardDefinition(std::make_unique<AFriendOrThree>());
    settings.addCardDefinition(std::make_unique<BountyOfTheHoard>());
    settings.addCardDefinition(std::make_unique<Concealment>());
    settings.addCardDefinition(std::make_unique<Fellowship>());
    settings.addCardDefinition(std::make_unique<GatesOfMorning>());
    settings.addCardDefinition(std::make_unique<GreatRoad>());
    settings.addCardDefinition(std::make_unique<HundredsOfButterflies>());
    settings.addCardDefinition(std::make_unique<LuckySearch>());
    settings.addCardDefinition(std::make_unique<MarvelsTold>());
    settings.addCardDefinition(std::make_unique<NotAtHome>());
    settings.addCardDefinition(std::make_unique<PromptingsOfWisdom>());
    settings.addCardDefinition(std::make_unique<RiskyBlow>());
    settings.addCardDefinition(std::make_unique<SmokeRings>());
    settings.addCardDefinition(std::make_unique<TheDwarvesAreUponYou>());
    settings.addCardDefinition(std::make_unique<TheOldThrush>());
    settings.addCardDefinition(std::make_unique<CovetousThoughts>());
    settings.addCardDefinition(std::make_unique<DespairOfTheHeart>());
    settings.addCardDefinition(std::make_unique<FoolishWords>());
    settings.addCardDefinition(std::make_unique<FullOfFrothAndRage>());
    settings.addCardDefinition(std::make_unique<LostInFreeDomains>());
    settings.addCardDefinition(std::make_unique<MusterDisperses>());
    settings.addCardDefinition(std::make_unique<River>());
    settings.addCardDefinition(std::make_unique<SeizedByTerror>());
    settings.addCardDefinition(std::make_unique<Twilight>());
    settings.addCardDefinition(std::make_unique<WakeOfWar>());
    settings.addCardDefinition(std::make_unique<CaveWorm>());
    settings.addCardDefinition(std::make_unique<Ghosts>());
    settings.addCardDefinition(std::make_unique<GiantSpiders>());
    settings.addCardDefinition(std::make_unique<LesserSpiders>());
    settings.addCardDefinition(std::make_unique<NeekerBreekers>());
    settings.addCardDefinition(std::make_unique<Wargs>());
    settings.addCardDefinition(std::make_unique<WatcherInTheWater>());
}

void addDwarfDeck(DeckDefinition & deck) {
    deck.deckType = HeroDeck;

    deck.playDeck = {
        {"A Friend or Three", 2},
        {"Bounty of the Hoard"},
        {"Concealment", 3},
        {"Fellowship", 2},
        {"Great-road"},
        {"Hundreds of Butterflies"},
        {"Lucky Search"},
        {"Marvels Told"},
        {"Not at Home", 2},
        {"Risky Blow"},
        {"Smoke Rings"},
        {"The Dwarves Are upon You!", 2},
        {"Gollum"},
        {"Quickbeam"},
        {"Men of Dorwinion"},
        {"Iron Hill Dwarves"},
        {"Thrór's Map"},
        {"Bow of Dragon-horn"},
        {"Hauberk of Bright Mail"},
        {"Magical Harp"},
        {"Enruned Shield"},
        {"Durin's Axe"},
        {"Wormsbane"},
        {"Book of Mazarbul"},
        {"Despair of the Heart", 3},
        {"Full of Froth and Rage", 3},
        {"River", 2},
        {"Seized by Terror", 2},
        {"Twilight", 2},
        {"Wake of War", 2},
        {"Cave Worm", 3},
        {"Giant Spiders", 3},
        {"Lesser Spiders", 3},
        {"Neeker-breekers", 2},
        {"Wargs", 2},
        {"Watcher in the Water", 3},
        {"Pallando", 2},
        {"Fíli"},
        {"Boromir II"},
        {"Celeborn"},
        {"Balin"},
        {"Gildor Inglorion"},
    };

    deck.locationDeck = {
        {"Lórien"},
        {"Grey Havens"},
        {"Blue Mountain Dwarf-hold"},
        {"Buhr Widu"},
        {"Caves of Ûlund"},
        {"Dead Marshes"},
        {"Goblin-gate"},
        {"Iron Hill Dwarf-hold"},
        {"Moria"},
        {"Ovir Hollow"},
        {"Sarn Goriwing"},
        {"Shrel-Kain"},
        {"The Lonely Mountain"},
        {"Wellinghall"},
    };

    deck.sideboard = {
        {"Gates of Morning", 2},
        {"Marvels Told", 2},
        {"Promptings of Wisdom", 2},
        {"The Old Thrush"},
        {"Blue Mountain Dwarves"},
        {"Covetous Thoughts", 2},
        {"Foolish Words", 2},
        {"Lost in Free-domains"},
        {"Muster Disperses"},
        {"Twilight"},
        {"Ghosts", 3},
    };

    deck.startingCompany = {
        "Rivendell",
        {
            {"Thorin II", "Cram"},
            {"Óin", "Cram"},
            {"Glóin"},
            {"Dori"},
            {"Kíli"},
        },
    };
}
