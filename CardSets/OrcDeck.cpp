#include "HazardCards.h"
#include "OrcDeck.h"
#include "../Middle-Earth/BaseClasses.h"

namespace mele {

/* Barad-dûr */
struct BaradDurDarkhaven : public LocationCardEffect {
    BaradDurDarkhaven(CardID host) : LocationCardEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<UpdateSiteType>(batch.et))
            return false;
        if (batch.scene.game.turnSequence.phase != UntapPhase)
            return false;
        UpdateSiteType & site = std::get<UpdateSiteType>(batch.et);
        if (site.location != host)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<UpdateSiteType>(batch.et).siteType = Darkhaven;
    }
};
struct BaradDurRingTestAction : public LocationCardAction {
    // Player may choose to test a Gold Ring Item at any time during the site
    // phase of a company at Barad-dûr. Any untested Gold Ring Items remaining
    // in the company at the end of its site phase will be automatically tested
    // (in arbitrary order) by an Effect.
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Explore>(context))
            return false;
        Explore & explore = std::get<Explore>(context);
        if (explore.site != host)
            return false;
        if (args.empty())
            return false;
        CardID target = args.front();
        CardType & type = game.getCardType(target);
        if (!std::holds_alternative<Item>(type))
            return false;
        Item & item = std::get<Item>(type);
        if (item.type != GoldRing)
            return false;
        if (!game.characterInCompany(game.getParent(target), explore.company))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.setupRingTest(args.front(), 0);
    }
};
struct BaradDurAutomaticRingTest : public LocationCardEffect {
    BaradDurAutomaticRingTest(CardID host) : LocationCardEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<CloseContext>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<Explore>(context))
            return false;
        CompanyID id = std::get<Explore>(context).company;
        Game & game = batch.scene.game;
        Side & side = game.getCurrentSide();
        auto it = side.findCompany(id);
        if (it == side.companies.end())
            return false;
        for (CardID character : it->characters) {
            auto & possessions = game.cards[character].possessions;
            for (CardID possession : possessions) {
                CardType & type = game.getCardType(possession);
                if (std::holds_alternative<Item>(type) && std::get<Item>(type).type == GoldRing)
                    return true;
            }
        }
        return false;
    }
    void activate(EffectBatch & batch) const {
        CompanyID id = std::get<Explore>(batch.getContext()).company;
        Game & game = batch.scene.game;
        auto & characters = game.getCurrentSide().findCompany(id)->characters;
        for (CardID character : characters) {
            auto & possessions = game.cards[character].possessions;
            for (CardID possession : possessions) {
                CardType & type = game.getCardType(possession);
                if (std::holds_alternative<Item>(type) && std::get<Item>(type).type == GoldRing)
                    batch.scene.setupRingTest(possession, 0);
            }
        }
    }
};
struct BaradDurRingTestModifier : public LocationCardEffect {
    BaradDurRingTestModifier(CardID host) : LocationCardEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Modifier>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<RingTest>(context))
            return false;
        RingTest & ringTest = std::get<RingTest>(context);
        Game & game = batch.scene.game;
        CardID wielder = game.getParent(ringTest.targetRing);
        if (game.getCurrentSide().findCompany(wielder)->currentLocation != host)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<Modifier>(batch.et).modifier -= 3;
    }
};
struct BaradDur : public LocationDefinition {
    BaradDur() {
        Location & def = std::get<Location>(type);
        def.minion = true;
        def.name = "Barad-dûr";
        def.type = DarkHold;
        def.sitePath.emplace_back(MinasMorgul);
        def.sitePath.back().sitePath = { ShadowLands, DarkDomain };
        def.region = Gorgoroth;
        def.drawCards = { 2, 2 };
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<BaradDurRingTestAction>(), host);
    }
    void getEffects(EffectList & effects, CardID host) {
        effects.push_back(std::make_shared<BaradDurDarkhaven>(host));
        effects.push_back(std::make_shared<BaradDurAutomaticRingTest>(host));
        effects.push_back(std::make_shared<BaradDurRingTestModifier>(host));
    }
};

/* Caves of Ûlund */
struct CavesOfUlund : public LocationDefinition {
    CavesOfUlund() {
        Location & def = std::get<Location>(type);
        def.minion = true;
        def.name = "Caves of Ûlund";
        def.type = RuinsLairs;
        def.sitePath.emplace_back(DolGuldur);
        def.sitePath.back().sitePath = { DarkDomain, Wilderness, Wilderness, Wilderness };
        def.region = WitheredHeath;
        def.playable = { Minor, Major, Greater, GoldRing };
        def.automaticAttack.add(Dragon, 1, 13);
        def.drawCards = { 2, 2 };
        def.containsHoard = true;
    }
};

/* Dead Marshes */
struct DeadMarshesCorruptionCheck : public AttackEffect {
    DeadMarshesCorruptionCheck(CardID host) : AttackEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<CloseContext>(batch.et))
            return false;
        if (!std::holds_alternative<BodyCheck>(context))
            return false;
        BodyCheck & bodyCheck = std::get<BodyCheck>(context);
        if (bodyCheck.target == host)
            return false;
        if (bodyCheck.roll + bodyCheck.modifier > bodyCheck.body)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        BodyCheck & body = std::get<BodyCheck>(batch.getContext());
        batch.scene.setupResolveCorruptionChecks(body.target, body.target, -2);
    }
};
struct DeadMarshesAttack : public AttackInfo {
    DeadMarshesAttack(Race race, int strikes, int prowess)
        : AttackInfo(race, strikes, prowess) {}
    void getEffects(EffectList & effects, CardID host) {
        effects.push_back(std::make_shared<DeadMarshesCorruptionCheck>(host));
    }
};
struct DeadMarshes : public LocationDefinition {
    DeadMarshes() {
        Location & def = std::get<Location>(type);
        def.minion = true;
        def.name = "Dead Marshes";
        def.type = ShadowHold;
        def.sitePath.emplace_back(DolGuldur);
        def.sitePath.back().sitePath = { DarkDomain, ShadowLands };
        def.region = Dagorlad;
        def.playable = { Minor, Major };
        auto attack = std::make_shared<DeadMarshesAttack>(Undead, 2, 8);
        def.automaticAttack.push_back(attack);
        def.drawCards = { 1, 1 };
    }
    void getEffects(EffectList & effects, CardID host) {
        effects.push_back(std::make_shared<NonNazgulNotDetainmentAgainstSite>(host));
    }
};

/* Goblin-gate */
struct GoblinGate : public LocationDefinition {
    GoblinGate() {
        Location & def = std::get<Location>(type);
        def.minion = true;
        def.name = "Goblin-gate";
        def.type = ShadowHold;
        def.sitePath.emplace_back(CarnDum);
        def.sitePath.back().sitePath = { ShadowLands, Wilderness, Wilderness };
        def.region = HighPass;
        def.playable = { Minor, GoldRing };
        def.automaticAttack.add(Orc, 3, 6);
        def.drawCards = { 2, 2 };
    }
    void getEffects(EffectList & effects, CardID host) {
        effects.push_back(std::make_shared<NonNazgulNotDetainmentAgainstSite>(host));
    }
};

/* Moria */
struct Moria : public LocationDefinition {
    Moria() {
        Location & def = std::get<Location>(type);
        def.minion = true;
        def.name = "Moria";
        def.type = ShadowHold;
        def.sitePath.emplace_back(DolGuldur);
        def.sitePath.back().sitePath = { DarkDomain, ShadowLands, Wilderness, Wilderness };
        def.region = RedhornGate;
        def.playable = { Minor, Major, Greater, GoldRing };
        def.automaticAttack.add(Orc, 4, 7);
        def.drawCards = { 2, 3 };
    }
    void getEffects(EffectList & effects, CardID host) {
        effects.push_back(std::make_shared<NonNazgulNotDetainmentAgainstSite>(host));
    }
};

/* Mount Doom */
struct MountDoomRingTestAction : public LocationCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Explore>(context))
            return false;
        Explore & explore = std::get<Explore>(context);
        if (explore.site != host || explore.state != Explore::ExploreSite)
            return false;
        if (args.size() < 2)
            return false;

        // Character as first argument
        auto it = args.begin();
        CardID character = *it;
        if (!std::holds_alternative<Character>(game.getCardType(character)))
            return false;
        if (game.getCardState(character) != Untapped)
            return false;

        // Gold Ring as second argument
        ++it;
        CardID target = *it;
        CardType & type = game.getCardType(target);
        if (!std::holds_alternative<Item>(type))
            return false;
        Item & item = std::get<Item>(type);
        if (item.type != GoldRing)
            return false;

        // Check that both are in the active company
        if (!game.characterInCompany(character, explore.company))
            return false;
        if (!game.characterInCompany(game.getParent(target), explore.company))
            return false;
        return scene.hasSkill(character, Sage);
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        auto it = args.begin();
        scene.tap(*it);
        ++it;
        scene.setupRingTest(*it, -3);
    }
};
struct MountDoom : public LocationDefinition {
    MountDoom() {
        Location & def = std::get<Location>(type);
        def.minion = true;
        def.name = "Mount Doom";
        def.type = ShadowHold;
        def.sitePath.emplace_back(MinasMorgul);
        def.sitePath.back().sitePath = { ShadowLands, DarkDomain };
        def.region = Gorgoroth;
        def.playable = { Information };
        def.automaticAttack.add(Orc, 1, 6);
        def.drawCards = { 2, 1 };
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<MountDoomRingTestAction>(), host);
    }
};

/* Mount Gram */
struct MountGram : public LocationDefinition {
    MountGram() {
        Location & def = std::get<Location>(type);
        def.minion = true;
        def.name = "Mount Gram";
        def.type = ShadowHold;
        def.sitePath.emplace_back(CarnDum);
        def.sitePath.back().sitePath = { ShadowLands };
        def.region = Angmar;
        def.drawCards = { 1, 1 };
    }
};

/* Mount Gundabad */
struct MountGundabad : public LocationDefinition {
    MountGundabad() {
        Location & def = std::get<Location>(type);
        def.minion = true;
        def.name = "Mount Gundabad";
        def.type = ShadowHold;
        def.sitePath.emplace_back(CarnDum);
        def.sitePath.back().sitePath = { ShadowLands, DarkDomain };
        def.region = Gundabad;
        def.playable = { Minor, Major };
        def.automaticAttack.add(Orc, 0, 7);
        def.automaticAttack.back()->detainmentAgainstOvertCompany = true;
        def.drawCards = { 2, 1 };
    }
};

/* Sarn Goriwing */
struct SarnGoriwing : public LocationDefinition {
    SarnGoriwing() {
        Location & def = std::get<Location>(type);
        def.minion = true;
        def.name = "Sarn Goriwing";
        def.type = ShadowHold;
        def.sitePath.emplace_back(DolGuldur);
        def.sitePath.back().sitePath = { DarkDomain, Wilderness };
        def.region = HeartOfMirkwood;
        def.playable = { Minor, Major };
        def.automaticAttack.add(Orc, 3, 5);
        def.drawCards = { 1, 1 };
    }
};

/* Shelob's Lair */
struct ShelobsLairEliminateWounded : public AttackEffect {
    ShelobsLairEliminateWounded(CardID host) : AttackEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<OpenContext>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<BodyCheck>(context))
            return false;
        if (std::get<BodyCheck>(context).target == host)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        BodyCheck & body = std::get<BodyCheck>(batch.getContext());
        body.state = BodyCheck::Close;
        batch.scene.context->autoProceed = true;
        batch.scene.eliminate(body.target);
    }
};
struct ShelobsLairAttack : public AttackInfo {
    ShelobsLairAttack(Race race, int strikes, int prowess)
        : AttackInfo(race, strikes, prowess) {}
    void getEffects(EffectList & effects, CardID host) {
        effects.push_back(std::make_shared<ShelobsLairEliminateWounded>(host));
    }
};
struct ShelobsLair : public LocationDefinition {
    ShelobsLair() {
        Location & def = std::get<Location>(type);
        def.minion = true;
        def.name = "Shelob's Lair";
        def.type = ShadowHold;
        def.sitePath.emplace_back(MinasMorgul);
        def.sitePath.back().sitePath = { ShadowLands };
        def.region = ImladMorgul;
        def.playable = { Minor, Major };
        def.automaticAttack.add(Orc, 2, 8);
        auto attack = std::make_shared<ShelobsLairAttack>(Spider, 1, 16);
        attack->mayNotBeCancelled = true;
        def.automaticAttack.push_back(attack);
        def.drawCards = { 1, 1 };
        def.containsHoard = true;
    }
    void getEffects(EffectList & effects, CardID host) {
        effects.push_back(std::make_shared<NonNazgulNotDetainmentAgainstSite>(host));
    }
};

/* Hoarmûrath the Ringwraith */
struct HoarmurathTheRingwraithHandsizeEffect : public CharacterEffect {
    HoarmurathTheRingwraithHandsizeEffect(CardID host) : CharacterEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<HandSize>(batch.et))
            return false;
        Game & game = batch.scene.game;
        if (std::get<HandSize>(batch.et).player != game.cards[host].owner)
            return false;
        Side & side = game.getSide(host);
        if (side.revealedAvatar != game.cards[host].dataIndex)
            return false;
        auto it = side.findCompany(host, Characters);
        Location & location = game.getLocation(it->currentLocation);
        if (location.type != side.havenType())
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<HandSize>(batch.et).handSize++;
    }
};
struct PlayHoarmurathTheRingwraith : public PlayRingwraithAction {
    void ringwraithAbilities(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<MagicProficiencyEffect>(host, Sorcery));
        side.effects.push_back(std::make_shared<HeraldedLordBonus>(host, 1));
        side.effects.push_back(std::make_shared<FellRiderBonus>(host, 2));
        side.effects.push_back(std::make_shared<HoarmurathTheRingwraithHandsizeEffect>(host));
    }
};
struct HoarmurathTheRingwraith : public CardDefinition {
    HoarmurathTheRingwraith() : CardDefinition(Character()) {
        Character & def = std::get<Character>(type);
        def.minion = true;
        def.name.push_back("Hoarmûrath the Ringwraith");
        def.name.push_back("Hoarmûrath of Dír");
        def.skills = { Scout, Ranger, Sage };
        def.race = Ringwraith;
        def.mp = 0;
        def.mind = 0;
        def.di = 3;
        def.prowess = 8;
        def.body = 9;
        def.homeSite = "Any site in Udûn";
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayHoarmurathTheRingwraith>(), host);
        // actions.add(std::make_shared<PlayHoarmurathAsFollower>(), host);
    }
};

/* Gorbag */
struct PlayGorbag : public PlayMinionCharacter {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<DiscardOnBodyCheck>(host, 9));
        side.effects.push_back(std::make_shared<InfluenceRaceBonus>(host, Orc, 3));
    }
};
struct Gorbag : public CardDefinition {
    Gorbag() : CardDefinition(Character()) {
        Character & def = std::get<Character>(type);
        def.minion = true;
        def.name.push_back("Gorbag");
        def.skills = { Warrior, Scout };
        def.race = Orc;
        def.leader = true;
        def.mp = 2;
        def.mind = 6;
        def.prowess = 6;
        def.body = 9;
        def.homeSite = "Minas Morgul";
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayGorbag>(), host);
    }
};

/* Grishnakh */
struct PlayGrishnakh : public PlayMinionCharacter {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<DiscardOnBodyCheck>(host, 8));
    }
};
struct Grishnakh : public CardDefinition {
    Grishnakh() : CardDefinition(Character()) {
        Character & def = std::get<Character>(type);
        def.minion = true;
        def.name.push_back("Grishnákh");
        def.skills = { Warrior, Scout };
        def.race = Orc;
        def.mp = 1;
        def.mind = 3;
        def.prowess = 4;
        def.body = 8;
        def.homeSite = "Any site in Imlad Morgul";
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayGrishnakh>(), host);
    }
};

/* Ill-favoured Fellow */
struct PlayIllFavouredFellow : public PlayMinionCharacter {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<DiscardOnBodyCheck>(host, 9));
    }
};
struct IllFavouredFellow : public CardDefinition {
    IllFavouredFellow() : CardDefinition(Character()) {
        Character & def = std::get<Character>(type);
        def.minion = true;
        def.unique = false;
        def.name.push_back("Ill-favoured Fellow");
        def.skills = { Warrior, Ranger };
        def.race = Orc;
        def.mp = 1;
        def.mind = 3;
        def.prowess = 2;
        def.body = 9;
        def.homeSite = "Any Dark-hold";
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayIllFavouredFellow>(), host);
    }
};

/* Lugdush */
struct PlayLugdush : public PlayMinionCharacter {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<DiscardOnBodyCheck>(host, 8));
    }
};
struct Lugdush : public CardDefinition {
    Lugdush() : CardDefinition(Character()) {
        Character & def = std::get<Character>(type);
        def.minion = true;
        def.name.push_back("Lugdush");
        def.skills = { Warrior, Scout };
        def.race = Orc;
        def.mp = 1;
        def.mind = 4;
        def.prowess = 5;
        def.body = 8;
        def.homeSite = "Isengard";
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayLugdush>(), host);
    }
};

/* Orc Tracker */
struct PlayOrcTracker : public PlayMinionCharacter {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<DiscardOnBodyCheck>(host, 7));
        side.effects.push_back(std::make_shared<DiscardOnBodyCheck>(host, 8));
        side.effects.push_back(std::make_shared<CorruptionCheckBonus>(host, -1));
    }
};
struct OrcTracker : public CardDefinition {
    OrcTracker() : CardDefinition(Character()) {
        Character & def = std::get<Character>(type);
        def.minion = true;
        def.unique = false;
        def.name.push_back("Orc Tracker");
        def.skills = { Warrior, Ranger };
        def.race = Orc;
        def.mp = 1;
        def.mind = 3;
        def.prowess = 3;
        def.body = 8;
        def.homeSite = "Any Dark-hold";
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayOrcTracker>(), host);
    }
};

/* Sly Southerner */
struct PlaySlySoutherner : public PlayMinionCharacter {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<DiscardOnBodyCheck>(host, 9));
    }
};
struct SlySoutherner : public CardDefinition {
    SlySoutherner() : CardDefinition(Character()) {
        Character & def = std::get<Character>(type);
        def.minion = true;
        def.unique = false;
        def.name.push_back("Sly Southerner");
        def.skills = { Warrior, Scout };
        def.race = Orc;
        def.mp = 0;
        def.mind = 2;
        def.prowess = 1;
        def.body = 9;
        def.homeSite = "Any Dark-hold";
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlaySlySoutherner>(), host);
    }
};

/* Ufthak */
struct PlayUfthak : public PlayMinionCharacter {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<DiscardOnBodyCheck>(host, 8));
    }
};
struct Ufthak : public CardDefinition {
    Ufthak() : CardDefinition(Character()) {
        Character & def = std::get<Character>(type);
        def.minion = true;
        def.name.push_back("Ufthak");
        def.skills = { Warrior, Scout, Ranger };
        def.race = Orc;
        def.mp = 1;
        def.mind = 4;
        def.prowess = 4;
        def.body = 8;
        def.homeSite = "Any site in Imlad Morgul";
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayUfthak>(), host);
    }
};

/* Great Lord of Goblin-gate */
struct GreatLordOfGoblinGateProwess : public AttackEffect {
    GreatLordOfGoblinGateProwess(CardID host) : AttackEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Stat>(batch.et))
            return false;
        Stat & stat = std::get<Stat>(batch.et);
        if (stat.type != Prowess)
            return false;
        Game & game = batch.scene.game;
        if (game.cards[host].owner != game.cards[stat.target].owner)
            return false;
        if (game.getRace(stat.target) != Orc)
            return false;
        if (game.getCompanyID(stat.target) != game.getCompanyID(host))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<Stat>(batch.et).stat += 2;
    }
};
struct GreatLordOfGoblinGateAction : public EquipmentAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        if (game.getCardState(host) != Untapped)
            return false;
        ContextType & context = scene.context->context;
        if (std::holds_alternative<Attack>(context)) {
            Attack & attack = std::get<Attack>(context);
            return game.characterInCompany(game.getParent(host), attack.company);
        }
        // TODO: Company vs company combat case
        return false;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.tap(host);
        scene.game.effects.push_back(std::make_shared<GreatLordOfGoblinGateProwess>(host));
    }
};
struct PlayGreatLordOfGoblinGate : public PlayAllyAction {
    PlayGreatLordOfGoblinGate() : PlayAllyAction({ "Goblin-gate" }) {}
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.actions.add(std::make_shared<GreatLordOfGoblinGateAction>(), host);
        side.effects.push_back(std::make_shared<OvertPossession>(host));
    }
};
struct GreatLordOfGoblinGate : public CardDefinition {
    GreatLordOfGoblinGate() : CardDefinition(Ally()) {
        Ally & def = std::get<Ally>(type);
        def.minion = true;
        def.name.push_back("Great Lord of Goblin-gate");
        def.name.push_back("The Great Goblin");
        def.race = Orc;
        def.unique = true;
        def.mp = 2;
        def.mind = 3;
        def.prowess = 5;
        def.body = 7;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayGreatLordOfGoblinGate>(), host);
    }
};

/* Stinker */
struct StinkerCancelAttack : public EquipmentAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        if (game.getCardState(host) != Untapped)
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Attack>(context))
            return false;
        Attack & attack = std::get<Attack>(context);
        if (!game.characterInCompany(game.getParent(host)), attack.company)
            return false;
        if (scene.getCompanySize(*game.getCurrentSide().findCompany(attack.company)) > 2)
            return false;
        if (!std::holds_alternative<PathSymbol>(attack.keyedTo))
            return false;
        PathSymbol pathSymbol = std::get<PathSymbol>(attack.keyedTo);
        if (pathSymbol != Wilderness && pathSymbol != ShadowLands)
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.tap(host);
        scene.cancelAttack();
    }
};
struct PlayStinker : public PlayAllyAction {
    PlayStinker() : PlayAllyAction({ "Goblin-gate", "Moria" }) {}
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.actions.add(std::make_shared<StinkerCancelAttack>(), host);
    }
};
struct Stinker : public CardDefinition {
    Stinker() : CardDefinition(Ally()) {
        Ally & def = std::get<Ally>(type);
        def.minion = true;
        def.name.push_back("Stinker");
        def.name.push_back("Gollum");
        def.unique = true;
        def.skills = { Scout };
        def.mp = 2;
        def.mind = 4;
        def.prowess = 2;
        def.body = 9;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayStinker>(), host);
    }
};

/* War-wolf */
bool automaticAttackType(Location & location, Race race) {
    for (std::shared_ptr<AttackInfo> info : location.automaticAttack)
        if (info->type == race)
            return true;
    return false;
}
struct PlayWarWolf : public PlayAtSiteAction {
    bool conditions(Scene scene, CardID target, Explore & explore) const {
        Location & location = scene.game.getLocation(explore.site);
        SiteType siteType = scene.getSiteType(explore.site);
        if (siteType == RuinsLairs && automaticAttackType(location, Wolf))
            return true;
        else if (siteType == ShadowHold && automaticAttackType(location, Orc))
            return true;
        return false;
    }
};
struct WarWolf : public CardDefinition {
    WarWolf() : CardDefinition(Ally()) {
        Ally & def = std::get<Ally>(type);
        def.minion = true;
        def.name.push_back("War-wolf");
        def.unique = false;
        def.mp = 1;
        def.mind = 1;
        def.prowess = 2;
        def.body = 7;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayWarWolf>(), host);
    }
};

/* Orcs of Mirkwood */
struct OrcsOfMirkwood : public OrcFactionDefinition {
    OrcsOfMirkwood() {
        Faction & def = std::get<Faction>(type);
        def.minion = true;
        def.name.push_back("Orcs of Mirkwood");
        def.race = Orc;
        def.homeSite = "Sarn Goriwing";
        def.mp = 2;
        def.requiredInfluence = 8;
        def.standardModifications["Orcs of Red Eye"] = -2;
        def.standardModifications["Orcs of Gorgoroth"] = 2;
    }
};

/* Orcs of Moria */
struct OrcsOfMoria : public OrcFactionDefinition {
    OrcsOfMoria() {
        Faction & def = std::get<Faction>(type);
        def.minion = true;
        def.name.push_back("Orcs of Moria");
        def.race = Orc;
        def.homeSite = "Moria";
        def.mp = 3;
        def.requiredInfluence = 10;
        def.standardModifications["Goblins of Goblin-gate"] = 2;
        def.standardModifications["Orcs of Dol guldur"] = -2;
    }
};

/* Snaga-hai */
struct PlaySnagaHai : public PlayOrcFactionAction {
    bool conditions(Scene scene, CardID target, Explore & explore) {
        return (scene.getSiteType(explore.site) == ShadowHold);
    }
};
struct SnagaHai : public CardDefinition {
    SnagaHai() : CardDefinition(Faction()) {
        Faction & def = std::get<Faction>(type);
        def.minion = true;
        def.name.push_back("Snaga-hai");
        def.race = Orc;
        def.unique = false;
        def.homeSite = "Any Shadow-hold";
        def.mp = 1;
        def.requiredInfluence = 9;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlaySnagaHai>(), host);
    }
};

/* Black Mace */
struct BlackMaceProwess : public EquipmentEffect {
    BlackMaceProwess(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        Game & game = batch.scene.game;
        if (game.possessedByRingwraith(host))
            return false;
        if (!std::holds_alternative<Stat>(batch.et))
            return false;
        Stat & stat = std::get<Stat>(batch.et);
        if (stat.type != Prowess)
            return false;
        if (stat.target != game.getParent(host))
            return false;
        if (batch.scene.hasSkill(stat.target, Warrior))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        Stat & stat = std::get<Stat>(batch.et);
        int & prowess = stat.stat;
        ContextType & context = batch.getContext();
        prowess += (stat.raceOfOpponent == Elf) ? 4 : 3;
        if (prowess > 10)
            prowess = 10;
    }
};
struct PlayBlackMace : public PlayItemAction {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<BlackMaceProwess>(host));
    }
};
struct BlackMace : public CardDefinition {
    BlackMace() : CardDefinition(Item()) {
        Item & def = std::get<Item>(type);
        def.minion = true;
        def.name.push_back("Black Mace");
        def.type = Greater;
        def.equipmentType = Weapon;
        def.mp = 2;
        def.cp = 3;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayBlackMace>(), host);
    }
};

/* Blasting Fire */
struct BlastingFireInfluence : public ExploreEffect {
    BlastingFireInfluence(CardID host)
        : ExploreEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<UnusedDI>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<InfluenceAttempt>(context))
            return false;
        InfluenceAttempt & influenceAttempt = std::get<InfluenceAttempt>(context);
        Game & game = batch.scene.game;
        CardType & type = game.getCardType(influenceAttempt.target);
        if (!std::holds_alternative<Faction>(type))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<UnusedDI>(batch.et).di += 2;
    }
};
struct DiscardBlastingFire : public EquipmentAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        Game & game = scene.game;
        if (game.possessedByRingwraith(host))
            return false;
        if (game.turnSequence.phase != SitePhase)
            return false;
        ContextType & context = scene.context->context;
        if (std::holds_alternative<Attack>(context)) {
            Attack & attack = std::get<Attack>(context);
            if (!attack.automaticAttack)
                return false;
            CardID parent = game.getParent(host);
            if (game.characterInCompany(parent, attack.company))
                return true;
        }
        else if (std::holds_alternative<Explore>(context) && std::get<Explore>(context).state == Explore::ExploreSite)
            return true;
        return false;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        scene.discard(host);
        if (std::holds_alternative<Attack>(scene.context->context)) {
            Encounter & encounter = std::get<Encounter>(game.contextStack[scene.context->parent]->context);
            encounter.currentAttack = encounter.attacks.end();
            scene.cancelAttack();
        }
        game.getCurrentSide().effects.push_back(std::make_shared<BlastingFireInfluence>(host));
    }
};
struct PlayBlastingFire : public PlayAtSiteAction {
    bool conditions(Scene scene, CardID target, Explore & explore) const {
        Location & location = scene.game.getLocation(explore.site);
        SiteType siteType = scene.getSiteType(explore.site);
        if (siteType == ShadowHold || siteType == DarkHold || automaticAttackType(location, Dwarf))
            return true;
        return false;
    }
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.actions.add(std::make_shared<DiscardBlastingFire>(), host);
    }
};
struct BlastingFire : public CardDefinition {
    BlastingFire() : CardDefinition(Item()) {
        Item & def = std::get<Item>(type);
        def.minion = true;
        def.name.push_back("Blasting Fire");
        def.type = Special;
        def.mp = 1;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayBlastingFire>(), host);
    }
};

/* Blazon of the Eye */
struct BlazonOfTheEyeInfluenceBonus : public EquipmentEffect {
    BlazonOfTheEyeInfluenceBonus(CardID host)
        : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<UnusedDI>(batch.et))
            return false;
        Game & game = batch.scene.game;
        if (game.possessedByRingwraith(host))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<InfluenceAttempt>(context))
            return false;
        InfluenceAttempt & influenceAttempt = std::get<InfluenceAttempt>(context);
        if (influenceAttempt.influencingCharacter != game.getParent(host))
            return false;
        CardType & type = game.getCardType(influenceAttempt.target);
        if (!std::holds_alternative<Faction>(type))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<UnusedDI>(batch.et).di += 2;
    }
};
struct BlazonOfTheEyeNoCharacterDuplicates : public EquipmentEffect {
    BlazonOfTheEyeNoCharacterDuplicates(CardID host)
        : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<TransferAllowed>(batch.et))
            return false;
        TransferAllowed & transfer = std::get<TransferAllowed>(batch.et);
        if (transfer.item != host)
            return false;
        if (batch.scene.game.duplicatePossession(transfer.recipient, host))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<TransferAllowed>(batch.et).allowed = false;
    }
};
struct PlayBlazonOfTheEye : public PlayAtSiteAction {
    bool conditions(Scene scene, CardID target, Explore & explore) const {
        Game & game = scene.game;
        if (scene.game.duplicatePossession(target, host))
            return false;
        for (ItemType playableType : explore.playable)
            if (playableType == Minor)
                return true;
        return false;
    }
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<BlazonOfTheEyeInfluenceBonus>(host));
        side.effects.push_back(std::make_shared< BlazonOfTheEyeNoCharacterDuplicates>(host));
    }
};
struct BlazonOfTheEye : public CardDefinition {
    BlazonOfTheEye() : CardDefinition(Item()) {
        Item & def = std::get<Item>(type);
        def.minion = true;
        def.name.push_back("Blazon of the Eye");
        def.type = Minor;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayBlazonOfTheEye>(), host);
    }
};

/* High Helm */
struct HighHelmDI : public EquipmentEffect {
    HighHelmDI(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        Game & game = batch.scene.game;
        if (game.possessedByRingwraith(host))
            return false;
        if (!std::holds_alternative<UnusedDI>(batch.et))
            return false;
        UnusedDI & di = std::get<UnusedDI>(batch.et);
        if (di.target != game.getParent(host))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<UnusedDI>(batch.et).di += 2;
    }
};
struct HighHelmProwess : public EquipmentEffect {
    HighHelmProwess(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        Game & game = batch.scene.game;
        if (game.possessedByRingwraith(host))
            return false;
        if (!std::holds_alternative<Stat>(batch.et))
            return false;
        Stat & stat = std::get<Stat>(batch.et);
        if (stat.type != Prowess)
            return false;
        CardID parent = game.getParent(host);
        if (stat.target != parent)
            return false;
        if (!batch.scene.hasSkill(parent, Warrior))
            return false;
        if (stat.stat >= 8)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        Stat & stat = std::get<Stat>(batch.et);
        int & prowess = stat.stat;
        prowess++;
        if (prowess > 8)
            prowess = 8;
    }
};
struct PlayHighHelm : public PlayItemAction {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<HighHelmDI>(host));
        side.effects.push_back(std::make_shared<EquipmentBodyBonus>(host, 1, 9));
        side.effects.push_back(std::make_shared<HighHelmProwess>(host));
    }
};
struct HighHelm : public CardDefinition {
    HighHelm() : CardDefinition(Item()) {
        Item & def = std::get<Item>(type);
        def.minion = true;
        def.name.push_back("High Helm");
        def.type = Major;
        def.equipmentType = Helmet;
        def.unique = true;
        def.mp = 2;
        def.cp = 2;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayHighHelm>(), host);
    }
};

/* Records Unread */
struct RecordsUnreadUntapSite : public EquipmentAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        Game & game = scene.game;
        if (game.possessedByRingwraith(host))
            return false;
        CompanyID id = game.getCompanyID(host);
        if (id < 0)
            return false;
        CardID site = game.getCurrentSide().findCompany(id)->currentLocation;
        if (scene.getSiteType(site) != ShadowHold)
            return false;
        if (game.getCardState(site) != Tapped)
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        CompanyID id = game.getCompanyID(host);
        CardID site = game.getCurrentSide().findCompany(id)->currentLocation;
        scene.discard(host);
        scene.untapSite(site);
    }
};
struct RecordsUnreadInformation : public EquipmentAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        Game & game = scene.game;
        if (game.possessedByRingwraith(host))
            return false;
        if (game.turnSequence.phase != SitePhase)
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Explore>(context))
            return false;
        Explore & explore = std::get<Explore>(context);
        CompanyID id = game.getCompanyID(host);
        if (explore.company != id || scene.getSiteType(explore.site) != ShadowHold)
            return false;
        for (ItemType type : explore.playable)
            if (type == Information)
                return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.discard(host);
        std::get<Explore>(scene.context->context).playable.push_back(Information);
    }
};
struct PlayRecordsUnread  :public PlayAtSiteAction {
    bool conditions(Scene scene, CardID target, Explore & explore) const {
        Company & company = *scene.game.getCurrentSide().findCompany(explore.company);
        if (scene.game.duplicatePossessionsInCompany(company, host) > 0)
            return false;
        for (ItemType playableType : explore.playable)
            if (playableType == Minor)
                return true;
        return false;
    }
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.actions.add(std::make_shared<RecordsUnreadUntapSite>(), host, "Untap");
        side.actions.add(std::make_shared<RecordsUnreadInformation>(), host, "Information");
        side.effects.push_back(std::make_shared<NoDuplicatesInCompanyEffect>(host));
    }
};
struct RecordsUnread : public CardDefinition {
    RecordsUnread() : CardDefinition(Item()) {
        Item & def = std::get<Item>(type);
        def.minion = true;
        def.name.push_back("Records Unread");
        def.type = Minor;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayRecordsUnread>(), host);
    }
};

/* Sable Shield */
struct SableShieldSpecialEffect : public SpecialEffect {
    SableShieldSpecialEffect(CardID host) : SpecialEffect(host) {}
    bool conditions(SpecialTest & test) const {
        return test.roll > 6;
    }
    void activate(EffectBatch & batch) const {
        batch.scene.discard(host);
    }
};
struct SableShieldEffect : public EquipmentEffect {
    SableShieldEffect(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        Game & game = batch.scene.game;
        if (game.possessedByRingwraith(host))
            return false;
        if (!std::holds_alternative<Roll>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<Strike>(context))
            return false;
        Strike & strike = std::get<Strike>(context);
        if (strike.target != game.getParent(host))
            return false;
        if (batch.scene.strikeResult(strike) < 0)
            return true;
        return false;
    }
    void activate(EffectBatch & batch) const {
        Game & game = batch.scene.game;
        Strike & strike = std::get<Strike>(batch.getContext());
        strike.outcome = Successful;
        strike.state = Strike::CloseStrike;
        batch.scene.setupSpecialTest(host, game.cards[host].owner, host);
        game.effects.push_back(std::make_shared<SableShieldSpecialEffect>(host));
    }
};
struct PlaySableShield : public PlayItemAction {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<SableShieldEffect>(host));
    }
};
struct SableShield : public CardDefinition {
    SableShield() : CardDefinition(Item()) {
        Item & def = std::get<Item>(type);
        def.minion = true;
        def.name.push_back("Sable Shield");
        def.type = Major;
        def.equipmentType = Shield;
        def.unique = true;
        def.mp = 2;
        def.cp = 2;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlaySableShield>(), host);
    }
};

/* Scroll of Isildur */
struct ScrollOfIsildurMP : public Effect {
    ScrollOfIsildurMP(CardID host) : Effect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<MarshallingPoints>(batch.et))
            return false;
        if (std::get<MarshallingPoints>(batch.et).target != host)
            return false;
        if (batch.scene.game.getDeck(host) != Stored)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<MarshallingPoints>(batch.et).mp = 5;
    }
    bool remove(EffectBatch & batch) const {
        return batch.scene.game.getDeck(host) != Stored;
    }
};
struct StoreScrollOfIsildurEffect : public EquipmentEffect {
    StoreScrollOfIsildurEffect(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<MoveCard>(batch.et))
            return false;
        MoveCard & moveCard = std::get<MoveCard>(batch.et);
        if (moveCard.target != host || moveCard.destination != Stored)
            return false;
        Game & game = batch.scene.game;
        Side & side = game.getCurrentSide();
        auto it = side.findCompany(game.getParent(host), Characters);
        if (it == side.companies.end())
            return false;
        Location & location = game.getLocation(it->currentLocation);
        if (location.name != "Barad-dûr")
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        batch.scene.game.getCurrentSide().effects.push_back(std::make_shared<ScrollOfIsildurMP>(host));
    }
};
struct StoreScrollOfIsildurAllowed : public EquipmentEffect {
    StoreScrollOfIsildurAllowed(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<AllowedOperation>(batch.et))
            return false;
        AllowedOperation & op = std::get<AllowedOperation>(batch.et);
        if (op.operation != Store || op.card != host)
            return false;
        if (!std::holds_alternative<OrganizeCompanies>(batch.getContext()))
            return false;
        Game & game = batch.scene.game;
        Side & side = game.getCurrentSide();
        auto it = side.findCompany(game.getParent(host), Characters);
        if (it == side.companies.end())
            return false;
        Location & location = game.getLocation(it->currentLocation);
        if (location.name != "Barad-dûr")
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<AllowedOperation>(batch.et).allowed = true;
    }
};
struct ScrollOfIsildurRingTest : public EquipmentEffect {
    ScrollOfIsildurRingTest(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        Game & game = batch.scene.game;
        if (game.possessedByRingwraith(host))
            return false;
        if (!std::holds_alternative<Modifier>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<RingTest>(context))
            return false;
        RingTest & test = std::get<RingTest>(context);
        if (game.getCompanyID(host) != game.getCompanyID(test.targetRing))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<Modifier>(batch.et).modifier++;
    }
};
struct PlayScrollOfIsildur : public PlayItemAction {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<ScrollOfIsildurRingTest>(host));
        side.effects.push_back(std::make_shared<StoreScrollOfIsildurAllowed>(host));
        side.effects.push_back(std::make_shared<StoreScrollOfIsildurEffect>(host));
    }
};
struct ScrollOfIsildur : public CardDefinition {
    ScrollOfIsildur() : CardDefinition(Item()) {
        Item & def = std::get<Item>(type);
        def.minion = true;
        def.name.push_back("Scroll of Isildur");
        def.type = Greater;
        def.unique = true;
        def.mp = 3;
        def.cp = 4;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayScrollOfIsildur>(), host);
    }
};

/* Strange Rations */
struct StrangeRationsUntapAction : public EquipmentAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        Game & game = scene.game;
        if (game.possessedByRingwraith(host))
            return false;
        CardID target = game.getParent(host);
        if (game.getCardState(target) != Tapped)
            return false;
        if (!scene.allowedOperation(target, Untap))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        CardID target = scene.game.getParent(host);
        scene.discard(host);
        scene.untap(target);
    }
};
struct StrangeRationsRegionEffect : public Effect {
    StrangeRationsRegionEffect(CardID host) : Effect(host) {}
    bool conditions(EffectBatch & batch) const {
        return std::holds_alternative<RegionLimit>(batch.et);
    }
    void activate(EffectBatch & batch) const {
        std::get<RegionLimit>(batch.et).limit++;
    }
    bool remove(EffectBatch & batch) const {
        return std::holds_alternative<NewPhase>(batch.et);
    }
};
struct StrangeRationsRegionAction : public EquipmentAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        if (scene.game.possessedByRingwraith(host))
            return false;
        if (!std::holds_alternative<OrganizeCompanies>(scene.context->context))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        CardID parent = game.getParent(host);
        scene.discard(host);
        Side & side = game.getCurrentSide();
        Company & company = *side.findCompany(parent, Characters);
        company.effects.push_back(std::make_shared<StrangeRationsRegionEffect>(host));
    }
};
struct PlayStrangeRations : public PlayItemAction {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.actions.add(std::make_shared<StrangeRationsUntapAction>(), host, "Untap");
        side.actions.add(std::make_shared<StrangeRationsRegionAction>(), host, "Movement");
    }
};
struct StrangeRations : public CardDefinition {
    StrangeRations() : CardDefinition(Item()) {
        Item & def = std::get<Item>(type);
        def.minion = true;
        def.name.push_back("Strange Rations");
        def.type = Minor;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayStrangeRations>(), host);
    }
};

/* The Arkenstone */
struct TheArkenstoneMind : public EquipmentEffect {
    TheArkenstoneMind(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        // Doesn't apply if stored or if borne by a Ringwraith
        if (batch.scene.game.possessedByRingwraith(host))
            return false;
        if (!std::holds_alternative<Stat>(batch.et))
            return false;
        Stat & stat = std::get<Stat>(batch.et);
        if (stat.type != Mind)
            return false;
        if (batch.scene.getRace(stat.target) != Dwarf)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<Stat>(batch.et).stat++;
    }
};
struct DiscardTheArkenstone : public EquipmentAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        Game & game = scene.game;
        if (game.possessedByRingwraith(host))
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Explore>(context))
            return false;
        Explore & explore = std::get<Explore>(context);
        if (explore.state != Explore::ExploreSite)
            return false;
        if (!game.characterInCompany(game.getParent(host), explore.company))
            return false;
        if (args.empty())
            return false;
        CardID target = args.front();
        int targetPlayer = game.cards[target].owner;
        if (targetPlayer == game.currentPlayer)
            return false;
        CardType & type = game.getCardType(target);
        if (!std::holds_alternative<Character>(type))
            return false;
        if (std::get<Character>(type).race != Dwarf)
            return false;
        CompanyID id = game.getCompanyID(target);
        if (id < 0)
            return false;
        Company & targetCompany = *game.sides[targetPlayer].findCompany(id);
        if (game.getLocation(explore.site).getName() != game.getLocation(targetCompany.currentLocation).getName())
            return false;
        if (!scene.allowedOperation(target, Discard))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.discard(host);
        scene.discard(args.front());
    }
};
struct PlayTheArkenstone : public PlayItemAction {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        game.effects.push_back(std::make_shared<TheArkenstoneMind>(host));
        side.effects.push_back(std::make_shared<EquipmentInfluenceRaceBonus>(host, Dwarf, 5));
        side.actions.add(std::make_shared<DiscardTheArkenstone>(), host);
    }
};
struct TheArkenstone : public CardDefinition {
    TheArkenstone() : CardDefinition(Item()) {
        Item & def = std::get<Item>(type);
        def.minion = true;
        def.name.push_back("The Arkenstone");
        def.type = Greater;
        def.unique = true;
        def.mp = 3;
        def.cp = 3;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayTheArkenstone>(), host);
    }
};

/* Vile Fumes */
struct VileFumesSiteType : public Effect {
    VileFumesSiteType(CardID host, CardID site)
        : Effect(host), site(site) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<UpdateSiteType>(batch.et))
            return false;
        CardID currentSite = std::get<UpdateSiteType>(batch.et).location;
        if (!batch.scene.game.isManifestation(site, currentSite))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<UpdateSiteType>(batch.et).siteType = RuinsLairs;
    }
    bool remove(EffectBatch & batch) const {
        return !batch.scene.game.siteCardInPlay(site);
    }
    const CardID site;
};
struct VileFumesAutomaticAttack : public Effect {
    VileFumesAutomaticAttack(CardID host, CardID site)
        : Effect(host), site(site) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<AutomaticAttack>(batch.et))
            return false;
        CardID currentSite = std::get<AutomaticAttack>(batch.et).site;
        if (!batch.scene.game.isManifestation(site, currentSite))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        AutomaticAttack & attack = std::get<AutomaticAttack>(batch.et);
        attack.info.clear();
        attack.info.add(Gas, 0, 7);
        attack.info.back()->mayNotBeCancelled = true;
    }
    bool remove(EffectBatch & batch) const {
        return !batch.scene.game.siteCardInPlay(site);
    }
    const CardID site;
};
struct DiscardVileFumes : public Effect {
    DiscardVileFumes(CardID host, CardID site)
        : Effect(host), site(site) {}
    bool conditions(EffectBatch & batch) const {
        return !batch.scene.game.siteCardInPlay(site);
    }
    void activate(EffectBatch & batch) const {
        batch.scene.discard(site);
    }
    bool remove(EffectBatch & batch) const {
        return batch.scene.game.getParent(host) != site;
    }
    const CardID site;
};
struct VileFumesAction : public EquipmentAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        Game & game = scene.game;
        if (game.possessedByRingwraith(host))
            return false;
        if (game.turnSequence.phase != SitePhase)
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Explore>(context))
            return false;
        Explore & explore = std::get<Explore>(context);
        if (explore.state != Explore::ExploreSite)
            return false;
        SiteType siteType = scene.getSiteType(explore.site);
        if (siteType != BorderHold && siteType != ShadowHold)
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        Explore & explore = std::get<Explore>(scene.context->context);
        scene.gainPossession(explore.site, host);
        game.effects.push_back(std::make_shared<VileFumesSiteType>(host, explore.site));
        game.effects.push_back(std::make_shared<VileFumesAutomaticAttack>(host, explore.site));
        game.getCurrentSide().effects.push_back(std::make_shared<DiscardVileFumes>(host, explore.site));
    }
};
struct PlayVileFumes : public PlayBlazonOfTheEye {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.actions.add(std::make_shared<VileFumesAction>(), host);
    }
};
struct VileFumes : public CardDefinition {
    VileFumes() : CardDefinition(Item()) {
        Item & def = std::get<Item>(type);
        def.minion = true;
        def.name.push_back("Vile Fumes");
        def.type = Special;
        def.mp = 1;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayVileFumes>(), host);
    }
};

/* A Nice Place to Hide */
struct PlayANicePlaceToHide : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        if (args.empty())
            return false;
        CardID card = args.front();
        Game & game = scene.game;
        if (game.getCardState(card) != Untapped)
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Attack>(context))
            return false;
        Attack & attack = std::get<Attack>(context);
        if (attack.mayNotBeCancelled)
            return false;
        if (!game.characterInCompany(card, attack.company))
            return false;
        if (!scene.hasSkill(card, Scout))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        CardID target = args.front();
        scene.tap(target);
        scene.cancelAttack();
        scene.discard(host);
    }
};
struct ANicePlaceToHide : public CardDefinition {
    ANicePlaceToHide() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.minion = true;
        def.name.push_back("A Nice Place to Hide");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayANicePlaceToHide>(), host);
    }
};

/* Bade to Rule */
struct BadeToRuleGI : public Effect {
    BadeToRuleGI(CardID host) : Effect(host) {}
    bool conditions(EffectBatch & batch) const {
        return std::holds_alternative<UnusedGI>(batch.et);
    }
    void activate(EffectBatch & batch) const {
        std::get<UnusedGI>(batch.et).gi += 5;
    }
    bool remove(EffectBatch & batch) const {
        DeckName deck = batch.scene.game.getDeck(host);
        return deck != Possessions && deck != PermanentEvents;
    }
};
struct BadeToRuleDI : public Effect {
    BadeToRuleDI(CardID host) : Effect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<UnusedDI>(batch.et))
            return false;
        UnusedDI di = std::get<UnusedDI>(batch.et);
        if (di.target != batch.scene.game.getParent(host))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<UnusedDI>(batch.et).di -= 2;
    }
    bool remove(EffectBatch & batch) const {
        DeckName deck = batch.scene.game.getDeck(host);
        return deck != Possessions && deck != PermanentEvents;
    }
};
struct BadeToRuleDiscard : public Action {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        return scene.resourceConditions(host);
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.discard(host);
    }
    bool remove(Scene scene) const {
        DeckName deck = scene.game.getDeck(host);
        return deck != PermanentEvents && deck != Possessions;
    }
};
struct BadeToRulePlayRingwraithEffect : public PermanentEventEffect {
    BadeToRulePlayRingwraithEffect(CardID host) : PermanentEventEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<MoveCard>(batch.et))
            return false;
        MoveCard & move = std::get<MoveCard>(batch.et);
        Game & game = batch.scene.game;
        if (move.destination != Characters)
            return false;
        if (game.getRace(move.target) != Ringwraith)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        batch.scene.gainPossession(std::get<MoveCard>(batch.et).target, host);
    }
};
struct PlayBadeToRule : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        Game & game = scene.game;
        if (game.duplicateEffect(host, game.currentPlayer))
            return false;
        Side & side = game.getCurrentSide();
        if (game.avatarInPlay(side) < 0)
            return true;
        // Additional conditions if the Ringwraith is in play:
        if (game.turnSequence.phase != OrganizationPhase)
            return false;
        for (Company & company : side.companies)
            for (CardID character : company.characters)
                if (game.getRace(character) == Ringwraith)
                    return scene.getSiteType(company.currentLocation) == Darkhaven;
        return false;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        Side & side = game.getCurrentSide();

        if (game.avatarInPlay(side) >= 0) {
            for (Company & company : side.companies)
                for (CardID character : company.characters)
                    if (game.getRace(character) == Ringwraith && game.getParent(character) < 0)
                        scene.gainPossession(character, host);
        } else {
            scene.moveCard(host, PermanentEvents, game.currentPlayer);
            side.effects.push_back(std::make_shared<BadeToRulePlayRingwraithEffect>(host));
        }
        side.effects.push_back(std::make_shared<BadeToRuleGI>(host));
        side.effects.push_back(std::make_shared<BadeToRuleDI>(host));
        side.actions.add(std::make_shared<BadeToRuleDiscard>(), host);
    }
};
struct BadeToRule : public CardDefinition {
    BadeToRule() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.minion = true;
        def.name.push_back("Bade to Rule");
        def.type = PermanentEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayBadeToRule>(), host);
    }
};

/* Crept Along Cleverly */
struct PlayCreptAlongCleverly : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        if (args.empty())
            return false;
        CardID ranger = args.front();
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Attack>(context))
            return false;
        Attack & attack = std::get<Attack>(context);
        if (attack.mayNotBeCancelled)
            return false;
        const std::vector<Race> races = { Wolf, Animal, Spider, Dragon, Drake, Undead };
        if (std::find(races.cbegin(), races.cend(), attack.creatureType) == races.cend())
            return false;
        if (!scene.game.characterInCompany(ranger, attack.company))
            return false;
        if (!scene.hasSkill(ranger, Ranger))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.cancelAttack();
        scene.discard(host);
    }
};
struct CreptAlongCleverly : public CardDefinition {
    CreptAlongCleverly() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.minion = true;
        def.name.push_back("Crept Along Cleverly");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayCreptAlongCleverly>(), host);
    }
};

/* Fell Rider */
struct FellRiderProwessBonus : public EquipmentEffect {
    FellRiderProwessBonus(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Stat>(batch.et))
            return false;
        Stat & stat = std::get<Stat>(batch.et);
        if (stat.type != Prowess)
            return false;
        if (stat.target != batch.scene.game.getParent(host))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<Stat>(batch.et).stat += 2;
    }
};
struct FellRiderInfluencePenalty : public EquipmentEffect {
    FellRiderInfluencePenalty(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<UnusedDI>(batch.et))
            return false;
        UnusedDI & di = std::get<UnusedDI>(batch.et);
        if (di.target != batch.scene.game.getParent(host))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<UnusedDI>(batch.et).di -= 3;
    }
};
struct FellRiderNoAlliesOrFollowers : public EquipmentEffect {
    FellRiderNoAlliesOrFollowers(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (std::holds_alternative<PlayAlly>(batch.et))
            return std::get<PlayAlly>(batch.et).target == batch.scene.game.getParent(host);
        if (std::holds_alternative<PlayCharacter>(batch.et)) {
            PlayCharacter & playCharacter = std::get<PlayCharacter>(batch.et);
            if (playCharacter.target != batch.scene.game.getParent(host))
                return false;
            return batch.scene.getRace(playCharacter.character) == Ringwraith;
        }
        return false;
    }
    void activate(EffectBatch & batch) const {
        if (std::holds_alternative<PlayAlly>(batch.et))
            std::get<PlayAlly>(batch.et).allowed = false;
        else if (std::holds_alternative<PlayCharacter>(batch.et))
            std::get<PlayCharacter>(batch.et).allowed = false;
    }
};
struct DiscardFellRider : public EquipmentAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        if (game.turnSequence.phase != OrganizationPhase)
            return false;
        Side & side = game.getCurrentSide();
        Company & company = *side.findCompany(game.getParent(host), Characters);
        if (scene.getSiteType(company.currentLocation) != Darkhaven)
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.discard(host);
    }
};
struct PlayFellRider : public PlayCardAction {
    // The card is not a possession but a permanent event on the company
    // However, if the Ringwraith is transferred out of the company, the Fell Rider
    // card should be transferred along with him. That behaviour is most easily
    // achieved by placing the card among the Ringwraith's possessions.
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        Game & game = scene.game;
        if (game.turnSequence.phase != OrganizationPhase)
            return false;
        Side & side = game.getCurrentSide();
        CardID ringwraith = game.avatarInPlay(side);
        if (ringwraith < 0)
            return false;
        Company & company = *side.findCompany(ringwraith, Characters);
        if (scene.getSiteType(company.currentLocation) != Darkhaven)
            return false;
        if (game.duplicatePossession(ringwraith, host))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        Side & side = game.getCurrentSide();
        CardID ringwraith = game.avatarInPlay(side);
        Company & company = *side.findCompany(ringwraith, Characters);
        scene.gainPossession(ringwraith, host);
        auto & possessions = game.cards[ringwraith].possessions;
        // Discard all allies and Ringwraith followers
        for (CardID possession : possessions) {
            CardType & type = game.getCardType(possession);
            if (std::holds_alternative<Ally>(type) ||
                std::holds_alternative<Character>(type) &&
                std::get<Character>(type).race == Ringwraith)
                scene.discard(possession);
        }
        // Discard other mode card affecting the Ringwraith (CRF rulings by term: Modes)
        scene.discardRingwraithModeCard(game.currentPlayer);
        insertPatches(scene.game);
    }
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<RingwraithModeEffect>(host, RingwraithMode::FellRiderMode));
        // Makes the Ringwraith's company overt (MELE, p.21, bottom)
        side.effects.push_back(std::make_shared<OvertPossession>(host));
        side.effects.push_back(std::make_shared<FellRiderProwessBonus>(host));
        side.effects.push_back(std::make_shared<FellRiderInfluencePenalty>(host));
        side.effects.push_back(std::make_shared<FellRiderNoAlliesOrFollowers>(host));
        side.actions.add(std::make_shared<DiscardFellRider>(), host);
    }
};
struct FellRider : public CardDefinition {
    FellRider() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.minion = true;
        def.name.push_back("Fell Rider");
        def.type = PermanentEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayFellRider>(), host);
    }
};

/* Orc Quarrels */
struct OrcQuarrelsCancelAttack : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Attack>(context))
            return false;
        Attack & attack = std::get<Attack>(context);
        if (attack.mayNotBeCancelled)
            return false;
        Race race = attack.creatureType;
        if (race != Orc && race != Troll && race != Man)
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.cancelAttack();
        scene.discard(host);
    }
};
struct OrcQuarrelsReduceStrikes : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Attack>(context))
            return false;
        Attack & attack = std::get<Attack>(context);
        if (attack.strikes < 2)
            return false;
        if (scene.game.resourceEventInPlay("Skies of Fire", PermanentEvent))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        int & strikes = std::get<Attack>(scene.context->context).strikes;
        strikes = (strikes + 1) / 2;
        scene.discard(host);
    }
};
struct OrcQuarrels : public CardDefinition {
    OrcQuarrels() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.minion = true;
        def.name.push_back("Orc Quarrels");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<OrcQuarrelsCancelAttack>(), host, "Cancel");
        actions.add(std::make_shared<OrcQuarrelsReduceStrikes>(), host, "Reduce strikes");
    }
};

/* Piercing All Shadows */
struct PiercingAllShadowsReturnToOrigin : public UntilEndOfTurnPossessionEffect {
    PiercingAllShadowsReturnToOrigin(CardID host) : UntilEndOfTurnPossessionEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<AllowedOperation>(batch.et))
            return false;
        AllowedOperation & op = std::get<AllowedOperation>(batch.et);
        if (op.operation != ReturnToOrigin)
            return false;
        Game & game = batch.scene.game;
        if (!game.characterInCompany(game.getParent(host)))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<AllowedOperation>(batch.et).allowed = false;
    }
};
struct PiercingAllShadowsTapSite : public UntilEndOfTurnPossessionEffect {
    PiercingAllShadowsTapSite(CardID host) : UntilEndOfTurnPossessionEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<AllowedOperation>(batch.et))
            return false;
        AllowedOperation & op = std::get<AllowedOperation>(batch.et);
        if (op.operation != Tap)
            return false;
        Game & game = batch.scene.game;
        if (!std::holds_alternative<Location>(game.getCardType(op.card)))
            return false;
        Company & company = *game.getCurrentSide().findCompany(game.getParent(host), Characters);
        if (op.card != company.newLocation)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<AllowedOperation>(batch.et).allowed = false;
    }
};
struct PiercingAllShadowsAction : public EquipmentAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        Game & game = scene.game;
        if (game.getCardState(game.getParent(host)) != Untapped)
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        Side & side = game.getCurrentSide();
        scene.tap(host);
        side.effects.push_back(std::make_shared<PiercingAllShadowsTapSite>(host));
        side.effects.push_back(std::make_shared<PiercingAllShadowsReturnToOrigin>(host));
        scene.setupResolveCorruptionChecks(host, game.getParent(host));
    }
};
struct PlayPiercingAllShadows : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        if (!scene.resourceConditions(host))
            return false;
        if (game.turnSequence.phase != OrganizationPhase)
            return false;
        if (args.empty())
            return false;
        CardID target = args.front();
        if (game.cards[target].owner != game.currentPlayer)
            return false;
        if (!std::holds_alternative<Character>(game.getCardType(target)))
            return false;
        if (!scene.hasSkill(target, Ranger))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.gainPossession(args.front(), host);
        insertPatches(scene.game);
    }
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.actions.add(std::make_shared<PiercingAllShadowsAction>(), host);
        side.effects.push_back(std::make_shared<IncreaseCorruptionEffect>(host, 2));
        side.effects.push_back(std::make_shared<NoDuplicatesInCompanyEffect>(host));
    }
};
struct PiercingAllShadows : public CardDefinition {
    PiercingAllShadows() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.minion = true;
        def.name.push_back("Piercing All Shadows");
        def.type = PermanentEvent;
        def.keywords = { LightEnchantment };
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayPiercingAllShadows>(), host);
    }
};

/* Sneakin' */
struct SneakinEffect : public UntilEndOfTurnEffect {
    SneakinEffect(CardID host, CardID scout) : UntilEndOfTurnEffect(host), scout(scout) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<AllowedOperation>(batch.et))
            return false;
        AllowedOperation & op = std::get<AllowedOperation>(batch.et);
        if (op.operation != Play)
            return false;
        Scene & scene = batch.scene;
        if (!std::holds_alternative<HazardCreature>(scene.game.getCardType(op.card)))
            return false;
        Side & side = scene.game.getCurrentSide();
        auto it = side.findCompany(scout);
        if (it == side.companies.end())
            return false;
        if (scene.getCompanySize(*it) > 2)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<AllowedOperation>(batch.et).allowed = false;
    }
    const CardID scout;
};

struct PlaySneakin : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        Game & game = scene.game;
        if (game.turnSequence.phase != OrganizationPhase)
            return false;
        if (args.empty())
            return false;
        CardID target = args.front();
        if (game.getCardState(target) != Untapped)
            return false;
        Side & side = game.getCurrentSide();
        auto it = side.findCompany(target, Characters);
        if (it == side.companies.end())
            return false;
        if (scene.getCompanySize(*it) > 2)
            return false;
        if (!scene.hasSkill(target, Scout))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.tap(args.front());
        scene.game.getCurrentSide().effects.push_back(std::make_shared<SneakinEffect>(host, args.front()));
        scene.discard(host);
    }
};

struct Sneakin : public CardDefinition {
    Sneakin() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.minion = true;
        def.name.push_back("Sneakin'");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlaySneakin>(), host);
    }
};

/* Sudden Call */
struct SuddenCallResource : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        Game & game = scene.game;
        if (game.councilCalled >= 0)
            return false;
        if (!scene.mayCallCouncil(game.currentPlayer))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.game.councilCalled = scene.game.currentPlayer;
        scene.discard(host);
    }
};
struct SuddenCallHazard : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.narrowHazardConditions(host))
            return false;
        Game & game = scene.game;
        if (game.councilCalled >= 0)
            return false;
        if (game.getCurrentSide().deckType == HeroDeck)
            return false;
        if (!scene.mayCallCouncil(game.currentPlayer))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.game.councilCalled = scene.game.currentPlayer;
        scene.discard(host);
    }
};
struct SuddenCallReshuffle : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        return !std::holds_alternative<ExhaustDeck>(scene.context->context);
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Side & side = scene.game.getSide(host);
        scene.moveCard(host, PlayDeck, side.player);
        if (scene.game.cards[host].deck == PlayDeck)
            scene.shuffleDeck(side);
    }
};
struct SuddenCall : public CardDefinition {
    SuddenCall() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.minion = true;
        def.name.push_back("Sudden Call");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<SuddenCallResource>(), host);
        actions.add(std::make_shared<SuddenCallHazard>(), host);
        actions.add(std::make_shared<SuddenCallReshuffle>(), host);
    }
};

/* Voices of Malice */
struct PlayVoicesOfMalice : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        if (args.size() < 2)
            return false;
        Game & game = scene.game;
        auto it = args.begin();
        CardID character = *it;
        if (game.cards[character].owner != game.cards[host].owner)
            return false;
        if (game.getCardState(character) != Untapped)
            return false;
        if (!scene.hasSkill(character, Sage))
            return false;
        CardID target = *(++it);
        Card & card = game.cards[target];
        if (card.deck <= OutOfPlay || card.deck == Pending || card.deck == OnGuard)
            return false;
        CardType & type = game.settings.cardDefinitions[card.dataIndex]->type;
        if (!std::holds_alternative<HazardEvent>(type))
            return false;
        HazardEvent & ev = std::get<HazardEvent>(type);
        if (ev.type != PermanentEvent && ev.type != LongEvent)
            return false;
        for (EventKeyword key : ev.keywords)
            if (key == Environment)
                return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        auto it = args.begin();
        CardID character = *it;
        scene.tap(character);
        scene.discard(*(++it));
        scene.discard(host);
        if (scene.getRace(character) != Ringwraith) {
            Scene resolve = scene.setupResolveCorruptionChecks(host, character);
            Scene corruption = resolve.setupCorruptionCheck(character);
            std::get<CorruptionCheck>(corruption.context->context).modifier -= 2;
        }
    }
};
struct VoicesOfMalice : public CardDefinition {
    VoicesOfMalice() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.minion = true;
        def.name.push_back("Voices of Malice");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayVoicesOfMalice>(), host);
    }
};
} // namespace mele

/* An Unexpected Outpost */
struct PlayAnUnexpectedOutpost : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.narrowHazardConditions(host))
            return false;
        Game & game = scene.game;
        Side & side = game.getCurrentSide();
        if (game.containsCardCategory(side.discardPile, HazardCard) ||
            game.containsCardCategory(side.sideboard, HazardCard))
            return true;
        return false;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        TransferCards context;
        TransferCards::TransferCardsOption option;
        option.sourceDeck = Sideboard;
        option.destinationDeck = PlayDeck;
        option.minimum = 1;
        option.maximum = game.hazardEventInPlay("Doors of Night", PermanentEvent) ? 2 : 1;
        option.categories = { HazardCard };
        context.transactions[game.currentPlayer].push_back(option);
        option.sourceDeck = DiscardPile;
        context.transactions[game.currentPlayer].push_back(option);
        game.contextStack.push_back(std::make_shared<Context>(context));
        // The card is automatically discarded after TransferCards has resolved
        // (delay the discard to prevent the card from recycling itself)
    }
};
struct AnUnexpectedOutpost : public CardDefinition {
    AnUnexpectedOutpost() : CardDefinition(HazardEvent()) {
        HazardEvent & def = std::get<HazardEvent>(type);
        def.name.push_back("An Unexpected Outpost");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayAnUnexpectedOutpost>(), host);
    }
};

/* Choking Shadows */
struct ChokingShadowsAutomatickAttackEffect : public Effect {
    ChokingShadowsAutomatickAttackEffect(CardID host, CardID site)
        : Effect(host), site(site) {}
    bool conditions(EffectBatch & batch) const {
        Game & game = batch.scene.game;
        if (game.turnSequence.phase != SitePhase)
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<Attack>(context))
            return false;
        Attack & attack = std::get<Attack>(context);
        if (!attack.automaticAttack)
            return false;
        Side & side = game.getCurrentSide();
        if (side.findCompany(attack.company)->currentLocation != site)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        Game & game = batch.scene.game;
        game.effects.push_back(std::make_shared<HazardProwessBonus>(host, 2));
        auto effect = std::make_shared<ChokingShadowsAutomatickAttackEffect>(*this);
        batch.effectQueue.front()->effect = effect;
        effect->trigged = true;
    }
    bool remove(EffectBatch & batch) const {
        Game & game = batch.scene.game;
        if (game.turnSequence.phase == EndOfTurnPhase)
            return true;
        return trigged;
    }
    const CardID site;
    bool trigged = false;
};
struct ChokingShadowsAutomaticAttackAction : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.hazardConditions(host))
            return false;
        Game & game = scene.game;
        Side & side = game.getCurrentSide();
        Company & company = *side.findCompany(game.getActiveCompany());
        if (scene.getSiteType(company.newLocation) != RuinsLairs)
            return false;
        if (game.turnSequence.phase == SitePhase)
            if (!std::holds_alternative<Attack>(scene.context->context))
                return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        Side & side = game.getCurrentSide();
        CardID site = side.findCompany(game.getActiveCompany())->newLocation;
        game.effects.push_back(std::make_shared<ChokingShadowsAutomatickAttackEffect>(host, site));
    }
};
struct ChokingShadowsSitePathAction : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.narrowHazardConditions(host))
            return false;
        Game & game = scene.game;
        if (game.hazardEventInPlay("Doors of Night", PermanentEvent))
            return false;
        SitePath sitePath = scene.getSitePath();
        auto it = std::find(sitePath.begin(), sitePath.end(), Wilderness);
        if (it == sitePath.end())
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Side & side = scene.game.getCurrentSide();
        side.effects.push_back(std::make_shared<ChangePathSymbol>(host, Wilderness, ShadowLands));
    }
};
struct ChokingShadowsSiteTypeAction : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.hazardConditions(host))
            return false;
        Game & game = scene.game;
        if (game.hazardEventInPlay("Doors of Night", PermanentEvent))
            return false;
        Movement & movement = game.getMovement();
        if (scene.getSiteType(movement.destination) != RuinsLairs)
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Side & side = scene.game.getCurrentSide();
        CardID site = scene.game.getMovement().destination;
        side.effects.push_back(std::make_shared<ChangeSiteType>(host, site, ShadowHold));
    }
};
struct ChokingShadows : public CardDefinition {
    ChokingShadows() : CardDefinition(HazardEvent()) {
        HazardEvent & def = std::get<HazardEvent>(type);
        def.name.push_back("Choking Shadows");
        def.type = ShortEvent;
        def.keywords = { Environment };
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<ChokingShadowsAutomaticAttackAction>(), host, "Prowess");
        actions.add(std::make_shared<ChokingShadowsSitePathAction>(), host, "Shadow-land");
        actions.add(std::make_shared<ChokingShadowsSiteTypeAction>(), host, "Shadow-hold");
    }
};

/* Doors of Night */
struct PlayDoorsOfNight : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.hazardConditions(host))
            return false;
        Game & game = scene.game;
        if (game.hazardEventInPlay("Doors of Night", PermanentEvent))
            return false;
        return true;
    }
    void removeResourceEnvironmentEffects(Scene scene, EffectList & effects) const {
        Game & game = scene.game;
        for (auto element : effects) {
            auto effect = element.effect;
            if (std::holds_alternative<ResourceEvent>(game.getCardType(effect->host))
                && scene.hasKeyword(effect->host, Environment))
                effects.remove(effect->host);
        }
    }
    void discardResourceEnvironmentCards(Scene scene, std::vector<CardID> & deck, int player) const {
        Game & game = scene.game;
        for (CardID card : deck)
            if (std::holds_alternative<ResourceEvent>(game.getCardType(card))
                && scene.hasKeyword(card, Environment))
                scene.discard(card, player);
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        scene.moveCard(host, PermanentEvents, game.hazardPlayer);
        removeResourceEnvironmentEffects(scene, game.effects);
        for (Side & side : game.sides) {
            discardResourceEnvironmentCards(scene, side.permanentEvents, side.player);
            discardResourceEnvironmentCards(scene, side.longEvents, side.player);
            removeResourceEnvironmentEffects(scene, side.effects);
            for (Company & company : side.companies)
                removeResourceEnvironmentEffects(scene, company.effects);
        }
    }
};
struct DoorsOfNight : public CardDefinition {
    DoorsOfNight() : CardDefinition(HazardEvent()) {
        HazardEvent & def = std::get<HazardEvent>(type);
        def.name.push_back("Doors of Night");
        def.type = PermanentEvent;
        def.keywords = { Environment };
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayDoorsOfNight>(), host);
    }
};

/* Exhalation of Decay */
struct PlayExhalationOfDecay : public PlayCardAction {
    // This implementation relies on fine details of how the surrounding
    // framework is implemented, so write thorough tests for this card.
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.narrowHazardConditions(host))
            return false;
        Game & game = scene.game;
        // Chain of effects should be empty since Exhalation of Decay
        // represents a creature
        if (game.getDeck(host) == Hand
            && std::holds_alternative<ChainOfEffects>(scene.context->context))
            return false;
        if (args.empty())
            return false;
        CardID target = args.front();
        Card & card = game.cards[target];
        if (card.owner != game.hazardPlayer)
            return false;
        if (card.deck != DiscardPile)
            return false;
        CardDefinition & def = *game.settings.cardDefinitions[card.dataIndex];
        if (!std::holds_alternative<HazardCreature>(def.type))
            return false;
        if (std::get<HazardCreature>(def.type).race != Undead)
            return false;
        ActionList actions;
        def.getActions(actions, host);
        if (!actions.back()->conditions(scene.actualScene(), {}))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        int dataIndex = game.cards[args.front()].dataIndex;
        CardDefinition & def = *game.settings.cardDefinitions[dataIndex];
        ActionList actions;
        def.getActions(actions, host);
        actions.back()->activate(scene.actualScene(), {});
        scene.discard(host);
    }
};
struct ExhalationOfDecay : public CardDefinition {
    ExhalationOfDecay() : CardDefinition(ResourceEvent()) {
        ResourceEvent & def = std::get<ResourceEvent>(type);
        def.name.push_back("Exhalation of Decay");
        def.type = ShortEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayExhalationOfDecay>(), host);
    }
};

/* Plague of Wights */
struct PlagueOfWightsEffect : public LongEventEffect {
    PlagueOfWightsEffect(CardID host, bool strikesDoubled)
        : LongEventEffect(host)
        , strikesDoubled(strikesDoubled) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Stat>(batch.et))
            return false;
        Stat & stat = std::get<Stat>(batch.et);
        if (stat.type != Prowess && (stat.type != Strikes || !strikesDoubled))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<Attack>(context))
            return false;
        Attack & attack = std::get<Attack>(context);
        if (attack.creatureType != Undead)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        Stat & stat = std::get<Stat>(batch.et);
        stat.type == Prowess ? stat.stat++ : stat.stat <<= 1;
    }
    const bool strikesDoubled;
};
struct PlayPlagueOfWights : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.hazardConditions(host))
            return false;
        if (scene.game.hazardEventInPlay(host))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.moveCard(host, LongEvents);
        insertPatches(scene.game);
    }
    void insertPatches(Game & game) const {
        bool strikesDoubled = game.hazardEventInPlay("Doors of Night", PermanentEvent);
        game.effects.push_back(std::make_shared<PlagueOfWightsEffect>(host, strikesDoubled));
    }
};
struct PlagueOfWights : public CardDefinition {
    PlagueOfWights() : CardDefinition(HazardEvent()) {
        HazardEvent & def = std::get<HazardEvent>(type);
        def.name.push_back("Plague of Wights");
        def.type = LongEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayPlagueOfWights>(), host);
    }
};

/* Power Built by Waiting */
struct TapPowerBuiltByWaiting : public PermanentEventAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        if (game.cards[host].owner != game.hazardPlayer)
            return false;
        if (!std::holds_alternative<Movement>(scene.context->context))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.tap(host);
        std::get<Movement>(scene.context->context).hazardLimit++;
    }
};
struct UntapPowerBuiltByWaiting : public PermanentEventAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        if (game.cards[host].owner != game.hazardPlayer)
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Movement>(context))
            return false;
        if (std::get<Movement>(context).hazardLimit < 2)
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        std::get<Movement>(scene.context->context).hazardLimit -= 2;
        scene.untap(host);
    }
};
struct PlayPowerBuiltByWaiting : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        return scene.narrowHazardConditions(host);
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.moveCard(host, PermanentEvents);
        insertPatches(scene.game);
    }
    void insertPatches(Game & game) const {
        ActionList & actions = game.getSide(host).actions;
        actions.add(std::make_shared<TapPowerBuiltByWaiting>(), host);
        actions.add(std::make_shared<UntapPowerBuiltByWaiting>(), host);
    }
};
struct PowerBuiltByWaiting : public CardDefinition {
    PowerBuiltByWaiting() : CardDefinition(HazardEvent()) {
        HazardEvent & def = std::get<HazardEvent>(type);
        def.name.push_back("Power Built by Waiting");
        def.type = LongEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayPowerBuiltByWaiting>(), host);
    }
};

/* Stench of Mordor */
struct StenchOfMordorTapCharacter : public PermanentEventEffect {
    StenchOfMordorTapCharacter(CardID host) : PermanentEventEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        Game & game = batch.scene.game;
        if (!std::holds_alternative<OpenContext>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<Explore>(context))
            return false;
        Location & location = game.getLocation(std::get<Explore>(context).site);
        PathSymbol regionSymbol = game.getRegionType(location.region);
        if (regionSymbol == DarkDomain)
            return true;
        if (regionSymbol == ShadowLands
            && game.hazardEventInPlay("Doors of Night", PermanentEvent))
            return true;
        return false;
    }
    void activate(EffectBatch & batch) const {
        Game & game = batch.scene.game;
        CompanyID id = std::get<Explore>(batch.getContext()).company;
        game.contextStack.push_back(std::make_shared<Context>(TapCharacters(game.currentPlayer, 1)));
        Scene currentScene = getCurrentScene(game);
        TapCharacters & tap = std::get<TapCharacters>(currentScene.context->context);
        Company & company = *game.getCurrentSide().findCompany(id);
        tap.characters = company.characters;
        currentScene.context->autoProceed = true; // Checks if no characters are untapped
    }
};
struct PlayStenchOfMordor : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.narrowHazardConditions(host))
            return false;
        if (scene.game.duplicateEffect(host))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.moveCard(host, PermanentEvents);
        insertPatches(scene.game);
    }
    void insertPatches(Game & game) const {
        game.effects.push_back(std::make_shared<StenchOfMordorTapCharacter>(host));
        game.effects.push_back(std::make_shared<DiscardWhenAnyPlayDeckIsExhausted>(host));
    }
};
struct StenchOfMordor : public CardDefinition {
    StenchOfMordor() : CardDefinition(HazardEvent()) {
        HazardEvent & def = std::get<HazardEvent>(type);
        def.name.push_back("Stench of Mordor");
        def.type = PermanentEvent;
        def.keywords = { Environment };
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayStenchOfMordor>(), host);
    }
};

/* The Moon Is Dead */
struct DiscardTheMoonIsDead : public PermanentEventEffect {
    DiscardTheMoonIsDead(CardID host) : PermanentEventEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<CloseContext>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<Attack>(context))
            return false;
        Attack & attack = std::get<Attack>(context);
        if (attack.detainment || attack.outcome == Successful)
            return false;
        if (attack.creatureType != Undead)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        batch.scene.discard(host);
    }
};
struct TheMoonIsDeadStrikesAndProwess : public PermanentEventEffect {
    TheMoonIsDeadStrikesAndProwess(CardID host) : PermanentEventEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Stat>(batch.et))
            return false;
        Stat & stat = std::get<Stat>(batch.et);
        if (stat.type != Strikes && stat.type != Prowess)
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<Attack>(context))
            return false;
        Attack & attack = std::get<Attack>(context);
        if (attack.creatureType != Undead)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<Stat>(batch.et).stat++;
    }
};
struct TheMoonIsDeadDuplicateAutomaticAttacks : public PermanentEventEffect {
    TheMoonIsDeadDuplicateAutomaticAttacks(CardID host) : PermanentEventEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<AutomaticAttack>(batch.et))
            return false;
        AutomaticAttack & attack = std::get<AutomaticAttack>(batch.et);
        for (auto info : attack.info)
            if (info->type == Undead)
                return true;
        return false;
    }
    void activate(EffectBatch & batch) const {
        AutomaticAttack & attack = std::get<AutomaticAttack>(batch.et);
        AutomaticAttacks attacks = attack.info;  // Make a copy
        for (auto info : attacks)
            if (info->type == Undead)
                attack.info.push_back(info);
    }
};
struct PlayTheMoonIsDead : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.hazardConditions(host))
            return false;
        if (scene.game.hazardEventInPlay(host))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        scene.moveCard(host, PermanentEvents, game.hazardPlayer);
        insertPatches(game);
    }
    void insertPatches(Game & game) const {
        game.effects.push_back(std::make_shared<TheMoonIsDeadStrikesAndProwess>(host));
        game.effects.push_back(std::make_shared<TheMoonIsDeadDuplicateAutomaticAttacks>(host));
        game.effects.push_back(std::make_shared<DiscardTheMoonIsDead>(host));
    }
};
struct TheMoonIsDead : public CardDefinition {
    TheMoonIsDead() : CardDefinition(HazardEvent()) {
        HazardEvent & def = std::get<HazardEvent>(type);
        def.name.push_back("The Moon Is Dead");
        def.type = PermanentEvent;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayTheMoonIsDead>(), host);
    }
};

/* Ambusher */
struct Ambusher : public HazardCreatureDefinition {
    Ambusher() {
        HazardCreature & def = std::get<HazardCreature>(type);
        def.name.push_back("Ambusher");
        def.playable = { FreeDomain, BorderLands };
        def.race = Man;
        def.strikes = 2;
        def.prowess = 10;
        def.attackerChoosesDefendingCharacters = true;
    }
};

/* Barrow-wight */
struct BarrowWightEffect : public Effect {
    BarrowWightEffect(CardID host)
        : Effect(host) {}
    bool conditions(EffectBatch & batch) const {
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<CloseContext>(batch.et))
            return false;
        if (std::holds_alternative<BodyCheck>(context)) {
            BodyCheck & bodyCheck = std::get<BodyCheck>(context);
            if (bodyCheck.target == host)
                return false;
            if (bodyCheck.roll + bodyCheck.modifier > bodyCheck.body)
                return false;
            return true;
        } else if (std::holds_alternative<Attack>(context)) {
            Attack & attack = std::get<Attack>(context);
            if (attack.card != host)
                return false;
            if (wounded.empty())
                return false;
            return true;
        }
        return false;
    }
    void activate(EffectBatch & batch) const {
        if (std::holds_alternative<BodyCheck>(batch.getContext())) {
            auto effect = std::make_shared<BarrowWightEffect>(*this);
            batch.effectQueue.front()->effect = effect;
            effect->wounded.push_back(std::get<BodyCheck>(batch.getContext()).target);
        } else {
            batch.effectQueue.front()->remove = true;
            ResolveCorruptionChecks resolve;
            for (CardID target : wounded)
                resolve.unresolvedChecks.emplace_back(target, target, -2);
            batch.scene.game.contextStack.push_back(std::make_shared<Context>(resolve));
        }
    }
    bool remove(EffectBatch & batch) const {
        return batch.effectQueue.front()->remove;
    }
    std::vector<CardID> wounded;
};
struct PlayBarrowWight : public PlayHazardCreature {
    void insertEffects(Scene scene, const std::list<CardID> & args) const {
        scene.game.effects.push_back(std::make_shared<BarrowWightEffect>(host));
    }
};
struct BarrowWight : public CardDefinition {
    BarrowWight() : CardDefinition(HazardCreature()) {
        HazardCreature & def = std::get<HazardCreature>(type);
        def.name.push_back("Barrow-wight");
        def.playable = { ShadowLands, DarkDomain, ShadowHold, DarkHold };
        def.race = Undead;
        def.strikes = 1;
        def.prowess = 12;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayBarrowWight>(), host);
    }
};

/* Chill Douser */
struct ChillDouserStrikesAndProwess : public UntilEndOfTurnEffect {
    ChillDouserStrikesAndProwess(CardID host) : UntilEndOfTurnEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Stat>(batch.et))
            return false;
        Stat & stat = std::get<Stat>(batch.et);
        if (stat.type != Strikes && stat.type != Prowess)
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<Attack>(context))
            return false;
        Attack & attack = std::get<Attack>(context);
        if (attack.creatureType != Undead)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<Stat>(batch.et).stat++;
    }
};
struct ChillDouserEffect : public Effect {
    ChillDouserEffect(CardID host) : Effect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<CloseContext>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<Attack>(context))
            return false;
        Attack & attack = std::get<Attack>(context);
        if (attack.card != host || attack.outcome == Cancelled)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        Game & game = batch.scene.game;
        CompanyID id = std::get<Attack>(batch.getContext()).company;
        Company & company = *game.getCurrentSide().findCompany(id);
        company.effects.push_back(std::make_shared<ChillDouserStrikesAndProwess>(host));
        auto effect = std::make_shared<ChillDouserEffect>(*this);
        batch.effectQueue.front()->effect = effect;
        effect->activated = true;
    }
    bool remove(EffectBatch & batch) const {
        if (activated)
            return true;
        if (!std::holds_alternative<CloseContext>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<Encounter>(context))
            return false;
        if (std::get<Encounter>(context).card != host)
            return false;
        return true;
    }
    bool activated = false;
};
struct PlayChillDouser : public PlayHazardCreature {
    void insertEffects(Scene scene, const std::list<CardID> & args) const {
        Side & side = scene.game.getCurrentSide();
        side.effects.push_back(std::make_shared<ChillDouserEffect>(host));
    }
};
struct ChillDouser : public CardDefinition {
    ChillDouser() : CardDefinition(HazardCreature()) {
        HazardCreature & def = std::get<HazardCreature>(type);
        def.name.push_back("Chill Douser");
        def.playable = { RuinsLairs, ShadowHold };
        def.race = Undead;
        def.strikes = 3;
        def.prowess = 8;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayChillDouser>(), host);
    }
};

/* Ghouls */
struct Ghouls : public HazardCreatureDefinition {
    Ghouls() {
        HazardCreature & def = std::get<HazardCreature>(type);
        def.name.push_back("Ghouls");
        def.playable = { ShadowLands, DarkDomain, ShadowHold, DarkHold };
        def.race = Undead;
        def.strikes = 7;
        def.prowess = 5;
    }
};

/* Stirring Bones */
struct StirringBones : public HazardCreatureDefinition {
    StirringBones() {
        HazardCreature & def = std::get<HazardCreature>(type);
        def.name.push_back("Stirring Bones");
        def.playable = { Wilderness, Wilderness, ShadowLands, DarkDomain, RuinsLairs, ShadowHold };
        def.race = Undead;
        def.strikes = 2;
        def.prowess = 9;
    }
};

/* Wisp of Pale Sheen */
struct WispOfPaleSheenEffect : public AttackEffect {
    WispOfPaleSheenEffect(CardID host) : AttackEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<CloseContext>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<Strike>(context))
            return false;
        Strike & strike = std::get<Strike>(context);
        if (strike.card != host)
            return false;
        if (strike.outcome == Cancelled)
            return false;
        Game & game = batch.scene.game;
        if (game.getCardState(strike.target) != Untapped)
            return false;
        CompanyID id = game.getActiveCompany();
        Scene & scene = batch.scene;
        int creatureProwess = scene.getCreatureStat(Prowess, host, Undead, strike.creatureBaseProwess);
        if (scene.getMind(strike.target, id) > creatureProwess)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        batch.scene.tap(std::get<Strike>(batch.getContext()).target);
    }
};
struct PlayWispOfPaleSheen : public PlayHazardCreature {
    void insertEffects(Scene scene, const std::list<CardID> & args) const {
        Side & side = scene.game.getCurrentSide();
        side.effects.push_back(std::make_shared<WispOfPaleSheenEffect>(host));
    }
};
struct WispOfPaleSheen : public CardDefinition {
    WispOfPaleSheen() : CardDefinition(HazardCreature()) {
        HazardCreature & def = std::get<HazardCreature>(type);
        def.name.push_back("Wisp of Pale Sheen");
        def.playable = { Wilderness, ShadowLands, DarkDomain, CoastalSeas, RuinsLairs, ShadowHold };
        def.race = Undead;
        def.strikes = 1;
        def.prowess = 6;
        def.attackerChoosesDefendingCharacters = true;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayWispOfPaleSheen>(), host);
    }
};

struct ActionSignature {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
    }
};

struct EffectSignature {
    bool conditions(EffectBatch & batch) const {
    }
    void activate(EffectBatch & batch) const {
    }
};

void addOrcDeckDefinitions(GameSettings & settings) {
    settings.addCardDefinition(std::make_unique<mele::BaradDur>());
    settings.addCardDefinition(std::make_unique<mele::CavesOfUlund>());
    settings.addCardDefinition(std::make_unique<mele::DeadMarshes>());
    settings.addCardDefinition(std::make_unique<mele::GoblinGate>());
    settings.addCardDefinition(std::make_unique<mele::Moria>());
    settings.addCardDefinition(std::make_unique<mele::MountDoom>());
    settings.addCardDefinition(std::make_unique<mele::MountGram>());
    settings.addCardDefinition(std::make_unique<mele::MountGundabad>());
    settings.addCardDefinition(std::make_unique<mele::SarnGoriwing>());
    settings.addCardDefinition(std::make_unique<mele::ShelobsLair>());
    settings.addCardDefinition(std::make_unique<mele::HoarmurathTheRingwraith>());
    settings.addCardDefinition(std::make_unique<mele::Gorbag>());
    settings.addCardDefinition(std::make_unique<mele::Grishnakh>());
    settings.addCardDefinition(std::make_unique<mele::IllFavouredFellow>());
    settings.addCardDefinition(std::make_unique<mele::Lugdush>());
    settings.addCardDefinition(std::make_unique<mele::OrcTracker>());
    settings.addCardDefinition(std::make_unique<mele::SlySoutherner>());
    settings.addCardDefinition(std::make_unique<mele::Ufthak>());
    settings.addCardDefinition(std::make_unique<mele::GreatLordOfGoblinGate>());
    settings.addCardDefinition(std::make_unique<mele::Stinker>());
    settings.addCardDefinition(std::make_unique<mele::WarWolf>());
    settings.addCardDefinition(std::make_unique<mele::OrcsOfMirkwood>());
    settings.addCardDefinition(std::make_unique<mele::OrcsOfMoria>());
    settings.addCardDefinition(std::make_unique<mele::SnagaHai>());
    settings.addCardDefinition(std::make_unique<mele::BlackMace>());
    settings.addCardDefinition(std::make_unique<mele::BlastingFire>());
    settings.addCardDefinition(std::make_unique<mele::BlazonOfTheEye>());
    settings.addCardDefinition(std::make_unique<mele::HighHelm>());
    settings.addCardDefinition(std::make_unique<mele::RecordsUnread>());
    settings.addCardDefinition(std::make_unique<mele::SableShield>());
    settings.addCardDefinition(std::make_unique<mele::ScrollOfIsildur>());
    settings.addCardDefinition(std::make_unique<mele::StrangeRations>());
    settings.addCardDefinition(std::make_unique<mele::TheArkenstone>());
    settings.addCardDefinition(std::make_unique<mele::VileFumes>());
    settings.addCardDefinition(std::make_unique<mele::ANicePlaceToHide>());
    settings.addCardDefinition(std::make_unique<mele::BadeToRule>());
    settings.addCardDefinition(std::make_unique<mele::CreptAlongCleverly>());
    settings.addCardDefinition(std::make_unique<mele::FellRider>());
    settings.addCardDefinition(std::make_unique<mele::OrcQuarrels>());
    settings.addCardDefinition(std::make_unique<mele::PiercingAllShadows>());
    settings.addCardDefinition(std::make_unique<mele::Sneakin>());
    settings.addCardDefinition(std::make_unique<mele::SuddenCall>());
    settings.addCardDefinition(std::make_unique<mele::VoicesOfMalice>());
    settings.addCardDefinition(std::make_unique<AnUnexpectedOutpost>());
    settings.addCardDefinition(std::make_unique<ChokingShadows>());
    settings.addCardDefinition(std::make_unique<DoorsOfNight>());
    settings.addCardDefinition(std::make_unique<ExhalationOfDecay>());
    settings.addCardDefinition(std::make_unique<FoolishWords>());
    settings.addCardDefinition(std::make_unique<PlagueOfWights>());
    settings.addCardDefinition(std::make_unique<PowerBuiltByWaiting>());
    settings.addCardDefinition(std::make_unique<StenchOfMordor>());
    settings.addCardDefinition(std::make_unique<TheMoonIsDead>());
    settings.addCardDefinition(std::make_unique<Twilight>());
    settings.addCardDefinition(std::make_unique<Ambusher>());
    settings.addCardDefinition(std::make_unique<BarrowWight>());
    settings.addCardDefinition(std::make_unique<ChillDouser>());
    settings.addCardDefinition(std::make_unique<Ghosts>());
    settings.addCardDefinition(std::make_unique<Ghouls>());
    settings.addCardDefinition(std::make_unique<StirringBones>());
    settings.addCardDefinition(std::make_unique<WispOfPaleSheen>());
}

void addOrcDeck(DeckDefinition & deck) {
    deck.deckType = MinionDeck;

    deck.playDeck = {
        {"A Nice Place to Hide*", 3},
        {"Bade to Rule*"},
        {"Crept Along Cleverly*", 2},
        {"Orc Quarrels*", 2},
        {"Sneakin'*"},
        {"War-wolf*", 3},
        {"Great Lord of Goblin-gate*"},
        {"Stinker*"},
        {"Snaga-hai*", 2},
        {"Orcs of Moria*"},
        {"Records Unread*", 3},
        {"Blasting Fire*", 3},
        {"Vile Fumes*", 3},
        {"High Helm*"},
        {"Sable Shield*"},
        {"The Arkenstone*"},
        {"Scroll of Isildur*"},
        {"An Unexpected Outpost", 2},
        {"Choking Shadows"},
        {"Doors of Night", 3},
        {"Exhalation of Decay"},
        {"Plague of Wights", 3},
        {"Power Built by Waiting"},
        {"The Moon Is Dead", 3},
        {"Twilight", 2},
        {"Barrow-wight", 2},
        {"Chill Douser", 3},
        {"Ghouls", 3},
        {"Stirring Bones", 3},
        {"Wisp of Pale Sheen", 3},
        {"Hoarmûrath the Ringwraith*", 2},
        {"Sly Southerner*", 3},
        {"Ill-favoured Fellow*", 2},
        {"Orc Tracker*", 2},
    };

    deck.locationDeck = {
        {"Minas Morgul*"},
        {"Carn Dûm*"},
        {"Barad-dûr*"},
        {"Caves of Ûlund*"},
        {"Dead Marshes*"},
        {"Goblin-gate*"},
        {"Moria*"},
        {"Mount Doom*"},
        {"Mount Gram*"},
        {"Mount Gundabad*"},
        {"Sarn Goriwing*"},
        {"Shelob's Lair*"},
    };

    deck.sideboard = {
        {"Crept Along Cleverly*"},
        {"Fell Rider*"},
        {"Orc Quarrels*"},
        {"Piercing All Shadows*", 2},
        {"Sudden Call*"},
        {"Voices of Malice*", 2},
        {"Orcs of Mirkwood*"},
        {"Black Mace*", 2},
        {"Foolish Words", 2},
        {"Stench of Mordor"},
        {"Twilight"},
        {"Ambusher", 3},
        {"Ghosts", 2},
    };

    deck.startingCompany = {
        "Dol Guldur*",
        {
            {"Gorbag*", "Blazon of the Eye*"},
            {"Lugdush*"},
            {"Ufthak*", "Strange Rations*"},
            {"Grishnákh*"},
            {"Ill-favoured Fellow*"},
        },
    };
}
