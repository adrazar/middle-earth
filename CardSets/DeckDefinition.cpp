#include "DeckDefinition.h"
#include "DwarfDeck.h"
#include "OrcDeck.h"

std::unordered_map<std::string, DeckDefinition> loadDeckDefinitions() {
    // Built-in decks
    std::unordered_map<std::string, DeckDefinition> decks;
    addDwarfDeck(decks["Dwarven Quest"]);
    addOrcDeck(decks["Stealthy Tribe"]);

    // Can load decks from disk here

    return decks;
}
