#pragma once
#include "DeckDefinition.h"
#include "../Middle-Earth/GameSettings.h"

void addOrcDeckDefinitions(GameSettings & settings);
void addOrcDeck(DeckDefinition & deck);
