#include "BoostServerApi.h"

#include <boost/asio/yield.hpp>
#include <boost/asio/error.hpp>

#include <charconv>

namespace {
const std::size_t headerSize = 8;
const int base = 16;

void createMsgHeader(std::string & header, std::size_t msgSize) {
    header.clear();
    header.resize(headerSize);
    auto * headerAddress = &(*header.begin());
    std::to_chars(headerAddress, headerAddress + headerSize, long(msgSize), base);
}

int readMsgHeader(const std::string & header) {
    return stol(header, nullptr, base);
}

std::error_code toStdErrorCode(const boost::system::error_code & ec) {
    return static_cast<std::error_code>(ec);
}
} // namespace


BoostServerApi::BoostServerApi(boost::asio::io_context & ioContext)
    : ioContext_(ioContext)
    , socket_(ioContext)
{ ; }

BoostServerApi::PendingRequest::PendingRequest(std::string && msg, SendHandler && handler)
    : msg(std::move(msg))
    , handler(std::move(handler))
{ ; }

void BoostServerApi::connect(const std::string & serverHost, uint16_t port, ConnectHandler && connectHandler) {
    boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string(serverHost), port);
    auto handler = [handler = std::move(connectHandler)](const boost::system::error_code & ec) {
        handler(static_cast<std::error_code>(ec));
    };
    socket_.async_connect(endpoint, std::move(handler));
}

void BoostServerApi::disconnect() {
    socket_.shutdown(boost::asio::socket_base::shutdown_both);
    socket_.close();
}

void BoostServerApi::startReceiveLoop(ReceiveHandler && receiveHandler) {
    handler_ = std::move(receiveHandler);
    receiveMessage(boost::system::error_code());
}

void BoostServerApi::send(std::string && msg, SendHandler && sendHandler) {
    bool noSendInProgress = sendQueue_.empty();

    std::string header;
    createMsgHeader(header, msg.size());
    sendQueue_.emplace(std::move(header), nullptr);
    sendQueue_.emplace(std::move(msg), std::move(sendHandler));

    if (noSendInProgress)
        sendNextQueuedMessage();
}

bool BoostServerApi::isConnectionLostError(const std::error_code & ec) {
    return ec == toStdErrorCode(boost::asio::error::eof) ||
        ec == toStdErrorCode(boost::asio::error::connection_reset);
}

bool BoostServerApi::isShuttingDownError(const std::error_code & ec) {
    return ec == toStdErrorCode(boost::asio::error::operation_aborted);
}

void BoostServerApi::sendNextQueuedMessage() {
    const auto & request = sendQueue_.front();

    auto writeHandler = [this, handler = std::move(request.handler)](const boost::system::error_code & ec, std::size_t size) {
        (void)size;
        if (handler)
            handler(static_cast<std::error_code>(ec));

        sendQueue_.pop();
        if (!sendQueue_.empty())
            sendNextQueuedMessage();
    };

    boost::asio::async_write(socket_, boost::asio::buffer(request.msg), std::move(writeHandler));
}

void BoostServerApi::receiveMessage(const boost::system::error_code & ec, boost::asio::coroutine coro) {
    if (ec) {
        handler_(static_cast<std::error_code>(ec), receiveBuffer_);
        return;
    }

    auto read = [this](boost::asio::coroutine coro, std::size_t size) {
        receiveBuffer_.resize(size);
        boost::asio::async_read(socket_, boost::asio::buffer(receiveBuffer_),
            [this, coro](const boost::system::error_code & ec, std::size_t bytesReceived) {
                (void)bytesReceived;
                receiveMessage(ec, coro);
            });
    };

    reenter(coro) {
        for (;;) {
            yield read(coro, headerSize);
            yield read(coro, readMsgHeader(receiveBuffer_));

            handler_(static_cast<std::error_code>(ec), receiveBuffer_);
        }
    }
}

