#pragma once

#include <boost/asio.hpp>
#include <functional>
#include <queue>
#include <string>

class BoostServerApi {
public:
    BoostServerApi(boost::asio::io_context & ioContext);

    using ConnectHandler = std::function<void(const std::error_code & ec)>;
    using SendHandler = std::function<void(const std::error_code & ec)>;
    using ReceiveHandler = std::function<void(const std::error_code & ec, const std::string & msg)>;

    void connect(const std::string & serverHost, uint16_t port, ConnectHandler && connectHandler);
    void disconnect();
    void startReceiveLoop(ReceiveHandler && receiveHandler);
    void send(std::string && msg, SendHandler && sendHandler);

    bool isConnectionLostError(const std::error_code & ec);
    bool isShuttingDownError(const std::error_code & ec);

private:

    struct PendingRequest {
        PendingRequest(std::string && msg, SendHandler && handler);
        const std::string msg;
        SendHandler handler;
    };

    boost::asio::io_context & ioContext_;
    boost::asio::ip::tcp::socket socket_;
    std::queue<PendingRequest> sendQueue_;
    std::string receiveBuffer_;
    ReceiveHandler handler_;

    void sendNextQueuedMessage();
    void receiveMessage(const boost::system::error_code & ec, boost::asio::coroutine coro = {});
};
