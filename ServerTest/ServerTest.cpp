#include "Fixtures.h"
#include "JsonConverters.h"
#include "../Middle-Earth/CardTopology.h"
#include <windows.h>

void jsonHeader(nlohmann::json & request) {
    request["requestId"] = 0;
    request["stateId"] = 0;
    request["tableId"] = 0;
}

int findCard(const std::string & cardName, const nlohmann::json & json) {
    auto it = json.begin();
    size_t index = 0;
    while (it != json.end()) {
        auto it2 = it->find("name");
        if (it2 != it->end() && *it2 == cardName)
            return index;
        ++it;
        ++index;
    }
    return -1;
}

int findCard(CardAlias id, const nlohmann::json & json) {
    auto it = json.begin();
    size_t index = 0;
    while (it != json.end()) {
        auto it2 = it->find("id");
        if (it2 != it->end() && *it2 == id)
            return index;
        ++it;
        ++index;
    }
    return -1;
}

bool containsId(const nlohmann::json & jsonArray, CardAlias id) {
    return std::find(jsonArray.begin(), jsonArray.end(), id) != jsonArray.end();
}

TEST_F(ServerFixture, FetchDwarfCompany) {
    CardSyncRequest request;
    request.sides.push_back(0);
    request.sides.back().companies = "all";

    nlohmann::json jsonRequest;
    jsonHeader(jsonRequest);
    jsonRequest["type"] = "syncCards";
    toJson(request, jsonRequest);

    Client & client = clients.back();
    client.send(jsonRequest);
    poll();

    std::string msg;
    client.api.read(msg);
    auto jsonResponse = nlohmann::json::parse(msg);

    ASSERT_EQ(jsonResponse["type"], "syncCards");
    auto it = jsonResponse.find("sides");
    ASSERT_TRUE(it != jsonResponse.end());
    ASSERT_EQ(it->size(), 1);
    auto & jsonSide = it->back();
    it = jsonSide.find("player");
    ASSERT_TRUE(it != jsonSide.end());
    EXPECT_EQ(*it, 0);
    it = jsonSide.find("companies");
    ASSERT_TRUE(it != jsonSide.end());
    auto & jsonCompanies = *it;
    ASSERT_TRUE(jsonCompanies.is_array());
    EXPECT_EQ(jsonCompanies.size(), 1);
    auto & jsonCompany = jsonCompanies.back();
    it = jsonCompany.find("characters");
    ASSERT_TRUE(it != jsonCompany.end());
    auto & jsonCharacters = *it;
    ASSERT_TRUE(jsonCharacters.is_array());
    EXPECT_EQ(jsonCharacters.size(), 5);

    it = jsonResponse.find("cardData");
    ASSERT_TRUE(it != jsonResponse.end());
    auto & jsonCardData = *it;
    int index = findCard("Thorin II", jsonCardData);
    ASSERT_NE(index, -1);
    thorin = jsonCardData[index]["id"];
    index = findCard("Óin", jsonCardData);
    ASSERT_NE(index, -1);
    oin = jsonCardData[index]["id"];
    index = findCard("Glóin", jsonCardData);
    ASSERT_NE(index, -1);
    gloin = jsonCardData[index]["id"];
    index = findCard("Kíli", jsonCardData);
    ASSERT_NE(index, -1);
    kili = jsonCardData[index]["id"];
    index = findCard("Dori", jsonCardData);
    ASSERT_NE(index, -1);
    dori = jsonCardData[index]["id"];

    // Thorin II
    {
        auto & jsonCharacter = jsonCharacters[findCard(thorin, jsonCharacters)];
        auto & jsonFollowers = jsonCharacter["followers"];
        ASSERT_EQ(jsonFollowers.size(), 2);
        EXPECT_EQ(std::set(jsonFollowers.begin(), jsonFollowers.end()), std::set({ kili, dori }));
        auto & jsonPossessions = jsonCharacter["possessions"];
        ASSERT_EQ(jsonPossessions.size(), 1);
        CardAlias cram = jsonPossessions.back();
        auto & jsonCard = jsonCardData[findCard(cram, jsonCardData)];
        EXPECT_EQ(jsonCard["name"], "Cram");
    }

    // Óin
    {
        auto & jsonCharacter = jsonCharacters[findCard(oin, jsonCharacters)];
        ASSERT_TRUE(jsonCharacter["followers"].empty());
        auto & jsonPossessions = jsonCharacter["possessions"];
        ASSERT_EQ(jsonPossessions.size(), 1);
        CardAlias cram = jsonPossessions.back();
        auto & jsonCard = jsonCardData[findCard(cram, jsonCardData)];
        EXPECT_EQ(jsonCard["name"], "Cram");
    }

    // Glóin
    {
        auto & jsonCharacter = jsonCharacters[findCard(gloin, jsonCharacters)];
        auto & jsonFollowers = jsonCharacter["followers"];
        ASSERT_EQ(jsonFollowers.size(), 1);
        EXPECT_EQ(CardAlias(jsonFollowers.back()), oin);
        EXPECT_TRUE(jsonCharacter["possessions"].empty());
    }

    // Kíli
    {
        auto & jsonCharacter = jsonCharacters[findCard(kili, jsonCharacters)];
        EXPECT_TRUE(jsonCharacter["followers"].empty());
        EXPECT_TRUE(jsonCharacter["possessions"].empty());
    }

    // Dori
    {
        auto & jsonCharacter = jsonCharacters[findCard(kili, jsonCharacters)];
        EXPECT_TRUE(jsonCharacter["followers"].empty());
        EXPECT_TRUE(jsonCharacter["possessions"].empty());
    }

    // Current location
    {
        CardAlias currentLocation = jsonCompany["currentLocation"];
        auto & jsonCard = jsonCardData[findCard(currentLocation, jsonCardData)];
        EXPECT_EQ(jsonCard["name"], "Rivendell");
    }
}

TEST_F(ServerFixture, FetchOrcCompany) {
    CardSyncRequest request;
    request.sides.push_back(1);
    request.sides.back().companies = "all";

    nlohmann::json jsonRequest;
    jsonHeader(jsonRequest);
    jsonRequest["type"] = "syncCards";
    toJson(request, jsonRequest);

    Client & client = clients.back();
    client.send(jsonRequest);
    poll();

    std::string msg;
    client.api.read(msg);
    auto jsonResponse = nlohmann::json::parse(msg);

    ASSERT_EQ(jsonResponse["type"], "syncCards");
    auto it = jsonResponse.find("sides");
    ASSERT_TRUE(it != jsonResponse.end());
    ASSERT_EQ(it->size(), 1);
    auto & jsonSide = it->back();
    it = jsonSide.find("player");
    ASSERT_TRUE(it != jsonSide.end());
    EXPECT_EQ(*it, 1);
    it = jsonSide.find("companies");
    ASSERT_TRUE(it != jsonSide.end());
    auto & jsonCompanies = *it;
    ASSERT_TRUE(jsonCompanies.is_array());
    EXPECT_EQ(jsonCompanies.size(), 2);
    size_t expectedSize = 4;
    for (auto & jsonCompany : jsonCompanies) {
        it = jsonCompany.find("characters");
        ASSERT_TRUE(it != jsonCompany.end());
        auto & jsonCharacters = *it;
        ASSERT_TRUE(jsonCharacters.is_array());
        EXPECT_EQ(jsonCharacters.size(), expectedSize);
        expectedSize -= 3;
    }

    it = jsonResponse.find("cardData");
    ASSERT_TRUE(it != jsonResponse.end());
    auto & jsonCardData = *it;
    int index = findCard("Gorbag", jsonCardData);
    ASSERT_NE(index, -1);
    gorbag = jsonCardData[index]["id"];
    index = findCard("Lugdush", jsonCardData);
    ASSERT_NE(index, -1);
    lugdush = jsonCardData[index]["id"];
    index = findCard("Grishnákh", jsonCardData);
    ASSERT_NE(index, -1);
    grishnakh = jsonCardData[index]["id"];
    index = findCard("Ufthak", jsonCardData);
    ASSERT_NE(index, -1);
    ufthak = jsonCardData[index]["id"];
    index = findCard("Ill-favoured Fellow", jsonCardData);
    ASSERT_NE(index, -1);
    illFavouredFellow = jsonCardData[index]["id"];
    auto & jsonCharacters = jsonCompanies[0]["characters"];

    // Gorbag
    {
        auto & jsonCharacter = jsonCharacters[findCard(gorbag, jsonCharacters)];
        auto & jsonFollowers = jsonCharacter["followers"];
        ASSERT_EQ(jsonFollowers.size(), 1);
        EXPECT_EQ(CardAlias(jsonFollowers.back()), grishnakh);
        auto & jsonPossessions = jsonCharacter["possessions"];
        ASSERT_EQ(jsonPossessions.size(), 1);
        CardAlias blazonOfTheEye = jsonPossessions.back();
        auto & jsonCard = jsonCardData[findCard(blazonOfTheEye, jsonCardData)];
        EXPECT_EQ(jsonCard["name"], "Blazon of the Eye");
    }

    // Lugdush
    {
        auto & jsonCharacter = jsonCharacters[findCard(lugdush, jsonCharacters)];
        EXPECT_TRUE(jsonCharacter["followers"].empty());
        EXPECT_TRUE(jsonCharacter["possessions"].empty());
    }

    // Grishnákh
    {
        auto & jsonCharacter = jsonCharacters[findCard(grishnakh, jsonCharacters)];
        EXPECT_TRUE(jsonCharacter["followers"].empty());
        EXPECT_TRUE(jsonCharacter["possessions"].empty());
    }

    // Ufthak
    {
        auto & jsonCharacter = jsonCharacters[findCard(ufthak, jsonCharacters)];
        EXPECT_TRUE(jsonCharacter["followers"].empty());
        auto & jsonPossessions = jsonCharacter["possessions"];
        ASSERT_EQ(jsonPossessions.size(), 1);
        CardAlias strangeRations = jsonPossessions.back();
        auto & jsonCard = jsonCardData[findCard(strangeRations, jsonCardData)];
        EXPECT_EQ(jsonCard["name"], "Strange Rations");
    }

    // Ill-favoured fellow
    {
        auto & jsonCharacters = jsonCompanies[1]["characters"];
        auto & jsonCharacter = jsonCharacters[findCard(illFavouredFellow, jsonCharacters)];
        EXPECT_TRUE(jsonCharacter["followers"].empty());
        EXPECT_TRUE(jsonCharacter["possessions"].empty());
    }

    // Current location
    {
        CardAlias currentLocation = jsonCompanies[0]["currentLocation"];
        EXPECT_EQ(jsonCompanies[1]["currentLocation"], currentLocation);
        auto & jsonCard = jsonCardData[findCard(currentLocation, jsonCardData)];
        EXPECT_EQ(jsonCard["name"], "Dol Guldur");
    }
}

TEST_F(ServerFixture, FetchHand) {
    SetConsoleOutputCP(65001); // Temporary - remove this along with <windows.h>


    CardSyncRequest request;
    request.sides.push_back(0);
    request.sides.back().decks = { Hand, Factions };

    nlohmann::json jsonRequest;
    jsonHeader(jsonRequest);
    jsonRequest["type"] = "syncCards";
    toJson(request, jsonRequest);

    Client & client = clients.back();
    client.send(jsonRequest);
    poll();

    std::string msg;
    client.api.read(msg);
    auto jsonResponse = nlohmann::json::parse(msg);

    ASSERT_EQ(jsonResponse["type"], "syncCards");
    auto it = jsonResponse.find("sides");
    ASSERT_TRUE(it != jsonResponse.end());
    ASSERT_EQ(it->size(), 1);
    auto & jsonSide = it->back();
    it = jsonSide.find("player");
    ASSERT_TRUE(it != jsonSide.end());
    EXPECT_EQ(*it, 0);


    it = jsonResponse.find("cardData");
    ASSERT_TRUE(it != jsonResponse.end());
    auto & jsonCardData = *it;

    it = jsonSide.find("decks");
    ASSERT_TRUE(it != jsonSide.end());
    auto & jsonDecks = *it;
    ASSERT_EQ(jsonDecks.size(), 2);
    {
        auto & jsonDeck = jsonDecks[0];
        it = jsonDeck.find("deck");
        ASSERT_TRUE(it != jsonDeck.end());
        ASSERT_EQ(*it, "hand");
        it = jsonDeck.find("cards");
        ASSERT_TRUE(it != jsonDeck.end());
        auto & jsonHand = *it;
        EXPECT_EQ(jsonHand.size(), 8);
        std::vector<std::string> hand = {
            "Hundreds of Butterflies",
            "Balin",
            "Wake of War",
            "Pallando",
            "Hauberk of Bright Mail",
            "Neeker-breekers",
            "Cave Worm",
            "Fellowship",
        };
        for (const std::string & cardName : hand) {
            int index = findCard(cardName, jsonCardData);
            ASSERT_NE(index, -1);
            EXPECT_TRUE(containsId(jsonHand, jsonCardData[index]["id"]));
        }
    }

    auto & jsonDeck = jsonDecks[1];
    it = jsonDeck.find("deck");
    ASSERT_TRUE(it != jsonDeck.end());
    ASSERT_EQ(*it, "factions");
    it = jsonDeck.find("cards");
    ASSERT_TRUE(it != jsonDeck.end());
    auto & jsonFactions = *it;
    EXPECT_EQ(jsonFactions.size(), 1);
    int index = findCard("Iron Hill Dwarves", jsonCardData);
    ASSERT_NE(index, -1);
    EXPECT_TRUE(containsId(jsonFactions, jsonCardData[index]["id"]));
}
