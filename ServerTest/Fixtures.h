#pragma once
#include "../CardSets/CardSetLoader.h"
#include "../Middle-Earth/GameConfigurer.h"
#include "../Server/Server.h"
#include "../ServerApi/BoostServerApi.h"
#include "../Tests/TestUtilities.h"
#include <gtest/gtest.h>
#include <deque>
#include <ostream>

// Used to suppress server log during testing
class NullStream : public std::ostream {
public:
    NullStream() : std::ostream(nullptr) {}
    NullStream(const NullStream &) : std::ostream(nullptr) {}
};

template <class T>
const NullStream & operator<<(NullStream && os, const T & value) {
    return os;
}

struct Client {
    Client() : api(ioContext) {}

    void connect() {
        api.connect("127.0.0.1", 8289, [this](const std::error_code & ec) { (void)ec; });
    }

    void sendAuthentication(const std::string & userName) {
        nlohmann::json authentication = {
            {"type", "authenticate"},
            {"userName", userName},
        };
        std::string msg = authentication.dump();
        api.send(std::move(msg), [](const std::error_code & ec) { (void)ec; });
    }

    void send(nlohmann::json & request) {
        ioContext.restart();  // because ioContext has no idle handlers
        api.send(
            std::move(request.dump()),
            [](const std::error_code & ec) {
                (void)ec;
            }
        );
        poll();
    }

    void poll() {
        ioContext.poll();
    }

    boost::asio::io_context ioContext;
    BoostServerApi api;
};

class ServerFixture : public ::testing::Test {
public:
    ServerFixture()
        : server_(ioContext_, os_)
    {
        server_.start();
        server::TableID tableId = server_.createTable();
        addClient("Tony");
        sit(tableId);

        MetaGame & metaGame = server_.tables[tableId]->metaGame;

        loadCardSet(metaGame.settings, CardSet::CoreCards);
        loadCardSet(metaGame.settings, CardSet::DwarfDeck);
        loadCardSet(metaGame.settings, CardSet::OrcDeck);
        auto decks = loadDeckDefinitions();
        GameConfigurer configurer(metaGame);
        Game & game = metaGame.gameStates.back();


        // Dwarf deck

        //PlayerConfigurer dwarfPlayer = configurer.addPlayer();
        //dwarfPlayer.loadDeck(decks["Dwarven Quest"]);
        PlayerConfigurer dwarfPlayer(configurer, 0);
        thorin = getCardID(game, 0, "Thorin II");
        gloin = getCardID(game, 0, "Glóin");
        oin = getCardID(game, 0, "Óin");
        kili = getCardID(game, 0, "Kíli");
        dori = getCardID(game, 0, "Dori");

        game.cards[kili].parent = thorin;
        game.cards[dori].parent = thorin;
        game.cards[oin].parent = gloin;
        dwarfPlayer.drawCard("Hundreds of Butterflies");
        dwarfPlayer.drawCard("Balin");
        dwarfPlayer.drawCard("Wake of War");
        dwarfPlayer.drawCard("Pallando");
        dwarfPlayer.drawCard("Hauberk of Bright Mail");
        dwarfPlayer.drawCard("Neeker-breekers");
        dwarfPlayer.drawCard("Cave Worm");
        dwarfPlayer.drawCard("Fellowship");
        dwarfPlayer.addFaction(getCardID(game, 0, "Iron Hill Dwarves"));


        // Orc deck

        //PlayerConfigurer orcPlayer = configurer.addPlayer();
        //orcPlayer.loadDeck(decks["Stealthy Tribe"]);
        PlayerConfigurer orcPlayer(configurer, 1);

        gorbag = getCardID(game, 1, "Gorbag*");
        lugdush = getCardID(game, 1, "Lugdush*");
        ufthak = getCardID(game, 1, "Ufthak*");
        grishnakh = getCardID(game, 1, "Grishnákh*");
        illFavouredFellow = getCardID(game, 1, "Ill-favoured Fellow*", PlayDeck);

        game.cards[grishnakh].parent = gorbag;
        orcPlayer.addCompany(getCardID(game, 1, "Dol Guldur*"));
        orcPlayer.addCharacterToCompany(1, illFavouredFellow);
        orcPlayer.drawCard("Snaga-hai*");
        orcPlayer.drawCard("Chill Douser");
        orcPlayer.drawCard("Ghouls");
        orcPlayer.drawCard("Sneakin'*");
        orcPlayer.drawCard("Orc Tracker*");
        orcPlayer.drawCard("Exhalation of Decay");
        orcPlayer.drawCard("Records Unread*");
        orcPlayer.drawCard("War-wolf*");
    }

    void addClient(const std::string & userName) {
        clients.emplace_back();
        Client & client = clients.back();
        client.connect();
        poll();
        client.sendAuthentication(userName);
        client.poll();
        poll();
    }

    void sit(server::TableID tableId) {
        nlohmann::json sitRequest = {
            { "type", "sit" },
            { "tableId", tableId },
        };
        clients.back().send(sitRequest);
        poll();
        std::string msg;
        clients.back().api.read(msg);
    }

    void poll() {
        ioContext_.poll();
    }

    CardID thorin, gloin, oin, kili, dori;
    CardID gorbag, lugdush, ufthak, grishnakh, illFavouredFellow;
    MetaGame metaGame;
    std::deque<Client> clients;

private:
    NullStream os_;
    boost::asio::io_context ioContext_;
    Server server_;
};
