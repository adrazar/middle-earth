#pragma once
#include "ContextInterface.h"
#include <memory>
#include <vector>

struct EffectBatch {
    explicit EffectBatch(Scene scene, EffectTrigger & et)
        : scene(scene)
        , et(et)
    { ; }
    void traverseList(EffectList & effects);
    void collect(int player = -1, CompanyID id = -1);
    void collectSide(Side & side);
    void collectAll(int player = -1);
    void execute();
    void execute(CardID card);
    ContextType & getContext();

    Scene scene;
    EffectTrigger & et;
    std::list<EffectList::iterator> effectQueue;
};

struct EffectTriggerListener {
    virtual void triggerHandler(const EffectBatch & batch) = 0;
    virtual ~EffectTriggerListener() {}
};

// By executing the EffectBatches through here instead of directly,
// the associated EffectTriggers can be used as application events.
// The broadcast function executes the EffectBatch first and then
// notifies all listeners using the resulting EffectTrigger.
struct EffectProcessor {
    void registerListener(std::unique_ptr<EffectTriggerListener> && listener);
    void broadcast(EffectBatch & batch);

    std::vector<std::unique_ptr<EffectTriggerListener>> listeners;
};
