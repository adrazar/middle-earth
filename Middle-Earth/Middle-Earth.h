#pragma once
#include "CardAbilities.h"
#include "CardTopology.h"
#include "CardTypes.h"
#include "Context.h"
#include "GameSettings.h"
#include "ManagedPtrs.h"
#include <list>
#include <string>
#include <unordered_map>
#include <vector>

/*
 * The GUI will mainly concern itself with the Card structure for displaying the cards.
 * It will refer to available Actions and pass the options to the user.
 *
 * An event-log should be convenient for allowing the opponent to interfere asynchronously.
 * It's also strictly necessary for carrying out the effects of certain cards correctly.
 *
 */

enum Phase { UntapPhase, OrganizationPhase, LongEventPhase, MovementHazardPhase, SitePhase, EndOfTurnPhase, NextTurn };
enum Part { Beginning, Middle, End };

struct TurnSequence {
    TurnSequence();  // { UntapPhase, Beginning }
    explicit TurnSequence(Phase phase, Part part);
    void setPhase(Phase phase, Part part);
    bool operator>(const TurnSequence & turn) const;
    bool operator==(const TurnSequence & turn) const;
    bool operator!=(const TurnSequence & turn) const;

    Phase phase;
    Part part;
    std::vector<int> remainingPlayers;
    int currentPlayerIndex = 0;
    int hazardPlayerIndex = 1;
    int round = 1;    // Opponent may not be influenced on the first round;
};


using CardAlias = CardID;

struct CardAliasTable {
    void add(CardID card);
    void remove(CardID card);

    CardAlias nextRef = 0;
    std::unordered_map<CardID, CardAlias> toAlias;
    std::unordered_map<CardAlias, CardID> fromAlias;
};


struct Company : public CompanyCards {
    std::vector<RegionNumber> regions;
    EffectList effects;
};

struct Side : public SideCards<Company> {
    Side(int player) : player(player) {}
    std::list<Company>::iterator findCompany(CardID card, DeckName name);
    std::list<Company>::iterator findCompany(CompanyID id);
    SiteType havenType();

    const int player;
    CompanyID nextID = 0;
    DeckType deckType = HeroDeck;
    int revealedAvatar = -1;    // Definition-index of revealed Wizard or Ringwraith
    int deckExhaustions = 0;
    std::list<CardID> selectedCards;
    EffectList effects;
    ActionList actions;
    UndoPtr<CardAliasTable> aliasTable;
};

struct Game {
    explicit Game(GameSettings & settings);

    Side & getCurrentSide();
    Side & getHazardSide();
    Side & getSide(CardID card);
    ContextType & getContext();
    CardState getCardState(CardID card);
    CardID getParent(CardID card);
    CardID getRoot(CardID card);               // The final parent in the chain
    DeckName getDeck(CardID card);
    PathSymbol getRegionType(RegionNumber region);
    CompanyID getCompanyID(CardID card);       // Characters and possessions only
    CardType & getCardType(CardID card);
    Character & getCharacter(CardID card);
    Location & getLocation(CardID card);
    Region & getRegion(RegionNumber region);
    Item & getItem(CardID card);
    CardID getEquippedItem(CardID card, EquipmentType type);
    Race getRace(CardID card);
    Race getAvatar(DeckType deckType);
    CardID avatarInPlay(Side & side);
    CardCategory getCardCategory(CardID card);
    CompanyID getActiveCompany();
    CardID getSite();
    CardID findSiteID(Side & side, int dataIndex);
    SitePath & getSitePath(CardID origin, CardID destination);
    Movement & getMovement();
    BottomContext & getBottomContext();

    bool hasPossession(CardID target, const std::string & possessionName);
    bool uniquePossession(CardID id, const std::vector<std::string> & name);
    bool uniqueInDeck(CardID id, const std::vector<std::string> & name, DeckName deckName);
    bool uniqueCharacter(CardID id, const std::vector<std::string> & name);
    bool uniqueItem(CardID id, const std::vector<std::string> & name);
    bool uniqueAlly(CardID id, const std::vector<std::string> & name);
    bool uniqueFaction(CardID id, const std::vector<std::string> & name);
    bool isManifestation(CardID cardA, CardID cardB);
    bool containsCardCategory(const std::vector<CardID> & deck, CardCategory category);
    bool adjacentSites(CardID source, CardID destination);
    bool siteCardInPlay(CardID location);
    bool resourceEventInPlay(const std::string & name, EventType type);
    bool hazardEventInPlay(const std::string & name, EventType type);
    bool hazardEventInPlay(CardID id);
    bool characterInCompany(CardID card, CompanyID id = -1, int player = -1);
    bool raceInCompany(Company & company, Race race);
    bool possessedByRingwraith(CardID possession);
    bool hasEffect(CardID card, int player = -1, CompanyID id = -1);
    bool duplicateEffect(CardID card, int player = -1, CompanyID id = -1);
    bool duplicatePossession(CardID target, CardID possession);
    int duplicatePossessionsInCompany(Company & company, CardID card);
    bool deckExhaustionLimitReached();

    void addSide();
    void setHostingDeck(CardID id, int recipient, DeckName destination);


    std::vector<Card> cards;
    std::vector<Side> sides;
    int currentPlayer = 0;
    int hazardPlayer = 1;
    int councilCalled = -1;                  // The player calling the Council/Audience
    TurnSequence turnSequence;
    ContextStack contextStack;
    CardID characterBroughtIntoPlay = -1;    // Returned to the hand if short of GI (MELE, p.20, top)
    std::set<CardID> receivedCorruptionCard; // Only one corruption card per character per turn (MELE, p.35, bottom)
    bool influencedOpponent = false;         // True if an influence attempt against the opponent has been made
    std::vector<CardID> errors;              // Used to prevent cards from being played without effect
    EffectList effects;
    GameSettings & settings;
    int recordSize = 0;                      // The number of steps taken to arrive at this state
};

bool sitePathMatch(PathSymbol key, const SitePath & sitePath, const std::vector<KeyedToType> & playable);
bool regionMatch(RegionNumber key, const std::vector<RegionNumber> & regions, const std::vector<KeyedToType> & playable);
bool siteMatch(SiteType key, SiteType siteType, const std::vector<KeyedToType> & playable);
bool nameMatch(const Location & location, const std::vector<KeyedToType> & playable);
