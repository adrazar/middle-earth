#pragma once
#include "ContextTypes.h"


struct Scene;

struct Context {
    Context(ContextType context, int parent = NoParent)
        : context(context)
        , parent(parent)
    { ; }
    ContextType context;
    int parent;
    bool autoProceed = false;
};

struct ContextStack : public std::vector<std::shared_ptr<Context>> {
    ContextStack() = default;
    ContextStack(const ContextStack & contextStack);
    ContextStack(ContextStack && contextStack) = default;
};

struct ProceedContext {
    ProceedContext(Scene scene, int player);
    int operator()(RollDice & rollDice);
    int operator()(BodyCheck & bodyCheck);
    int operator()(Strike & strike);
    int operator()(AssignStrikes & assign);
    int operator()(Attack & attack);
    int operator()(Encounter & encounter);
    int operator()(CorruptionCheck & corruptionCheck);
    int operator()(ResolveCorruptionChecks & resolve);
    int operator()(StoreCard & storeCard);
    int operator()(RemoveCorruptionCard & remove);
    int operator()(InfluenceAttempt & influence);
    int operator()(RingTest & ringTest);
    int operator()(SpecialTest & specialTest);
    int operator()(Movement & movement);
    int operator()(DrawCards & drawCards);
    int operator()(Arrival & arrival);
    int operator()(Explore & explore);
    int operator()(SelectCompany & selectCompany);
    int operator()(TransferCards & transfer);
    int operator()(TransferItems & transfer);
    int operator()(ReturnToHandSize & handSize);
    int operator()(ExhaustDeck & exhaust);
    int operator()(OrganizeCompanies & organize);
    int operator()(DiscardSubset & discard);
    int operator()(TapCharacters & tap);
    int operator()(ChainOfEffects & chain);
    int operator()(BottomContext & bottomContext);

    std::unique_ptr<Scene> scene;
    int player;
};

std::string getContextName(ContextType & context);
