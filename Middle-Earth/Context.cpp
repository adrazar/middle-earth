#include "Context.h"
#include "ContextInterface.h"
#include "ErrorCodes.h"

ContextStack::ContextStack(const ContextStack & contextStack) {
    for (auto context : contextStack)
        push_back(std::make_shared<Context>(*context));
}

std::string getContextName(ContextType & context) {
    return std::visit(NameGetter{}, context).front();
}

ProceedContext::ProceedContext(Scene scene, int player)
    : scene(std::make_unique<Scene>(scene))
    , player(player)
{ ; }

int ProceedContext::operator()(RollDice & rollDice) {
    // Any player can proceed
    if (rollDice.selectedRoll < 0)
        return RollNotSelected;
    Scene parent(scene->game, scene->context->parent);
    Game & game = scene->game;
    game.contextStack.pop_back();
    int result = rollDice.rolls[rollDice.selectedRoll];
    EffectTrigger roll = Roll(result);
    ContextType & contextType = parent.context->context;
    int targetPlayer = -1;
    CompanyID id = -1;
    if (std::holds_alternative<Strike>(contextType)) {
        Strike & strike = std::get<Strike>(contextType);
        strike.roll = result;
        targetPlayer = game.cards[strike.target].owner;
        id = game.getCompanyID(strike.target);
    }
    else if (std::holds_alternative<CorruptionCheck>(contextType)) {
        CorruptionCheck & corruption = std::get<CorruptionCheck>(contextType);
        corruption.roll = result;
        targetPlayer = game.cards[corruption.targetCharacter].owner;
        id = game.getCompanyID(corruption.targetCharacter);
    }
    else if (std::holds_alternative<BodyCheck>(contextType)) {
        BodyCheck & body = std::get<BodyCheck>(contextType);
        body.roll = result;
        targetPlayer = game.cards[body.target].owner;
        id = body.defendingCompany;
    }
    else if (std::holds_alternative<InfluenceAttempt>(contextType)) {
        InfluenceAttempt & influence = std::get<InfluenceAttempt>(contextType);
        if (influence.roll == 0)
            influence.roll = result;
        else
            influence.roll -= result;
        targetPlayer = game.cards[influence.influencingCharacter].owner;
        id = game.getCompanyID(influence.influencingCharacter);
    }
    else if (std::holds_alternative<RemoveCorruptionCard>(contextType)) {
        std::get<RemoveCorruptionCard>(contextType).roll = result;
    }
    else if (std::holds_alternative<SpecialTest>(contextType)) {
        std::get<SpecialTest>(contextType).roll = result;
    }
    parent.context->autoProceed = true;
    parent.applyEffects(roll, targetPlayer, id);
    return Success;
}

int ProceedContext::operator()(BodyCheck & bodyCheck) {
    Game & game = scene->game;
    auto parent = game.contextStack[scene->context->parent];
    Strike & strike = std::get<Strike>(parent->context);
    switch (bodyCheck.state) {
    case BodyCheck::Start:
        if (player != game.currentPlayer)
            return WrongPlayer;
        if (strike.target == bodyCheck.target)
            player = game.hazardPlayer;
        scene->setupRollDice(player);
        bodyCheck.state = BodyCheck::Resolve;
        break;
    case BodyCheck::Resolve:
        bodyCheck.state = BodyCheck::Close;
        {
            Outcome outcome = (bodyCheck.body < bodyCheck.roll + bodyCheck.modifier) ? Failed : Successful;
            if (strike.target == bodyCheck.target) {
                if (outcome == Successful)
                    scene->wound(strike.target);
                else
                    scene->eliminate(strike.target);
            }
            else if (outcome == Failed)
                strike.outcome = Failed;
        }
    case BodyCheck::Close:
        if (scene->closeContext(game.currentPlayer))
            parent->autoProceed = true;
        break;
    }
    return Success;
}

int ProceedContext::operator()(Strike & strike) {
    Game & game = scene->game;
    int result, targetProwess, creatureProwess, creatureBody;
    switch (strike.state) {

    case Strike::PlayHazards:
        if (player != game.hazardPlayer)
            return WrongPlayer;
        if (strike.excessStrikes == 0)
            strike.state = Strike::TapCharacter;
        else if (!strike.finalTarget)
            strike.state = Strike::ExcessStrikes;
        else {
            strike.strikesSpent = strike.excessStrikes;
            strike.excessStrikes = 0;
            strike.state = Strike::TapCharacter;
        }
        break;

    case Strike::ExcessStrikes:
        if (player != game.hazardPlayer)
            return WrongPlayer;
        strike.state = Strike::TapCharacter;
        break;

    case Strike::TapCharacter:
        if (player != game.currentPlayer)
            return WrongPlayer;
        switch (game.getCardState(strike.target)) {
        case Untapped:
            strike.targetProwessModifier -= 3;
            break;
        case Tapped:
            strike.targetProwessModifier -= 1;
            break;
        case Wounded:
            strike.targetProwessModifier -= 2;
            break;
        }
        strike.state = Strike::PlayResources;
        break;

    case Strike::PlayResources:
        if (player != game.currentPlayer)
            return WrongPlayer;
        targetProwess = scene->getTargetProwess(strike);
        creatureProwess = scene->getCreatureStat(Prowess, strike.card, strike.creatureType, strike.creatureBaseProwess);
        if (targetProwess - strike.strikesSpent + 2 > creatureProwess)
            strike.roll = 2;
        else
            scene->setupRollDice(player);
        scene->context->autoProceed = true;
        strike.state = Strike::ResolveStrike;
        break;

    case Strike::ResolveStrike:
        result = scene->strikeResult(strike);
        if (result > 0) {
            creatureBody = scene->getCreatureStat(Body, strike.card, strike.creatureType, strike.creatureBaseBody);
            if (creatureBody < 2)
                strike.outcome = Failed;
            else {
                scene->setupBodyCheck(strike.card, creatureBody);
            }
        }
        else if (result < 0) {
            strike.outcome = Successful;
            if (strike.detainment)
                scene->tap(strike.target);
            else
                scene->setupBodyCheck(strike.target, scene->getTargetBody(strike));
        }
        else {
            strike.outcome = Ineffectual;
        }
        strike.state = Strike::CloseStrike;
        break;

    case Strike::CloseStrike:
        int parentIndex = scene->context->parent;
        if (parentIndex < 0) {
            scene->closeContext(game.currentPlayer, -1);
            break;
        }
        auto parent = game.contextStack[parentIndex];
        AssignStrikes & resolve = std::get<AssignStrikes>(parent->context);
        if (!scene->closeContext(game.currentPlayer, resolve.company))
            break;
        if (strike.outcome != Failed)
            resolve.undefeatedStrikes++;
        resolve.unassignedStrikes -= strike.strikesSpent;
        parent->autoProceed = true;
        break;
    }
    return Success;
}

int ProceedContext::operator()(AssignStrikes & assign) {
    Game & game = scene->game;
    switch (assign.state) {
    case AssignStrikes::PlayerAssign:
        if (player != game.currentPlayer)
            return WrongPlayer;
        if (assign.unassignedStrikes == 0) {
            assign.unresolvedStrikes = assign.assignments;
            assign.state = AssignStrikes::ResolveStrikes;
            scene->context->autoProceed = true;
            break;
        }
        assign.attackerChoosesDefendingCharacters = true;
        assign.state = AssignStrikes::OpponentAssign;
        break;
    case AssignStrikes::OpponentAssign:
        if (player != game.hazardPlayer)
            return WrongPlayer;
        {
            Company & company = *game.getCurrentSide().findCompany(assign.company);
            int remainingTargets = scene->getNumberOfTargets(company);
            if (assign.unassignedStrikes >= remainingTargets)
                scene->autoAssignStrikes(company);
            if (assign.unassignedStrikes == 0 || remainingTargets == 0) {
                assign.unresolvedStrikes = assign.assignments;
                assign.state = AssignStrikes::ResolveStrikes;
                scene->context->autoProceed = true;
                break;
            }
        }
        break;
    case AssignStrikes::ResolveStrikes:
        if (assign.unresolvedStrikes.size() == 1) {
            CardID target = assign.unresolvedStrikes.back().target;
            assign.unresolvedStrikes.pop_back();
            scene->setupStrike(target);
        }
        else if (assign.unresolvedStrikes.empty()) {
            int parentIndex = scene->context->parent;
            if (!scene->closeContext(game.currentPlayer, assign.company) || parentIndex < 0)
                break;
            auto parent = game.contextStack[parentIndex];
            Attack & attack = std::get<Attack>(parent->context);
            if (attack.outcome == Unresolved)
                attack.outcome = assign.undefeatedStrikes == 0 ? Failed : Successful;
            // Update trophy candidates
            if (attack.outcome == Failed && !attack.detainment && !attack.automaticAttack) {
                if (game.getCurrentSide().deckType != HeroDeck && parent->parent != NoParent) {
                    Encounter & encounter = std::get<Encounter>(game.contextStack[parent->parent]->context);
                    for (AssignStrikes::StrikeAssignment & assignment : assign.assignments) {
                        CardType & type = game.getCardType(assignment.target);
                        if (std::holds_alternative<Character>(type)) {
                            Race race = std::get<Character>(type).race;
                            if (race == Orc || race == Troll)
                                encounter.trophyCandidates.insert(assignment.target);
                        }
                    }
                }
            }
            parent->autoProceed = true;
        }
        else
            return MoreThanOneUnresolvedStrikes;
        break;
    }
    return Success;
}

int ProceedContext::operator()(Attack & attack) {
    Game & game = scene->game;
    switch (attack.state) {
    case Attack::StartAttack:
        scene->setupAssignStrikes();
        attack.state = Attack::CloseAttack;
        break;
    case Attack::CloseAttack:
        int parentIndex = scene->context->parent;
        if (!scene->closeContext(game.currentPlayer, attack.company) || parentIndex < 0)
            break;
        auto parent = game.contextStack[parentIndex];
        if (attack.outcome == Failed && !attack.detainment)
            std::get<Encounter>(parent->context).defeatedAttacks++;
        parent->autoProceed = true;
        break;
    }
    return Success;
}

int ProceedContext::operator()(Encounter & encounter) {
    Game & game = scene->game;
    switch (encounter.state) {
    case Encounter::ResolveAttacks:
        scene->setupAttack();
        if (encounter.currentAttack == encounter.attacks.end())
            encounter.state = Encounter::ResolveEncounter;
        scene->context->autoProceed = true;
        break;
    case Encounter::ResolveEncounter:
        encounter.state = Encounter::CloseEncounter;
        if (!encounter.automaticAttack
            && encounter.defeatedAttacks == int(encounter.attacks.size())
            && getBaseMP(game.getCardType(encounter.card)) > 0) {
            CardType & type = game.getCardType(encounter.card);
            bool asterisk = std::holds_alternative<HazardCreature>(type)
                && std::get<HazardCreature>(type).asterisk;
            // Remove any trophy candidates that for some reason have left play
            auto it = encounter.trophyCandidates.begin();
            int cardOwner = game.cards[encounter.card].owner;
            while (it != encounter.trophyCandidates.end())
                game.getDeck(*it) != Characters ? it = encounter.trophyCandidates.erase(it) : ++it;
            if (game.cards[encounter.card].owner == game.currentPlayer)  // (MELE, p.63, top)
                scene->moveCard(encounter.card, OutOfPlay, game.currentPlayer);
            else if (game.getCurrentSide().deckType == HeroDeck)
                asterisk ? scene->moveCard(encounter.card, OutOfPlay, cardOwner)
                         : scene->moveCard(encounter.card, Kills, game.currentPlayer);
            else if (encounter.trophyCandidates.empty())
                asterisk ? scene->moveCard(encounter.card, Kills, game.currentPlayer)
                         : scene->moveCard(encounter.card, OutOfPlay, cardOwner);
            else
                encounter.state = Encounter::ReceiveTrophy;
        } else if (game.cards[encounter.card].deck == Creatures) {
            scene->discard(encounter.card);
        }
        scene->context->autoProceed = (encounter.state == Encounter::CloseEncounter);
        break;
    case Encounter::ReceiveTrophy:
        encounter.state = Encounter::CloseEncounter;
        {
            bool asterisk = std::get<HazardCreature>(game.getCardType(encounter.card)).asterisk;
            scene->moveCard(encounter.card, asterisk ? Kills : OutOfPlay, game.currentPlayer);
        }
        scene->context->autoProceed = true;
        break;
    case Encounter::CloseEncounter:
        scene->closeContext(game.currentPlayer, encounter.company);
        break;
    }
    return Success;
}

int ProceedContext::operator()(CorruptionCheck & corruptionCheck) {
    Game & game = scene->game;
    CardID target = corruptionCheck.targetCharacter;
    switch (corruptionCheck.state) {
    case CorruptionCheck::Start:
        if (player != game.cards[target].owner)
            return WrongPlayer;
        if (corruptionCheck.base < corruptionCheck.modifier + 2)
            corruptionCheck.roll = 2;
        else
            scene->setupRollDice(player);
        corruptionCheck.state = CorruptionCheck::Close;
        scene->context->autoProceed = true;
        break;
    case CorruptionCheck::Close:
        if (!scene->closeContext(game.currentPlayer))
            break;
        int result = corruptionCheck.roll + corruptionCheck.modifier;
        if (result < corruptionCheck.base - 1)
            scene->eliminate(target);
        else if (result <= corruptionCheck.base) {
            if (game.cards[target].dataIndex == game.sides[player].revealedAvatar)
                scene->removePlayer(player);
            else if (game.getSide(target).deckType == HeroDeck)
                scene->discard(target);
            else
                scene->tap(target);
        }
        int parentIndex = scene->context->parent;
        if (parentIndex >= 0) {
            auto parent = game.contextStack[parentIndex];
            if (std::holds_alternative<ResolveCorruptionChecks>(parent->context)) {
                parent->autoProceed = true;
                if (game.getDeck(target) != Characters) {
                    // Remove all unresolved corruption checks involving <target>
                    auto & unresolved = std::get<ResolveCorruptionChecks>(parent->context).unresolvedChecks;
                    auto it = unresolved.begin();
                    while (it != unresolved.end())
                        it->target == target ? it = unresolved.erase(it) : ++it;
                }
            }
        }
        break;
    }
    return Success;
}

int ProceedContext::operator()(ResolveCorruptionChecks & resolve) {
    if (resolve.unresolvedChecks.empty()) {
        Game & game = scene->game;
        int parentIndex = scene->context->parent;
        if (scene->closeContext(game.currentPlayer) && parentIndex >= 0) {
            TransferItems & transfer = std::get<TransferItems>(game.contextStack[parentIndex]->context);
            if (game.getDeck(transfer.items.front()) == Possessions) {
                auto & assignment = *transfer.assignments.begin();
                scene->gainPossession(assignment.second, assignment.first);
            }
        }
    } else {
        return UnresolvedCorruptionCheck;
    }
    return Success;
}

int ProceedContext::operator()(StoreCard & storeCard) {
    Game & game = scene->game;
    int owner = game.cards[storeCard.target].owner;
    if (scene->closeContext() && game.getParent(storeCard.target) >= 0) {
        scene->moveCard(storeCard.target, Stored, owner);
        game.cards[storeCard.target].state = Untapped;

        // Stored minion Gold Rings are automatically tested (MELE, p.45, top)
        if (game.sides[owner].deckType != HeroDeck) {
            CardType & type = game.getCardType(storeCard.target);
            if (std::holds_alternative<Item>(type)) {
                Item & item = std::get<Item>(type);
                if (item.minion && item.type == GoldRing)
                    scene->setupRingTest(storeCard.target, -2);
            }
        }
    }
    return Success;
}

int ProceedContext::operator()(RemoveCorruptionCard & remove) {
    Game & game = scene->game;
    switch (remove.state) {
    case RemoveCorruptionCard::Start:
        remove.state = RemoveCorruptionCard::Close;
        scene->setupRollDice(game.cards[game.getParent(remove.target)].owner);
        break;
    case RemoveCorruptionCard::Close:
        if (scene->closeContext() && (remove.roll + remove.modifier > remove.requiredNumber))
            scene->discard(remove.target);
        break;
    }
    return Success;
}

int ProceedContext::operator()(InfluenceAttempt & influence) {
    Game & game = scene->game;
    Outcome outcome;
    switch (influence.state) {
    case InfluenceAttempt::Start:
        if (player != game.currentPlayer)
            return WrongPlayer;
        if (scene->allowedOperation(influence.target, Discard)) {
            influence.state = InfluenceAttempt::Resolve;
            if (influence.influenceOpponent)
                scene->setupRollDice(game.cards[influence.target].owner);
            if (!influence.influenceOpponent && (influence.modifier + 2 > influence.requiredInfluence))
                influence.roll = 2;
            else
                scene->setupRollDice(player);
        } else {
            influence.state = InfluenceAttempt::Close;
        }
        scene->context->autoProceed = true;
        break;
    case InfluenceAttempt::Resolve:
        outcome = (influence.roll + influence.modifier > influence.requiredInfluence) ? Successful : Failed;
        influence.state = InfluenceAttempt::Close;
        scene->context->autoProceed = true;
        if (influence.influenceOpponent) {
            if (outcome == Successful) {
                scene->discard(influence.target);
                if (influence.revealedCard >= 0) {
                    if (std::holds_alternative<Faction>(game.getCardType(influence.revealedCard)))
                        scene->moveCard(influence.revealedCard, Factions, game.currentPlayer);
                    else {
                        influence.state = InfluenceAttempt::Take;
                        scene->context->autoProceed = false;
                    }
                }
            } else if (influence.revealedCard >= 0)
                scene->discard(influence.revealedCard);
        }
        else if (outcome == Failed)
            scene->discard(influence.target);
        else {
            scene->tap(std::get<Explore>(game.contextStack[scene->context->parent]->context).site); // Tap site
            scene->moveCard(influence.target, Factions, game.cards[influence.influencingCharacter].owner);
        }
        break;
    case InfluenceAttempt::Take:
        if (player != game.currentPlayer)
            return WrongPlayer;
        scene->discard(influence.revealedCard);
        influence.state = InfluenceAttempt::Close;
        scene->context->autoProceed = true;
        break;
    case InfluenceAttempt::Close:
        scene->closeContext(game.currentPlayer);
        break;
    }
    return Success;
}

int ProceedContext::operator()(RingTest & ringTest) {
    if (player != scene->game.currentPlayer)
        return WrongPlayer;
    if (scene->closeContext())
        scene->discard(ringTest.targetRing);
    return Success;
}

int ProceedContext::operator()(SpecialTest & specialTest) {
    switch (specialTest.state) {
    case SpecialTest::Start:
        scene->setupRollDice(specialTest.player);
        specialTest.state = SpecialTest::Close;
        scene->context->autoProceed = true;
        break;
    case SpecialTest::Close:
        scene->closeContext();
        break;
    }
    return Success;
}

int ProceedContext::operator()(Movement & movement) {
    Game & game = scene->game;
    if (player == game.hazardPlayer) {
        int parentIndex = scene->context->parent;
        if (scene->closeContext(game.currentPlayer, movement.company)) {
            scene->setupReturnToHandSize(false);
            if (parentIndex >= 0)
                game.contextStack[parentIndex]->autoProceed = true;
        }
    } else {
        return WrongPlayer;
    }
    return Success;
}

int ProceedContext::operator()(DrawCards & drawCards) {
    Game & game = scene->game;
    int result = WrongPlayer;
    if (player == game.currentPlayer && drawCards.playerCardsDrawn > 0) {
        drawCards.playerCardsDrawn = drawCards.playerLimit;
        result = Success;
    } else if (player == game.hazardPlayer && drawCards.opponentCardsDrawn > 0) {
        drawCards.opponentCardsDrawn = drawCards.opponentLimit;
        result = Success;
    }
    if (drawCards.playerCardsDrawn == drawCards.playerLimit && drawCards.opponentCardsDrawn == drawCards.opponentLimit) {
        scene->closeContext();
        result = Success;
    }
    return result;
}

int ProceedContext::operator()(Arrival & arrival) {
    Side & side = scene->game.getCurrentSide();
    auto & companies = arrival.returnToOrigin;
    CompanyID company = arrival.selectedCompany;
    if (companies.empty() && scene->verifyArrivingCompanies()) {
        std::list<Company> & companies = side.companies;
        std::map<CardID, std::vector<CompanyID>> sameDestination;
        for (Company & company : companies) {
            sameDestination[company.newLocation].push_back(company.id);
            if (company.currentLocation != company.newLocation)
                scene->arriveAtLocation(company);
        }
        for (auto & newCompany : sameDestination) {
            auto & companies = newCompany.second;
            for (auto it = std::next(companies.begin()); it != companies.end(); ++it)
                scene->combineCompanies(scene->game.currentPlayer, *it, companies.front());
        }
        scene->closeContext(scene->game.currentPlayer);
    } else {
        auto it = std::find(companies.begin(), companies.end(), company);
        if (it == companies.end())
            return InvalidCompanyID;
        scene->returnToOrigin(*side.findCompany(*it));
        companies.clear();
        scene->context->autoProceed = true;
    }
    return Success;
}

int ProceedContext::operator()(Explore & explore) {
    Game & game = scene->game;
    switch (explore.state) {
    case Explore::EnterSite:
        if (player != game.currentPlayer)
            return WrongPlayer;
        explore.state = Explore::Close;
        scene->context->autoProceed = true;
        break;
    case Explore::RevealOnGuard:
        if (player != game.hazardPlayer)
            return WrongPlayer;
        scene->setupAutomaticAttack();
        break;
    case Explore::ExploreSite:
        if (player != game.currentPlayer)
            return WrongPlayer;
        explore.state = Explore::Close;
        scene->context->autoProceed = true;
        break;
    case Explore::Close:
        int parentIndex = scene->context->parent;
        if (scene->closeContext(game.currentPlayer, explore.company) && parentIndex >= 0)
            game.contextStack[parentIndex]->autoProceed = true;
        break;
    }
    return Success;
}

int ProceedContext::operator()(SelectCompany & selectCompany) {
    auto & companies = selectCompany.remainingCompanies;
    if (companies.empty())
        scene->closeContext(scene->game.currentPlayer);
    else {
        if (selectCompany.remainingCompanies.size() == 1)
            selectCompany.selectedCompany = selectCompany.remainingCompanies.back();
        CompanyID company = selectCompany.selectedCompany;
        auto it = std::find(companies.begin(), companies.end(), company);
        if (it == companies.end())
            return InvalidCompanyID;
        companies.erase(it);
        if (scene->game.turnSequence.phase == MovementHazardPhase)
            scene->setupMovement(company).setupDrawCards(company);
        else
            scene->setupExplore(company);
    }
    return Success;
}

int ProceedContext::operator()(TransferCards & transfer) {
    if (!transfer.transactions.empty())
        return NoCardsSelected;
    scene->closeContext();
    return Success;
}

int ProceedContext::operator()(TransferItems & transfer) {
    switch (transfer.state) {
    case TransferItems::SelectItems:
        if (player != transfer.player)
            return WrongPlayer;
        for (auto & assignment : transfer.assignments)
            scene->gainPossession(assignment.second, assignment.first);
        transfer.state = TransferItems::Close;
        scene->context->autoProceed = true;
        break;
    case TransferItems::Close:
        int parentIndex = scene->context->parent;
        if (scene->closeContext(transfer.player, transfer.company) && parentIndex >= 0) {
            // Character eliminated
            Scene parent = scene->parent();
            BodyCheck & bodyCheck = std::get<BodyCheck>(parent.context->context);
            Game & game = scene->game;
            Side & side = game.getCurrentSide();
            Company & company = *side.findCompany(transfer.company);
            parent.stripCharacter(bodyCheck.target, company.id);
            parent.moveCard(bodyCheck.target, Eliminated, game.cards[bodyCheck.target].owner);
            if (company.characters.empty())
                scene->removeCompany(transfer.player, transfer.company);
        }
        break;
    }
    return Success;
}

int ProceedContext::operator()(ReturnToHandSize & handSize) {
    Game & game = scene->game;
    if (game.getCurrentSide().hand.size() != handSize.resourcePlayerHandSize
        || game.sides[game.hazardPlayer].hand.size() != handSize.hazardPlayerHandSize) {
        if (player == game.currentPlayer) {
            Side & side = game.getCurrentSide();
            if (int(side.hand.size()) >= handSize.resourcePlayerHandSize)
                return WrongPlayer;
            handSize.resourcePlayerMayDiscard = false;
            while (!side.playDeck.empty() && side.hand.size() < handSize.resourcePlayerHandSize)
                scene->drawCard(game.currentPlayer);
        }
        else if (player == game.hazardPlayer) {
            Side & side = game.sides[game.hazardPlayer];
            if (int(side.hand.size()) >= handSize.hazardPlayerHandSize)
                return WrongPlayer;
            handSize.hazardPlayerMayDiscard = false;
            while (!side.playDeck.empty() && side.hand.size() < handSize.hazardPlayerHandSize)
                scene->drawCard(game.hazardPlayer);
        } else {
            return WrongPlayer;
        }
    }
    if (game.getCurrentSide().hand.size() == handSize.resourcePlayerHandSize
        && game.sides[game.hazardPlayer].hand.size() == handSize.hazardPlayerHandSize)
        scene->closeContext();
    return Success;
}

int ProceedContext::operator()(ExhaustDeck & exhaust) {
    if (player != exhaust.player)
        return WrongPlayer;
    if (exhaust.fromDiscardPile.size() != exhaust.fromSideboard.size())
        return WrongNumberOfCards;
    if (scene->closeContext(player)) {
        Game & game = scene->game;
        for (CardID card : exhaust.fromSideboard)
            moveCardFast(game, card, DiscardPile, player);
        for (CardID card : exhaust.fromDiscardPile)
            moveCardFast(game, card, Sideboard, player);
        Side & side = game.sides[player];
        std::swap(side.discardPile, side.playDeck);
        for (CardID card : side.playDeck) {
            Card & cardData = game.cards[card];
            game.setHostingDeck(card, exhaust.player, PlayDeck);
            cardData.faceUp = false;
        }
        scene->shuffleDeck(side);
    }
    return Success;
}

int updateUnusedGI(Scene scene) {
    Game & game = scene.game;
    Side & side = game.getCurrentSide();
    // Verify followers
    for (Company & company : side.companies)
        for (CardID character : company.characters) {
            CardID & parent = game.cards[character].parent;
            if (parent == HadParent)
                parent = NoParent;
        }
    return scene.getUnusedGI(game.currentPlayer);
}

int verifyCompanies(Scene scene) {
    Game & game = scene.game;
    Side & side = game.getCurrentSide();
    // General influence
    int unusedGI = updateUnusedGI(scene);
    if (unusedGI < 0) {
        if (game.characterBroughtIntoPlay < 0)
            return InsufficientInfluence;
        CardID character = game.characterBroughtIntoPlay;
        game.characterBroughtIntoPlay = -1;   // Allow characters to be discarded
        if (game.cards[character].parent >= 0)
            return InsufficientInfluence;
        CompanyID id = game.getCompanyID(character);
        if (id < 0)
            return InsufficientInfluence;
        if (scene.getMind(character, id) + unusedGI < 0)
            return InsufficientInfluence;
        scene.returnCardToHand(character, game.currentPlayer, id);
        if (updateUnusedGI(scene) < 0)
            return InsufficientInfluence;
    }
    // Direct influence and composition
    for (Company & company : side.companies) {
        for (CardID character : company.characters)
            if (game.getParent(character) < 0 && scene.getUnusedDI(character, company.id, Unspecified) < 0)
                return InsufficientInfluence;
        int result = scene.verifyCompanyComposition(company);
        if (result)
            return result;
    }
    return Success;
}

int ProceedContext::operator()(OrganizeCompanies & organize) {
    if (player != scene->game.currentPlayer)
        return WrongPlayer;
    int result = verifyCompanies(*scene);
    if (result != Success)
        return result;
    scene->closeContext(player);
    return Success;
}

int ProceedContext::operator()(DiscardSubset & discard) {
    if (player != discard.player)
        return WrongPlayer;
    if (discard.selection.size() != discard.subsetSize)
        return WrongNumberOfCards;
    for (CardID card : discard.selection)
        scene->discard(card);
    scene->closeContext(player);
    return Success;
}

int ProceedContext::operator()(TapCharacters & tap) {
    Game & game = scene->game;
    if (tap.numberOfCharactersToTap != 0)
        for (CardID card : tap.characters)
            if (game.getCardState(card) == Untapped)
                return MustTap;
    scene->closeContext(tap.player);
    return Success;
}

int ProceedContext::operator()(ChainOfEffects & context) {
    Game & game = scene->game;
    Scene parent = scene->parent();
    std::vector<ChainOfEffects::ChainLink> & chain = context.chainOfEffects;
    if (chain.empty())
        scene->closeContext();
    else {
        if (chain.back().resolved) {
            // Conclude previous action
            auto action = chain.back().action;
            if (game.getDeck(action->host) == Pending)
                parent.discard(action->host);
            if (!action->remove(parent))
                game.sides[chain.back().player].actions.push_back(action);
            chain.pop_back();
        }
        if (!chain.empty()) {
            // Perform action
            ChainOfEffects::ChainLink & link = chain.back();
            parent.actionPtr = &link.action;
            if (!link.action->remove(parent) && link.action->conditions(parent, link.arguments))
                link.action->activate(parent, link.arguments);
            link.resolved = true;
        }
        scene->context->autoProceed = true;
    }
    return Success;
}

struct RefreshCompanies {
    RefreshCompanies(Scene scene) : scene(scene) {
        Game & game = scene.game;
        Side & side = game.getCurrentSide();
        for (Company & company : side.companies) {
            healing = scene.healingSite(company.currentLocation);
            for (CardID character : company.characters) {
                refresh(character);
                auto & possessions = game.cards[character].possessions;
                for (CardID possession : possessions)
                    refresh(possession);
            }
        }
    }
    void refresh(CardID target) {
        CardState cardState = scene.game.getCardState(target);
        if (cardState == Tapped)
            scene.untap(target);
        else if (cardState == Wounded && healing)
            scene.heal(target);
    }
    Scene scene;
    bool healing;
};

int ProceedContext::operator()(BottomContext & bottomContext) {
    Game & game = scene->game;
    TurnSequence & turn = game.turnSequence;

    scene->context->autoProceed = !scene->proceedTurnSequence();
    switch (turn.phase) {

    case UntapPhase:
        if (turn.part == Middle) {
            RefreshCompanies refresh(*scene);
            if (game.avatarInPlay(game.getCurrentSide()) >= 0)
                scene->context->autoProceed = false;
        }
        break;

    case OrganizationPhase:
        if (turn.part == Middle)
            game.contextStack.push_back(std::make_shared<Context>(OrganizeCompanies()));
        if (turn.part == End)
            scene->context->autoProceed = false;
        break;

    case LongEventPhase:
        if (turn.part == Middle) {
            for (CardID longEvent : game.getCurrentSide().longEvents)
                scene->discard(longEvent);
            scene->context->autoProceed = false;
        }
        else if (turn.part == End) {
            for (CardID longEvent : game.sides[game.hazardPlayer].longEvents)
                scene->discard(longEvent);
        }
        break;

    case MovementHazardPhase:
        if (turn.part == Middle)
            scene->setupSelectCompanies();
        else if (turn.part == End) {
            game.contextStack.push_back(std::make_shared<Context>(Arrival()));
            game.contextStack.back()->autoProceed = true;
        }
        break;

    case SitePhase:
        if (turn.part == Middle)
            scene->setupSelectCompanies();
        else if (turn.part == End) {
            for (Company & company : game.getCurrentSide().companies)
                for (CardID onGuard : company.onGuard)
                    scene->returnCardToHand(onGuard, game.currentPlayer, company.id);
        }
        break;

    case EndOfTurnPhase:
        if (turn.part == Middle)
            scene->setupReturnToHandSize(true);
        break;

    case NextTurn:
        scene->startNewTurn();
        break;
    }
    return Success;
}
