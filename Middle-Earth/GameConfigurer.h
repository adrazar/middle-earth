#pragma once
#include "CardTopology.h"
#include "../CardSets/DeckDefinition.h"
#include <string>

struct MetaGame;
struct Game;
struct Side;
struct GameConfigurer;

struct PlayerConfigurer {
    PlayerConfigurer(GameConfigurer & gameConfigurer, int player)
        : gameConfigurer(gameConfigurer)
        , player(player)
    { ; }

    // Print new cards
    CardID makeCard(std::string cardName);
    CardID addCardToDeck(std::string cardName, DeckName deckName = PlayDeck);
    CardID addCardToHand(std::string cardName);
    void addCompany(std::string locationName);
    void addCardsToDeck(DeckName deckName, const DeckDescription & params);
    void loadDeck(const DeckDefinition & deck);
    CardID addCharacterToCompany(std::string cardName);
    CardID addStartingItem(std::string name, CardID target);
    CardID addStartingPermanentEvent(std::string name, bool inCompany);

    // Use existing cards
    CompanyID addCompany(CardID location);
    CardID drawCard(const std::string & cardName);
    CardID putOnTopOfPlayDeck(const std::string & cardName);
    void drawCard(CardID card);
    void putOnTopOfPlayDeck(CardID card);
    void addCharacterToCompany(CompanyID id, CardID character);
    void setCurrentLocation(CompanyID id, CardID location);
    void addFaction(CardID faction);
    void addLongEvent(CardID longEvent, const std::string & action = "");
    void addPermanentEvent(CardID permanentEvent, const std::string & action = "");
    void addPermanentEvent(CompanyID id, CardID permanentEvent, const std::string & action = "");
    void insertLocationEffects();

    Side & getSide();
    Game & game();

private:
    GameConfigurer & gameConfigurer;
    int player;
};

struct GameConfigurer {
    explicit GameConfigurer(MetaGame & metaGame) : metaGame(metaGame) { ; }

    // Use existing cards
    void setCardState(CardID target, CardState state);
    void addPossession(CardID target, CardID possession);
    void insertPatches(CardID target, const std::string & action = "");
    void drawCard(CardID card);

    PlayerConfigurer addPlayer();
    int owner();

    Game & game();
    PlayerConfigurer currentPlayerConfiguerer();

    MetaGame & metaGame;
};

void pickUpCard(Game & game, CardID target); // Does not work on kills or locations in use
void printPatchCount(Game & game);
void listActions(Game & game, int player);
CardID getCardID(Game & game, int owner, const std::string & cardName, DeckName excludedDeck = Pending);
CardID getLocationID(Game & game, int player, const std::string & locationName);
