#include "BaseClasses.h"
#include "ErrorCodes.h"
#include "GameInterface.h"
#include <algorithm>

MetaGame::MetaGame() {
    gameStates.emplace_back(settings);
}
MetaGame::MetaGame(Game && gameState)
    : gameStates({ std::move(gameState) })
{ ; }


// Main control

Scene MetaGame::currentScene() {
    Game & game = gameStates.back();
    return getCurrentScene(game);
}

int MetaGame::done(int player) {
    Game & game = gameStates.back();
    GameRecord & gameRecord = game.settings.gameRecord;
    gameRecord.append(RecordEntryType::Done, player);
    int errorCode = proceed(player);
    if (errorCode != Success && gameRecord.record.back().type == RecordEntryType::Done)
        gameRecord.record.pop_back();
    return Success;
}

int MetaGame::proceed(int player) {
    Game & game = gameStates.back();
    auto & contextStack = game.contextStack;
    int errorCode = WrongPlayer;
    do {
        Scene scene = currentScene();
        scene.context->autoProceed = false;
        ProceedContext proceed(scene, player);
        int result = std::visit(proceed, scene.context->context);
        if (errorCode != Success)
            errorCode = result;
    } while (contextStack.back()->autoProceed);
    return errorCode;
}

void MetaGame::cloneState() {
    Game & game = gameStates.back();
    removeEffects(game);        // Erase all effects that are marked for removal
    gameStates.push_back(game);
}


// Roll dice

int MetaGame::roll(int player, int result) {
    Scene scene = currentScene();
    ContextType & context = scene.context->context;
    if (!std::holds_alternative<RollDice>(context))
        return WrongContext;
    RollDice & rollDice = std::get<RollDice>(context);
    if (rollDice.player != player)
        return WrongPlayer;
    GameRecord & gameRecord = scene.game.settings.gameRecord;
    gameRecord.append(RecordEntryType::Roll, player);
    if (!gameRecord.replay) {
        while (rollDice.nRolls--)
            rollDice.rolls.push_back(scene.generateRoll());
        if (result > 0)
            rollDice.rolls[0] = result;
        gameRecord.append(RecordEntryType::RollResult, player, rollDice.rolls);
    } else {
        rollDice.rolls = gameRecord.nextEntry().args;
        rollDice.nRolls = 0;
    }
    if (rollDice.rolls.size() == 1)
        rollDice.selectedRoll = 0;
    return Success;
}
int MetaGame::selectRoll(int player, int rollNumber) {
    Scene scene = currentScene();
    ContextType & context = scene.context->context;
    if (!std::holds_alternative<RollDice>(context))
        return WrongContext;
    RollDice & rollDice = std::get<RollDice>(context);
    if (rollDice.player != player)
        return WrongPlayer;
    if (rollDice.nRolls > 0)
        return DiceNotRolled;
    if (rollNumber < 0 || rollNumber > int(rollDice.rolls.size()))
        return InvalidParameter;
    rollDice.selectedRoll = rollNumber;
    scene.game.settings.gameRecord.append(RecordEntryType::SelectRoll, player, rollNumber);
    proceed(player);
    return Success;
}


// Movement

int MetaGame::selectDestination(int player, CompanyID id, CardID destination, std::vector<RegionNumber> regions) {
    Game & game = gameStates.back();
    if (game.currentPlayer != player)
        return WrongPlayer;
    if (game.turnSequence.phase != OrganizationPhase || game.turnSequence.part != Middle)
        return WrongPhase;
    Side & side = game.sides[player];
    auto it = side.findCompany(id);
    if (it == side.companies.end())
        return InvalidCompanyID;
    Company & company = *it;
    if (destination < 0)
        destination = company.currentLocation;
    else if (game.cards[destination].owner != game.currentPlayer)
        return InvalidTarget;
    CardType & type = game.getCardType(destination);
    if (!std::holds_alternative<Location>(type))
        return NotALocation;
    DeckName deckName = game.cards[destination].deck;
    if (deckName != LocationDeck && deckName != Sites)
        return LocationNotAvailable;
    if (side.deckType == MinionDeck) {
        // Ringwraiths may not travel with non-Ringwraith characters.
        //
        // The CRF Turn Sequence Ruling, Annotation 25, states that a moving company
        // is not considered to be at either the site of origin or destination during
        // its movment hazard phase. Hence a company is not at Darkhaven during the
        // transition, and so may not contain non-Ringwraith characters according to
        // the Ringwraith effects. (MELE, p.59, 1st bullet point)
        //
        // Did a thorough search both online and in the rule book, but could not find
        // any sources explicity confirming or contradicting the validity of this
        // interpretation, surprisingly.

        int ringwraithCount = 0;
        for (CardID character : company.characters)
            if (game.getRace(character) == Ringwraith)
                ringwraithCount++;
        if (ringwraithCount != 0 && ringwraithCount != int(company.characters.size()))
            return RingwraithInCompany;
    }
    Scene scene = currentScene();
    ContextType & context = scene.context->context;
    if (std::holds_alternative<ChainOfEffects>(context))
        return WrongContext;

    if (company.currentLocation == destination)
        company.regions.clear();
    else {
        // Check that no other company at currentLocation has the same destination
        for (Company & otherCompany : side.companies) {
            if (otherCompany.id != id
                && otherCompany.currentLocation == company.currentLocation
                && otherCompany.newLocation == destination)
                return ParallelMovement;
        }
        if (!regions.empty()) {
            // Ringwraiths can't use region movement
            if (game.raceInCompany(company, Ringwraith))
                return RingwraithInCompany;

            // Check that the selected path starts and ends in the right regions
            auto it = regions.begin();
            if (game.getLocation(company.currentLocation).region != *it)
                return RegionMismatch;
            if (regions.back() != std::get<Location>(type).region)
                return RegionMismatch;

            // Verify that the selected path is consistent
            if (regions.size() > scene.getRegionLimit(player, id))
                return RegionLimitReached;
            std::unordered_set<RegionNumber> adjacentRegions;
            std::vector path = { *it };
            for (it = it + 1; it != regions.end(); ++it) {
                getAdjacentRegions(player, path, adjacentRegions);
                if (adjacentRegions.find(*it) == adjacentRegions.end())
                    return RegionNotAdjacent;
                path.push_back(*it);
            }
            company.regions = regions;
        }
        else {
            if (!game.adjacentSites(company.currentLocation, destination))
                return HavenMismatch;
            if (game.raceInCompany(company, Ringwraith)) {
                SitePath & sitePath = game.getSitePath(company.currentLocation, destination);
                if (std::find(sitePath.begin(), sitePath.end(), CoastalSeas) != sitePath.end())
                    return MayNotEnterCoastalSeas;
                if (scene.getSiteType(destination) != Darkhaven &&
                    scene.getRingwraithMode(player).mode == RingwraithMode::None)
                    return MayNotMoveToNonDarkhaven;     // (MELE, p.25, middle)
            }
            company.regions.clear();
        }
    }
    if (company.newLocation != destination)
        game.settings.gameRecord.appendSelectDestination(player, id, destination, company.regions);
    scene.setNewLocation(id, destination);
    return Success;
}

int MetaGame::resetDestination(int player, CompanyID id) {
    return selectDestination(player, id, -1);
}

int MetaGame::getAdjacentRegions(int player, const std::vector<RegionNumber> & path, std::unordered_set<RegionNumber> & regions) {
    Scene scene = currentScene();
    Game & game = scene.game;
    regions = scene.getAdjacentRegions(path.back());
    for (RegionNumber region : path)
        regions.erase(region);
    return Success;
}


// Organize companies

int MetaGame::takeFollower(int player, CardID leader, CardID follower) {
    // Ringwraiths may take non-Ringwraith followers (MELE, p.19, Nick's company)
    Game & game = gameStates.back();
    if (player != game.currentPlayer)
        return WrongPlayer;
    if (!std::holds_alternative<OrganizeCompanies>(game.getContext()))
        return WrongContext;
    Side & side = game.sides[player];
    auto it = side.findCompany(leader, Characters);
    if (it == side.companies.end() || leader == follower)
        return InvalidTarget;
    if (std::find(it->characters.begin(), it->characters.end(), follower) == it->characters.end())
        return InvalidTarget;
    if (game.cards[follower].parent == leader)
        return AlreadyInPossession;
    Scene scene = currentScene();
    if (scene.getUnusedDI(leader, it->id, game.getRace(follower)) < scene.getMind(follower, it->id))
        return InsufficientInfluence;
    game.settings.gameRecord.append(RecordEntryType::TakeFollower, player, { leader, follower });
    scene.dropFollowers(follower);
    game.cards[follower].parent = leader;
    return Success;
}

int MetaGame::dropFollower(int player, CardID follower) {
    Game & game = gameStates.back();
    if (player != game.currentPlayer)
        return WrongPlayer;
    if (!std::holds_alternative<OrganizeCompanies>(game.getContext()))
        return WrongContext;
    Card & card = game.cards[follower];
    if (card.owner != player)
        return WrongPlayer;
    if (card.deck != Characters || card.parent < 0)
        return InvalidTarget;
    // ManageFollowers needs card.parent
    game.settings.gameRecord.append(RecordEntryType::DropFollower, player, { card.parent, follower });
    card.parent = NoParent;
    return Success;
}

int MetaGame::transferCharacter(int player, CardID target, CompanyID company) {
    Game & game = gameStates.back();
    if (player != game.currentPlayer)
        return WrongPlayer;
    if (!std::holds_alternative<OrganizeCompanies>(game.getContext()))
        return WrongContext;
    Scene scene = getCurrentScene(game);
    Side & side = game.getCurrentSide();
    auto it = side.findCompany(target, Characters);
    if (it == side.companies.end())
        return InvalidTarget;
    Company & sourceCompany = *it;
    it = side.findCompany(company);
    if (it == side.companies.end())
        company = scene.addCompany(sourceCompany.currentLocation);
    else if (it->currentLocation != sourceCompany.currentLocation)
        return WrongLocation;
    game.settings.gameRecord.append(RecordEntryType::TransferCharacter, player, { target, company });
    scene.transferCharacter(target, company);
    return Success;
}

int MetaGame::discardCharacter(int player, CardID target) {
    Game & game = gameStates.back();
    if (player != game.currentPlayer)
        return WrongPlayer;
    if (!std::holds_alternative<OrganizeCompanies>(game.getContext()))
        return WrongContext;
    if (game.characterBroughtIntoPlay >= 0)
        return CharacterAlreadyBroughtIntoPlay;
    Side & side = game.getCurrentSide();
    auto it = side.findCompany(target, Characters);
    if (it == side.companies.end())
        return InvalidTarget;
    if (game.cards[target].dataIndex == side.revealedAvatar)
        return InvalidTarget;
    Scene scene = currentScene();
    Character & character = game.getCharacter(target);
    bool allowed = scene.getSiteType(it->currentLocation) == side.havenType() ||
        scene.matchingHomeSite(character.homeSite, it->currentLocation);
    if (!scene.allowedOperation(target, Discard, allowed))
        return IllegalOperation;
    game.settings.gameRecord.append(RecordEntryType::DiscardCharacter, player, { target });
    scene.discard(target);
    game.characterBroughtIntoPlay = target;
    return Success;
}

int MetaGame::rearrangeEquipment(int player, CardID target, std::vector<CardID> equipment) {
    Game & game = gameStates.back();
    std::vector<CardID> & possessions = game.cards[target].possessions;
    if (equipment.empty() || equipment.size() != possessions.size())
        return WrongNumberOfCards;
    for (CardID possession : possessions)
        if (std::find(equipment.begin(), equipment.end(), possession) == equipment.end())
            return InvalidParameter;
    possessions = equipment;
    std::vector<CardID> args = { target };
    for (CardID possession : possessions)
        args.push_back(possession);
    game.settings.gameRecord.append(RecordEntryType::RearrangeEquipment, player, args);
    return Success;
}

int MetaGame::transferItem(int player, CardID item, CardID recipient) {
    // An item may be transferred between two characters at the same site,
    // hence they need not be in the same company (MELE, p.48, bottom)
    Game & game = gameStates.back();
    if (player != game.currentPlayer)
        return WrongPlayer;
    if (!std::holds_alternative<OrganizeCompanies>(game.getContext()))
        return WrongContext;
    if (!std::holds_alternative<Item>(game.getCardType(item)))
        return NotAnItem;
    CardID parent = game.cards[item].parent;
    if (parent < 0 || game.cards[parent].deck != Characters)
        return CardNotInPlay;
    if (parent == recipient)
        return AlreadyInPossession;
    Side & side = game.getCurrentSide();
    auto it1 = side.findCompany(parent, Characters);
    if (it1 == side.companies.end())
        return CardNotInCompany;
    auto it2 = side.findCompany(recipient, Characters);
    if (it2 == side.companies.end())
        return CardNotInCompany;
    if (it1->currentLocation != it2->currentLocation)
        return WrongLocation;
    if (!getCurrentScene(game).transferAllowed(item, recipient))
        return InvalidTarget;
    game.settings.gameRecord.append(RecordEntryType::TransferItem, player, { item, recipient });
    game.contextStack.push_back(std::make_shared<Context>(TransferItems()));
    Scene scene = getCurrentScene(game);
    TransferItems & transfer = std::get<TransferItems>(scene.context->context);
    transfer.player = game.currentPlayer;
    transfer.items = { item };
    transfer.assignments[item] = recipient;
    transfer.state = TransferItems::Close;
    scene.context->autoProceed = true;
    if (game.getRace(parent) != Ringwraith) {
        Scene resolve = getCurrentScene(game).setupResolveCorruptionChecks(parent, parent);
        resolve.context->parent = scene.contextIndex;
        resolve.setupCorruptionCheck(parent);
    }
    return Success;
}

int MetaGame::store(int player, CardID target) {
    Game & game = gameStates.back();
    if (player != game.currentPlayer)
        return WrongPlayer;
    if (!std::holds_alternative<OrganizeCompanies>(game.getContext()))
        return WrongContext;
    if (game.cards[target].owner != player)
        return InvalidTarget;

    Scene scene = currentScene();
    bool allowed = false;
    CardType & type = game.getCardType(target);
    CardID parent = game.getParent(target);
    if (std::holds_alternative<Item>(type) && game.cards[target].deck == Possessions) {
        Company & company = *game.getCurrentSide().findCompany(parent, Characters);
        allowed = (game.turnSequence.phase == OrganizationPhase &&
                   game.getLocation(company.currentLocation).type == game.getCurrentSide().havenType());
    }
    if (!scene.allowedOperation(target, Store, allowed))
        return IllegalOperation;
    game.settings.gameRecord.append(RecordEntryType::Store, player, { target });
    scene.setupStoreCard(target).context->autoProceed = true;
    if (std::holds_alternative<Item>(type) && game.getRace(parent) != Ringwraith) {
        Scene resolve = scene.setupResolveCorruptionChecks(target, parent);
        resolve.setupCorruptionCheck(target);
    }
    return Success;
}


// Intermediate phase

int MetaGame::chooseCompany(int player, CompanyID company) {
    Game & game = gameStates.back();
    if (player != game.currentPlayer)
        return WrongPlayer;
    ContextType & context = game.getContext();
    if (std::holds_alternative<SelectCompany>(context)) {
        SelectCompany & select = std::get<SelectCompany>(context);
        auto & companies = select.remainingCompanies;
        if (std::find(companies.begin(), companies.end(), company) == companies.end())
            return InvalidCompanyID;
        select.selectedCompany = company;
    }
    else if (std::holds_alternative<Arrival>(context)) {
        Arrival & arrival = std::get<Arrival>(context);
        auto & companies = arrival.returnToOrigin;
        if (std::find(companies.begin(), companies.end(), company) == companies.end())
            return InvalidCompanyID;
        arrival.selectedCompany = company;
    }
    else
        return WrongContext;
    game.settings.gameRecord.append(RecordEntryType::ChooseCompany, player, { company });
    proceed(player);
    return Success;
}

int discardFromHand(int player, MetaGame & metaGame, std::vector<CardID> & cards) {
    Game & game = metaGame.gameStates.back();
    removeDuplicateArguments(cards);
    for (CardID target : cards) {
        Card & card = game.cards[target];
        if (card.owner != player)
            return WrongPlayer;
        if (card.deck != Hand)
            return CardNotOnHand;
    }
    game.settings.gameRecord.append(RecordEntryType::Discard, player, cards);
    Scene scene = getCurrentScene(game);
    for (CardID target : cards)
        scene.discard(target);
    metaGame.proceed(player);
    return Success;
}

int discardSubset(int player, MetaGame & metaGame, std::vector<CardID> && cards) {
    Game & game = metaGame.gameStates.back();
    DiscardSubset & discard = std::get<DiscardSubset>(game.getContext());
    if (discard.player != player)
        return WrongPlayer;
    removeDuplicateArguments(cards);
    if (discard.subsetSize != cards.size())
        return WrongNumberOfCards;
    for (CardID card : cards)
        if (std::find(discard.cards.begin(), discard.cards.end(), card) == discard.cards.end())
            return InvalidParameter;
    discard.selection = cards;
    game.settings.gameRecord.append(RecordEntryType::Discard, player, discard.selection);
    metaGame.proceed(player);
    return Success;
}

int MetaGame::discard(int player, std::vector<CardID> cards) {
    Game & game = gameStates.back();
    ContextType & contextType = game.getContext();
    if (std::holds_alternative<DiscardSubset>(contextType))
        return discardSubset(player, *this, std::move(cards));
    if (!std::holds_alternative<ReturnToHandSize>(contextType))
        return WrongContext;
    ReturnToHandSize & context = std::get<ReturnToHandSize>(contextType);
    bool mayDiscard;
    int handSize;
    if (player == game.currentPlayer) {
        handSize = context.resourcePlayerHandSize;
        mayDiscard = context.resourcePlayerMayDiscard;
    } else if (player == game.hazardPlayer) {
        handSize = context.hazardPlayerHandSize;
        mayDiscard = context.hazardPlayerMayDiscard;
    } else
        return WrongPlayer;

    if (mayDiscard) {
        if (cards.size() != 1)
            return WrongNumberOfCards;
    } else {
        Side & side = game.sides[player];
        int remainingDiscards = side.hand.size() - handSize;
        if (int(cards.size()) > remainingDiscards)
            return WrongNumberOfCards;
    }
    return discardFromHand(player, *this, cards);
}

int MetaGame::drawCard(int player) {
    Game & game = gameStates.back();
    Scene scene = getCurrentScene(game);
    ContextType & context = scene.context->context;
    if (!std::holds_alternative<DrawCards>(context))
        return WrongContext;
    DrawCards & drawCards = std::get<DrawCards>(context);
    if (player == game.currentPlayer) {
        if (drawCards.playerCardsDrawn == drawCards.playerLimit)
            return CardLimitReached;
        game.settings.gameRecord.append(RecordEntryType::DrawCard, player);
        scene.drawCard(player);
        drawCards.playerCardsDrawn++;
    }
    else if (player == game.hazardPlayer) {
        if (drawCards.opponentCardsDrawn == drawCards.opponentLimit)
            return CardLimitReached;
        game.settings.gameRecord.append(RecordEntryType::DrawCard, player);
        scene.drawCard(player);
        drawCards.opponentCardsDrawn++;
    }
    else
        return WrongPlayer;
    return Success;
}

int MetaGame::transferCards(int player, std::vector<CardID> cards, int option) {
    Game & game = gameStates.back();
    ContextType & context = game.getContext();
    if (!std::holds_alternative<TransferCards>(context))
        return WrongContext;
    TransferCards & transfer = std::get<TransferCards>(context);
    if (transfer.transactions.find(player) == transfer.transactions.end())
        return WrongPlayer;
    if (option < 0 || option >= int(transfer.transactions[player].size()))
        return InvalidParameter;
    TransferCards::TransferCardsOption & transferOption = transfer.transactions[player][option];
    removeDuplicateArguments(cards);
    if (cards.size() < transferOption.minimum || cards.size() > transferOption.maximum)
        return WrongNumberOfCards;
    std::vector<CardCategory> & categories = transferOption.categories;
    for (CardID card : cards) {
        if (game.cards[card].deck != transferOption.sourceDeck)
            return WrongDeck;
        if (std::find(categories.begin(), categories.end(), game.getCardCategory(card)) == categories.end())
            return WrongCardCategory;
    }
    game.settings.gameRecord.append(RecordEntryType::TransferCards, player, cards, option);
    for (CardID card : cards)
        moveCardFast(game, card, transferOption.destinationDeck, player);
    if (transferOption.destinationDeck == PlayDeck)
        getCurrentScene(game).shuffleDeck(game.sides[player]);
    transfer.transactions.erase(player);
    if (transfer.transactions.empty())
        proceed(player);
    return Success;
}


// Exhaust deck

int MetaGame::exchangeCard(int player, CardID card) {
    Game & game = gameStates.back();
    ContextType & context = game.getContext();
    if (!std::holds_alternative<ExhaustDeck>(context))
        return WrongContext;
    ExhaustDeck & exhaust = std::get<ExhaustDeck>(context);
    if (player != exhaust.player)
        return WrongPlayer;
    DeckName deckName = game.getDeck(card);
    if (deckName != DiscardPile || deckName != Sideboard)
        return WrongDeck;
    auto & selectedCards = (deckName == DiscardPile) ? exhaust.fromDiscardPile : exhaust.fromSideboard;
    if (selectedCards.size() > 4)
        return CardLimitReached;
    if (!selectedCards.contains(card))
        selectedCards.insert(card);
    else
        selectedCards.erase(card);
    game.settings.gameRecord.append(RecordEntryType::ExchangeCard, player, { card }, deckName == Sideboard ? 0 : 1);
    return Success;
}


// Hazard play

int MetaGame::setKeyedTo(int player, KeyedToType key) {
    Game & game = gameStates.back();
    if (player != game.hazardPlayer)
        return WrongPlayer;
    ContextType & type = game.getContext();
    if (!std::holds_alternative<Movement>(type))
        return WrongContext;
    std::get<Movement>(type).key = key;

    // This can be done here without bloating issues, because setKeyedTo will
    // probably not be exposed directly to the user - it will be called as part
    // of a high level function for performing hazard creature attacks.
    game.settings.gameRecord.appendSetKeyedTo(player, key);
    return Success;
}

int MetaGame::placeOnGuard(int player, CardID card) {
    Scene scene = currentScene();
    Game & game = scene.game;
    if (player != game.hazardPlayer)
        return WrongPlayer;
    ContextType & type = game.getContext();
    if (!std::holds_alternative<Movement>(type))
        return WrongContext;
    DeckName & deckName = game.cards[card].deck;
    if (deckName != Hand)
        return InvalidTarget;
    Movement & movement = game.getMovement();
    if (movement.hazardsPlayed >= movement.hazardLimit)
        return HazardLimitReached;
    game.settings.gameRecord.append(RecordEntryType::PlaceOnGuard, player, { card });
    scene.moveCard(card, OnGuard, game.currentPlayer, movement.company);
    movement.hazardsPlayed++;
    return Success;
}


// Combat

int MetaGame::assignStrike(int player, CardID target) {
    Scene scene = currentScene();
    ContextType & context = scene.context->context;
    if (!std::holds_alternative<AssignStrikes>(context))
        return WrongContext;
    AssignStrikes & assign = std::get<AssignStrikes>(context);
    if (assign.state == AssignStrikes::PlayerAssign && player != scene.game.currentPlayer
        || assign.state == AssignStrikes::OpponentAssign && player != scene.game.hazardPlayer)
        return WrongPlayer;
    if (assign.unassignedStrikes == 0)
        return OutOfStrikes;
    int assignments = assign.assignments.size();
    scene.target(target);
    if (assignments == assign.assignments.size())
        return InvalidTarget;
    scene.game.settings.gameRecord.append(RecordEntryType::AssignStrikes, player, { target });
    return Success;
}

int MetaGame::unassignStrike(int player, CardID target) {
    Scene scene = currentScene();
    ContextType & context = scene.context->context;
    if (!std::holds_alternative<AssignStrikes>(context))
        return WrongContext;
    AssignStrikes & assign = std::get<AssignStrikes>(context);
    if (assign.state == AssignStrikes::PlayerAssign && player != scene.game.currentPlayer
        || assign.state == AssignStrikes::OpponentAssign && player != scene.game.hazardPlayer)
        return WrongPlayer;
    auto it = std::find(assign.assignments.begin(), assign.assignments.end(), target);
    if (it == assign.assignments.end())
        return InvalidTarget;
    if (it->player != player)
        return WrongPlayer;
    assign.assignments.erase(it);
    assign.unassignedStrikes++;
    scene.game.settings.gameRecord.append(RecordEntryType::UnassignStrike, player, { target });
    return Success;
}

int MetaGame::resolveStrike(int player, CardID target) {
    Scene scene = currentScene();
    Game & game = scene.game;
    if (player != game.currentPlayer)
        return WrongPlayer;
    ContextType & context = scene.context->context;
    if (!std::holds_alternative<AssignStrikes>(context))
        return WrongContext;
    AssignStrikes & assign = std::get<AssignStrikes>(context);
    if (assign.state != AssignStrikes::ResolveStrikes)
        return WrongState;
    auto it = std::find(assign.unresolvedStrikes.begin(), assign.unresolvedStrikes.end(), target);
    if (it == assign.unresolvedStrikes.end())
        return InvalidTarget;
    game.settings.gameRecord.append(RecordEntryType::ResolveStrike, player, { target });
    assign.unresolvedStrikes.erase(it);
    scene.setupStrike(target);
    return Success;
}

int MetaGame::useStrikes(int player, int strikes) {
    Scene scene = currentScene();
    if (player != scene.game.hazardPlayer)
        return WrongPlayer;
    ContextType & context = scene.context->context;
    if (!std::holds_alternative<Strike>(context))
        return WrongContext;
    Strike & strike = std::get<Strike>(context);
    if (strike.state != Strike::ExcessStrikes)
        return WrongState;
    int availableStrikes = strike.excessStrikes + strike.strikesSpent;
    if (strikes < 0 || strikes > availableStrikes || strikes == strike.strikesSpent)
        return InvalidParameter;
    strike.strikesSpent = strikes;
    strike.excessStrikes = availableStrikes - strikes;
    scene.game.settings.gameRecord.append(RecordEntryType::UseStrikes, player, strikes);
    return Success;
}

int MetaGame::tap(int player, CardID target) {
    Scene scene = currentScene();
    Game & game = scene.game;
    if (game.getCardState(target) != Untapped)
        return NotUntapped;
    ContextType & context = scene.context->context;
    if (std::holds_alternative<Strike>(context)) {
        if (player != game.currentPlayer)
            return WrongPlayer;
        Strike & strike = std::get<Strike>(context);
        if (strike.state == Strike::TapCharacter && target == strike.target) {
            // Tap character
            game.settings.gameRecord.append(RecordEntryType::Tap, player, { target });
            scene.tap(target);
            strike.state = Strike::PlayResources;
        }
        else if (strike.state == Strike::PlayResources) {
            // +1 prowess against strike
            CardType & type = game.getCardType(target);
            if (!std::holds_alternative<Character>(type) && !std::holds_alternative<Ally>(type))
                return InvalidTarget;
            if (strike.mayNotSupport.find(target) != strike.mayNotSupport.end())
                return IllegalOperation;
            if (game.getCompanyID(target) != game.getActiveCompany())
                return CardNotInCompany;
            game.settings.gameRecord.append(RecordEntryType::Tap, player, { target });
            scene.tap(target);
            strike.targetProwessModifier++;
            strike.mayNotSupport.insert(target);
        }
        else
            return WrongState;
    }
    else if (std::holds_alternative<CorruptionCheck>(context)) {
        // Corruption check modifier +1
        if (player != game.currentPlayer)
            return WrongPlayer;
        CorruptionCheck & corruption = std::get<CorruptionCheck>(context);
        if (!std::holds_alternative<Character>(game.getCardType(target)))
            return InvalidTarget;
        if (corruption.mayNotSupport.find(target) != corruption.mayNotSupport.end())
            return InvalidTarget;
        if (game.getCompanyID(target) != game.getCompanyID(corruption.targetCharacter))
            return InvalidTarget;
        game.settings.gameRecord.append(RecordEntryType::Tap, player, { target });
        scene.tap(target);
        corruption.modifier++;
        corruption.mayNotSupport.insert(target);
    }
    else if (std::holds_alternative<TapCharacters>(context)) {
        TapCharacters & tap = std::get<TapCharacters>(context);
        if (player != tap.player)
            return WrongPlayer;
        if (std::find(tap.characters.begin(), tap.characters.end(), target) == tap.characters.end())
            return InvalidTarget;
        game.settings.gameRecord.append(RecordEntryType::Tap, player, { target });
        scene.tap(target);
        tap.numberOfCharactersToTap--;
        proceed(player);
    }
    else
        return WrongContext;
    return Success;
}

int MetaGame::selectItemRecipient(int player, CardID item, CardID target) {
    // Only one item per unwounded character (MELE, p.50, 2nd bullet)
    Game & game = gameStates.back();
    if (player != game.currentPlayer)
        return WrongPlayer;
    ContextType & context = game.getContext();
    if (!std::holds_alternative<TransferItems>(context))
        return WrongContext;
    if (!std::holds_alternative<Character>(game.getCardType(target)))
        return InvalidTarget;
    TransferItems & transfer = std::get<TransferItems>(context);
    if (game.getCompanyID(target) != transfer.company)
        return InvalidTarget;
    if (transfer.unwoundedOnly && game.getCardState(target) == Wounded)
        return InvalidTarget;
    auto & items = transfer.items;
    if (std::find(items.begin(), items.end(), item) == items.end())
        return InvalidParameter;
    if (!currentScene().transferAllowed(item, target))
        return InvalidTarget;
    game.settings.gameRecord.append(RecordEntryType::DistributePossessions, player, { item, target });
    auto & assignments = transfer.assignments;
    // Remove any item previously assigned to selected target.
    for (auto it = assignments.begin(); it != assignments.end(); ++it)
        if (it->second == target) {
            CardID assignedItem = it->first;
            assignments.erase(it);
            // Reassigning the item removes assignment
            if (assignedItem == item)
                return Success;
            break;
        }
    assignments[item] = target;
    return Success;
}

int MetaGame::cancelHomeSite(int player, CardID target) {
    Game & game = gameStates.back();
    if (player != game.currentPlayer)
        return WrongPlayer;
    ContextType & context = game.getContext();
    if (!std::holds_alternative<Attack>(context))
        return WrongContext;
    if (!std::holds_alternative<Character>(game.getCardType(target)))
        return InvalidTarget;
    if (game.getCardState(target) != Untapped)
        return NotUntapped;
    Attack & attack = std::get<Attack>(context);
    if (!attack.automaticAttack)
        return WrongPhase;
    if (!game.characterInCompany(target, attack.company))
        return CardNotInCompany;
    Character & character = game.getCharacter(target);
    CardID location = game.getCurrentSide().findCompany(attack.company)->newLocation;
    if (character.homeSite != game.getLocation(location).name)
        return WrongLocation;
    game.settings.gameRecord.append(RecordEntryType::CancelHomeSite, player, { target });
    Scene scene = getCurrentScene(game);
    scene.tap(target);
    scene.cancelAttack();
    proceed(player);
    return Success;
}


// Site phase

int MetaGame::enterSite(int player) {
    Game & game = gameStates.back();
    if (player != game.currentPlayer)
        return WrongPlayer;
    ContextType & context = game.getContext();
    if (!std::holds_alternative<Explore>(context))
        return WrongContext;
    Explore & explore = std::get<Explore>(context);
    if (explore.state != Explore::EnterSite)
        return SiteAlreadyEntered;
    Scene scene = currentScene();
    if (!scene.allowedOperation(explore.site, EnterSite))
        return SiteMayNotBeEntered;
    game.settings.gameRecord.append(RecordEntryType::EnterSite, player);
    if (!game.getCurrentSide().findCompany(explore.company)->onGuard.empty())
        explore.state = Explore::RevealOnGuard;
    else {
        if (scene.setupAutomaticAttack())
            proceed(player);
    }
    return Success;
}

int MetaGame::influenceOpponent(int player, CardID influencingCharacter, CardID target) {
    Game & game = gameStates.back();
    if (player != game.currentPlayer)
        return WrongPlayer;
    ContextType & context = game.getContext();
    if (!std::holds_alternative<Explore>(context))
        return WrongContext;
    Explore & explore = std::get<Explore>(context);
    if (explore.state != Explore::ExploreSite)
        return WrongState;
    if (game.influencedOpponent)
        return AlreadyInfluencedOpponent;
    if (game.getCardState(influencingCharacter) != Untapped)
        return NotUntapped;
    if (!game.characterInCompany(influencingCharacter, explore.company))
        return CardNotInCompany;
    Card & card = game.cards[target];
    if (card.owner == player)
        return InvalidTarget;
    // Not allowed on first turn. Avatars can't do it on the turn they are revealed.
    if (game.turnSequence.round == 1)
        return MayNotInflueceOpponentFirstRound;
    Side & side = game.getCurrentSide();
    if (influencingCharacter == game.characterBroughtIntoPlay &&
        game.cards[influencingCharacter].dataIndex == side.revealedAvatar)
        return AvatarRevealedThisTurn;
    // Items controlled by an Avatar can't be influenced.
    // A Ringwraith follower (i.e. a Ringwraith who follows the Ringwraith) may not be influenced.
    // An ally controlled by a Ringwraith (not Ringwraith followers) may not be influenced.
    // Wizards can have their allies influenced away.
    // Items with permanent events on them can't be influenced (MELE, p.66, above clarification)
    Company & influencingCompany = *side.findCompany(explore.company);
    CardType & type = game.getCardType(target);
    DeckName deck = game.getDeck(target);
    Scene scene = getCurrentScene(game);
    if (deck == Factions) {
        if (!std::holds_alternative<Faction>(type))
            return InvalidTarget;
        Faction & faction = std::get<Faction>(type);
        if (!scene.matchingHomeSite(faction.homeSite, influencingCompany.currentLocation))
            return WrongLocation;
    }
    else {
        CompanyID id = game.getCompanyID(target);
        if (id < 0)
            return CardNotInCompany;
        Side & opponent = game.sides[card.owner];
        Company & targetCompany = *opponent.findCompany(id);
        if (game.getLocation(influencingCompany.currentLocation).getName()
            != game.getLocation(targetCompany.currentLocation).getName())
            return WrongLocation;
        if (!scene.allowedOperation(target, Discard))
            return InvalidTarget;
        if (std::holds_alternative<Character>(type)) {
            if (std::get<Character>(type).race == game.getAvatar(opponent.deckType))
                return InvalidTarget;
        }
        else if (std::holds_alternative<Item>(type)) {
            if (game.getRace(card.parent) == game.getAvatar(opponent.deckType))
                return InvalidTarget;
            if (!card.possessions.empty())
                return InvalidTarget;
        }
        else if (std::holds_alternative<Ally>(type)) {
            if (opponent.deckType == MinionDeck && game.cards[card.parent].dataIndex == opponent.revealedAvatar)
                return InvalidTarget;
        }
        else
            return InvalidTarget;
    }
    game.settings.gameRecord.append(RecordEntryType::InfluenceOpponent, player, { influencingCharacter, target });
    scene.tap(influencingCharacter);
    scene.setupInfluenceAttempt(influencingCharacter, target);
    game.influencedOpponent = true;
    return Success;
}

int MetaGame::revealIdenticalCard(int player, CardID card) {
    Game & game = gameStates.back();
    if (player != game.currentPlayer)
        return WrongPlayer;
    ContextType & context = game.getContext();
    if (!std::holds_alternative<InfluenceAttempt>(context))
        return WrongContext;
    InfluenceAttempt & influence = std::get<InfluenceAttempt>(context);
    if (influence.state != InfluenceAttempt::Start)
        return WrongState;
    if (influence.revealedCard != -1)
        return CardAlreadyRevealed;
    if (!game.isManifestation(card, influence.target))
        return NotAManifestation;
    game.settings.gameRecord.append(RecordEntryType::RevealIdenticalCard, player, { card });
    Scene scene = getCurrentScene(game);
    scene.moveCard(card, Pending, player);
    influence.requiredInfluence = 0;
    influence.revealedCard = card;
    return Success;
}

int MetaGame::takeInfluencedCard(int player, CardID leader) {
    Game & game = gameStates.back();
    if (player != game.currentPlayer)
        return WrongPlayer;
    ContextType & context = game.getContext();
    if (!std::holds_alternative<InfluenceAttempt>(context))
        return WrongContext;
    InfluenceAttempt & influence = std::get<InfluenceAttempt>(context);
    if (influence.state != InfluenceAttempt::Take)
        return WrongState;
    CardType & type = game.getCardType(influence.revealedCard);
    Scene scene = getCurrentScene(game);
    if (std::holds_alternative<Character>(type)) {
        CompanyID id = std::get<Explore>(scene.parent().context->context).company;
        int mind = scene.getMind(influence.revealedCard, id);
        if (leader < 0) {
            if (mind > scene.getUnusedGI(player))
                return InsufficientInfluence;
            game.settings.gameRecord.append(RecordEntryType::TakeInfluencedCard, player, { leader });
            scene.moveCard(influence.revealedCard, Characters, player, id);
        } else {
            if (mind > scene.getUnusedDI(leader, id, std::get<Character>(type).race))
                return InsufficientInfluence;
            game.settings.gameRecord.append(RecordEntryType::TakeInfluencedCard, player, { leader });
            scene.moveCard(influence.revealedCard, Characters, player, id);
            game.cards[influence.revealedCard].parent = leader;
        }
    } else {
        game.settings.gameRecord.append(RecordEntryType::TakeInfluencedCard, player, { influence.influencingCharacter });
        scene.gainPossession(influence.influencingCharacter, influence.revealedCard);
    }
    scene.insertPatches(influence.revealedCard);
    influence.state = InfluenceAttempt::Close;
    proceed(player);
    return Success;
}


// Actions

int MetaGame::selectCard(int player, CardID card) {
    Game & game = gameStates.back();
    auto & selectedCards = game.sides[player].selectedCards;
    selectedCards.clear();
    selectedCards.push_back(card);
    if (std::holds_alternative<Location>(game.getCardType(card)))
        selectedCards.push_back(-1); // No company
    return Success;
}

int MetaGame::selectCards(int player, const std::vector<CardID> & cards) {
    Game & game = gameStates.back();
    auto & selectedCards = game.sides[player].selectedCards;
    selectedCards.clear();
    for (CardID card : cards)
        selectedCards.push_back(card);
    return Success;
}

int MetaGame::selectCompany(int player, CompanyID id) {
    Side & side = gameStates.back().sides[player];
    auto it = side.findCompany(id);
    if (it == side.companies.end())
        return InvalidCompanyID;
    side.selectedCards.clear();
    side.selectedCards.push_back(it->currentLocation);
    side.selectedCards.push_back(id);
    return Success;
}

int MetaGame::clearSelection(int player) {
    gameStates.back().sides[player].selectedCards.clear();
    return Success;
}

int MetaGame::useAction(int player, ActionID id) {
    Game & game = gameStates.back();
    Scene scene = getCurrentScene(game).actualScene();
    ContextType & type = scene.context->context;
    if (std::holds_alternative<RollDice>(type) ||
        std::holds_alternative<TransferCards>(type))
        return WrongContext;
    Side & side = game.sides[player];
    ActionList & actions = side.actions;
    actions.update(scene);
    for (ActionList::iterator it = actions.begin(); it != actions.end(); ++it) {
        std::shared_ptr<const Action> action = *it;
        if (action->info.id == id) {
            if (action->conditions(scene, side.selectedCards)) {
                DeckName deck = game.getDeck(action->host);
                if (deck == Hand) {
                    if (player == game.hazardPlayer && scene.narrowHazardConditions(action->host))
                        scene.payHazard(action->host);
                    game.cards[action->host].faceUp = true;
                    moveCardFast(game, action->host, Pending, player);
                } else if (deck == OnGuard)
                    game.cards[action->host].faceUp = true;
                auto & chain = std::get<ChainOfEffects>(scene.setupChainOfEffects().context->context).chainOfEffects;
                chain.emplace_back(player, action, side.selectedCards);
                game.settings.gameRecord.appendAction(player, side.selectedCards, id);
                side.selectedCards.clear();
                actions.erase(it);    // The action might be reinserted after this declaration is resolved.
                EffectTrigger et = DeclareAction(action->host);
                scene.applyEffects(et, player);
                return Success;
            }
            return IllegalAction;
        }
    }
    return InvalidActionID;
}

std::vector<ActionInfo> MetaGame::getActions(int player, CardID card) {
    std::vector<ActionInfo> actions;
    Game & game = gameStates.back();
    Side & side = game.sides[player];
    Scene scene = getCurrentScene(game).actualScene();
    side.actions.update(scene);
    for (std::shared_ptr<const Action> action : side.actions) {
        if (action->host == card && action->conditions(scene, side.selectedCards))
            actions.push_back(action->info);
    }
    return actions;
}


// Corruption check

int MetaGame::resolveCorruptionCheck(int player, CardID id) {
    Scene scene = currentScene();
    ContextType & context = scene.context->context;
    if (!std::holds_alternative<ResolveCorruptionChecks>(context))
        return WrongContext;
    ResolveCorruptionChecks & resolve = std::get<ResolveCorruptionChecks>(context);
    for (ResolveCorruptionChecks::TargetModifier & target : resolve.unresolvedChecks)
        if (target.id == id){
            Game & game = gameStates.back();
            if (game.cards[target.target].owner != player)
                return WrongPlayer;
            game.settings.gameRecord.append(RecordEntryType::ResolveCorruptionCheck, player, { id });
            scene.setupCorruptionCheck(id);
            return Success;
        }
    return InvalidTarget;
}

void removeDuplicateArguments(std::vector<CardID> & cards) {
    std::sort(cards.begin(), cards.end());
    auto endIt = std::unique(cards.begin(), cards.end());
    cards.resize(std::distance(cards.begin(), endIt));
}
