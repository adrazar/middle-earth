#pragma once
#include "CardAbilities.h"
#include "CardTopology.h"
#include "CardTypes.h"
#include <list>
#include <map>
#include <memory>
#include <unordered_set>


enum Outcome { Unresolved, Cancelled, Failed, Successful, Ineffectual };

struct CorruptionCheck {
    CorruptionCheck()
        : state(Start)
        , roll(0) {}
    std::vector<std::string> getName() { return { "CorruptionCheck" }; }
    enum States { Start, Close };

    States state;
    CardID targetCharacter;
    std::unordered_set<CardID> mayNotSupport; // Characters who have already tapped in support
    int base;
    int modifier;
    int roll;
};

struct ResolveCorruptionChecks {
    std::vector<std::string> getName() { return { "ResolveCorruptionChecks" }; }

    struct TargetModifier {
        TargetModifier(CardID id, CardID target, int modifier = 0)
            : id(id)
            , target(target)
            , modifier(modifier) {}
        CardID id;       // Can normally be the same as target, but cards like Covetous Thoughts must use items here instead
        CardID target;
        int modifier;
    };

    std::list<TargetModifier> unresolvedChecks;
};

struct RingTest {
    RingTest(CardID ring)
        : targetRing(ring)
        , modifier(0)
        , roll(0) {}
    std::vector<std::string> getName() { return { "RingTest" }; }

    const CardID targetRing;
    int modifier;
    int roll;
};

struct InfluenceAttempt {
    InfluenceAttempt()
        : state(Start)
        , roll(0)
        , influenceOpponent(false) {}
    std::vector<std::string> getName() { return { "InfluenceAttempt" }; }
    enum States { Start, Resolve, Take, Close };

    States state;
    CardID influencingCharacter;
    CardID target;
    CardID revealedCard = -1;
    int modifier;
    int requiredInfluence;
    int roll;
    bool influenceOpponent;
};

struct BodyCheck {
    BodyCheck()
        : state(Start)
        , defendingCompany(-1)
        , modifier(0)
        , roll(0) {}
    std::vector<std::string> getName() { return { "BodyCheck" }; }
    enum States { Start, Resolve, Close };

    States state;
    CardID target;
    CompanyID defendingCompany;
    int modifier; // +1 if target is wounded
    int body;
    int roll;
};

struct RemoveCorruptionCard {
    std::vector<std::string> getName() { return { "RemoveCorruptionCard" }; }
    enum States { Start, Close };
    States state = Start;
    CardID target;
    int modifier;
    int requiredNumber;
    int roll;
};

/* Omit this unless the player should click on a roll button
   before resolving the test */
struct SpecialTest {
    SpecialTest(CardID host, int player, CardID target)
        : state(Start)
        , player(player)
        , host(host)
        , target(target)
        , modifier(0) {}
    std::vector<std::string> getName() { return { "SpecialTest" }; }
    enum States { Start, Close };

    States state;
    const int player;
    const CardID host;
    const CardID target;
    int modifier;
    int roll;
};

struct Strike {
    std::vector<std::string> getName() { return { "Strike" }; }
    enum States { PlayHazards, ExcessStrikes, TapCharacter, PlayResources, ResolveStrike, CloseStrike };

    States state = PlayHazards;
    CardID card = -1;
    Race creatureType = Unspecified;
    KeyedToType keyedTo;
    StatType statType = Prowess;
    int creatureBaseProwess = 0;
    int creatureBaseBody = 0;
    CardID target;
    int targetProwessModifier = 0;
    int targetBodyModifier = 0;
    int excessStrikes = 0;
    int strikesSpent = 0;
    int skillCardsUsed = 0;
    int roll = 0;
    bool detainment = false;
    bool mayNotBeCancelled = false;
    bool finalTarget = false;
    std::unordered_set<CardID> mayNotSupport; // Characters who have already tapped in support
    Outcome outcome = Unresolved;
};

struct AssignStrikes {
    AssignStrikes()
        : state(PlayerAssign)
        , undefeatedStrikes(0) {}
    std::vector<std::string> getName() { return { "AssignStrikes" }; }
    enum States { PlayerAssign, OpponentAssign, ResolveStrikes };

    struct StrikeAssignment {
        StrikeAssignment(int player, CardID target)
            : player(player)
            , target(target) {}
        bool operator==(const CardID other) const {
            return other == target;
        }
        int player;    // The number of the player who made the assignment
        CardID target;
    };

    States state;
    CompanyID company;
    CardID card;
    Race creatureType;
    KeyedToType keyedTo;
    StatType statType;
    int prowess;
    int body;
    std::list<StrikeAssignment> assignments;
    std::list<StrikeAssignment> unresolvedStrikes; // Copy of assignments; targets can't tap to support
    int unassignedStrikes;
    bool detainment;
    bool automaticAttack;     // Skip Strike::PlayHazards if true
    bool mayNotBeCancelled;
    bool attackerChoosesDefendingCharacters; // Set to true when the defender is done assigning strikes
    int undefeatedStrikes;
};

struct Attack {
    Attack()
        : state(StartAttack)
        , statType(Prowess)
        , detainment(false)
        , automaticAttack(false)
        , mayNotBeCancelled(false)
        , attackerChoosesDefendingCharacters(false)
        , outcome(Unresolved) {}
    std::vector<std::string> getName() { return { "Attack" }; }
    enum States { StartAttack, CloseAttack };

    States state;
    CompanyID company;
    CardID card;
    Race creatureType;
    KeyedToType keyedTo;
    StatType statType;
    int prowess;
    int body;
    int strikes;
    bool detainment;
    bool automaticAttack;
    bool mayNotBeCancelled;
    bool attackerChoosesDefendingCharacters;
    Outcome outcome;
};

struct Encounter {
    Encounter()
        : state(ResolveAttacks)
        , defeatedAttacks(0)
        , automaticAttack(false) {}
    std::vector<std::string> getName() { return { "Encounter" }; }
    enum States { ResolveAttacks, ResolveEncounter, ReceiveTrophy, CloseEncounter };

    States state;
    CompanyID company;
    CardID card;
    AutomaticAttacks attacks;
    AutomaticAttacks::iterator currentAttack;
    KeyedToType keyedTo;
    std::unordered_set<CardID> trophyCandidates;
    int defeatedAttacks;
    bool automaticAttack;
};

struct Movement {
    Movement() : hazardsPlayed(0) {}
    std::vector<std::string> getName() { return { "Movement" }; }
    CompanyID company;
    CardID siteOfOrigin;
    CardID destination;
    SitePath originalSitePath;
    std::vector<RegionNumber> regions;  // Empty if siteOfOrigin == newLocation
    KeyedToType key;                    // Creatures will be keyed to this (may be changed freely by the hazard player)
    int hazardLimit;
    int hazardsPlayed;
    bool overtCompany;
};

struct Arrival {
    // Used if two or more companies with the same destination can't be combined
    // without violating the limitations for company composition. (MELE, p.61)
    std::vector<std::string> getName() { return { "Arrival" }; }
    std::vector<CompanyID> returnToOrigin;
    CompanyID selectedCompany = -1;
};

struct Explore {
    Explore()
        : state(EnterSite) {}
    std::vector<std::string> getName() { return { "Explore" }; }
    enum States { EnterSite, RevealOnGuard, ExploreSite, Close };

    bool isPlayable(ItemType type) {
        for (ItemType playableType : playable)
            if (type == playableType)
                return true;
        return false;
    }

    States state;
    CompanyID company;
    CardID site;
    std::list<ItemType> playable;  // Item types are treated as not playable for tapped sites
    bool containsHoard;
};

struct TransferItems {
    TransferItems()
        : state(SelectItems)
        , company(-1)
        , unwoundedOnly(true) {}
    std::vector<std::string> getName() { return { "TransferItems" }; }
    enum States { SelectItems, Close };
    States state;
    int player;
    CompanyID company;
    std::vector<CardID> items;
    int allowedTransfers;
    std::map<CardID, CardID> assignments;   // <Item, Character>
    bool unwoundedOnly;
};

struct SelectCompany {
    std::vector<std::string> getName() { return { "SelectCompany" }; }
    std::vector<CompanyID> remainingCompanies;
    CompanyID selectedCompany = -1;
};

struct DrawCards {
    DrawCards()
        : playerCardsDrawn(0)
        , opponentCardsDrawn(0) {}
    std::vector<std::string> getName() { return { "DrawCards" }; }
    CompanyID company;
    int playerLimit;
    int playerCardsDrawn;
    int opponentLimit;
    int opponentCardsDrawn;
};

struct StoreCard {
    std::vector<std::string> getName() { return { "StoreCard" }; }
    CardID target;
};

struct TransferCards {
    struct TransferCardsOption {
        DeckName sourceDeck;
        DeckName destinationDeck;
        std::vector<CardCategory> categories;
        size_t minimum, maximum;
    };

    std::vector<std::string> getName() { return { "TransferCards" }; }
    std::map<int, std::vector<TransferCardsOption>> transactions;
};

struct ExhaustDeck {
    ExhaustDeck(int player)
        : player(player) {}
    std::vector<std::string> getName() { return { "ExhaustDeck" }; }
    const int player;
    std::unordered_set<CardID> fromSideboard, fromDiscardPile;
};

struct ReturnToHandSize {
    std::vector<std::string> getName() { return { "ReturnToHandSize" }; }
    bool resourcePlayerMayDiscard, hazardPlayerMayDiscard;
    size_t resourcePlayerHandSize, hazardPlayerHandSize;
};

struct OrganizeCompanies {
    std::vector<std::string> getName() { return { "OrganizeCompanies" }; }
};

struct DiscardSubset {
    DiscardSubset(int player, size_t subsetSize)
        : player(player)
        , subsetSize(subsetSize) {}
    std::vector<std::string> getName() { return { "DiscardSubset" }; }
    const int player;
    std::vector<CardID> cards;
    std::vector<CardID> selection;
    const size_t subsetSize;
};

struct TapCharacters {
    TapCharacters(int player, int numberOfCharactersToTap)
        : player(player)
        , numberOfCharactersToTap(numberOfCharactersToTap) {}
    std::vector<std::string> getName() { return { "TapCharacters" }; }
    const int player;
    std::vector<CardID> characters;
    int numberOfCharactersToTap;
};

struct RollDice {
    RollDice(int player, int n)
        : player(player)
        , nRolls(n)
        , selectedRoll(-1) {}
    std::vector<std::string> getName() { return { "RollDice" }; }
    const int player;
    std::vector<short> rolls;
    int nRolls;
    int selectedRoll;
};

struct ChainOfEffects {
    struct ChainLink {
        ChainLink(int player, std::shared_ptr<const Action> action, std::list<CardID> & args)
            : player(player)
            , action(action)
            , arguments(args)
        { ; }
        int player;                // The player who declared the action
        std::shared_ptr<const Action> action;
        std::list<CardID> arguments;
        bool resolved = false;
    };

    std::vector<std::string> getName() { return { "ChainOfEffects" }; }
    std::vector<ChainLink> chainOfEffects;
};

struct BottomContext {
    std::vector<std::string> getName() { return { "BottomContext" }; }
};

using ContextType = std::variant<CorruptionCheck, ResolveCorruptionChecks, RingTest, InfluenceAttempt,
    BodyCheck, RemoveCorruptionCard, SpecialTest, Strike, AssignStrikes, Attack, Encounter, Movement,
    Arrival, Explore, TransferItems, SelectCompany, DrawCards, StoreCard, TransferCards,
    ReturnToHandSize, ExhaustDeck, OrganizeCompanies, DiscardSubset, RollDice, ChainOfEffects,
    TapCharacters, BottomContext>;
