#pragma once
#include "ContextInterface.h"
#include "EffectProcessor.h"

/* Removes the action when the host card has left the hand */
struct PlayCardAction : public Action {
    PlayCardAction() {
        info.description = "Play";
    }
    bool remove(Scene scene) const {
        DeckName deckName = scene.game.getDeck(host);
        return deckName != Hand && deckName != Pending && deckName != OnGuard;
    }
};

struct EquipmentAction : public Action {
    bool remove(Scene scene) const {
        return scene.game.getDeck(host) != Possessions;
    }
};

struct CharacterAction : public Action {
    bool remove(Scene scene) const {
        return scene.game.getDeck(host) != Characters;
    }
};

struct PermanentEventAction : public Action {
    bool remove(Scene scene) const {
        return scene.game.getDeck(host) != PermanentEvents;
    }
};

struct PlayCardEffect : public Effect {
    PlayCardEffect(CardID host) : Effect(host) {}
    bool remove(EffectBatch & batch) const {
        DeckName deckName = batch.scene.game.getDeck(host);
        return deckName != Hand && deckName != Pending && deckName != OnGuard;
    }
};

struct LongEventEffect : public Effect {
    LongEventEffect(CardID host) : Effect(host) {}
    bool remove(EffectBatch & batch) const {
        return batch.scene.game.getDeck(host) != LongEvents;
    }
};

struct PermanentEventEffect : public Effect {
    PermanentEventEffect(CardID host) : Effect(host) {}
    bool remove(EffectBatch & batch) const {
        return batch.scene.game.getDeck(host) != PermanentEvents;
    }
};

struct CompanyPermanentEventEffect : public Effect {
    CompanyPermanentEventEffect(CardID host) : Effect(host) {}
    bool remove(EffectBatch & batch) const {
        return batch.scene.game.getDeck(host) != CompanyPermanentEvents;
    }
};

struct CharacterEffect : public Effect {
    CharacterEffect(CardID host)
        : Effect(host) {}
    bool remove(EffectBatch & batch) const {
        return batch.scene.game.getDeck(host) != Characters;
    }
};

struct EquipmentEffect : public Effect {
    EquipmentEffect(CardID host) : Effect(host) {}
    bool remove(EffectBatch & batch) const {
        return batch.scene.game.getDeck(host) != Possessions;
    }
};

struct FactionEffect : public Effect {
    FactionEffect(CardID host) : Effect(host) {}
    bool remove(EffectBatch & batch) const {
        DeckName deckName = batch.scene.game.getDeck(host);
        return deckName != Factions && deckName != Pending;
    }
};

struct UntilEndOfTurnEffect : public Effect {
    UntilEndOfTurnEffect(CardID host) : Effect(host) {}
    bool remove(EffectBatch & batch) const {
        if (!std::holds_alternative<NewPhase>(batch.et))
            return false;
        return batch.scene.game.turnSequence.phase == NextTurn;
    }
};

struct UntilEndOfTurnPossessionEffect : public Effect {
    UntilEndOfTurnPossessionEffect(CardID host) : Effect(host) {}
    bool remove(EffectBatch & batch) const {
        if (!std::holds_alternative<NewPhase>(batch.et))
            return false;
        Game & game = batch.scene.game;
        return (game.turnSequence.phase == NextTurn || game.getDeck(host) != Possessions);
    }
};

struct SpecialEffect : public Effect {
    SpecialEffect(CardID host) : Effect(host) {}
    virtual bool conditions(SpecialTest & test) const = 0;
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Roll>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<SpecialTest>(context))
            return false;
        SpecialTest & test = std::get<SpecialTest>(context);
        if (test.host != host)
            return false;
        return conditions(test);
    }
    bool remove(EffectBatch & batch) const {
        if (!std::holds_alternative<CloseContext>(batch.et))
            return false;
        if (!std::holds_alternative<SpecialTest>(batch.getContext()))
            return false;
        return true;
    }
};

struct HaltProgressEffect : public Effect {
    HaltProgressEffect(CardID host, Phase phase, Part part)
        : Effect(host), turn(phase, part) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<NewPhase>(batch.et))
            return false;
        if (batch.scene.game.turnSequence != turn)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<NewPhase>(batch.et).halt = true;
    }
    bool remove(EffectBatch & batch) const {
        return batch.scene.game.turnSequence > turn;
    }
    const TurnSequence turn;
};

/* Removes the effect at the conclusion of the specified context */
template<class T>
struct ContextEffect : public Effect {
    ContextEffect(CardID host) : Effect(host) {}
    bool remove(EffectBatch & batch) const {
        if (!std::holds_alternative<CloseContext>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<T>(context))
            return false;
        return true;
    }
};
using AttackEffect = ContextEffect<Attack>;
using StrikeEffect = ContextEffect<Strike>;
using ExploreEffect = ContextEffect<Explore>;
using MovementEffect = ContextEffect<Movement>;


/* Operation effects */

struct DisallowOperationEffect : public Effect {
    DisallowOperationEffect(CardID host, CardID target, Operation operation)
        : Effect(host)
        , target(target)
        , operation(operation) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<AllowedOperation>(batch.et))
            return false;
        AllowedOperation & op = std::get<AllowedOperation>(batch.et);
        if (op.card == target && op.operation == operation)
            return true;
        return false;
    }
    void activate(EffectBatch & batch) const {
        std::get<AllowedOperation>(batch.et).allowed = false;
    }
    const CardID target;
    const Operation operation;
};

struct DoNothingDuringSitePhaseEffect : public DisallowOperationEffect {
    DoNothingDuringSitePhaseEffect(CardID host, CardID target)
        : DisallowOperationEffect(host, target, EnterSite) {}
    bool remove(EffectBatch & batch) const {
        if (!std::holds_alternative<CloseContext>(batch.et))
            return false;
        return std::holds_alternative<Explore>(batch.getContext());
    }
};

struct DiscardWhenAnyPlayDeckIsExhausted : public PermanentEventEffect {
    DiscardWhenAnyPlayDeckIsExhausted(CardID host) : PermanentEventEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<OpenContext>(batch.et))
            return false;
        if (!std::holds_alternative<ExhaustDeck>(batch.getContext()))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        batch.scene.discard(host);
    }
};


/* Corruption Cards */

struct IncreaseCorruptionEffect : Effect {
    IncreaseCorruptionEffect(CardID host, int amount)
        : Effect(host), amount(amount) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<CorruptionTotal>(batch.et))
            return false;
        CorruptionTotal & corruptionValues = std::get<CorruptionTotal>(batch.et);
        return corruptionValues.target == host;
    }
    void activate(EffectBatch & batch) const {
        CorruptionTotal & corruptionValues = std::get<CorruptionTotal>(batch.et);
        corruptionValues.corruption += amount;
    }
    bool remove(EffectBatch & batch) const {
        return batch.scene.game.getParent(host) < 0;
    }
    const int amount;
};

struct PlayCorruptionCard : public PlayCardAction {
    PlayCorruptionCard() {}
    PlayCorruptionCard(const std::list<Race> & list)
        : PlayCardAction(), unaffected(list) {}
    void placeCard(Scene scene, CardID target) const {
        scene.gainPossession(target, host);
        scene.game.receivedCorruptionCard.insert(target);
    }
    bool coreConditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.narrowHazardConditions(host))
            return false;
        if (args.empty())
            return false;
        CardID target = args.front();
        Game & game = scene.game;
        if (game.cards[target].owner != game.currentPlayer)
            return false;
        CardType type = game.getCardType(target);
        if (!std::holds_alternative<Character>(type))
            return false;
        if (!game.characterInCompany(target))
            return false;
        if (game.receivedCorruptionCard.find(target) != game.receivedCorruptionCard.end())
            return false;
        if (game.duplicatePossession(target, host))
            return false;
        Race race = std::get<Character>(type).race;
        if (race == Ringwraith)
            return false;           // MELE, p.35, bottom
        if (std::find(unaffected.begin(), unaffected.end(), race) != unaffected.end())
            return false;
        return true;
    }
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        return coreConditions(scene, args);
    }
    const std::list<Race> unaffected;
};

struct RemoveCorruptionCardAction : public Action {
    RemoveCorruptionCardAction(int number)
        : Action(), requiredNumber(number) {}
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        if (game.turnSequence.phase != OrganizationPhase)
            return false;
        CardID target = game.getParent(host);
        Card & card = game.cards[target];
        if (card.owner != game.currentPlayer)
            return false;
        if (card.state != Untapped)
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        CardID target = game.getParent(host);
        scene.tap(target);
        RemoveCorruptionCard context;
        context.target = host;
        context.requiredNumber = requiredNumber;
        context.modifier = 0;
        game.contextStack.push_back(std::make_shared<Context>(context));
        game.contextStack.back()->autoProceed = true;
    }
    bool remove(Scene scene) const {
        return scene.game.getParent(host) < 0;
    }
    const int requiredNumber;
};


/* Hazard Creatures */

struct PlayHazardCreature : public PlayCardAction {
    PlayHazardCreature() {
        info.description = "Attack";
    }
    virtual void insertEffects(Scene scene, const std::list<CardID> & args) const {}
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.hazardConditions(host))
            return false;
        if (!scene.allowedOperation(host, Play))
            return false;
        Game & game = scene.game;
        // Chain of effects must be empty (MELE, p.69, bottom)
        if (game.getDeck(host) == Hand   // - but only before the creature is declared.
            && std::holds_alternative<ChainOfEffects>(scene.context->context))
            return false;
        auto & playable = std::get<HazardCreature>(game.getCardType(host)).playable;
        ContextType & context = scene.context->context;

        if (std::holds_alternative<Movement>(context)) {
            Movement & movement = std::get<Movement>(context);
            auto & playable = std::get<HazardCreature>(game.getCardType(host)).playable;
            if (std::holds_alternative<PathSymbol>(movement.key))
                return sitePathMatch(std::get<PathSymbol>(movement.key), scene.getSitePath(), playable);
            else if (std::holds_alternative<RegionNumber>(movement.key))
                return regionMatch(std::get<RegionNumber>(movement.key), movement.regions, playable);
            else if (std::holds_alternative<SiteType>(movement.key))
                return siteMatch(std::get<SiteType>(movement.key), scene.getSiteType(movement.destination), playable);
            else
                return nameMatch(game.getLocation(movement.destination), playable);
        }

        else if (std::holds_alternative<Explore>(context)) {
            // On-guard
            Explore & explore = std::get<Explore>(context);
            if (explore.state != Explore::RevealOnGuard)
                return false;
            SiteType siteType = scene.getSiteType(explore.site);
            if (nameMatch(game.getLocation(explore.site), playable)
                || siteMatch(siteType, siteType, playable))
                return true;
        }
        return false;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        scene.moveCard(host, Creatures, game.hazardPlayer);
        insertEffects(scene, args);

        auto & playable = std::get<HazardCreature>(game.getCardType(host)).playable;
        ContextType & context = scene.context->context;
        if (std::holds_alternative<Movement>(context)) {
            Movement & movement = std::get<Movement>(context);
            scene.setupEncounter(host, movement.company, movement.key);
        }
        else if (std::holds_alternative<Explore>(context)) {
            Explore & explore = std::get<Explore>(context);
            Location & location = game.getLocation(explore.site);
            KeyedToType key = nameMatch(location, playable) ?
                KeyedToType(location.name) : scene.getSiteType(explore.site);
            scene.setupEncounter(host, explore.company, key);
            scene.setupAutomaticAttack();
        }
    }
};

struct HazardCreatureDefinition : public CardDefinition {
    HazardCreatureDefinition() : CardDefinition(HazardCreature()) {}
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayHazardCreature>(), host);
    }
};

struct HazardProwessBonus : public AttackEffect {
    HazardProwessBonus(CardID host, int bonus)
        : AttackEffect(host), bonus(bonus) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Stat>(batch.et))
            return false;
        Stat & stat = std::get<Stat>(batch.et);
        if (stat.type != Prowess)
            return false;
        Game & game = batch.scene.game;
        if (stat.target >= 0) {
            CardType & type = game.getCardType(stat.target);
            if (std::holds_alternative<Character>(type) || std::holds_alternative<Ally>(type))
                return false;
        }
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<Stat>(batch.et).stat += bonus;
    }
    const int bonus;
};


/* Items */

struct PlayAtSiteAction : public PlayCardAction {
    virtual bool conditions(Scene scene, CardID target, Explore & explore) const = 0;
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        if (!scene.resourceConditions(host))
            return false;
        if (args.empty())
            return false;
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<Explore>(context))
            return false;
        Explore & explore = std::get<Explore>(context);
        if (explore.state != Explore::ExploreSite)
            return false;
        Game & game = scene.game;
        CardID target = args.front();
        if (!std::holds_alternative<Character>(game.getCardType(target)))
            return false;
        if (!game.cards[target].state == Untapped)
            return false;
        if (!game.characterInCompany(target, explore.company))
            return false;
        return conditions(scene, target, explore);
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        CardID target = args.front();
        Explore & explore = std::get<Explore>(scene.context->context);
        scene.tap(target);
        scene.gainPossession(target, host);
        scene.tapSite(explore.site);
        insertPatches(scene.game);
    }
};

struct PlayItemAction : public PlayAtSiteAction {
    bool conditions(Scene scene, CardID target, Explore & explore) const {
        Game & game = scene.game;
        Item & item = game.getItem(host);
        if (item.hoard && !explore.containsHoard)
            return false;
        if (item.unique && !game.uniqueItem(host, item.name))
            return false;
        return explore.isPlayable(item.type);
    }
};

struct PlayAllyAction : public PlayAtSiteAction {
    PlayAllyAction(const std::vector<std::string> & playable)
        : playable(playable) {}
    bool conditions(Scene scene, CardID target, Explore & explore) const {
        Game & game = scene.game;
        if (game.cards[explore.site].state != Untapped)
            return false;
        Ally & ally = std::get<Ally>(game.getCardType(host));
        if (ally.unique && !game.uniqueAlly(host, ally.name))
            return false;
        Location & location = game.getLocation(explore.site);
        for (const std::string & name : playable)
            if (name == location.name) {
                EffectTrigger et = PlayAlly(host, target, true);
                scene.applyEffects(et, game.cards[target].owner, explore.company);
                return std::get<PlayAlly>(et).allowed;
            }
        return false;
    }
    const std::vector<std::string> playable;
};

struct PlayFactionAction : public PlayAtSiteAction {
    virtual void insertEffects(Scene scene, const std::list<CardID> & args) const {}
    bool conditions(Scene scene, CardID target, Explore & explore) const {
        Game & game = scene.game;
        if (game.cards[explore.site].state != Untapped)
            return false;
        Faction & faction = std::get<Faction>(game.getCardType(host));
        if (!game.uniqueFaction(host, faction.name))
            return false;
        if (!scene.matchingHomeSite(faction.homeSite, explore.site))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        insertEffects(scene, args);
        CardID target = args.front();
        // The framework will tap the site if the attempt is successful (MELE, p.42, middle)
        scene.tap(target);
        scene.setupInfluenceAttempt(target, host);
    }
};

struct FactionDefinition : public CardDefinition {
    FactionDefinition() : CardDefinition(Faction()) {}
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayFactionAction>(), host);
    }
};

struct ZeroRequiredInfluence : public FactionEffect {
    ZeroRequiredInfluence(CardID host)
        : FactionEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Stat>(batch.et))
            return false;
        Stat & stat = std::get<Stat>(batch.et);
        if (stat.type != Mind)
            return false;
        if (stat.target != host)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<Stat>(batch.et).stat = 0;
    }
};

struct PlayOrcFactionAction : public PlayFactionAction {
    void activate(Scene scene, const std::list<CardID> & args) const {
        CardID target = args.front();
        scene.tap(target);
        scene.setupInfluenceAttempt(target, host);
        scene.game.effects.push_back(std::make_shared<ZeroRequiredInfluence>(host));
    }
};

struct OrcFactionDefinition : public CardDefinition {
    OrcFactionDefinition() : CardDefinition(Faction()) {}
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayOrcFactionAction>(), host);
    }
};

struct EquipmentBodyBonus : public EquipmentEffect {
    EquipmentBodyBonus(CardID host, short bonus, short max)
        : EquipmentEffect(host), bonus(bonus), max(max) {}
    bool conditions(EffectBatch & batch) const {
        Game & game = batch.scene.game;
        if (game.possessedByRingwraith(host))
            return false;
        if (!std::holds_alternative<Stat>(batch.et))
            return false;
        Stat & stat = std::get<Stat>(batch.et);
        if (stat.type != Body)
            return false;
        if (stat.target != game.getParent(host))
            return false;
        if (stat.stat >= max)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        int & body = std::get<Stat>(batch.et).stat;
        body += bonus;
        if (body > max)
            body = max;
    }
    const short bonus;
    const short max;
};

struct EquipmentInfluenceRaceBonus : public EquipmentEffect {
    EquipmentInfluenceRaceBonus(CardID host, Race race, int bonus)
        : EquipmentEffect(host), race(race), bonus(bonus) {}
    bool conditions(EffectBatch & batch) const {
        Game & game = batch.scene.game;
        if (game.possessedByRingwraith(host))
            return false;
        if (!std::holds_alternative<UnusedDI>(batch.et))
            return false;
        UnusedDI & di = std::get<UnusedDI>(batch.et);
        if (di.target != game.getParent(host))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<UnusedDI>(batch.et).raceDi[race] += bonus;
    }
    const Race race;
    const int bonus;
};

struct OvertPossession : public EquipmentEffect {
    OvertPossession(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<OvertCompany>(batch.et))
            return false;
        OvertCompany & overt = std::get<OvertCompany>(batch.et);
        Game & game = batch.scene.game;
        if (!game.characterInCompany(game.getParent(host), overt.id))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<OvertCompany>(batch.et).overt = true;
    }
};

struct NoDuplicatesInCompanyEffect : public EquipmentEffect {
    NoDuplicatesInCompanyEffect(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<NewPhase>(batch.et))
            return false;
        Game & game = batch.scene.game;
        Company & company = *game.getCurrentSide().findCompany(game.getParent(host), Characters);
        if (game.duplicatePossessionsInCompany(company, host) < 2)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        // Discard until only one copy remains within the company (MELE, p.17, top)
        Game & game = batch.scene.game;
        int dataIndex = game.cards[host].dataIndex;
        Company & company = *game.getCurrentSide().findCompany(game.getParent(host), Characters);
        std::vector<CardID> copies;
        for (CardID character : company.characters) {
            std::vector<CardID> & possessions = game.cards[character].possessions;
            for (CardID possession : possessions)
                if (game.cards[possession].dataIndex == dataIndex)
                    copies.push_back(possession);
        }
        game.contextStack.push_back(std::make_shared<Context>(DiscardSubset(game.currentPlayer, copies.size() - 1)));
        DiscardSubset & selectCompany = std::get<DiscardSubset>(game.getContext());
        selectCompany.cards = std::move(copies);
    }
};


/* Rings */

struct PlayRingSpecialItem : public PlayCardAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<RingTest>(context))
            return false;
        Game & game = scene.game;
        RingTest & ringTest = std::get<RingTest>(context);
        int player = game.cards[host].owner;
        if (game.cards[ringTest.targetRing].owner != player)
            return false;
        EffectTrigger et = RingType(ringTest.targetRing, ringTest.roll + ringTest.modifier);
        scene.applyEffects(et, player, game.getCompanyID(ringTest.targetRing));
        EquipmentType ringType = game.getItem(host).equipmentType;
        auto & testTypes = std::get<RingType>(et).type;
        for (EquipmentType testedType : testTypes)
            if (ringType == testedType)
                return true;
        return false;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        RingTest & ringTest = std::get<RingTest>(scene.context->context);
        Game & game = scene.game;
        CardID ringBearer = game.getParent(ringTest.targetRing);
        scene.discard(ringTest.targetRing);
        if (ringBearer >= 0) {
            scene.gainPossession(ringBearer, host);
            insertPatches(game);
        } else {
            scene.moveCard(host, Stored);   // (MELE, p.45, top)
        }
        scene.context->autoProceed = true;
    }
};

// Example implementation of a Gold Ring item (for potential future reference)
struct FairGoldRingEffect : public EquipmentEffect {
    FairGoldRingEffect(CardID host) : EquipmentEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<RingType>(batch.et))
            return false;
        return std::get<RingType>(batch.et).goldRing == host;
    }
    void activate(EffectBatch & batch) const {
        RingType & ringType = std::get<RingType>(batch.et);
        int result = ringType.testResult;
        if (1 <= result && result <= 6)
            ringType.type.push_back(MagicRing);
        if (result >= 9)
            ringType.type.push_back(DwarvenRing);
        if (result >= 11)
            ringType.type.push_back(TheOneRing);
    }
};
struct PlayFairGoldRing : public PlayItemAction {
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.effects.push_back(std::make_shared<FairGoldRingEffect>(host));
    }
};
struct FairGoldRing : public CardDefinition {
    FairGoldRing() : CardDefinition(Item()) {
        Item & def = std::get<Item>(type);
        def.name.push_back("Fair Gold Ring");
        def.type = GoldRing;
        def.mp = 1;
    }
    void getActions(ActionList & actions, CardID host) {
        actions.add(std::make_shared<PlayFairGoldRing>(), host);
    }
};


/* Locations */

struct LocationDefinition : public CardDefinition {
    LocationDefinition() : CardDefinition(Location()) {}
    void getActions(ActionList & actions, CardID host) {}
};

struct RegionDefinition : public CardDefinition {
    RegionDefinition() : CardDefinition(Region()) {}
    void getActions(ActionList & actions, CardID host) {}
};

struct LocationCardAction : public Action {
    bool remove(Scene scene) const {
        return scene.game.getDeck(host) != LocationDeck;
    }
};

struct LocationCardEffect : public Effect {
    LocationCardEffect(CardID host) : Effect(host) {}
    bool remove(EffectBatch & batch) const {
        return batch.scene.game.getDeck(host) != LocationDeck;
    }
};

struct NonNazgulNotDetainmentAgainstSite : public LocationCardEffect {
    NonNazgulNotDetainmentAgainstSite(CardID host) : LocationCardEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Detainment>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<Attack>(context))
            return false;
        Attack & attack = std::get<Attack>(context);
        if (!std::holds_alternative<SiteType>(attack.keyedTo) &&
            !std::holds_alternative<std::string>(attack.keyedTo))
            return false;
        Game & game = batch.scene.game;
        Side & side = game.getCurrentSide();
        auto it = side.findCompany(attack.company);
        if (it == side.companies.end())
            return false;
        if (it->newLocation != host)
            return false;
        CardType & type = game.getCardType(attack.card);
        if (!std::holds_alternative<HazardCreature>(type))
            return false;
        if (std::get<HazardCreature>(type).race == Nazgul)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<Detainment>(batch.et).detainment = false;
    }
};

struct ChangeSiteType : public UntilEndOfTurnEffect {
    ChangeSiteType(CardID host, CardID site, SiteType siteType)
        : UntilEndOfTurnEffect(host), site(site), siteType(siteType) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<UpdateSiteType>(batch.et))
            return false;
        CardID currentSite = std::get<UpdateSiteType>(batch.et).location;
        if (currentSite != site)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<UpdateSiteType>(batch.et).siteType = siteType;
    }
    const CardID site;
    const SiteType siteType;
};

struct ChangePathSymbol : public MovementEffect {
    ChangePathSymbol(CardID host, PathSymbol affectedSymbol, PathSymbol newSymbol)
        : MovementEffect(host), affectedSymbol(affectedSymbol), newSymbol(newSymbol) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<UpdateSitePath>(batch.et))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        auto & sitePath = std::get<UpdateSitePath>(batch.et).sitePath;
        auto it = std::find(sitePath.begin(), sitePath.end(), affectedSymbol);
        if (it != sitePath.end())
            *it = newSymbol;
    }
    const PathSymbol affectedSymbol;
    const PathSymbol newSymbol;
};


/* Characters */

struct PlayCharacterAction : public PlayCardAction {
    virtual bool playableAtLocation(Scene scene, Character & character, CardID location) const = 0;
    virtual bool rulebookConditions(Scene scene, CardID target) const = 0;

    bool conditions(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        if (args.empty())
            return false;
        CardID target = args.front();
        EffectTrigger et = PlayCharacter(host, target, rulebookConditions(scene, target));
        scene.applyEffects(et, game.currentPlayer, game.getCompanyID(target));
        return std::get<PlayCharacter>(et).allowed;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        Side & side = game.getCurrentSide();
        CardID target = args.front();
        CardType type = game.getCardType(target);
        if (std::holds_alternative<Character>(type)) {
            scene.moveCard(host, Characters, game.currentPlayer, game.getCompanyID(target));
            game.cards[host].parent = target;
        } else {
            CompanyID id = *std::next(args.begin());
            if (side.findCompany(id) == side.companies.end())
                id = scene.addCompany(target);
            scene.moveCard(host, Characters, game.currentPlayer, id);
        }
        game.characterBroughtIntoPlay = host;
        insertPatches(game);
    }
};

/* Normal characters */
struct PlayNonAvatarAction : public PlayCharacterAction {
    virtual bool avatarLocationConflict(Scene scene, CardID target) const = 0;

    bool badAvatarLocation(Scene scene, Race avatar, CardID targetLocation) const {
        Side & side = scene.game.getCurrentSide();
        for (Company & company : side.companies)
            for (CardID character : company.characters)
                if (scene.getRace(character) == avatar)
                    return company.currentLocation != targetLocation;
        return false;
    }

    bool rulebookConditions(Scene scene, CardID target) const {
        Game & game = scene.game;
        if (!scene.resourceConditions(host))
            return false;
        if (!std::holds_alternative<OrganizeCompanies>(scene.context->context))
            return false;
        if (game.characterBroughtIntoPlay >= 0)
            return false;
        Character & character = game.getCharacter(host);
        if (character.unique && !game.uniqueCharacter(host, character.name))
            return false;

        CardType type = game.getCardType(target);
        if (std::holds_alternative<Character>(type)) {
            // Play using direct influence
            if (game.getParent(target) >= 0)
                return false;                 // Followers can't have followers
            Side & side = game.getCurrentSide();
            auto it = side.findCompany(target, Characters);
            if (it == side.companies.end())
                return false;
            if (!playableAtLocation(scene, character, it->currentLocation))
                return false;
            if (scene.getUnusedDI(target, it->id, character.race) < scene.getMind(host, it->id))
                return false;
            return true;
        }
        else if (std::holds_alternative<Location>(type)) {
            // Play using general influence
            if (game.cards[target].deck == DiscardPile)
                return false;
            if (!playableAtLocation(scene, character, target))
                return false;
            if (avatarLocationConflict(scene, target))
                return false;
            return true;
        }
        return false;
    }
};

struct PlayHeroCharacter : public PlayNonAvatarAction {
    bool playableAtLocation(Scene scene, Character & character, CardID locationCard) const {
        Location & location = scene.game.getLocation(locationCard);
        if (location.type == Haven)
            return true;
        if (character.homeSite == location.getName().front())
            return true;
        return false;
    }
    bool avatarLocationConflict(Scene scene, CardID target) const {
        return badAvatarLocation(scene, Wizard, target);
    }
};

struct PlayMinionCharacter : public PlayNonAvatarAction {
    bool playableAtLocation(Scene scene, Character & character, CardID locationCard) const {
        Location & location = scene.game.getLocation(locationCard);
        if (location.type == Darkhaven)
            return true;
        if (character.homeSite == location.getName().front())
            return true;
        if (character.homeSite == "Any Dark-hold" && scene.getSiteType(locationCard) == DarkHold)
            return true;
        if (character.homeSite == "Any site in Imlad Morgul" && location.region == ImladMorgul)
            return true;
        return false;
    }
    bool avatarLocationConflict(Scene scene, CardID target) const {
        return badAvatarLocation(scene, Ringwraith, target);
    }
};

/* Avatars */
struct HalfHazardLimitEffect : public UntilEndOfTurnEffect {
    HalfHazardLimitEffect() : UntilEndOfTurnEffect(-1) {}
    bool conditions(EffectBatch & batch) const {
        return std::holds_alternative<HazardLimit>(batch.et);
    }
    void activate(EffectBatch & batch) const {
        int & hazardLimit = std::get<HazardLimit>(batch.et).hazardLimit;
        hazardLimit = (hazardLimit + 1) / 2;
    }
};

struct TransferHazardsFromSideboard : public CharacterAction {
    TransferHazardsFromSideboard(int player)
        : player(player), round(0) {}
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        if (game.turnSequence.phase != UntapPhase)
            return false;
        if (player != game.hazardPlayer)
            return false;
        if (round == game.turnSequence.round)
            return false;  // This action may only be performed once per round
        if (!game.containsCardCategory(game.sides[player].sideboard, HazardCard))
            return false;
        return true;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        getCurrentScene(game).setupMoveHazardsFromSideboard(player);
        game.effects.push_back(std::make_shared<HalfHazardLimitEffect>());
        auto action = std::make_shared<TransferHazardsFromSideboard>(*this);
        *scene.actionPtr = action;
        action->round = game.turnSequence.round;
    }
    const int player;
    int round;
};

struct PlayAvatarAction : public PlayCharacterAction {
    bool avatarInPlay(Scene scene, Side & side, Race avatar) const {
        for (Company & company : side.companies)
            for (CardID character : company.characters)
                if (scene.getRace(character) == avatar)
                    return true;
        return false;
    }
    bool rulebookConditions(Scene scene, CardID destination) const {
        Game & game = scene.game;
        if (!scene.resourceConditions(host))
            return false;
        if (!std::holds_alternative<OrganizeCompanies>(scene.context->context))
            return false;
        if (game.characterBroughtIntoPlay >= 0)
            return false;
        if (!std::holds_alternative<Location>(game.getCardType(destination)))
            return false;
        Character & character = game.getCharacter(host);
        if (!playableAtLocation(scene, character, destination))
            return false;
        if (avatarInPlay(scene, game.getCurrentSide(), character.race))
            return false;
        int avatarIndex = game.cards[host].dataIndex;
        int revealedAvatar = game.getSide(host).revealedAvatar;
        if (revealedAvatar >= 0 && avatarIndex != revealedAvatar)
            return false;          // May not reveal a different avatar
        if (revealedAvatar < 0)
            for (Side & side : game.sides)
                if (side.revealedAvatar == avatarIndex)
                    return false;  // Avatar aleady revealed by another player
        return true;
    }
    void insertTransferHazardsAction(Game & game, CardID avatar) const {
        int player = game.cards[avatar].owner;
        auto & players = game.turnSequence.remainingPlayers;
        for (auto p : players)
            if (p != player)
                game.sides[p].actions.add(std::make_shared<TransferHazardsFromSideboard>(p), avatar);
    }
};

struct AvatarSideBoardAction : public CharacterAction {
    bool conditions(Scene scene, const std::list<CardID> & args) const {
        Game & game = scene.game;
        if (game.turnSequence.phase != OrganizationPhase)
            return false;
        if (!std::holds_alternative<BottomContext>(game.getContext()))
            return false;
        if (game.getCardState(host) != Untapped)
            return false;
        Side & side = game.getCurrentSide();
        if (game.containsCardCategory(side.sideboard, ResourceCard) ||
            game.containsCardCategory(side.sideboard, CharacterCard))
            return true;
        return false;
    }
    void activate(Scene scene, const std::list<CardID> & args) const {
        scene.tap(host);
        Game & game = scene.game;
        TransferCards context;
        TransferCards::TransferCardsOption option;
        option.sourceDeck = Sideboard;
        option.destinationDeck = DiscardPile;
        option.minimum = 1;
        option.maximum = 5;
        option.categories = { ResourceCard, CharacterCard };
        context.transactions[game.currentPlayer].push_back(option);
        if (game.getCurrentSide().playDeck.size() >= 5) {
            option.destinationDeck = PlayDeck;
            option.maximum = 1;
            context.transactions[game.currentPlayer].push_back(option);
        }
        game.contextStack.push_back(std::make_shared<Context>(context));
    }
};

struct PlayWizardAction : public PlayAvatarAction {
    virtual void wizardAbilities(Game & game) const = 0;
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.revealedAvatar = game.cards[host].dataIndex;
        side.actions.add(std::make_shared<AvatarSideBoardAction>(), host);
        insertTransferHazardsAction(game, host);
        wizardAbilities(game);
    }
    bool playableAtLocation(Scene scene, Character & character, CardID locationCard) const {
        Location & location = scene.game.getLocation(locationCard);
        if (location.name == "Rivendell")
            return true;
        if (character.homeSite == location.getName().front())
            return true;
        if (character.homeSite == "Any Haven" && location.type == Haven)
            return true;
        return false;
    }
};

struct RingwraithAutomaticRingTest : public CharacterEffect {
    RingwraithAutomaticRingTest(CardID host) : CharacterEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<NewPhase>(batch.et))
            return false;
        Game & game = batch.scene.game;
        if (game.turnSequence != TurnSequence(EndOfTurnPhase, Beginning))
            return false;
        Side & side = game.getCurrentSide();
        CompanyID id = game.getCompanyID(host);
        auto it = side.findCompany(id);
        for (CardID character : it->characters) {
            auto & possessions = game.cards[character].possessions;
            for (CardID possession : possessions) {
                CardType & type = game.getCardType(possession);
                if (std::holds_alternative<Item>(type) && std::get<Item>(type).type == GoldRing)
                    return true;
            }
        }
        return false;
    }
    void activate(EffectBatch & batch) const {
        Game & game = batch.scene.game;
        CompanyID id = game.getCompanyID(host);
        auto & characters = game.getCurrentSide().findCompany(id)->characters;
        for (CardID character : characters) {
            auto & possessions = game.cards[character].possessions;
            for (CardID possession : possessions) {
                CardType & type = game.getCardType(possession);
                if (std::holds_alternative<Item>(type) && std::get<Item>(type).type == GoldRing)
                    batch.scene.setupRingTest(possession, 0);
            }
        }
    }
};

struct RingwraithRingTestModifier : public CharacterEffect {
    RingwraithRingTestModifier(CardID host) : CharacterEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Modifier>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<RingTest>(context))
            return false;
        RingTest & ringTest = std::get<RingTest>(context);
        Game & game = batch.scene.game;
        CardID wielder = game.getParent(ringTest.targetRing);
        CompanyID id = game.getCurrentSide().findCompany(wielder)->id;
        if (!game.characterInCompany(host, id))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<Modifier>(batch.et).modifier -= 2;
    }
};

struct RingwraithReturnToHand : public CharacterEffect {
    RingwraithReturnToHand(CardID host) : CharacterEffect(host) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Roll>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<BodyCheck>(context))
            return false;
        BodyCheck & body = std::get<BodyCheck>(context);
        if (body.target != host)
            return false;
        int result = std::get<Roll>(batch.et).result + body.modifier;
        if (result != 7 && result != 8)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        Game & game = batch.scene.game;
        int player = game.cards[host].owner;
        CompanyID id = game.getCompanyID(host);
        batch.scene.returnCardToHand(host, player, id);
    }
};

struct PlayRingwraithAction : public PlayAvatarAction {
    virtual void ringwraithAbilities(Game & game) const = 0;
    void insertPatches(Game & game) const {
        Side & side = game.getSide(host);
        side.revealedAvatar = game.cards[host].dataIndex;
        side.actions.add(std::make_shared<AvatarSideBoardAction>(), host);
        side.effects.push_back(std::make_shared<RingwraithAutomaticRingTest>(host));
        side.effects.push_back(std::make_shared<RingwraithRingTestModifier>(host));
        side.effects.push_back(std::make_shared<RingwraithReturnToHand>(host));
        insertTransferHazardsAction(game, host);
        ringwraithAbilities(game);
    }
    bool playableAtLocation(Scene scene, Character & character, CardID locationCard) const {
        Location & location = scene.game.getLocation(locationCard);
        if (location.name == "Minas Morgul")
            return true;
        if (character.homeSite == location.getName().front())
            return true;
        if (character.homeSite == "Any site in Udûn" && location.region == Udun)
            return true;
        return false;
    }
};

/* Character effects */
struct ProwessBonus : public CharacterEffect {
    ProwessBonus(CardID host, Race race, int bonus)
        : CharacterEffect(host), race(race), bonus(bonus) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Stat>(batch.et))
            return false;
        Stat & stat = std::get<Stat>(batch.et);
        if (stat.type != Prowess)
            return false;
        if (stat.target == host && stat.raceOfOpponent == race)
            return true;
        return false;
    }
    void activate(EffectBatch & batch) const {
        std::get<Stat>(batch.et).stat += bonus;
    }
    const Race race;
    const int bonus;
};

struct CorruptionCheckBonus : public CharacterEffect {
    CorruptionCheckBonus(CardID host, int bonus)
        : CharacterEffect(host), bonus(bonus) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Modifier>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<CorruptionCheck>(context))
            return false;
        if (std::get<CorruptionCheck>(context).targetCharacter != host)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        Modifier & modifier = std::get<Modifier>(batch.et);
        modifier.modifier += bonus;
    }
    const int bonus;
};

struct InfluenceRaceBonus : public CharacterEffect {
    InfluenceRaceBonus(CardID host, Race race, int bonus)
        : CharacterEffect(host), race(race), bonus(bonus) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<UnusedDI>(batch.et))
            return false;
        UnusedDI & di = std::get<UnusedDI>(batch.et);
        if (di.target != host)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<UnusedDI>(batch.et).raceDi[race] += bonus;
    }
    const Race race;
    const int bonus;
};

struct InfluenceFactionBonus : public CharacterEffect {
    InfluenceFactionBonus(CardID host, std::string name, int bonus)
        : CharacterEffect(host), name(name), bonus(bonus) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<UnusedDI>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<InfluenceAttempt>(context))
            return false;
        InfluenceAttempt & influenceAttempt = std::get<InfluenceAttempt>(context);
        if (influenceAttempt.influencingCharacter != host)
            return false;
        CardType & type = batch.scene.game.getCardType(influenceAttempt.target);
        if (!std::holds_alternative<Faction>(type))
            return false;
        if (!name.empty() && std::get<Faction>(type).name.front() != name)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<UnusedDI>(batch.et).di += bonus;
    }
    const std::string name;
    const int bonus;
};

struct InfluenceCharacterBonus : public CharacterEffect {
    InfluenceCharacterBonus(CardID host, std::string name, int bonus)
        : CharacterEffect(host), name(name), bonus(bonus) {}
    bool matchingCharacter(Game & game, CardID target) const {
        CardType & type = game.getCardType(target);
        if (std::holds_alternative<Character>(type)) {
            std::vector<std::string> & names = std::get<Character>(type).name;
            if (std::find(names.begin(), names.end(), name) != names.end())
                return true;
        }
        return false;
    }
    bool influenceAttemptOnTarget(Scene scene) const {
        /*
         *  There are several cases here:
         *  - Target tries to influence a follower/item/ally controlled by the Host
         *  - Host tries to influence Target or a follower/item/ally controlled by Target
         *  In these cases the host should gain the bonus in the form of plain direct influence.
         */
        ContextType & context = scene.context->context;
        if (!std::holds_alternative<InfluenceAttempt>(context))
            return false;
        InfluenceAttempt & influenceAttempt = std::get<InfluenceAttempt>(context);
        Game & game = scene.game;
        if (game.getParent(influenceAttempt.target) == host &&
            matchingCharacter(game, influenceAttempt.influencingCharacter))
            return true;
        if (influenceAttempt.influencingCharacter != host)
            return false;
        if (matchingCharacter(game, influenceAttempt.target))
            return true;
        CardID parent = game.getParent(influenceAttempt.target);
        if (parent < 0)
            return false;
        if (matchingCharacter(game, parent))
            return true;
        return false;
    }
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<UnusedDI>(batch.et))
            return false;
        if (std::get<UnusedDI>(batch.et).target != host)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        UnusedDI & di = std::get<UnusedDI>(batch.et);
        if (!influenceAttemptOnTarget(batch.scene))
            di.characterDi[name] += bonus;
        else
            di.di += bonus;
    }
    const std::string name;
    const int bonus;
};

struct DiscardOnBodyCheck : public CharacterEffect {
    DiscardOnBodyCheck(CardID host, int discardOnResult)
        : CharacterEffect(host), discardOnResult(discardOnResult) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Roll>(batch.et))
            return false;
        ContextType & context = batch.getContext();
        if (!std::holds_alternative<BodyCheck>(context))
            return false;
        BodyCheck & body = std::get<BodyCheck>(context);
        if (body.target != host)
            return false;
        if (std::get<Roll>(batch.et).result + body.modifier != discardOnResult)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        batch.scene.discard(host);
    }
    const int discardOnResult;
};

struct MagicProficiencyEffect : public CharacterEffect {
    MagicProficiencyEffect(CardID host, MagicType type)
        : CharacterEffect(host), type(type) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<MagicProficiency>(batch.et))
            return false;
        MagicProficiency & proficiency = std::get<MagicProficiency>(batch.et);
        if (proficiency.target != host)
            return false;
        if (proficiency.magicType != type)
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<MagicProficiency>(batch.et).proficient = true;
    }
    const MagicType type;
};

struct FellRiderBonus : public CharacterEffect {
    FellRiderBonus(CardID host, int bonus) : CharacterEffect(host), bonus(bonus) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<Stat>(batch.et))
            return false;
        Stat & stat = std::get<Stat>(batch.et);
        if (std::get<Stat>(batch.et).type != Prowess)
            return false;
        if (stat.target != host)
            return false;
        if (!batch.scene.game.hasPossession(host, "Fell Rider"))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<Stat>(batch.et).stat += bonus;
    }
    const int bonus;
};

struct HeraldedLordBonus : public CharacterEffect {
    HeraldedLordBonus(CardID host, int bonus) : CharacterEffect(host), bonus(bonus) {}
    bool conditions(EffectBatch & batch) const {
        if (!std::holds_alternative<UnusedDI>(batch.et))
            return false;
        if (std::get<UnusedDI>(batch.et).target != host)
            return false;
        if (!batch.scene.game.hasPossession(host, "Heralded Lord"))
            return false;
        return true;
    }
    void activate(EffectBatch & batch) const {
        std::get<UnusedDI>(batch.et).di += bonus;
    }
    const int bonus;
};

struct RingwraithModeEffect : public EquipmentEffect {
    RingwraithModeEffect(CardID host, RingwraithMode mode)
        : EquipmentEffect(host), mode(mode) {}
    bool conditions(EffectBatch & batch) const {
        return std::holds_alternative<ActiveRingwraithMode>(batch.et);
    }
    void activate(EffectBatch & batch) const {
        ActiveRingwraithMode & activeMode = std::get<ActiveRingwraithMode>(batch.et);
        activeMode.mode = mode;
        activeMode.modeCard = host;
    }
    const RingwraithMode mode;
};
