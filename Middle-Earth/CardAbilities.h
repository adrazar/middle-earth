#pragma once
#include "CardTopology.h"
#include <memory>
#include <string>

/*
 * Actions and Effects that satisfy both conditions() and remove() will only be removed.
 * That way the applicability conditions will be guaranteed disjoint, which means these
 * functions may be implemented more freely. Actions gain most benefit from this, as
 * Effects can easily be made disjoint by demanding different EffectTriggers. It does allow
 * Effects to rely solely on circumstantial conditions instead of just EffectTriggers, however.
 */

struct Game;               // Middle-Earth.h
struct Scene;              // ContextInterface.h
struct EffectBatch;        // EffectProcessor.h

struct Effect {
    Effect(CardID host)
        : host(host) {}
    const CardID host;
    virtual bool conditions(EffectBatch & batch) const = 0;
    virtual void activate(EffectBatch & batch) const = 0;
    virtual bool remove(EffectBatch & batch) const = 0;
    virtual ~Effect() {}
};

struct EffectListElement {
    EffectListElement(std::shared_ptr<Effect> effect);
    EffectListElement(const EffectListElement & element);
    std::shared_ptr<const Effect> effect;
    bool remove;
};

struct EffectList : public std::list<EffectListElement> {
    void push_back(std::shared_ptr<Effect> effect);
    void remove(CardID card);  // Marks elements for removal
    void remove();             // Removes all elements marked for removal
    bool matchingEffectHost(CardID card);
};


using ActionID = int;

struct ActionInfo {
    ActionID id;
    std::string description;
};

struct Action {
    CardID host;
    ActionInfo info;
    virtual bool conditions(Scene scene, const std::list<CardID> & args) const = 0;
    virtual void activate(Scene scene, const std::list<CardID> & args) const = 0;
    virtual bool remove(Scene scene) const = 0;
    virtual void insertPatches(Game & game) const;   // Inserts any belonging Actions and/or Effects
    virtual ~Action() {}
};

struct ActionList : public std::list<std::shared_ptr<const Action>> {
    void add(std::shared_ptr<Action>, CardID host);
    void add(std::shared_ptr<Action>, CardID host, const std::string description);
    void update(Scene scene);
    ActionID nextID = 0;       // One ActionList per player => nextID can be non-static
};
