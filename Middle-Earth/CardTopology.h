#pragma once
#include <algorithm>
#include <cstdlib>
#include <list>
#include <vector>

using CardID = short;
using CompanyID = short;

enum CardState { Untapped, Tapped, Wounded };
enum ParentState { NoParent = -1, HadParent = -2 };

enum DeckName {
    Hand, PlayDeck, DiscardPile, LocationDeck, Sideboard, OutOfPlay, Eliminated, Kills,
    Stored, Factions, PermanentEvents, LongEvents, Pending, Creatures,
    InCompany, Characters, CompanyPermanentEvents, OnGuard, Possessions, Sites
};

struct Card {
    Card(int dataIndex, int owner)
        : dataIndex(dataIndex)
        , owner(owner)
        , deck(PlayDeck)
        , faceUp(false)
        , state(Untapped)
        , parent(NoParent)
    { ; }
    const int dataIndex;            // Pointer to card definition
    const int owner;                // The owner's player number
    DeckName deck;                  // Indication of where the card can be found
    bool faceUp;                    // Face up, face down
    CardState state;                // Untapped, tapped, wounded
    CardID parent;                  // Controlling Card (mainly Characters, but also cards like Crown of Flowers)
    std::vector<CardID> possessions;  // (NOT Followers!) The cards having this card as parent
};

struct CompanyCards {
    std::vector<CardID> & getDeck(DeckName name) {
        switch (name) {
        case Characters:
            return characters;
        case OnGuard:
            return onGuard;
        default:
            return permanentEvents;
        }
    }
    CompanyID id;
    std::vector<CardID> characters;
    std::vector<CardID> permanentEvents;
    CardID currentLocation;
    CardID newLocation;
    std::vector<CardID> onGuard;
};

template <class CompanyData = CompanyCards>
struct SideCards {
    std::vector<CardID> & getDeck(DeckName name) {
        switch (name) {
        case Hand:
            return hand;
        case PlayDeck:
            return playDeck;
        case DiscardPile:
            return discardPile;
        case LocationDeck:
            return locationDeck;
        case Sideboard:
            return sideboard;
        case OutOfPlay:
            return outOfPlay;
        case Eliminated:
            return eliminated;
        case Kills:
            return kills;
        case Stored:
            return stored;
        case Factions:
            return factions;
        case PermanentEvents:
            return permanentEvents;
        case LongEvents:
            return longEvents;
        case Creatures:
            return creatures;
        default:
            return pending;
        }
    }
    std::list<CompanyData> companies;
    std::vector<CardID> hand, playDeck, discardPile, locationDeck, sideboard;
    std::vector<CardID> factions, kills, stored, eliminated, outOfPlay, pending;
    std::vector<CardID> permanentEvents, longEvents, creatures;
};
