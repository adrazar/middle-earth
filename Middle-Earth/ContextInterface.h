#pragma once
#include "EffectTrigger.h"
#include "Middle-Earth.h"
#include <unordered_set>

struct Scene {
    Scene(Game & game, int contextIndex);

    bool allowedOperation(CardID card, Operation operation, bool allowed = true);
    bool transferAllowed(CardID item, CardID recipient);
    void tap(CardID card);
    void untap(CardID card);
    void heal(CardID card);
    void wound(CardID card);
    void target(CardID card);
    void tapSite(CardID card);
    void untapSite(CardID card);
    void drawCard(int player);
    void discard(CardID card, int hostPlayer = -1);
    void eliminate(CardID card);
    void removePlayer(int player);
    void stripCharacter(CardID target, CompanyID id);
    void moveCard(CardID card, DeckName destination, int recipientPlayer = -1, CompanyID company = -1);
    void returnCardToHand(CardID card, int player, CompanyID id);
    void gainPossession(CardID target, CardID possession);
    CompanyID addCompany(CardID location, Side & side);
    CompanyID addCompany(CardID location);
    void combineCompanies(int player, CompanyID source, CompanyID destination);
    void removeCompany(int player, CompanyID company);
    void transferCharacter(CardID card, CompanyID id);
    void dropFollowers(CardID card);
    void setNewLocation(CompanyID company, CardID location);
    void arriveAtLocation(Company & movingCompany);
    void returnToOrigin(Company & movingCompany);
    void payHazard(CardID card);
    void removeCardEffects(CardID card, int player = -1, CompanyID id = -1);
    void discardRingwraithModeCard(int player);
    void forceQuitCompany();
    void cancelAttack();
    void shuffleDeck(Side & side);
    int generateRoll();

    int getModifier(int playerNumber, CompanyID id);
    int getProwess(CardID card, Race raceOfOpponent, CompanyID id);
    int getBody(CardID card, Race raceOfOpponent, CompanyID id);
    int getMind(CardID card, CompanyID id);
    int getStat(StatType type, CardID card, Race raceOfOpponent, CompanyID id);
    int getUnusedDI(CardID card, CompanyID id, Race race);
    int getUnusedGI(int player);
    int getHandSize(int player);
    int getMP(CardID card, int player);
    MarshallingCount getTotalMP(int player);
    int getCreatureStat(StatType type, CardID card, Race race, int base);
    Race getRace(CardID card);
    Skills getSkills(CardID card, int playerNumber, CompanyID id);
    SiteType getSiteType(CardID card);
    SitePath getSitePath();
    size_t getRegionLimit(int player, CompanyID id);
    std::unordered_set<RegionNumber> getAdjacentRegions(RegionNumber regionNumber);
    ActiveRingwraithMode getRingwraithMode(int player);
    int getTargetProwess(Strike & strike);
    int getTargetBody(Strike & strike);
    int strikeResult(Strike & strike);
    int getNumberOfTargets(Company & company);
    int getCompanySize(Company & company);
    int getHazardLimitCost(CardID card);
    int getTrophyPoints(CardID target);
    int trophyProwessBonus(CardID target, int currentProwess);
    int trophyInfluenceBonus(CardID target);
    bool healingSite(CardID location);
    bool matchingHomeSite(const std::string & homeSite, CardID location);
    bool hasSkill(CardID card, Skill skill);
    bool hasKeyword(CardID card, EventKeyword keyword);
    bool overtCompany(Company & company, int player = -1);
    int verifyCompanyComposition(Company & company);
    bool verifyArrivingCompanies();
    bool resourceConditions(CardID card);
    bool hazardConditions(CardID card);
    bool narrowHazardConditions(CardID card);   // Can't be revealed when on-guard
    bool mayCallCouncil(int player);

    bool proceedTurnSequence();
    void startNewTurn();
    bool closeContext(int player = -1, CompanyID id = -1);
    void insertPatches(CardID card);
    void applyEffects(EffectTrigger & et, int player = -1, CompanyID id = -1);
    void applyAllEffects(EffectTrigger & et, int player = -1);

    Scene setupStrike(CardID target);
    void autoAssignStrikes(Company & company);
    Scene setupAssignStrikes();
    Scene setupAttack();
    Scene setupEncounter(CardID card, CompanyID company, KeyedToType key);
    bool setupAutomaticAttack();
    Scene setupBodyCheck(CardID target, int body);
    Scene setupTransferItems(const std::vector<CardID> & items);
    Scene setupMovement(CompanyID id);
    Scene setupExplore(CompanyID id);
    Scene setupSelectCompanies();
    Scene setupCorruptionCheck(CardID id);
    Scene setupResolveCorruptionChecks(CardID id, CardID target, int modifier = 0, int repetitions = 1);
    Scene setupInfluenceAttempt(CardID influencingCharacter, CardID target);
    Scene setupRingTest(CardID ring, int modifier, int rolls = 1);
    Scene setupSpecialTest(CardID host, int rollingPlayer, CardID target);
    Scene setupStoreCard(CardID card);
    Scene setupMoveHazardsFromSideboard(int player);
    Scene setupDrawCards(CompanyID id);
    Scene setupReturnToHandSize(bool mayDiscard);
    Scene setupRollDice(int player, int rolls = 1);
    Scene setupChainOfEffects();
    Scene actualScene();  // Returns parent if current context is ChainOfEffects
    Scene parent();

    Game & game;
    std::shared_ptr<Context> context;
    int contextIndex;
    std::shared_ptr<const Action> * actionPtr;
};

struct MarshallingCounter {
    MarshallingCounter(Scene scene, int player)
        : scene(scene)
        , player(player)
        , deckType(scene.game.sides[player].deckType)
    { ; }
    void updateCount(CardID card);
    void operator()(Character & character);
    void operator()(Ally & ally);
    void operator()(Item & item);
    void operator()(Faction & faction);
    void operator()(ResourceEvent & resource);
    void operator()(HazardCreature & creature);
    void operator()(HazardEvent & hazard);
    void operator()(Location & location);
    void operator()(Region & region);

    Scene scene;
    const int player;
    const DeckType deckType;
    MarshallingCount count;
    CardID target;
    int mp;
};

void moveCardFast(Game & game, CardID target, DeckName destination, int recipientPlayer);
void removeLocation(Scene & scene, CardID location);
void removeEffects(Game & game);
Scene getCurrentScene(Game & game);
