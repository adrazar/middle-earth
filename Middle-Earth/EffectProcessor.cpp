#include "EffectProcessor.h"

void EffectBatch::traverseList(EffectList & effects) {
    for (EffectList::iterator it = effects.begin(); it != effects.end(); ++it) {
        auto effect = it->effect;
        if (effect->remove(*this))
            it->remove = true;
        else if (!it->remove && effect->conditions(*this))
            effectQueue.push_back(it);
    }
}
void EffectBatch::collect(int player, CompanyID id) {
    Game & game = scene.game;
    traverseList(game.effects);
    if (player >= 0) {
        Side & side = game.sides[player];
        traverseList(side.effects);
        if (id >= 0) {
            Company & company = *side.findCompany(id);
            traverseList(company.effects);
        }
    }
}
void EffectBatch::collectSide(Side & side) {
    traverseList(side.effects);
    for (Company & company : side.companies)
        traverseList(company.effects);
}
void EffectBatch::collectAll(int player) {
    Game & game = scene.game;
    traverseList(game.effects);
    if (player < 0)
        for (Side & side : game.sides)
            collectSide(side);
    else
        collectSide(game.sides[player]);
}
void EffectBatch::execute() {
    while (!effectQueue.empty()) {
        EffectListElement & element = *effectQueue.front();
        auto effect = element.effect;
        if (effect->remove(*this))
            element.remove = true;
        if (!element.remove && effect->conditions(*this))
            effect->activate(*this);
        effectQueue.pop_front();
    }
}
void EffectBatch::execute(CardID card) {
    auto it = effectQueue.begin();
    while (it != effectQueue.end()) {
        EffectListElement & element = **it;
        if (element.effect->host == card) {
            auto effect = element.effect;
            if (effect->remove(*this))
                element.remove = true;
            if (!element.remove && effect->conditions(*this))
                effect->activate(*this);
            it = effectQueue.erase(it);
        }
        else {
            ++it;
        }
    }
}
ContextType & EffectBatch::getContext() {
    return scene.context->context;
}

void EffectProcessor::registerListener(std::unique_ptr<EffectTriggerListener> && listener) {
    listeners.emplace_back(std::move(listener));
}
void EffectProcessor::broadcast(EffectBatch & batch) {
    batch.execute();
    for (auto & listener : listeners)
        listener->triggerHandler(batch);
}
