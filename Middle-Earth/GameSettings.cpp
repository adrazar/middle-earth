#include "EffectProcessor.h"
#include "EventListeners.h"
#include "GameSettings.h"
#include <chrono>

GameSettings::GameSettings()
    : rnd(unsigned int(std::chrono::system_clock::now().time_since_epoch().count()))
    , effectProcessor(std::make_unique<EffectProcessor>())
    , gameLength(GameLength::ShortGame)
{ ; }
GameSettings::~GameSettings() = default;

void GameSettings::addCardDefinition(std::unique_ptr<CardDefinition> && cardDefinition) {
    std::string lookupName = cardLookupName(cardDefinition->type);
    auto it = cardNameToDefinitionIndex.find(lookupName);
    // Only one definition allowed for a single card
    if (it == cardNameToDefinitionIndex.end()) {
        int dataIndex = int(cardDefinitions.size());
        cardDefinitions.push_back(std::move(cardDefinition));
        cardNameToDefinitionIndex[lookupName] = dataIndex;
    }
}
int GameSettings::getDataIndex(const std::string & cardName) {
    auto it = cardNameToDefinitionIndex.find(cardName);
    return (it != cardNameToDefinitionIndex.end()) ? it->second : -1;
}

struct MinionGetter {
    template <class T>
    bool operator()(T & t) {
        return t.isMinion();
    }
};
bool isMinion(CardType & type) {
    return std::visit(MinionGetter{}, type);
}

std::vector<std::string> getName(CardType & type) {
    return std::visit(NameGetter{}, type);
}

struct MPGetter {
    template <class T>
    int operator()(T & t) {
        return t.getMP();
    }
};

int getBaseMP(CardType & type) {
    return std::visit(MPGetter{}, type);
}

std::string cardLookupName(CardType & type) {
    std::string name = getName(type).front();
    if (isMinion(type))
        name += "*";
    return name;
}
