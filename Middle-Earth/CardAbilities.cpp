#include "CardAbilities.h"
#include "ContextInterface.h"

EffectListElement::EffectListElement(std::shared_ptr<Effect> effect)
    : effect(effect)
    , remove(false)
{ ; }
EffectListElement::EffectListElement(const EffectListElement & element)
    : effect(element.effect)
    , remove(element.remove)
{ ; }

void EffectList::push_back(std::shared_ptr<Effect> effect) {
    emplace_back(effect);
}
void EffectList::remove(CardID card) {
    for (auto it = begin(); it != end(); ++it)
        if (it->effect->host == card)
            it->remove = true;
}
void EffectList::remove() {
    auto it = begin();
    while (it != end())
        it->remove ? it = erase(it) : ++it;
}
bool EffectList::matchingEffectHost(CardID card) {
    for (auto it = begin(); it != end(); ++it)
        if (!it->remove && it->effect->host == card)
            return true;
    return false;
}

void ActionList::add(std::shared_ptr<Action> action, CardID host) {
    action->host = host;
    action->info.id = nextID++;
    push_back(action);
}
void ActionList::add(std::shared_ptr<Action> action, CardID host, const std::string description) {
    action->host = host;
    action->info.id = nextID++;
    action->info.description = description;
    push_back(action);
}
void ActionList::update(Scene scene) {
    auto it = begin();
    while (it != end())
        (*it)->remove(scene) ? it = erase(it) : ++it;
}
void Action::insertPatches(Game & game) const {}
