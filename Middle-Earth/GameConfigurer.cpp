#include "ContextInterface.h"
#include "GameConfigurer.h"
#include "GameInterface.h"
#include <cassert>
#include <iostream>

// Print new cards

CardID PlayerConfigurer::makeCard(std::string name) {
    auto it = game().settings.cardNameToDefinitionIndex.find(name);
    if (it == game().settings.cardNameToDefinitionIndex.end()) {
        std::cout << "No card with name " << name << std::endl;
        std::abort();
    }
    int definitionIndex = it->second;
    CardID card = CardID(game().cards.size());
    game().cards.emplace_back(definitionIndex, player);
    return card;
}

CardID PlayerConfigurer::addCardToDeck(std::string name, DeckName deckName) {
    assert(int(game().sides.size()) > 0);
    CardID card = makeCard(name);
    game().setHostingDeck(card, player, deckName);
    getSide().getDeck(deckName).push_back(card);
    return card;
}

CardID PlayerConfigurer::addCardToHand(std::string cardName) {
    CardID card = addCardToDeck(cardName);
    gameConfigurer.drawCard(card);
    return card;
}

void PlayerConfigurer::addCardsToDeck(DeckName deckName, const DeckDescription & cards) {
    for (const NameNumberParam & param : cards) {
        int n = param.n;
        while (n--)
            addCardToDeck(param.name, deckName);
    }
}

void PlayerConfigurer::addCompany(std::string locationName) {
    assert(int(game().sides.size()) > 0);
    CardID location = addCardToDeck(locationName, LocationDeck);
    getCurrentScene(game()).addCompany(location, getSide());
    game().setHostingDeck(location, gameConfigurer.owner(), Sites);
    game().cards[location].faceUp = true;
}

CardID PlayerConfigurer::addCharacterToCompany(std::string name) {
    assert(int(game().sides.size()) > 0);
    assert(int(getSide().companies.size()) >= 1);
    CardID card = makeCard(name);
    getSide().companies.back().characters.push_back(card);
    game().setHostingDeck(card, player, Characters);
    game().cards[card].faceUp = true;
    gameConfigurer.insertPatches(card);
    return card;
}

CardID PlayerConfigurer::addStartingItem(std::string name, CardID parent) {
    CardID card = makeCard(name);
    Card & item = game().cards[card];
    Card & character = game().cards[parent];
    character.possessions.push_back(card);
    game().setHostingDeck(card, player, Possessions);
    item.faceUp = true;
    item.parent = parent;
    gameConfigurer.insertPatches(card);
    return card;
}

CardID PlayerConfigurer::addStartingPermanentEvent(std::string name, bool inCompany) {
    CardID card = makeCard(name);
    Card & item = game().cards[card];
    inCompany ? getSide().companies.back().permanentEvents.push_back(card)
              : getSide().permanentEvents.push_back(card);
    game().setHostingDeck(card, player, PermanentEvents);
    item.faceUp = true;
    gameConfigurer.insertPatches(card);
    return card;
}

void PlayerConfigurer::loadDeck(const DeckDefinition & deck) {
    getSide().deckType = deck.deckType;
    addCardsToDeck(PlayDeck, deck.playDeck);
    addCardsToDeck(LocationDeck, deck.locationDeck);
    addCardsToDeck(Sideboard, deck.sideboard);
    const StartingCompany & company = deck.startingCompany;
    addCompany(company.startingLocation);
    for (const StartingCharacter & startingCharacter : company.startingCharacters) {
        CardID character = addCharacterToCompany(startingCharacter.character);
        if (!startingCharacter.startingItem.empty())
            addStartingItem(startingCharacter.startingItem, character);
    }
    for (const StartingPermanentEvent & permanentEvent : company.startingPermanentEvents)
        addStartingPermanentEvent(permanentEvent.permanentEvent, permanentEvent.inCompany);
    game().settings.decks.push_back(deck);
}

Side & PlayerConfigurer::getSide() {
    return gameConfigurer.game().sides[player];
}

Game & PlayerConfigurer::game() {
    return gameConfigurer.game();
}

// Use existing cards

CompanyID PlayerConfigurer::addCompany(CardID location) {
    return getCurrentScene(game()).addCompany(location, getSide());
}

CardID PlayerConfigurer::drawCard(const std::string & cardName) {
    CardID card = getCardID(gameConfigurer.game(), player, cardName);
    gameConfigurer.drawCard(card);
    return card;
}

CardID PlayerConfigurer::putOnTopOfPlayDeck(const std::string & cardName) {
    CardID card = getCardID(gameConfigurer.game(), player, cardName);
    moveCardFast(gameConfigurer.game(), card, PlayDeck, player);
    return card;
}

void PlayerConfigurer::drawCard(CardID card) {
    gameConfigurer.drawCard(card);
}

void PlayerConfigurer::putOnTopOfPlayDeck(CardID card) {
    moveCardFast(gameConfigurer.game(), card, PlayDeck, player);
}

void PlayerConfigurer::addCharacterToCompany(CompanyID id, CardID character) {
    pickUpCard(game(), character);
    getSide().findCompany(id)->characters.push_back(character);
    Card & card = game().cards[character];
    game().setHostingDeck(character, player, Characters);
    card.faceUp = true;
    gameConfigurer.insertPatches(character);
}

void PlayerConfigurer::setCurrentLocation(CompanyID id, CardID location) {
    Company & company = *getSide().findCompany(id);
    CardID origin = company.currentLocation;
    CardID destination = company.newLocation;
    company.newLocation = company.currentLocation = location;
    game().cards[location].faceUp = true;
    Scene scene = getCurrentScene(game());
    removeLocation(scene, origin);
    removeLocation(scene, destination);
}

void PlayerConfigurer::addFaction(CardID faction) {
    pickUpCard(game(), faction);
    getSide().factions.push_back(faction);
    Card & card = game().cards[faction];
    game().setHostingDeck(faction, player, Factions);
    card.faceUp = true;
}

void PlayerConfigurer::addLongEvent(CardID longEvent, const std::string & action) {
    pickUpCard(game(), longEvent);
    getSide().longEvents.push_back(longEvent);
    Card & card = game().cards[longEvent];
    game().setHostingDeck(longEvent, player, LongEvents);
    card.faceUp = true;
    gameConfigurer.insertPatches(longEvent, action);
}

void PlayerConfigurer::addPermanentEvent(CardID permanentEvent, const std::string & action) {
    pickUpCard(game(), permanentEvent);
    getSide().permanentEvents.push_back(permanentEvent);
    Card & card = game().cards[permanentEvent];
    game().setHostingDeck(permanentEvent, player, PermanentEvents);
    card.faceUp = true;
    gameConfigurer.insertPatches(permanentEvent, action);
}

void PlayerConfigurer::addPermanentEvent(CompanyID id, CardID permanentEvent, const std::string & action) {
    pickUpCard(game(), permanentEvent);
    getSide().findCompany(id)->permanentEvents.push_back(permanentEvent);
    Card & card = game().cards[permanentEvent];
    game().setHostingDeck(permanentEvent, player, CompanyPermanentEvents);
    card.faceUp = true;
    gameConfigurer.insertPatches(permanentEvent, action);
}

void PlayerConfigurer::insertLocationEffects() {
    Side & side = game().sides[player];
    for (CardID location : side.locationDeck)
        game().settings.cardDefinitions[game().cards[location].dataIndex]->getEffects(side.effects, location);
}

// Use existing cards

void GameConfigurer::drawCard(CardID card) {
    const Card & cardData = game().cards[card];
    Side & side = game().sides[cardData.owner];
    getCurrentScene(game()).moveCard(card, Hand, cardData.owner);
    game().settings.cardDefinitions[cardData.dataIndex]->getActions(side.actions, card);
}

void GameConfigurer::setCardState(CardID target, CardState state) {
    game().cards[target].state = state;
}

void GameConfigurer::addPossession(CardID target, CardID possession) {
    getCurrentScene(game()).gainPossession(target, possession);
    insertPatches(possession);
}


void GameConfigurer::insertPatches(CardID target, const std::string & actionDescription) {
    ActionList actions;
    int dataIndex = game().cards[target].dataIndex;
    game().settings.cardDefinitions[dataIndex]->getActions(actions, target);
    if (actionDescription.empty()) {
        actions.back()->insertPatches(game());
    } else {
        auto it = std::find_if(actions.begin(), actions.end(), [&actionDescription](std::shared_ptr<const Action> & action) {
            return action->info.description == actionDescription;
        });
        if (it == actions.end()) {
            std::cout << "Action description '" << actionDescription <<  "' did not match any actions" << std::endl;
            std::abort();
        }
        (*it)->insertPatches(game());
    }
}

PlayerConfigurer GameConfigurer::addPlayer() {
    game().addSide();
    game().turnSequence.remainingPlayers.push_back(owner());
    return PlayerConfigurer(*this, owner());
}

int GameConfigurer::owner() {
    return int(game().sides.size()) - 1;
}

Game & GameConfigurer::game() {
    return metaGame.gameStates.back();
}

PlayerConfigurer GameConfigurer::currentPlayerConfiguerer() {
    return PlayerConfigurer(*this, metaGame.gameStates.back().currentPlayer);
}


CardID getCardID(Game & game, int owner, const std::string & cardName, DeckName excludedDeck) {
    // Goes through the *entire* game.cards and returns the first matching CardID
    // => general but inefficient.
    int dataIndex = game.settings.getDataIndex(cardName);
    if (dataIndex >= 0)
        for (CardID id = 0; id < CardID(game.cards.size()); id++) {
            Card & card = game.cards[id];
            if (card.dataIndex == dataIndex && card.owner == owner && card.deck != excludedDeck)
                return id;
        }
    return -1;
}

CardID getLocationID(Game & game, int player, const std::string & locationName) {
    int dataIndex = game.settings.getDataIndex(locationName);
    assert(dataIndex >= 0);
    CardID location = game.findSiteID(game.sides[player], dataIndex);
    if (location < 0) {
        for (CardID location : game.sides[player].discardPile)
            if (game.cards[location].dataIndex == dataIndex)
                return location;
    }
    return location;
}

void printPatchCount(Game & game) {
    std::cout << "Global Effects: " << game.effects.size() << std::endl;
    int player = 1;
    for (Side & side : game.sides) {
        std::cout << "Player " << player++ << ":" << std::endl;
        std::cout << "  Effects: " << side.effects.size() << std::endl;
        std::cout << "  Actions: " << side.actions.size() << std::endl;
        for (Company & company : side.companies)
            std::cout << "  Company " << company.id << ": " << company.effects.size() << std::endl;
    }
}

void listActions(Game & game, int player) {
    ActionList & actions = game.sides[player].actions;
    for (auto action : actions)
        std::cout << action->info.id << ": "
        << (cardLookupName(game.getCardType(action->host))) << " ("
        << action->host << ") "
        << action->info.description << std::endl;
}
