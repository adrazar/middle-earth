#include "ContextInterface.h"
#include "EffectProcessor.h"
#include "ErrorCodes.h"

int getCorruption(Scene & scene, CardID card, CompanyID id);
int getCorruptionTotal(Scene & scene, CardID card, CompanyID id);
DrawCardsLimit getDrawCardsLimit(Scene & scene, CompanyID id);
void performMoveCard(Scene & scene, MoveCard & moveCard, int fromPlayer, CompanyID sourceCompany);

Scene::Scene(Game & game, int contextIndex)
    : game(game)
    , context(game.contextStack[contextIndex])
    , contextIndex(contextIndex)
{ ; }

bool Scene::allowedOperation(CardID card, Operation operation, bool allowed) {
    AllowedOperation test(card, operation, allowed);
    EffectTrigger et = test;
    applyEffects(et, game.currentPlayer);
    return std::get<AllowedOperation>(et).allowed;
}
bool Scene::transferAllowed(CardID item, CardID recipient) {
    TransferAllowed transfer(item, recipient);
    EffectTrigger et = transfer;
    applyEffects(et, game.currentPlayer);
    return std::get<TransferAllowed>(et).allowed;
}
void Scene::tap(CardID card) {
    game.cards[card].state = Tapped;
}
void Scene::untap(CardID card) {
    if (allowedOperation(card, Untap))
        game.cards[card].state = Untapped;
}
void Scene::heal(CardID card) {
    if (allowedOperation(card, Heal))
        game.cards[card].state = Tapped;
}
void Scene::wound(CardID card) {
    game.cards[card].state = Wounded;
}
bool allowedTarget(Game & game, CardID target, AssignStrikes & assign) {
    if (!assign.attackerChoosesDefendingCharacters && game.getCardState(target) != Untapped)
        return false;
    for (AssignStrikes::StrikeAssignment & assignment : assign.assignments)
        if (assignment.target == target)
            return false;
    return true;
}
void Scene::target(CardID card) {
    CardType & type = game.getCardType(card);
    if (!(std::holds_alternative<Character>(type) || std::holds_alternative<Ally>(type)))
        return;
    if (!std::holds_alternative<AssignStrikes>(context->context))
        return;
    AssignStrikes & assign = std::get<AssignStrikes>(context->context);
    CardID parent = game.getParent(card);
    if (!game.characterInCompany((parent < 0) ? card : parent, assign.company))
        return;
    if (!allowedOperation(card, Target, allowedTarget(game, card, assign)))
        return;
    if (assign.unassignedStrikes > 0) {
        int player = assign.attackerChoosesDefendingCharacters ?
            game.hazardPlayer : game.currentPlayer;
        assign.assignments.emplace_back(player, card);
        assign.unassignedStrikes--;
    }
}
void Scene::tapSite(CardID card) {
    if (std::holds_alternative<Explore>(context->context)) {
        Explore & explore = std::get<Explore>(context->context);
        if (game.cards[card].state == Untapped)
            explore.playable = { Minor };    // Perhaps playability of Information persists
        else
            explore.playable.clear();
    }
    tap(card);
}
void Scene::untapSite(CardID card) {
    untap(card);
    if (game.getCardState(card) != Untapped)
        return;
    ContextType & context = game.getContext();
    if (!std::holds_alternative<Explore>(context))
        return;
    Explore & explore = std::get<Explore>(context);
    if (explore.site == card) {
        Location & location = game.getLocation(card);
        for (ItemType type : location.playable)
            explore.playable.push_back(type);
    }
}
void insertCardPatches(Scene scene, CardID target) {
    Game & game = scene.game;
    Side & side = game.getSide(target);
    CardDefinition & definition = *game.settings.cardDefinitions[game.cards[target].dataIndex];
    definition.getActions(side.actions, target);
    definition.getEffects(side.effects, target);
}
void Scene::drawCard(int player) {
    Side & side = game.sides[player];
    CardID card = side.playDeck.back();
    Card & cardData = game.cards[card];
    side.playDeck.pop_back();
    side.hand.push_back(card);
    game.setHostingDeck(card, player, Hand);
    insertCardPatches(*this, card);
    if (side.playDeck.empty()) {
        // Exhaust deck
        side.deckExhaustions++;
        if (game.deckExhaustionLimitReached())
            game.councilCalled = player;    // Automatically call the Free Council
        std::vector<CardID> & pile = side.discardPile;
        auto it = pile.begin();
        while (it != pile.end()) {
            if (std::holds_alternative<Location>(game.getCardType(*it))) {
                side.locationDeck.push_back(*it);
                game.cards[*it].faceUp = false;
                CardDefinition & def = *game.settings.cardDefinitions[*it];
                def.getActions(side.actions, *it);
                def.getEffects(side.effects, *it);
                it = pile.erase(it);
            } else
                ++it;
        }
        game.contextStack.push_back(std::make_shared<Context>(ExhaustDeck(player)));
        EffectTrigger et = OpenContext();
        getCurrentScene(game).applyEffects(et, player);
    }
}
void putInDiscardPile(Scene & scene, CardID target, int player, CompanyID id) {
    Game & game = scene.game;
    Card & card = game.cards[target];
    Side & side = game.sides[card.owner];
    MoveCard moveCard(target, card.owner, DiscardPile);
    performMoveCard(scene, moveCard, player, id);
}
void discardPossessions(Scene & scene, CardID target, int fromPlayer, CompanyID id) {
    Game & game = scene.game;
    auto & possessions = game.cards[target].possessions;
    if (possessions.empty())
        return;
    CardType & type = game.getCardType(target);
    if (std::holds_alternative<Character>(type))
        scene.stripCharacter(target, id);
    else {
        while (!possessions.empty()) {
            CardID possession = possessions.back();
            possessions.pop_back();
            putInDiscardPile(scene, possession, fromPlayer, id);
        }
    }
}
void removeFromList(std::vector<CardID> & list, CardID target) {
    auto it = list.begin();
    while (*it != target)
        ++it;
    list.erase(it);
}
void Scene::stripCharacter(CardID target, CompanyID id) {
    Card & card = game.cards[target];
    Side & side = game.sides[card.owner];
    std::vector<CardID> & possessions = card.possessions;
    while (!possessions.empty()) {
        CardID possession = possessions.back();
        possessions.pop_back();
        CardType & possessionType = game.getCardType(possession);
        if (std::holds_alternative<HazardCreature>(possessionType)) {
            // Trophies (Orc & Troll characters only)
            Card & trophy = game.cards[possession];
            trophy.state = Untapped;
            trophy.parent = NoParent;
            if (std::get<HazardCreature>(possessionType).asterisk) {
                trophy.deck = Kills;
                side.kills.push_back(possession);
            } else {
                trophy.deck = OutOfPlay;
                side.outOfPlay.push_back(possession);
            }
        } else {
            // Other possessions
            putInDiscardPile(*this, possession, card.owner, id);
        }
    }
    dropFollowers(target);
}
void Scene::discard(CardID target, int hostPlayer) {
    if (!allowedOperation(target, Discard))
        return;
    Card & card = game.cards[target];
    if (hostPlayer < 0)
        hostPlayer = card.parent < 0 ? card.owner : game.cards[game.getRoot(card.parent)].owner;
    if (card.deck > InCompany) {
        Side & side = game.sides[card.owner];
        CardType & type = game.getCardType(target);
        if (std::holds_alternative<Character>(type)) {
            // Character
            Company & company = *side.findCompany(target, Characters);
            stripCharacter(target, company.id);
            putInDiscardPile(*this, target, hostPlayer, company.id);
        } else if (std::holds_alternative<Location>(type)) {
            // Location
            removeLocation(*this, target);
        } else if (card.parent >= 0) {
            // Possession
            CardID root = game.getRoot(target);
            Side & side = game.sides[game.cards[root].owner];
            DeckName deck = game.cards[root].deck;
            CompanyID id = -1;
            if (deck > InCompany) {
                auto it = side.findCompany(root, deck);
                if (it != side.companies.end())
                    id = it->id;
            }
            putInDiscardPile(*this, target, hostPlayer, id);
        } else {
            // Permanent-event
            std::list<Company>::iterator it = side.findCompany(target, CompanyPermanentEvents);
            putInDiscardPile(*this, target, hostPlayer, it->id);
        }
    } else {
        putInDiscardPile(*this, target, hostPlayer, -1);
    }
}
void Scene::eliminate(CardID target) {
    int player = game.cards[target].owner;
    if (game.cards[target].dataIndex == game.sides[player].revealedAvatar) {
        removePlayer(player);
        return;
    }
    CardID companyCharacter = game.getParent(target);
    Company & company = *game.sides[player].findCompany((companyCharacter < 0) ? target : companyCharacter, Characters);
    if (!std::holds_alternative<Ally>(game.getCardType(target))) {
        if (std::holds_alternative<BodyCheck>(context->context)) {
            std::vector<CardID> items;
            for (CardID possession : game.cards[target].possessions)
                if (std::holds_alternative<Item>(game.getCardType(possession)))
                    items.push_back(possession);
            if (!items.empty()) {
                setupTransferItems(items);
                return;  // The remaining cards are taken care of at the close of TransferItems
            }
        }
        stripCharacter(target, company.id);
    }
    moveCard(target, Eliminated, player);
    if (company.characters.empty())
        removeCompany(player, company.id);
}
void Scene::removePlayer(int player) {
    TurnSequence & turn = game.turnSequence;
    auto it = std::remove(turn.remainingPlayers.begin(), turn.remainingPlayers.end(), player);
    turn.remainingPlayers.erase(it, turn.remainingPlayers.end());
    int remainingPlayers(turn.remainingPlayers.size());
    if (player == game.currentPlayer) {
        turn.currentPlayerIndex = (turn.currentPlayerIndex + remainingPlayers - 1) % remainingPlayers;
        game.currentPlayer = turn.remainingPlayers[turn.currentPlayerIndex];
        while (!std::holds_alternative<BottomContext>(game.getContext()))
            getCurrentScene(game).closeContext();
        startNewTurn();
    } else if (player == game.hazardPlayer) {
        turn.hazardPlayerIndex = (turn.hazardPlayerIndex + 1) % remainingPlayers;
        game.hazardPlayer = turn.remainingPlayers[turn.hazardPlayerIndex];
    }
}
void losePossession(Game & game, CardID target, CardID possession) {
    std::vector<CardID> & possessions = game.cards[target].possessions;
    for (auto it = possessions.begin(); it != possessions.end(); ++it) {
        if (*it == possession) {
            possessions.erase(it);
            game.cards[possession].parent = NoParent;
            break;
        }
    }
}
void pickUpCard(Game & game, CardID target) {
    Side & side = game.getSide(game.getRoot(target));
    Card & card = game.cards[target];
    if (card.deck < InCompany)
        removeFromList(side.getDeck(card.deck), target);
    else if (card.deck == Possessions)
        losePossession(game, card.parent, target);
    else if (card.deck == OnGuard)
        removeFromList(game.getCurrentSide().findCompany(target, OnGuard)->onGuard, target);
    else if (card.deck > InCompany && card.deck != Sites) {
        std::vector<CardID> & deck = side.findCompany(target, card.deck)->getDeck(card.deck);
        removeFromList(deck, target);
    }
}
void putCard(Game & game, const MoveCard & moveCard) {
    Side & side = game.sides[moveCard.recipientPlayer];
    DeckName destination = moveCard.destination;
    CardID target = moveCard.target;
    if (destination == Possessions)
        game.cards[moveCard.parent].possessions.push_back(target);
    else if (destination < InCompany)
        side.getDeck(destination).push_back(target);
    else
        side.findCompany(moveCard.id)->getDeck(destination).push_back(target);
    Card & card = game.cards[target];
    card.faceUp = moveCard.faceUp;
    card.state = moveCard.state;
    card.parent = moveCard.parent;
    game.setHostingDeck(target, moveCard.recipientPlayer, destination);
}
void performMoveCard(Scene & scene, MoveCard & moveCard, int fromPlayer, CompanyID sourceCompany) {
    Game & game = scene.game;
    Card & card = game.cards[moveCard.target];
    EffectTrigger et = moveCard;
    scene.applyEffects(et, fromPlayer, sourceCompany);
    MoveCard & mc = std::get<MoveCard>(et);
    card.faceUp = mc.faceUp;
    card.state = mc.state;
    if (mc.destination != card.deck || mc.recipientPlayer != fromPlayer || mc.id != sourceCompany || mc.parent != card.parent) {
        pickUpCard(game, mc.target);
        putCard(game, moveCard);
        CardMoved cardMoved(moveCard.target);
        EffectTrigger et = cardMoved;
        scene.applyEffects(et, mc.recipientPlayer, mc.id);
        DeckName deck = card.deck;
        if (deck == Hand || deck == DiscardPile || deck == Eliminated || deck == OutOfPlay) {
            if (std::holds_alternative<Character>(game.getCardType(mc.target)))
                scene.stripCharacter(mc.target, sourceCompany);
            else
                discardPossessions(scene, mc.target, fromPlayer, sourceCompany);
        }
    }
}
void Scene::moveCard(CardID target, DeckName destination, int recipientPlayer, CompanyID id) {
    Card & targetData = game.cards[target];
    if (recipientPlayer < 0)
        recipientPlayer = targetData.owner;
    MoveCard moveCard(target, recipientPlayer, destination);
    moveCard.id = id;
    moveCard.faceUp = targetData.faceUp;
    moveCard.state = targetData.state;
    CardID host = game.getRoot(target);
    Card & hostData = game.cards[host];
    int fromPlayer = hostData.owner;
    CompanyID fromCompany = -1;
    if (hostData.deck == Characters || hostData.deck == CompanyPermanentEvents) {
        Side & side = game.sides[fromPlayer];
        fromCompany = side.findCompany(host, hostData.deck)->id;
    }
    performMoveCard(*this, moveCard, fromPlayer, fromCompany);
}
void Scene::returnCardToHand(CardID target, int player, CompanyID id) {
    Card & card = game.cards[target];
    MoveCard moveCard(target, card.owner, Hand);
    moveCard.faceUp = false;
    moveCard.state = Untapped;
    performMoveCard(*this, moveCard, player, id);
    if (card.deck == Hand)
        insertCardPatches(*this, target);
}
void Scene::gainPossession(CardID target, CardID possession) {
    pickUpCard(game, possession);
    game.cards[target].possessions.push_back(possession);
    Card & card = game.cards[possession];
    int recipientPlayer = game.cards[target].owner;
    game.setHostingDeck(possession, recipientPlayer, Possessions);
    card.faceUp = true;
    card.parent = target;
    // Broadcast event
    GainPossession gainPossession;
    gainPossession.target = target;
    gainPossession.possession = possession;
    EffectTrigger et = gainPossession;
    applyEffects(et, recipientPlayer, game.getCompanyID(target));
}
CompanyID Scene::addCompany(CardID location, Side & side) {
    side.companies.emplace_back();
    Company & company = side.companies.back();
    company.id = side.nextID++;
    company.newLocation = company.currentLocation = location;
    return company.id;
}
CompanyID Scene::addCompany(CardID location) {
    return addCompany(location, game.getCurrentSide());
}
void Scene::combineCompanies(int player, CompanyID source, CompanyID destination) {
    Side & side = game.getCurrentSide();
    auto it = side.findCompany(source);
    Company & sourceCompany = *it;
    Company & destinationCompany = *side.findCompany(destination);
    for (CardID character : sourceCompany.characters)
        moveCard(character, Characters, player, destination);
    for (CardID permanentEvent : sourceCompany.permanentEvents)
        destinationCompany.permanentEvents.push_back(permanentEvent);
    for (CardID onGuard : sourceCompany.onGuard)
        destinationCompany.onGuard.push_back(onGuard);
    destinationCompany.effects.splice(destinationCompany.effects.end(), sourceCompany.effects);
    CardID newLocation = sourceCompany.newLocation;
    side.companies.erase(it);
    removeLocation(*this, newLocation);
}
void Scene::removeCompany(int player, CompanyID id) {
    Side & side = game.sides[player];
    auto it = side.findCompany(id);
    Company & company = *it;
    for (CardID permanentEvent : company.permanentEvents)
        discard(permanentEvent, player);
    for (CardID onGuard : company.onGuard)
        moveCard(onGuard, Hand);
    CardID origin = company.currentLocation;
    CardID destination = company.newLocation;
    side.companies.erase(it);
    removeLocation(*this, origin);
    removeLocation(*this, destination);
    if (game.getActiveCompany() == id)
        forceQuitCompany();
}
void Scene::transferCharacter(CardID target, CompanyID id) {
    int player = game.cards[target].owner;
    Side & side = game.sides[player];
    auto sourceCompany = side.findCompany(target, Characters);
    auto destinationCompany = side.findCompany(id);
    auto end = side.companies.end();
    if (sourceCompany != end && destinationCompany != end && sourceCompany != destinationCompany) {
        std::vector<CardID> & characters = sourceCompany->characters;
        std::vector<CardID> followers;
        for (CardID character : characters)
            if (game.getParent(character) == target)
                followers.push_back(character);
        moveCard(target, Characters, player, id);
        game.cards[target].parent = HadParent;
        // Transfer followers
        for (CardID follower : followers) {
            moveCard(follower, Characters, player, id);
            game.cards[follower].parent = target;
        }
        if (characters.empty())
            combineCompanies(player, sourceCompany->id, id);
        // Reset newLocation if the resulting company contains a Ringwraith
        if (game.getAvatar(side.deckType) == Ringwraith
            && destinationCompany->newLocation != destinationCompany->currentLocation) {
            for (CardID character : destinationCompany->characters)
                if (game.getRace(character) == Ringwraith) {
                    destinationCompany->regions.clear();
                    setNewLocation(id, destinationCompany->currentLocation);
                    break;
                }
        }
    }
}
void Scene::dropFollowers(CardID target) {
    // Dropped followers does not use GI until next organization phase (MELE p.19, third paragraph)
    // Mark dropped followers by setting parent to HadParent (-2) instead of NoParent (-1).
    // It is automatically changed to NoParent at the end of next organization phase.
    Side & side = game.getSide(target);
    for (Company & company : side.companies)
        for (CardID follower : company.characters)
            if (game.cards[follower].parent == target)
                game.cards[follower].parent = HadParent;
}
void Scene::setNewLocation(CompanyID id, CardID location) {
    Company & company = *game.getCurrentSide().findCompany(id);
    CardID prevLocation = company.newLocation;
    company.newLocation = location;
    game.setHostingDeck(location, game.currentPlayer, Sites);
    removeLocation(*this, prevLocation);
}
void Scene::arriveAtLocation(Company & movingCompany) {
    CardID origin = movingCompany.currentLocation;
    movingCompany.currentLocation = movingCompany.newLocation;
    movingCompany.regions.clear();
    removeLocation(*this, origin);
    ArriveAtLocation arrive(movingCompany.newLocation);
    EffectTrigger et = arrive;
    applyEffects(et, game.currentPlayer);
}
void Scene::returnToOrigin(Company & movingCompany) {
    CardID destination = movingCompany.newLocation;
    movingCompany.newLocation = movingCompany.currentLocation;
    movingCompany.regions.clear();
    removeLocation(*this, destination);
}
void Scene::payHazard(CardID card) {
    Movement & movement = game.getMovement();
    movement.hazardsPlayed += getHazardLimitCost(card);
}
void Scene::removeCardEffects(CardID card, int player, CompanyID id) {
    game.effects.remove(card);
    if (player >= 0) {
        Side & side = game.sides[player];
        side.effects.remove(card);
        if (id >= 0) {
            Company & company = *side.findCompany(id);
            company.effects.remove(card);
        }
    }
}
void Scene::discardRingwraithModeCard(int player) {
    EffectTrigger et = ActiveRingwraithMode();
    applyEffects(et, player);
    CardID modeCard = std::get<ActiveRingwraithMode>(et).modeCard;
    if (modeCard >= 0)
        discard(modeCard);
}
void Scene::forceQuitCompany() {
    while (!std::holds_alternative<SelectCompany>(game.getContext()))
        getCurrentScene(game).closeContext();
    game.contextStack.back()->autoProceed = true;
    if (game.turnSequence.phase == MovementHazardPhase)
        setupReturnToHandSize(false);
}
void Scene::cancelAttack() {
    Attack & attack = std::get<Attack>(context->context);
    attack.outcome = Cancelled;
    attack.state = Attack::CloseAttack;
    context->autoProceed = true;
}
void Scene::shuffleDeck(Side & side) {
    std::vector<CardID> & playDeck = side.playDeck;
    if (!game.settings.gameRecord.replay) {
        std::shuffle(playDeck.begin(), playDeck.end(), game.settings.rnd);
        game.settings.gameRecord.append(RecordEntryType::ShuffleDeck, side.player, playDeck);
    } else {
        side.playDeck = game.settings.gameRecord.nextEntry().args;
    }
}
int Scene::generateRoll() {
    auto & rnd = game.settings.rnd;
    std::uniform_int_distribution<int> dist(1, 6);
    return dist(rnd) + dist(rnd);
}

int getBaseProwess(Scene & scene, CardID card) {
    CardType type = scene.game.getCardType(card);
    if (std::holds_alternative<Character>(type))
        return std::get<Character>(type).prowess;
    else
        return std::get<Ally>(type).prowess;
}
int getBaseBody(Scene & scene, CardID card) {
    CardType type = scene.game.getCardType(card);
    if (std::holds_alternative<Character>(type))
        return std::get<Character>(type).body;
    else
        return std::get<Ally>(type).body;
}
int getBaseMind(Scene & scene, CardID card) {
    CardType type = scene.game.getCardType(card);
    if (std::holds_alternative<Character>(type))
        return std::get<Character>(type).mind;
    else if (std::holds_alternative<Ally>(type))
        return std::get<Ally>(type).mind;
    else
        return std::get<Faction>(type).requiredInfluence;
}
int getCorruptionTotal(Scene & scene, CardID card, CompanyID id) {
    Game & game = scene.game;
    Side & side = game.getCurrentSide();
    int corruption = 0;
    std::vector<CardID> & possessions = game.cards[card].possessions;
    for (CardID possession : possessions) {
        CardType type = game.getCardType(possession);
        if (std::holds_alternative<Item>(type))
            corruption += std::get<Item>(type).cp;
        corruption += getCorruption(scene, possession, id);
    }
    return corruption;
}
const std::vector<Skill> & getBaseSkills(Scene & scene, CardID card) {
    CardType & cardType = scene.game.getCardType(card);
    if (std::holds_alternative<Character>(cardType)) {
        Character & character = std::get<Character>(cardType);
        return character.skills;
    }
    Ally & ally = std::get<Ally>(cardType);
    return ally.skills;
}

/* Gets effect-adjusted values */
int getCorruption(Scene & scene, CardID card, CompanyID id) {
    CorruptionTotal corruption;
    corruption.target = card;
    EffectTrigger et = corruption;
    scene.applyEffects(et, scene.game.currentPlayer, id);
    return std::get<CorruptionTotal>(et).corruption;
}
DrawCardsLimit getDrawCardsLimit(Scene & scene, CompanyID id) {
    Game & game = scene.game;
    Side & side = game.getCurrentSide();
    Company & company = *side.findCompany(id);
    Location & currentLocation = game.getLocation(company.currentLocation);
    Location & newLocation = game.getLocation(company.newLocation);
    DrawCardsLimit limit;
    SiteType havenType = side.havenType();
    if (company.newLocation == company.currentLocation)
        limit.playerLimit = limit.opponentLimit = 0;
    else if (newLocation.type != havenType) {
        limit.playerLimit = newLocation.drawCards.first;
        limit.opponentLimit = newLocation.drawCards.second;
    } else if (currentLocation.type != havenType) {
        limit.playerLimit = currentLocation.drawCards.first;
        limit.opponentLimit = currentLocation.drawCards.second;
    } else {
        limit.playerLimit = limit.opponentLimit = 2;
    }
    EffectTrigger et = limit;
    scene.applyEffects(et, game.currentPlayer, id);
    return std::get<DrawCardsLimit>(et);
}
void removeUnequippedItemEffects(EffectBatch & batch, CardID target, EquipmentType equipmentType) {
    // Side effect: moves item effects to the front of the list (MELE p. 43, middle)
    Game & game = batch.scene.game;
    CardID equippedItem = game.getEquippedItem(target, equipmentType);
    if (equippedItem >= 0) {
        std::vector<std::list<EffectList::iterator>::iterator> eraseQueue;
        std::list<EffectList::iterator> & effectList = batch.effectQueue;
        for (auto itit = effectList.begin(); itit != effectList.end(); ++itit) {
            auto it = *itit;
            CardID host = it->effect->host;
            CardType & type = game.getCardType(host);
            if (std::holds_alternative<Item>(type)) {
                Item & item = std::get<Item>(type);
                if (item.equipmentType == equipmentType) {
                    eraseQueue.push_back(itit);
                    if (host == equippedItem)
                        effectList.push_front(it);
                }
            }
        }
        for (auto it : eraseQueue)
            effectList.erase(it);
    }
}
void moveEquipmentEffectsToFront(EffectBatch & batch, CardID target) {
    Game & game = batch.scene.game;
    std::vector<EquipmentType> equipped;
    std::vector<CardID> & possessions = game.cards[target].possessions;
    for (CardID possession : possessions) {
        CardType & type = game.getCardType(target);
        if (std::holds_alternative<Item>(type)) {
            Item & item = std::get<Item>(type);
            if (item.equipmentType != None || item.equipmentType < LesserRing)
                if (std::find(equipped.begin(), equipped.end(), item.equipmentType) == equipped.end())
                    equipped.push_back(item.equipmentType);
        }
    }
    while (!equipped.empty()) {
        // Apply the following function to all equipment types in the character's
        // possession so that their effects get moved to the front of the effect queue.
        // Work through the equipment list from the back to sort the item effects
        // in the same order as the items appear in the inventory.
        removeUnequippedItemEffects(batch, target, equipped.back());
        equipped.pop_back();
    }
}
int Scene::getModifier(int playerNumber, CompanyID id) {
    EffectTrigger et = Modifier(0);
    applyEffects(et, playerNumber, id);
    return std::get<Modifier>(et).modifier;
}
int Scene::getProwess(CardID card, Race raceOfOpponent, CompanyID id) {
    EffectTrigger et = Stat(Prowess, card, raceOfOpponent, getBaseProwess(*this, card));
    EffectBatch batch(*this, et);
    batch.collect(game.cards[card].owner, id);
    moveEquipmentEffectsToFront(batch, card);   // Apply equipment effects first (MELE p. 33, bottom)
    CardID weapon = game.getEquippedItem(card, Weapon);
    if (weapon >= 0)
        batch.execute(weapon);
    // Apply trophy prowess bonuses, then execute the remaining batch
    Stat & prowess = std::get<Stat>(batch.et);
    prowess.stat += trophyProwessBonus(card, prowess.stat);
    game.settings.effectProcessor->broadcast(batch);
    return prowess.stat;
}
int Scene::getBody(CardID card, Race raceOfOpponent, CompanyID id) {
    EffectTrigger et = Stat(Body, card, raceOfOpponent, getBaseBody(*this, card));
    EffectBatch batch(*this, et);
    batch.collect(game.cards[card].owner, id);
    moveEquipmentEffectsToFront(batch, card);
    game.settings.effectProcessor->broadcast(batch);
    return std::get<Stat>(batch.et).stat;
}
int Scene::getMind(CardID card, CompanyID id) {
    EffectTrigger et = Stat(Mind, card, Unspecified, getBaseMind(*this, card));
    applyEffects(et, game.cards[card].owner, id);
    return std::get<Stat>(et).stat;
}
int Scene::getStat(StatType type, CardID card, Race raceOfOpponent, CompanyID id) {
    if (type == Prowess)
        return getProwess(card, raceOfOpponent, id);
    if (type == Body)
        return getBody(card, raceOfOpponent, id);
    if (type == Mind)
        return getMind(card, id);
    return 0;
}
int Scene::getUnusedDI(CardID target, CompanyID id, Race race) {
    UnusedDI di;
    di.target = target;
    di.di = game.getCharacter(target).di + trophyInfluenceBonus(target);
    EffectTrigger et = di;
    int player = game.cards[target].owner;
    applyEffects(et, player, id);
    di = std::get<UnusedDI>(et);
    std::vector<CardID> & characters = game.sides[player].findCompany(id)->characters;
    for (CardID character : characters) {
        if (game.getParent(character) == target) {
            int mind = getMind(character, id);
            Character & follower = game.getCharacter(character);
            if (!di.characterDi.empty())
                for (std::string & name : follower.name) {
                    auto it = di.characterDi.find(name);
                    if (it != di.characterDi.end()) {
                        int bonus = it->second;
                        mind = (mind > bonus) ? mind - bonus : 0;
                    }
                }
            Race type = follower.race;
            int influence = di.raceDi[type];
            if (mind < influence)
                influence = mind;
            if (influence) {
                di.raceDi[type] -= influence;
                mind -= influence;
            }
            if (mind) {
                di.di -= mind;
            }
        }
    }
    return di.di + di.raceDi[race];
}
int Scene::getUnusedGI(int player) {
    Side & side = game.sides[player];
    EffectTrigger et = UnusedGI();
    applyEffects(et, player);
    int gi = std::get<UnusedGI>(et).gi;
    for (Company & company : side.companies)
        for (CardID character : company.characters)
            if (game.cards[character].parent == NoParent)
                gi -= getMind(character, company.id);
    return gi;
}
int Scene::getHandSize(int player) {
    EffectTrigger et = HandSize(player);
    applyEffects(et, player);
    return std::get<HandSize>(et).handSize;
}
int Scene::getMP(CardID card, int player) {
    int mp = (game.cards[card].deck != Eliminated) ? getBaseMP(game.getCardType(card)) : 0;
    EffectTrigger et = MarshallingPoints(card, mp);
    applyEffects(et, player);
    return std::get<MarshallingPoints>(et).mp;
}
MarshallingCount Scene::getTotalMP(int player) {
    MarshallingCounter counter(*this, player);
    Side & side = game.sides[player];
    std::vector<DeckName> decks = { Factions, Stored, Kills, Eliminated };
    for (DeckName deckName : decks) {
        auto & deck = side.getDeck(deckName);
        for (CardID card : deck)
            counter.updateCount(card);
    }
    for (Company & company : side.companies) {
        for (CardID character : company.characters) {
            counter.updateCount(character);
            auto & possessions = game.cards[character].possessions;
            for (CardID possession : possessions)
                counter.updateCount(possession);
        }
    }
    return counter.count;
}
int Scene::getCreatureStat(StatType type, CardID card, Race race, int base) {
    EffectTrigger et = Stat(type, card, race, base);
    applyEffects(et, game.currentPlayer);
    return std::get<Stat>(et).stat;
}
Race Scene::getRace(CardID card) {
    return game.getRace(card);
}
Skills Scene::getSkills(CardID card, int playerNumber, CompanyID id) {
    Skills skills;
    skills.target = card;
    const std::vector<Skill> & baseSkills = getBaseSkills(*this, card);
    for (Skill skill : baseSkills)
        skills.skills.push_back(skill);
    EffectTrigger et = skills;
    applyEffects(et, playerNumber, id);
    return std::get<Skills>(et);
}
SiteType Scene::getSiteType(CardID card) {
    Location & location = game.getLocation(card);
    UpdateSiteType siteType;
    siteType.location = card;
    siteType.siteType = location.type;
    EffectTrigger et = siteType;
    applyEffects(et);
    return std::get<UpdateSiteType>(et).siteType;
}
SitePath Scene::getSitePath() {
    Movement & movement = game.getMovement();
    UpdateSitePath updatedSitePath;
    updatedSitePath.sitePath = movement.originalSitePath;
    EffectTrigger et = updatedSitePath;
    applyEffects(et, game.currentPlayer, movement.company);
    return std::get<UpdateSitePath>(et).sitePath;
}
size_t Scene::getRegionLimit(int player, CompanyID id) {
    RegionLimit regionLimit(id);
    EffectTrigger et = regionLimit;
    applyEffects(et, player, id);
    return std::get<RegionLimit>(et).limit;
}
std::unordered_set<RegionNumber> Scene::getAdjacentRegions(RegionNumber regionNumber) {
    Region & region = game.getRegion(regionNumber);
    AdjacentRegions adjacentRegions;
    adjacentRegions.adjacentRegions = region.adjacentRegions;
    EffectTrigger et = adjacentRegions;
    applyEffects(et);
    return std::get<AdjacentRegions>(et).adjacentRegions;
}
ActiveRingwraithMode Scene::getRingwraithMode(int player) {
    EffectTrigger et = ActiveRingwraithMode();
    applyAllEffects(et, player);
    return std::get<ActiveRingwraithMode>(et);
}
int Scene::getTargetProwess(Strike & strike) {
    CompanyID id = game.getActiveCompany();
    int targetProwess = getStat(strike.statType, strike.target, strike.creatureType, id);
    return targetProwess + strike.targetProwessModifier;
}
int Scene::getTargetBody(Strike & strike) {
    CompanyID id = game.getActiveCompany();
    int targetBody = getBody(strike.target, getRace(strike.target), id);
    return targetBody + strike.targetBodyModifier;
}
int Scene::strikeResult(Strike & strike) {
    int targetProwess = getTargetProwess(strike);
    int creatureProwess = getCreatureStat(Prowess, strike.card, strike.creatureType, strike.creatureBaseProwess);
    return strike.roll + targetProwess - strike.strikesSpent - creatureProwess;
}
int Scene::getNumberOfTargets(Company & company) {
    int availableTargets = 0;
    for (CardID character : company.characters) {
        if (allowedOperation(character, Target))
            availableTargets++;
        for (CardID possession : game.cards[character].possessions)
            if (std::holds_alternative<Ally>(game.getCardType(possession)))
                if (allowedOperation(possession, Target))
                    availableTargets++;
    }
    return availableTargets;
}
int Scene::getCompanySize(Company & company) {
    int companySize = 1;
    for (CardID characterCard : company.characters) {
        Race race = getRace(characterCard);
        if (race == Hobbit)
            companySize++;
        else if (race == Orc && hasSkill(characterCard, Scout))
            companySize++;
        else
            companySize += 2;
    }
    return companySize >> 1;
}
int Scene::getHazardLimitCost(CardID card) {
    HazardLimitCost cost;
    cost.target = card;
    cost.cost = 1;
    EffectTrigger et = cost;
    applyEffects(et, game.cards[card].owner);
    return std::get<HazardLimitCost>(et).cost;
}
int Scene::getTrophyPoints(CardID target) {
    if (!std::holds_alternative<Character>(game.getCardType(target)))
        return 0;
    Race race = getRace(target);
    if (race != Orc && race != Troll)
        return 0;
    Card & card = game.cards[target];
    auto & possessions = card.possessions;
    int trophyPoints = 0;
    for (CardID possession : possessions) {
        CardType & type = game.getCardType(possession);
        if (std::holds_alternative<HazardCreature>(type)
            || std::holds_alternative<HazardEvent>(type)) {
            trophyPoints += getMP(possession, card.owner);
        }
    }
    return trophyPoints;
}
int Scene::trophyProwessBonus(CardID target, int currentProwess) {
    if (currentProwess >= 9)
        return 0;
    int trophyPoints = getTrophyPoints(target);
    if (trophyPoints > 4)
        trophyPoints = 4;
    int prowessBonus = trophyPoints >> 1;
    if (currentProwess + prowessBonus > 9)
        return 1;
    return prowessBonus;
}
int Scene::trophyInfluenceBonus(CardID target) {
    int trophyPoints = getTrophyPoints(target);
    if (trophyPoints > 4)
        trophyPoints = 4;
    return (trophyPoints + 1) >> 1;
}
bool Scene::healingSite(CardID location) {
    HealingSite healing(location, getSiteType(location) == game.getCurrentSide().havenType());
    EffectTrigger et = healing;
    applyEffects(et, game.currentPlayer);
    return std::get<HealingSite>(et).healingSite;
}
bool Scene::matchingHomeSite(const std::string & homeSite, CardID locationCard) {
    Location & location = game.getLocation(locationCard);
    if (homeSite == location.getName().front())
        return true;
    SiteType siteType = getSiteType(locationCard);
    if (homeSite == "Any Shadow-hold")
        return siteType == ShadowHold;
    return false;
}
bool Scene::hasSkill(CardID card, Skill skill) {
    try {
        Skills skills = getSkills(card, game.currentPlayer, game.getCompanyID(card));
        for (Skill s : skills.skills) {
            if (s == skill)
                return true;
        }
    }
    catch (std::exception) {}
    return false;
}
bool Scene::hasKeyword(CardID card, EventKeyword keyword) {
    CardType & type = game.getCardType(card);
    std::vector<EventKeyword> keywords;
    if (std::holds_alternative<ResourceEvent>(type))
        keywords = std::get<ResourceEvent>(type).keywords;
    else if (std::holds_alternative<HazardEvent>(type))
        keywords = std::get<HazardEvent>(type).keywords;
    else
        return false;
    for (EventKeyword key : keywords)
        if (key == keyword)
            return true;
    return false;
}
bool containsOrcOrTroll(Game & game, Company & company) {
    for (CardID character : company.characters) {
        Race race = game.getRace(character);
        if (race == Orc || race == Troll)
            return true;
    }
    return false;
}
bool Scene::overtCompany(Company & company, int player) {
    if (player < 0)
        player = game.currentPlayer;
    EffectTrigger et = OvertCompany(player, company.id, containsOrcOrTroll(game, company));
    applyEffects(et, player, company.id);
    return std::get<OvertCompany>(et).overt;
}
int Scene::verifyCompanyComposition(Company & company) {
    if (company.newLocation == company.currentLocation) {
        SiteType siteType = game.getLocation(company.newLocation).type;
        if (siteType == Haven || siteType == Darkhaven)
            return Success;
    }
    int companySize = getCompanySize(company);
    if (companySize > 7)
        return CompanySizeLimitExceeded;
    // Minion company limitations
    unsigned int leaders, ringwraiths, evilCount, goodCount;
    leaders = ringwraiths = evilCount = goodCount = 0;
    for (CardID card : company.characters) {
        Character & character = game.getCharacter(card);
        Race race = character.race;
        if (character.leader)  // May be altered by effects, facilitate this when needed
            leaders++;
        if (race == Ringwraith)
            ringwraiths++;
        else if (race == Orc || race == Troll)
            evilCount++;
        else if (race == Elf || race == Dwarf || race == Dunadan)
            goodCount++;
    }
    if (ringwraiths > 0 && company.characters.size() > ringwraiths)
        return RingwraithInCompany;
    if (evilCount > 0 && goodCount > 0)
        return RaceConflict;
    if (leaders > 1)
        return TooManyLeaders;
    return Success;
}
bool Scene::verifyArrivingCompanies() {
    // Verifies the composition that follows from joining moving companies.

    // Can be called any time. The function has side-effects only if the scene holds the
    // Arrival context.

    // It is allowed to move any number and any kind of company to the same destination,
    // as the company composition requirements may be met if characters get eliminated
    // during the movment/hazard-phase. Return companies to the site of origin if there
    // are composition violations (MELE, p.61, upper half)

    std::list<Company> & companies = game.getCurrentSide().companies;
    std::map<CardID, std::vector<CompanyID>> sameDestination;
    for (Company & company : companies)
        sameDestination[company.newLocation].push_back(company.id);
    bool combine = false;
    for (auto newCompany : sameDestination)
        if (newCompany.second.size() > 1) {
            combine = true;
            break;
        }
    if (combine) {
        Game gameCopy = game;   // Create a copy of game
        Side & side = gameCopy.getCurrentSide();
        Scene combine(gameCopy, contextIndex);
        for (auto & newCompany : sameDestination) {
            auto & companies = newCompany.second;
            if (companies.size() > 1) {
                std::vector<CompanyID> movingCompanies;
                for (CompanyID id : companies) {
                    Company & company = *side.findCompany(companies.front());
                    if (company.currentLocation != company.newLocation)
                        movingCompanies.push_back(id);
                }
                auto it = ++companies.begin();
                for (; it != companies.end(); ++it)
                    combine.combineCompanies(gameCopy.currentPlayer, *it, companies.front());
                if (combine.verifyCompanyComposition(*side.findCompany(companies.front()))) {
                    if (std::holds_alternative<Arrival>(context->context)) {
                        if (movingCompanies.size() > 1) {
                            Arrival & arrival = std::get<Arrival>(context->context);
                            arrival.returnToOrigin = movingCompanies;
                            return false;
                        } else {
                            returnToOrigin(*side.findCompany(movingCompanies.back()));
                        }
                    } else {
                        return false;
                    }
                }
            }
        }
    }
    return true;
}

/* Checks that
 *  - The card belongs to currentPlayer
 */
bool Scene::resourceConditions(CardID card) {
    return game.cards[card].owner == game.currentPlayer;
}
/* Checks that
 * - The card belongs to the hazard player
 * - The hazard limit cost does not exceed the remaining hazard limit.
 */
bool Scene::hazardConditions(CardID card) {
    if (game.cards[card].owner != game.hazardPlayer)
        return false;
    if (std::holds_alternative<BottomContext>(context->context) ||
        std::holds_alternative<SelectCompany>(context->context))
        return false;
    if (game.turnSequence.phase == MovementHazardPhase) {
        if (game.getDeck(card) == Hand) {
            Movement & movement = game.getMovement();
            if (getHazardLimitCost(card) + movement.hazardsPlayed > movement.hazardLimit)
                return false;
        }
    }
    else if (game.turnSequence.phase == SitePhase && game.getDeck(card) == OnGuard) {
        auto & onGuard = game.getCurrentSide().findCompany(game.getActiveCompany())->onGuard;
        if (std::find(onGuard.begin(), onGuard.end(), card) == onGuard.end())
            return false;
    }
    else
        return false;
    return true;
}
bool Scene::narrowHazardConditions(CardID card) {
    if (game.cards[card].owner != game.hazardPlayer)
        return false;
    if (std::holds_alternative<BottomContext>(context->context) ||
        std::holds_alternative<SelectCompany>(context->context))
        return false;
    if (game.turnSequence.phase != MovementHazardPhase)
        return false;
    if (game.getDeck(card) == Hand) {
        Movement & movement = game.getMovement();
        if (movement.hazardsPlayed + getHazardLimitCost(card) > movement.hazardLimit)
            return false;
    }
    return true;
}
bool Scene::mayCallCouncil(int player) {
    Side & side = game.sides[player];
    switch (game.settings.gameLength) {
    case GameLength::ShortGame:
        if (side.deckExhaustions == 1 && getTotalMP(player).sum() >= 25)
            return true;
        return side.deckExhaustions > 1;
    case GameLength::LongGame:
        if (side.deckExhaustions == 2 && getTotalMP(player).sum() >= 30)
            return true;
        return side.deckExhaustions > 2;
    case GameLength::CampaignGame:
        if (side.deckExhaustions == 3 && getTotalMP(player).sum() >= 40)
            return true;
        return side.deckExhaustions > 3;
    }
    return false;
}

bool Scene::proceedTurnSequence() {
    TurnSequence & turn = game.turnSequence;
    if (turn.part != End)
        turn.part = Part(int(turn.part) + 1);
    else {
        turn.phase = Phase(int(turn.phase) + 1);
        turn.part = Beginning;
    }
    EffectTrigger et = NewPhase();
    applyEffects(et, game.currentPlayer, game.getActiveCompany());
    game.getCurrentSide().actions.update(*this);
    return std::get<NewPhase>(et).halt;
}
void Scene::startNewTurn() {
    // Sets up a FreeCouncil context if the council has been called
    EffectTrigger et = NewPhase();
    applyAllEffects(et);
    TurnSequence & turn = game.turnSequence;
    turn.currentPlayerIndex = turn.hazardPlayerIndex;
    turn.hazardPlayerIndex = (turn.hazardPlayerIndex + 1) % turn.remainingPlayers.size();
    game.currentPlayer = turn.remainingPlayers[turn.currentPlayerIndex];
    game.hazardPlayer = turn.remainingPlayers[turn.hazardPlayerIndex];
    game.characterBroughtIntoPlay = -1;
    game.receivedCorruptionCard.clear();
    game.influencedOpponent = false;
    if (turn.currentPlayerIndex == 0)
        turn.round++;
    turn.setPhase(UntapPhase, Beginning);
    applyEffects(et, game.currentPlayer);
    game.getCurrentSide().actions.update(*this);
    context->autoProceed = !std::get<NewPhase>(et).halt;
}
bool Scene::closeContext(int player, CompanyID id) {
    if (context == game.contextStack.back()) {
        game.contextStack.pop_back();
        EffectTrigger et = CloseContext();
        applyEffects(et, player, id);
        return true;
    }
    context->autoProceed = true;
    return false;
}
void Scene::insertPatches(CardID card) {
    ActionList actions;
    int dataIndex = game.cards[card].dataIndex;
    game.settings.cardDefinitions[dataIndex]->getActions(actions, card);
    actions.back()->insertPatches(game);
}
void Scene::applyEffects(EffectTrigger & et, int player, CompanyID id) {
    EffectBatch batch(*this, et);
    batch.collect(player, id);
    game.settings.effectProcessor->broadcast(batch);
}
void Scene::applyAllEffects(EffectTrigger & et, int player) {
    EffectBatch batch(*this, et);
    batch.collectAll(player);
    game.settings.effectProcessor->broadcast(batch);
}

bool detainment(Scene & scene, KeyedToType keyedTo, Race race) {
    Game & game = scene.game;
    Side & side = game.getCurrentSide();
    if (side.deckType != HeroDeck) {
        if (race == Nazgul)
            return true;
        else if (std::holds_alternative<SiteType>(keyedTo) || std::holds_alternative<std::string>(keyedTo)) {
            SiteType key = std::holds_alternative<SiteType>(keyedTo)
                ? std::get<SiteType>(keyedTo) : scene.getSiteType(game.getSite());
            if (key == DarkHold || key == ShadowHold)
                return true;
        }
        else if (std::holds_alternative<PathSymbol>(keyedTo) ||
            std::holds_alternative<RegionNumber>(keyedTo)) {
            PathSymbol key = std::holds_alternative<PathSymbol>(keyedTo) ?
                std::get<PathSymbol>(keyedTo) : game.getRegionType(std::get<RegionNumber>(keyedTo));
            if (key == DarkDomain)
                return true;
            else if (key == ShadowLands && (race == Orc || race == Troll || race == Undead || race == Man))
                return true;
        }
    }
    return false;
}
Scene Scene::setupStrike(CardID target) {
    AssignStrikes & resolveStrikes = std::get<AssignStrikes>(context->context);
    game.contextStack.push_back(std::make_shared<Context>(Strike(), contextIndex));
    Scene currentScene = getCurrentScene(game);
    Strike & strike = std::get<Strike>(currentScene.context->context);
    strike.card = resolveStrikes.card;
    strike.creatureType = resolveStrikes.creatureType;
    strike.keyedTo = resolveStrikes.keyedTo;
    strike.statType = resolveStrikes.statType;
    strike.creatureBaseProwess = resolveStrikes.prowess;
    strike.creatureBaseBody = resolveStrikes.body;
    strike.target = target;
    CompanyID companyID = game.getActiveCompany();
    strike.excessStrikes = resolveStrikes.unassignedStrikes;
    strike.finalTarget = resolveStrikes.assignments.empty();
    strike.detainment = resolveStrikes.detainment;
    strike.mayNotBeCancelled = resolveStrikes.mayNotBeCancelled;
    for (AssignStrikes::StrikeAssignment & assignment : resolveStrikes.assignments)
        strike.mayNotSupport.insert(assignment.target);
    if (game.turnSequence.phase == SitePhase)
        strike.state = (strike.excessStrikes == 0) ? Strike::TapCharacter : strike.state = Strike::ExcessStrikes;
    EffectTrigger et = OpenContext();
    currentScene.applyEffects(et, game.currentPlayer, companyID);
    return currentScene;
}
void Scene::autoAssignStrikes(Company & company) {
    AssignStrikes & assign = std::get<AssignStrikes>(context->context);
    for (CardID character : company.characters) {
        assign.unassignedStrikes = 1;
        target(character);
        for (CardID possession : game.cards[character].possessions) {
            if (std::holds_alternative<Ally>(game.getCardType(possession))) {
                assign.unassignedStrikes = 1;
                target(possession);
            }
        }
    }
    assign.unassignedStrikes = 0;
}
Scene Scene::setupAssignStrikes() {
    Attack & attack = std::get<Attack>(context->context);
    game.contextStack.push_back(std::make_shared<Context>(AssignStrikes(), contextIndex));
    Scene currentScene = getCurrentScene(game);
    AssignStrikes & resolveStrikes = std::get<AssignStrikes>(currentScene.context->context);
    resolveStrikes.company = attack.company;
    resolveStrikes.card = attack.card;
    resolveStrikes.creatureType = attack.creatureType;
    resolveStrikes.keyedTo = attack.keyedTo;
    resolveStrikes.statType = attack.statType;
    resolveStrikes.prowess = attack.prowess;
    resolveStrikes.body = attack.body;
    resolveStrikes.detainment = attack.detainment;
    resolveStrikes.automaticAttack = attack.automaticAttack;
    resolveStrikes.mayNotBeCancelled = attack.mayNotBeCancelled;
    resolveStrikes.attackerChoosesDefendingCharacters = attack.attackerChoosesDefendingCharacters;
    // Defender's assignment capacity may be increased, so don't auto assign in other cases but this:
    if (attack.strikes == 0) {
        Company & company = *game.getCurrentSide().findCompany(attack.company);
        currentScene.autoAssignStrikes(company);
        resolveStrikes.unresolvedStrikes = resolveStrikes.assignments;
        resolveStrikes.unassignedStrikes = 0;
        resolveStrikes.state = AssignStrikes::ResolveStrikes;
    } else {
        resolveStrikes.unassignedStrikes = getCreatureStat(Strikes, attack.card, attack.creatureType, attack.strikes);
        resolveStrikes.state = resolveStrikes.attackerChoosesDefendingCharacters ?
            AssignStrikes::OpponentAssign : AssignStrikes::PlayerAssign;
    }
    return currentScene;
}
Scene Scene::setupAttack() {
    Side & side = game.getCurrentSide();
    Encounter & encounter = std::get<Encounter>(context->context);
    AttackInfo & info = **(encounter.currentAttack++);
    game.contextStack.push_back(std::make_shared<Context>(Attack(), contextIndex));
    Scene currentScene = getCurrentScene(game);
    Attack & attack = std::get<Attack>(currentScene.context->context);
    attack.company = encounter.company;
    attack.card = encounter.card;
    attack.creatureType = info.type;
    attack.keyedTo = encounter.keyedTo;
    attack.statType = info.statType;
    attack.automaticAttack = encounter.automaticAttack;
    attack.mayNotBeCancelled = info.mayNotBeCancelled;
    attack.strikes = info.strikes;
    attack.prowess = info.prowess;
    attack.body = info.body;
    info.getEffects(side.effects, encounter.card);

    if (info.detainmentAgainstOvertCompany && overtCompany(*side.findCompany(attack.company)))
        attack.detainment = true;
    else
        attack.detainment = detainment(currentScene, attack.keyedTo, info.type);
    EffectTrigger et = Detainment(attack.detainment);
    applyEffects(et, game.currentPlayer, attack.company);
    attack.detainment = std::get<Detainment>(et).detainment;

    if (attack.card >= 0) {
        CardType type = game.getCardType(attack.card);
        if (std::holds_alternative<HazardCreature>(type)) {
            HazardCreature & creature = std::get<HazardCreature>(type);
            attack.attackerChoosesDefendingCharacters = creature.attackerChoosesDefendingCharacters;
        }
    }
    et = OpenContext();
    currentScene.applyEffects(et, game.currentPlayer, attack.company);
    return currentScene;
}
Scene Scene::setupEncounter(CardID card, CompanyID company, KeyedToType key) {
    game.contextStack.push_back(std::make_shared<Context>(Encounter()));
    Scene currentScene = getCurrentScene(game);
    Encounter & encounter = std::get<Encounter>(currentScene.context->context);
    encounter.company = company;
    encounter.card = card;
    encounter.keyedTo = key;
    CardType & type = game.getCardType(card);
    if (std::holds_alternative<HazardCreature>(type)) {
        HazardCreature & creature = std::get<HazardCreature>(type);
        AttackInfo attackInfo;
        attackInfo.statType = creature.stat;
        attackInfo.type = creature.race;
        attackInfo.strikes = creature.strikes;
        attackInfo.prowess = creature.prowess;
        attackInfo.body = creature.body;
        attackInfo.mayNotBeCancelled = false;
        for (int i = 0; i < creature.attacks; i++)
            encounter.attacks.push_back(std::make_shared<AttackInfo>(attackInfo));
    } else if (std::holds_alternative<Location>(type)) {
        // Automatic attack
        Location & location = std::get<Location>(type);
        EffectTrigger et = AutomaticAttack(card, location.automaticAttack);
        AutomaticAttack & attacks = std::get<AutomaticAttack>(et);
        currentScene.applyEffects(et, game.currentPlayer, company);
        if (attacks.info.empty())
            return currentScene;
        encounter.attacks = attacks.info;
        encounter.automaticAttack = true;
    }
    encounter.currentAttack = encounter.attacks.begin();
    currentScene.context->autoProceed = true;
    return currentScene;
}
bool Scene::setupAutomaticAttack() {
    Explore & explore = std::get<Explore>(context->context);
    Location & location = game.getLocation(explore.site);
    explore.state = Explore::ExploreSite;
    if (!location.automaticAttack.empty()) {
        setupEncounter(explore.site, explore.company, location.name);
        return true;
    }
    return false;
}
Scene Scene::setupBodyCheck(CardID target, int body) {
    Strike & strike = std::get<Strike>(context->context);
    game.contextStack.push_back(std::make_shared<Context>(BodyCheck(), contextIndex));
    Scene currentScene = getCurrentScene(game);
    BodyCheck & bodyCheck = std::get<BodyCheck>(currentScene.context->context);
    bodyCheck.target = target;
    bodyCheck.body = body;
    CompanyID company = game.getCompanyID(strike.target);
    bodyCheck.defendingCompany = (target == strike.target) ? company : -1;
    bodyCheck.modifier = currentScene.getModifier(game.currentPlayer, company);
    if (game.cards[target].state == Wounded)
        bodyCheck.modifier++;
    currentScene.context->autoProceed = true;  // May not use miruvor before resolving the body check (MELE, p.31, top)
    EffectTrigger et = OpenContext();
    currentScene.applyEffects(et, game.currentPlayer, company);
    return currentScene;
}
Scene Scene::setupTransferItems(const std::vector<CardID> & items) {
    game.contextStack.push_back(std::make_shared<Context>(TransferItems(), contextIndex));
    Scene currentScene = getCurrentScene(game);
    TransferItems & transfer = std::get<TransferItems>(currentScene.context->context);
    transfer.player = game.currentPlayer;
    transfer.company = game.getActiveCompany();
    transfer.items = items;
    transfer.allowedTransfers = int(items.size());
    return currentScene;
}
Scene Scene::setupMovement(CompanyID id) {
    game.contextStack.push_back(std::make_shared<Context>(Movement(), contextIndex));
    Scene currentScene = getCurrentScene(game);
    Movement & movement = std::get<Movement>(currentScene.context->context);
    Side & side = game.getCurrentSide();
    Company & company = *side.findCompany(id);
    movement.company = id;
    movement.siteOfOrigin = company.currentLocation;
    movement.destination = company.newLocation;
    if (company.regions.empty()) {
        if (movement.destination != movement.siteOfOrigin) {
            movement.originalSitePath = game.getSitePath(movement.siteOfOrigin, movement.destination);
            movement.regions.push_back(game.getLocation(movement.siteOfOrigin).region);
            movement.regions.push_back(game.getLocation(movement.destination).region);
        }
    } else {
        for (CardID regionCard : company.regions) {
            Region & region = std::get<Region>(game.getCardType(regionCard));
            movement.originalSitePath.push_back(region.type);
            movement.regions.push_back(region.number);
        }
    }
    int hazardLimit = currentScene.getCompanySize(company);
    if (hazardLimit < 2)
        hazardLimit = 2;
    EffectTrigger et = HazardLimit(hazardLimit);
    applyEffects(et, game.currentPlayer, id);
    movement.hazardLimit = std::get<HazardLimit>(et).hazardLimit;
    movement.overtCompany = currentScene.overtCompany(company);
    return currentScene;
}
Scene Scene::setupExplore(CompanyID id) {
    game.contextStack.push_back(std::make_shared<Context>(Explore(), contextIndex));
    Scene currentScene = getCurrentScene(game);
    Explore & explore = std::get<Explore>(currentScene.context->context);
    Company & company = *game.getCurrentSide().findCompany(id);
    explore.company = id;
    explore.site = company.currentLocation;
    Location & location = game.getLocation(explore.site);
    if (game.getCardState(explore.site) == Untapped)
        for (ItemType type : location.playable)
            explore.playable.push_back(type);
    explore.containsHoard = location.containsHoard;
    EffectTrigger et = OpenContext();
    currentScene.applyEffects(et, game.currentPlayer, company.id);
    return currentScene;
}
Scene Scene::setupSelectCompanies() {
    game.contextStack.push_back(std::make_shared<Context>(SelectCompany()));
    Scene currentScene = getCurrentScene(game);
    SelectCompany & selectCompany = std::get<SelectCompany>(currentScene.context->context);
    auto & companies = game.getCurrentSide().companies;
    for (Company & company : companies)
        selectCompany.remainingCompanies.push_back(company.id);
    currentScene.context->autoProceed = true;
    return currentScene;
}
Scene Scene::setupCorruptionCheck(CardID id) {
    ResolveCorruptionChecks & resolveCorruptionChecks = std::get<ResolveCorruptionChecks>(context->context);
    game.contextStack.push_back(std::make_shared<Context>(CorruptionCheck(), contextIndex));
    Scene currentScene = getCurrentScene(game);
    CorruptionCheck & corruptonCheck = std::get<CorruptionCheck>(currentScene.context->context);
    auto it = resolveCorruptionChecks.unresolvedChecks.begin();
    while (it->id != id)
        ++it;
    ResolveCorruptionChecks::TargetModifier targetModifier = *it;
    resolveCorruptionChecks.unresolvedChecks.erase(it);
    CardID target = targetModifier.target;
    corruptonCheck.targetCharacter = target;
    CompanyID companyID = game.getCompanyID(target);
    corruptonCheck.base = getCorruptionTotal(currentScene, target, companyID);
    corruptonCheck.modifier = targetModifier.modifier;
    corruptonCheck.modifier += currentScene.getModifier(game.currentPlayer, companyID);
    return currentScene;
}
Scene Scene::setupResolveCorruptionChecks(CardID id, CardID target, int modifier, int repetitions) {
    game.contextStack.push_back(std::make_shared<Context>(ResolveCorruptionChecks()));
    Scene currentScene = getCurrentScene(game);
    if (getRace(target) == Ringwraith) {  // Ringwraiths never make corruption checks
        currentScene.context->autoProceed = true;
        return currentScene;
    }
    ResolveCorruptionChecks & resolve = std::get<ResolveCorruptionChecks>(currentScene.context->context);
    if (target < 0)
        target = id;
    ResolveCorruptionChecks::TargetModifier targetModifier(id, target, modifier);
    while (repetitions--)
        resolve.unresolvedChecks.push_back(targetModifier);
    return currentScene;
}
Scene Scene::setupInfluenceAttempt(CardID influencingCharacter, CardID target) {
    CompanyID id = game.getCompanyID(influencingCharacter);
    game.contextStack.push_back(std::make_shared<Context>(InfluenceAttempt(), contextIndex));
    Scene currentScene = getCurrentScene(game);
    InfluenceAttempt & context = std::get<InfluenceAttempt>(currentScene.context->context);
    int targetOwner = game.cards[target].owner;
    context.influencingCharacter = influencingCharacter;
    context.target = target;
    CardType & type = game.getCardType(target);
    if (std::holds_alternative<Faction>(type)) {
        Faction & faction = std::get<Faction>(type);
        context.requiredInfluence = getMind(target, -1); // Gets adjusted value of faction.requiredInfluence
        context.modifier = currentScene.getUnusedDI(influencingCharacter, id, faction.race);
        for (std::pair<const FactionModifier, int> & mod : faction.standardModifications) {
            if (std::holds_alternative<Race>(mod.first) && std::get<Race>(mod.first) == faction.race)
                context.modifier += mod.second;
            else if (std::holds_alternative<std::string>(mod.first) &&
                     !game.uniqueFaction(target, { std::get<std::string>(mod.first) }))
                context.modifier += mod.second;
        }
    } else {
        Race race = std::holds_alternative<Character>(type) ? std::get<Character>(type).race : Unspecified;
        CardID parent = game.getParent(target);
        CompanyID targetCompany = game.getCompanyID(target);
        if (std::holds_alternative<Character>(type) || std::holds_alternative<Ally>(type))
            context.requiredInfluence = getMind(target, targetCompany);
        else if (std::holds_alternative<Item>(type)) {
            context.requiredInfluence = getMind(parent, targetCompany);
            race = game.getRace(parent);
        }
        context.modifier = getUnusedDI(influencingCharacter, id, race);
        if (parent >= 0 && std::holds_alternative<Character>(game.getCardType(parent)))
            context.modifier -= getUnusedDI(parent, targetCompany, race);
    }
    if (context.influenceOpponent = (targetOwner != game.currentPlayer)) {
        int unusedGI = getUnusedGI(targetOwner);
        if (unusedGI > 0)   // Tournament rules: GI modifier can't go below 0 (internet)
            context.modifier -= unusedGI;
        DeckType playerDeck = game.getCurrentSide().deckType;
        DeckType opponentDeck = game.getSide(target).deckType;
        if (playerDeck == HeroDeck && opponentDeck != HeroDeck || playerDeck != HeroDeck && opponentDeck == HeroDeck)
            context.modifier -= 5;   // (MELE, p.85, top)
    }
    context.modifier += currentScene.getModifier(game.currentPlayer, id);
    return currentScene;
}
Scene Scene::setupRingTest(CardID ring, int modifier, int rolls) {
    game.contextStack.push_back(std::make_shared<Context>(RingTest(ring)));
    Scene currentScene = getCurrentScene(game);
    EffectTrigger et = Modifier(modifier);
    currentScene.applyEffects(et, game.currentPlayer, game.getCompanyID(ring));
    RingTest & ringTest = std::get<RingTest>(currentScene.context->context);
    ringTest.modifier = std::get<Modifier>(et).modifier;
    currentScene.setupRollDice(game.currentPlayer, rolls);
    return currentScene;
}
Scene Scene::setupSpecialTest(CardID host, int rollingPlayer, CardID target) {
    game.contextStack.push_back(std::make_shared<Context>(SpecialTest(host, rollingPlayer, target)));
    Scene currentScene = getCurrentScene(game);
    currentScene.context->autoProceed = true;
    return currentScene;
}
Scene Scene::setupStoreCard(CardID card) {
    StoreCard storeCard;
    storeCard.target = card;
    game.contextStack.push_back(std::make_shared<Context>(storeCard));
    return getCurrentScene(game);
}
Scene Scene::setupMoveHazardsFromSideboard(int player) {
    game.contextStack.push_back(std::make_shared<Context>(TransferCards()));
    Scene currentScene = getCurrentScene(game);
    TransferCards & context = std::get<TransferCards>(currentScene.context->context);
    TransferCards::TransferCardsOption option;
    option.sourceDeck = Sideboard;
    option.destinationDeck = DiscardPile;
    option.minimum = 1;
    option.maximum = 5;
    option.categories = { HazardCard };
    context.transactions[player].push_back(option);
    if (game.sides[player].playDeck.size() >= 5) {
        option.destinationDeck = PlayDeck;
        option.maximum = 1;
        context.transactions[player].push_back(option);
    }
    return currentScene;
}
bool mindRequirement(Scene & scene, CompanyID id) {
    // Check if the company contains an avatar or a character with mind >= 3 (MELE, p.68, bottom)
    Game & game = scene.game;
    Side & side = game.getCurrentSide();
    Company & company = *side.findCompany(id);
    Race avatar = game.getAvatar(side.deckType);
    for (CardID card : company.characters) {
        Character & character = game.getCharacter(card);
        if (character.race == avatar || scene.getMind(card, id) > 2)
            return true;
    }
    return false;
}
Scene Scene::setupDrawCards(CompanyID id) {
    game.contextStack.push_back(std::make_shared<Context>(DrawCards()));
    Scene currentScene = getCurrentScene(game);
    DrawCards & drawCards = std::get<DrawCards>(currentScene.context->context);
    drawCards.company = id;
    DrawCardsLimit limit = getDrawCardsLimit(currentScene, id);
    drawCards.playerLimit = mindRequirement(currentScene, id) ? limit.playerLimit : 0;
    drawCards.opponentLimit = limit.opponentLimit;
    if (drawCards.playerLimit == 0 && drawCards.opponentLimit == 0)
        currentScene.context->autoProceed = true;
    EffectTrigger et = OpenContext();
    currentScene.applyEffects(et, game.currentPlayer, id);
    return currentScene;
}
Scene Scene::setupReturnToHandSize(bool mayDiscard) {
    game.contextStack.push_back(std::make_shared<Context>(ReturnToHandSize()));
    Scene currentScene = getCurrentScene(game);
    ReturnToHandSize & handSize = std::get<ReturnToHandSize>(currentScene.context->context);
    handSize.resourcePlayerHandSize = getHandSize(game.currentPlayer);
    handSize.hazardPlayerHandSize = getHandSize(game.hazardPlayer);
    if (mayDiscard) {
        handSize.resourcePlayerMayDiscard = int(game.getCurrentSide().hand.size()) <= handSize.resourcePlayerHandSize;
        handSize.hazardPlayerMayDiscard = int(game.sides[game.hazardPlayer].hand.size()) <= handSize.hazardPlayerHandSize;
    } else {
        handSize.resourcePlayerMayDiscard = handSize.hazardPlayerMayDiscard = false;
    }
    return currentScene;
}
Scene Scene::setupRollDice(int player, int rolls) {
    game.contextStack.push_back(std::make_shared<Context>(RollDice(player, rolls), contextIndex));
    return getCurrentScene(game);
}
Scene Scene::setupChainOfEffects() {
    if (!std::holds_alternative<ChainOfEffects>(game.getContext()))
        game.contextStack.push_back(std::make_shared<Context>(ChainOfEffects(), contextIndex));
    return getCurrentScene(game);
}
Scene Scene::actualScene() {
    return std::holds_alternative<ChainOfEffects>(context->context) ? parent() : *this;
}
Scene Scene::parent() {
    return Scene(game, context->parent);
}

void MarshallingCounter::updateCount(CardID card) {
    mp = scene.getMP(card, player);
    target = card;
    std::visit(*this, scene.game.getCardType(card));
}
void MarshallingCounter::operator()(Character & character) {
    PointCategories category = (scene.game.cards[target].deck != Kills)
        ? CharacterPoints
        : KillPoints;
    count.points[category] += mp;
}
void MarshallingCounter::operator()(Ally & ally) {
    PointCategories category = (scene.game.cards[target].deck != Kills)
        ? AllyPoints
        : KillPoints;
    count.points[category] += mp;
}
void MarshallingCounter::operator()(Item & item) {
    count.points[ItemPoints] += mp;
}
void MarshallingCounter::operator()(Faction & faction) {
    count.points[FactionPoints] += mp;
}
void MarshallingCounter::operator()(ResourceEvent & resource) {
    count.points[MiscellaneousPoints] += mp;
}
void MarshallingCounter::operator()(HazardCreature & creature) {
    if (deckType == HeroDeck || creature.asterisk)
        count.points[KillPoints] += mp;
}
void MarshallingCounter::operator()(HazardEvent & hazard) {
    PointCategories category = (scene.game.cards[target].deck == Kills)
        ? KillPoints
        : MiscellaneousPoints;
    count.points[category] += mp;
}
void MarshallingCounter::operator()(Location & location) {}
void MarshallingCounter::operator()(Region & region) {}

void moveCardFast(Game & game, CardID target, DeckName destination, int recipientPlayer) {
    pickUpCard(game, target);
    MoveCard moveCard(target, recipientPlayer, destination);
    moveCard.faceUp = game.cards[target].faceUp;
    moveCard.state = game.cards[target].state;
    putCard(game, moveCard);
}

void removeLocation(Scene & scene, CardID target) {
    Game & game = scene.game;
    Side & side = game.getCurrentSide();
    if (!game.siteCardInPlay(target)) {
        Location & location = game.getLocation(target);
        Card & card = game.cards[target];
        if (card.state == Untapped || location.type == side.havenType()) {
            // Return to location deck
            card.state = Untapped;
            card.faceUp = false;
            scene.game.setHostingDeck(target, card.owner, LocationDeck);
        }
        else {
            putInDiscardPile(scene, target, card.owner, -1);
        }
    }
}

void removeEffects(Game & game) {
    game.effects.remove();
    Side & side = game.getCurrentSide();
    side.effects.remove();
    for (Company & company : side.companies)
        company.effects.remove();
}

Scene getCurrentScene(Game & game) {
    return Scene(game, int(game.contextStack.size()) - 1);
}
