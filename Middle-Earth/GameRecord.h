#pragma once
#include "CardTopology.h"
#include "CardTypes.h"
#include "ContextTypes.h"
#include "../CardSets/DeckDefinition.h"
#include <vector>

enum class RecordEntryType {
    Done, Action, Roll, RollResult, SelectRoll, ShuffleDeck,
    SelectDestination,
    TakeFollower, DropFollower,
    TransferCharacter, DiscardCharacter,
    RearrangeEquipment, TransferItem, Store,
    ChooseCompany, DrawCard, Discard, TransferCards, ExchangeCard, ExchangeCards,
    SetKeyedTo, PlaceOnGuard,
    AssignStrikes, UnassignStrike, ResolveStrike, UseStrikes,
    Tap, DistributePossessions, CancelHomeSite,
    EnterSite, AttackOpponent, InfluenceOpponent,
    RevealIdenticalCard, TakeInfluencedCard,
    ResolveCorruptionCheck
};

struct RecordEntry {
    RecordEntry() = default;
    RecordEntry(RecordEntryType type, int player, std::vector<CardID> args, int option = 0)
        : type(type)
        , player(player)
        , args(args)
        , option(option)
    {}
    RecordEntry(RecordEntryType type, int player)
        : type(type)
        , player(player)
        , args{}
        , option(0)
    {}
    RecordEntryType type;
    int player;
    std::vector<CardID> args;
    int option;
};

struct GameRecord {
    void append(RecordEntryType type, int player, std::vector<CardID> args = {}) {
        if (!replay)
            record.emplace_back(type, player, args);
    }
    void append(RecordEntryType type, int player, int option) {
        if (!replay) {
            record.emplace_back(type, player, std::vector<CardID>{});
            record.back().option = option;
        }
    }
    void append(RecordEntryType type, int player, std::vector<CardID> args, int option) {
        if (!replay)
            record.emplace_back(type, player, args, option);
    }
    void appendAction(int player, std::list<CardID> args, int id) {
        if (!replay) {
            RecordEntry & recordEntry = record.emplace_back(RecordEntryType::Action, player);
            for (CardID arg : args)
                recordEntry.args.push_back(arg);
            record.back().option = id;
        }
    }
    void appendSelectDestination(int player, CompanyID id, CardID destination, const std::vector<RegionNumber> & regions) {
        if (!replay) {
            RecordEntry & recordEntry = record.emplace_back(RecordEntryType::SelectDestination, player);
            recordEntry.args.push_back(id);
            recordEntry.args.push_back(destination);
            for (RegionNumber region : regions)
                recordEntry.args.push_back(CardID(region));
        }
    }
    void appendSetKeyedTo(int player, KeyedToType key) {
        if (!replay) {
            RecordEntry & recordEntry = record.emplace_back(RecordEntryType::SetKeyedTo, player);
            if (std::holds_alternative<PathSymbol>(key)) {
                recordEntry.option = 0;
                recordEntry.args.push_back(std::get<PathSymbol>(key));
            } else if (std::holds_alternative<SiteType>(key)) {
                recordEntry.option = 1;
                recordEntry.args.push_back(std::get<SiteType>(key));
            } else if (std::holds_alternative<RegionNumber>(key)) {
                recordEntry.option = 2;
                recordEntry.args.push_back(std::get<RegionNumber>(key));
            } else if (std::holds_alternative<std::string>(key)) {
                recordEntry.option = 3;
                std::string & siteName = std::get<std::string>(key);
                for (char letter : siteName)
                    recordEntry.args.push_back(letter);
            }
        }
    }
    void startReplay() {
        replayPos = record.begin();
        replay = replayPos != record.end();
    }
    const RecordEntry & nextEntry() {
        const RecordEntry & recordEntry = *replayPos;
        ++replayPos;
        return recordEntry;
    }

    std::vector<RecordEntry> record;
    std::vector<RecordEntry>::iterator replayPos;
    bool replay;
};
