#pragma once
#include "CardAbilities.h"
#include <array>
#include <map>
#include <set>
#include <string>
#include <unordered_set>
#include <variant>
#include <vector>


/* Characters */

enum Skill { Warrior, Scout, Ranger, Sage, Diplomat };
enum Race {
    Man, Dunadan, Dwarf, Elf, Hobbit, Wizard, Orc, Troll, Ringwraith, Balrog,
    Dragon, Drake, Animal, Spider, Wolf, Undead, Nazgul, Gas, Unspecified
};
enum CharacterType { Hero, Minion, Agent };
enum StatType { Prowess, Body, Mind, Strikes };

struct Character {
    Character()
        : minion(false)
        , unique(true)
        , leader(false) {}
    bool isMinion() { return minion; }
    std::vector<std::string> getName() { return name; }
    int getMP() { return mp; }
    bool minion;
    std::vector<std::string> name;
    std::vector<Skill> skills;
    Race race;
    bool unique;
    bool leader;
    int mp;
    int mind;
    int di;
    int prowess;
    int body;
    std::string homeSite;
};

struct Ally {
    Ally()
        : minion(false)
        , race(Unspecified) {}
    bool isMinion() { return minion; }
    std::vector<std::string> getName() { return name; }
    int getMP() { return mp; }
    bool minion;
    std::vector<std::string> name;
    std::vector<Skill> skills;
    Race race;
    bool unique;
    int mp;
    int mind;
    int prowess;
    int body;
};


/* Items */

enum ItemType { Minor, Major, Greater, GoldRing, Special, Information };
enum EquipmentType {
    None, Weapon, Shield, Armor, Helmet,
    LesserRing, MagicRing, DwarvenRing, TheOneRing
};

struct Item {
    Item()
        : minion(false)
        , equipmentType(None)
        , unique(false)
        , hoard(false)
        , mp(0)
        , cp(1) {}
    bool isMinion() { return minion; }
    std::vector<std::string> getName() { return name; }
    int getMP() { return mp; }
    bool minion;
    std::vector<std::string> name;
    ItemType type;
    EquipmentType equipmentType;
    bool unique;
    bool hoard;
    int mp;
    int cp;
};


using FactionModifier = std::variant<Race, std::string>;

struct Faction {
    Faction() : minion(false), unique(true) {}
    bool isMinion() { return minion; }
    std::vector<std::string> getName() { return name; }
    int getMP() { return mp; }
    bool minion;
    std::vector<std::string> name;
    Race race;
    bool unique;
    std::string homeSite;
    int mp;
    int requiredInfluence;
    std::map<FactionModifier, int> standardModifications;
};


/* Locations */

enum PathSymbol { CoastalSeas, FreeDomain, BorderLands, Wilderness, ShadowLands, DarkDomain };
enum SiteType { Haven, FreeHold, BorderHold, RuinsLairs, ShadowHold, DarkHold, Darkhaven, SiteInRegion };
enum RegionNumber {
    Lindon, Numeriador, Forochel, Arthedain, TheShire, Cardolan, Angmar, Rhudaur, Hollin,
    Dunland, Enedhwaith, OldPukelLand, Gundabad, HighPass, RedhornGate, GapOfIsen, AnduinVales,
    WoldAndFoothills, Fangorn, Rohan, GreyMountainNarrows, WoodlandRealm, WesternMirkwood, HeartOfMirkwood,
    SouthernMirkwood, BrownLands, Dagorlad, WitheredHeath, NorthernRhovanion, SouthernRhovanion, IronHills,
    Dorwinion, HorsePlains, OldPukelGap, Andrast, Anfalas, Lamedon, Belfalas, Lebennin, Anorien, Ithilien,
    Harandor, Khand, ImladMorgul, Udun, Gorgoroth, Nurn, ElvenShores, EriadoranCoast, AndrastCoast,
    BayOfBelfalas, MouthsOfTheAnduin, RegionEnd
};

using KeyedToType = std::variant<PathSymbol, SiteType, RegionNumber, std::string>;
using SitePath = std::vector<PathSymbol>;

struct AttackInfo {
    AttackInfo()
        : detainmentAgainstOvertCompany(false) {}
    AttackInfo(Race type, int strikes, int prowess)
        : statType(Prowess)
        , type(type)
        , strikes(strikes)
        , prowess(prowess)
        , body(0)
        , detainmentAgainstOvertCompany(false)
        , mayNotBeCancelled(false) {}

    virtual void getEffects(EffectList & effects, CardID host) {}
    StatType statType;
    Race type;
    int strikes;
    int prowess;
    int body;
    bool detainmentAgainstOvertCompany;
    bool mayNotBeCancelled;
};

struct AutomaticAttacks : public std::vector<std::shared_ptr<AttackInfo>> {
    AttackInfo & add(Race type, int strikes, int prowess) {
        push_back(std::make_shared<AttackInfo>(type, strikes, prowess));
        return *back();
    }
};

enum HavenNumber { Rivendell = RegionEnd, Lorien, GreyHavens, Edhellond, MinasMorgul, DolGuldur, CarnDum, GeannALisch };

struct HavenConnection {
    HavenConnection(HavenNumber haven)
        : nearestHaven(haven) {}
    HavenNumber nearestHaven;
    SitePath sitePath;
};

struct Location {
    Location() : minion(false) {}
    bool isMinion() { return minion; }
    std::vector<std::string> getName() { return { name }; }
    int getMP() { return 0; }
    bool minion;
    std::string name;
    SiteType type;
    std::vector<HavenConnection> sitePath;
    RegionNumber region;
    std::vector<ItemType> playable;
    AutomaticAttacks automaticAttack;
    std::pair<int, int> drawCards;                // <currentPlayer, hazardPlayer>
    bool containsHoard = false;
};

struct Region {
    bool isMinion() { return false; }
    std::vector<std::string> getName() { return { name }; }
    int getMP() { return 0; }
    std::string name;
    RegionNumber number;
    PathSymbol type;
    std::unordered_set<RegionNumber> adjacentRegions;
};


/* Events */

enum EventType { ShortEvent, LongEvent, PermanentEvent };
enum EventKeyword { Environment, Spell, Corruption, Ritual, LightEnchantment };

struct ResourceEvent {
    ResourceEvent() : minion(false), mp(0) {}
    bool isMinion() { return minion; }
    int getMP() { return mp; }
    std::vector<std::string> getName() { return name; }
    bool minion;
    std::vector<std::string> name;
    EventType type;
    int mp;
    std::vector<EventKeyword> keywords;
};

struct HazardCreature {
    HazardCreature()
        : stat(Prowess)
        , unique(false)
        , mp(1)
        , attacks(1)
        , body(0)
        , asterisk(false)
        , attackerChoosesDefendingCharacters(false) {}
    bool isMinion() { return false; }
    std::vector<std::string> getName() { return name; }
    int getMP() { return mp; }
    std::vector<std::string> name;
    std::vector<KeyedToType> playable;
    Race race;
    StatType stat;
    bool unique;
    int mp;
    int attacks;
    int strikes;                      // 0 if each character faces a strike
    int prowess;
    int body;
    bool asterisk;
    bool attackerChoosesDefendingCharacters;
};

struct HazardEvent {
    HazardEvent()
        : unique(false), mp(0) {}
    bool isMinion() { return false; }
    std::vector<std::string> getName() { return name; }
    int getMP() { return 0; }
    std::vector<std::string> name;
    EventType type;
    bool unique;
    int mp;
    std::vector<EventKeyword> keywords;
};

enum CardCategory { ResourceCard, HazardCard, CharacterCard, SiteCard, RegionCard };

using CardType = std::variant<Character, Ally, Item, Faction, Location, Region,
    ResourceEvent, HazardCreature, HazardEvent>;

enum PointCategories {
    CharacterPoints, ItemPoints, FactionPoints, AllyPoints, KillPoints, MiscellaneousPoints
};
struct MarshallingCount {
    int sum() {
        int sum = 0;
        for (int p : points)
            sum += p;
        return sum;
    }
    std::array<int, 6> points = { 0 };
};
