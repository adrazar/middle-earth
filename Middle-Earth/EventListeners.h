#pragma once
#include "EffectProcessor.h"

// This particular listener is obsolete.
// But leave it as reference until a real listener has been implemented.
struct CardAliasUpdater : public EffectTriggerListener {
    void triggerHandler(const EffectBatch & batch);
};

void CardAliasUpdater::triggerHandler(const EffectBatch & batch) {
    if (!std::holds_alternative<MoveCard>(batch.et))
        return;
    const MoveCard & moveCard = std::get<MoveCard>(batch.et);
    batch.scene.game.setHostingDeck(moveCard.target, moveCard.recipientPlayer, moveCard.destination);
}
