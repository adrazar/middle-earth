#pragma once
#include "Middle-Earth.h"
#include <unordered_set>

// If any function here returns anything that is not Success, the
// (currently non-existing) automatically cloned state can safely
// be discarded.

struct MetaGame {
    MetaGame();
    MetaGame(Game && gameState);

    Scene currentScene();

    // Main control
    int done(int player);      // The proper way to call proceed from client
    int wait(int player);
    int ready(int player);

    int proceed(int player);   // Run chain of effects / proceed to next phase
    void cloneState();

    // Roll dice
    int roll(int player, int result = 0);
    int selectRoll(int player, int rollNumber);

    // Select destination
    int selectDestination(int player, CompanyID id, CardID destination, std::vector<RegionNumber> regions = {});
    int resetDestination(int player, CompanyID id);
    int getAdjacentRegions(int player, const std::vector<RegionNumber> & path, std::unordered_set<RegionNumber> & regions);

    // Organize companies
    int takeFollower(int player, CardID leader, CardID follower);
    int dropFollower(int player, CardID follower);
    int transferCharacter(int player, CardID target, CompanyID destination = -1);
    int discardCharacter(int player, CardID target);
    int rearrangeEquipment(int player, CardID target, std::vector<CardID> equipment);
    int transferItem(int player, CardID item, CardID recipient);
    int store(int player, CardID target);

    // Intermediate phase
    int chooseCompany(int player, CompanyID company);
    int drawCard(int player);
    int discard(int player, std::vector<CardID> cards);
    int transferCards(int player, std::vector<CardID> cards, int option = 0);

    // Exhaust deck
    int exchangeCard(int player, CardID card);

    // Hazard play
    int setKeyedTo(int player, KeyedToType key);
    int placeOnGuard(int player, CardID card);

    // Combat
    int assignStrike(int player, CardID target);
    int unassignStrike(int player, CardID target);
    int resolveStrike(int player, CardID target);
    int useStrikes(int player, int strikes);
    int tap(int player, CardID target);            // Tap against strike or tap to support strike/corruption check
    int selectItemRecipient(int player, CardID item, CardID target);
    int cancelHomeSite(int player, CardID target); // (MELE, p.62, top)

    // Site phase
    int enterSite(int player);
    int influenceOpponent(int player, CardID influencingCharacter, CardID target);
    int revealIdenticalCard(int player, CardID card);
    int takeInfluencedCard(int player, CardID leader = -1);
    int attackOpponent(int player);

    // Actions
    int selectCard(int player, CardID card);
    int selectCards(int player, const std::vector<CardID> & cards);
    int selectCompany(int player, CompanyID company);
    int clearSelection(int player);
    int useAction(int player, ActionID action);
    std::vector<ActionInfo> getActions(int player, CardID card);

    // Corruption check
    int resolveCorruptionCheck(int player, CardID id);


    GameSettings settings;
    std::list<Game> gameStates;
    int readyFlags = 0;
};

void removeDuplicateArguments(std::vector<CardID> & cards);
