#pragma once
#include "CardTypes.h"
#include "GameRecord.h"
#include "../CardSets/CardSets.h"
#include "../CardSets/DeckDefinition.h"
#include <random>

struct CardDefinition {
    CardDefinition(CardType type) : type(type) {}
    CardType type;
    virtual void getActions(ActionList & actions, CardID host) = 0;
    virtual void getEffects(EffectList & effects, CardID host);
    virtual ~CardDefinition() {}
};

struct EffectProcessor;

enum class GameLength { ShortGame, LongGame, CampaignGame };

struct GameSettings {
    GameSettings();
    ~GameSettings();
    void addCardDefinition(std::unique_ptr<CardDefinition> && cardDefinition);
    int getDataIndex(const std::string & cardName);

    std::vector<CardSet> loadedCardSets;
    std::vector<std::unique_ptr<CardDefinition>> cardDefinitions;
    std::map<std::string, int> cardNameToDefinitionIndex;
    std::vector<DeckDefinition> decks;
    std::default_random_engine rnd;
    std::unique_ptr<EffectProcessor> effectProcessor;
    GameLength gameLength;
    GameRecord gameRecord;
};

struct NameGetter {
    template <class T>
    std::vector<std::string> operator()(T & t) {
        return t.getName();
    }
};

std::vector<std::string> getName(CardType & type);
std::string cardLookupName(CardType & type);
bool isMinion(CardType & type);
int getBaseMP(CardType & type);
