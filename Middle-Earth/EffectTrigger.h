#pragma once
#include "CardTopology.h"
#include "CardTypes.h"
#include <list>
#include <unordered_set>
#include <variant>

enum Operation { Tap, Untap, Heal, Store, Target, Play, Discard, EnterSite, ReturnToOrigin };

struct AllowedOperation {
    AllowedOperation(CardID card, Operation operation, bool allowed)
        : card(card), operation(operation), allowed(allowed) {}
    CardID card;
    //CardID invoker;     // The card invoking the operation (-1 if not invoked by a card)
    Operation operation;
    bool allowed;
};

enum MagicType { Sorcery, ShadowMagic, SpiritMagic };

struct MagicProficiency {
    MagicProficiency(CardID target, MagicType magicType)
        : target(target), magicType(magicType), proficient(false) {}
    const CardID target;
    const MagicType magicType;
    bool proficient;
};

enum class RingwraithMode { None, BlackRiderMode, FellRiderMode, HeraldedLordMode };

struct ActiveRingwraithMode {
    ActiveRingwraithMode()
        : mode(RingwraithMode::None)
        , modeCard(-1) {}
    RingwraithMode mode;
    CardID modeCard;
};

struct Roll {
    // The type of roll can be seen from the last item in the Context stack
    Roll(int result)
        : result(result) {}
    const int result;       // Any modifiers are sorted out prior to the roll
};

struct Stat {
    Stat(StatType type, CardID target, Race raceOfOpponent, int stat)
        : type(type)
        , target(target)
        , raceOfOpponent(raceOfOpponent)
        , stat(stat) {}
    const StatType type;
    const CardID target;
    const Race raceOfOpponent;
    int stat;
};

struct UnusedDI {
    CardID target;
    int di;
    std::map<Race, int> raceDi;
    std::map<std::string, int> characterDi;
};

struct Modifier {
    Modifier(int modifier) : modifier(modifier) {}
    int modifier; // See what kind of modifier from Context
};

struct UnusedGI {
    UnusedGI() : gi(20) {}
    int gi;
};

struct CorruptionTotal {
    CorruptionTotal()
        : corruption(0) {}
    CardID target;
    int corruption;
};

struct Skills {
    CardID target;
    std::list<Skill> skills;
};

struct HandSize {
    HandSize(int player)
        : player(player) {}
    const int player;
    int handSize = 8;
};

struct HealingSite {
    HealingSite(CardID location, bool healing)
        : location(location)
        , healingSite(healing) {}
    const CardID location;
    bool healingSite;
};

struct PlayCard {
    CardID card;
};

struct MoveCard {
    MoveCard(CardID target, int recipientPlayer, DeckName destination)
        : target(target), recipientPlayer(recipientPlayer), destination(destination) {}
    const CardID target;
    int recipientPlayer;
    DeckName destination;
    CompanyID id = -1;
    CardID parent = NoParent;
    bool faceUp = false;
    CardState state = Untapped;
};

struct CardMoved { //  Not yet used by any cards
    CardMoved(CardID target)
        : target(target) {}
    const CardID target;
};

struct GainPossession {
    CardID target;
    CardID possession;
};

struct DrawCardsLimit {
    int playerLimit;
    int opponentLimit;
};

struct UpdateSiteType {
    CardID location;
    SiteType siteType;
};

struct UpdateSitePath {
    SitePath sitePath;
};

struct AdjacentRegions {
    RegionNumber region;
    std::unordered_set<RegionNumber> adjacentRegions;
};

struct RegionLimit {
    RegionLimit(CompanyID id)
        : id(id), limit(4)
    {}
    const CompanyID id;
    size_t limit;
};

struct NewPhase {
    // The exact phase can be found in Game::TurnSequence
    bool halt = false;
};

struct MarshallingPoints {
    MarshallingPoints(CardID target, int mp)
        : target(target)
        , mp(mp) {}
    const CardID target;
    int mp;
};

struct HazardLimit {
    HazardLimit(int hazardLimit)
        : hazardLimit(hazardLimit) {}
    int hazardLimit;
};

struct HazardLimitCost {
    CardID target;
    int cost;
};

struct ArriveAtLocation {
    ArriveAtLocation(CardID location)
        : location(location) {}
    const CardID location;
};

struct TransferAllowed {
    TransferAllowed(CardID item, CardID recipient)
        : item(item)
        , recipient(recipient)
        , allowed(true) {}
    const CardID item;
    const CardID recipient;
    bool allowed;
};

struct AutomaticAttack {
    AutomaticAttack(CardID site, AutomaticAttacks info)
        : site(site), info(info) {}
    const CardID site;
    AutomaticAttacks info;
};

struct OvertCompany {
    OvertCompany(int player, CompanyID id, bool overt)
        : player(player)
        , id(id)
        , overt(overt) {}
    const int player;
    const CompanyID id;
    bool overt;
};

struct Detainment {
    Detainment(bool detainment)
        : detainment(detainment) {}
    bool detainment;
};

struct PlayCharacter {
    PlayCharacter(CardID character, CardID target, bool allowed)
        : character(character), target(target), allowed(allowed) {}
    const CardID character, target;
    bool allowed;
};

struct PlayAlly {
    PlayAlly(CardID ally, CardID target, bool allowed)
        : ally(ally), target(target), allowed(allowed) {}
    const CardID ally, target;
    bool allowed;
};

struct DeclareAction {
    DeclareAction(CardID host)
        : host(host) {}
    const CardID host;
};

struct RingType {
    RingType(CardID goldRing, int testResult)
        : goldRing(goldRing)
        , testResult(testResult)
        , type({ LesserRing })
    {}
    const CardID goldRing;
    const int testResult;
    std::vector<EquipmentType> type;
};

struct OpenContext {};
struct CloseContext {};

using EffectTrigger = std::variant<Roll, Stat, UnusedDI, Modifier, UnusedGI, CorruptionTotal, Skills,
    HandSize, AllowedOperation, HealingSite, NewPhase, PlayCard, MoveCard, CardMoved, GainPossession,
    UpdateSiteType, UpdateSitePath, AdjacentRegions, RegionLimit, ArriveAtLocation,
    HazardLimit, HazardLimitCost, DrawCardsLimit, TransferAllowed,
    AutomaticAttack, OvertCompany, MagicProficiency, Detainment,
    PlayCharacter, PlayAlly, ActiveRingwraithMode, RingType, MarshallingPoints,
    DeclareAction, OpenContext, CloseContext>;
