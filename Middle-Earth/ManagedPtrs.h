#pragma once
#include <memory>

template <class T>
struct UndoPtr {
    UndoPtr()
        : data(std::make_shared<T>())
        , cloned(false)
    { ; }
    UndoPtr(UndoPtr & ptr)
        : data(ptr.data)
        , cloned(false)
    { ; }
    UndoPtr(const UndoPtr & ptr)
        : data(ptr.data)
        , cloned(false)
    { ; }

    void clone() {
        if (!cloned) {
            data = std::make_shared<T>(*data);
            cloned = true;
        }
    }
    T & operator*() {
        return *data;
    }
    std::shared_ptr<T> operator->() {
        return data;
    }

    std::shared_ptr<T> data;
    bool cloned = false;
};
