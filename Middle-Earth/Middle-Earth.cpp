#include "GameConfigurer.h"
#include "Middle-Earth.h"
#include <array>
#include <exception>


void CardDefinition::getEffects(EffectList & effects, CardID host) {}

void CardAliasTable::add(CardID id) {
    fromAlias[nextRef] = id;
    toAlias[id] = nextRef;
    nextRef++;
}
void CardAliasTable::remove(CardID id) {
    auto it = toAlias.find(id);
    fromAlias.erase(fromAlias.find(it->second));
    toAlias.erase(it);
}

std::list<Company>::iterator Side::findCompany(CardID target, DeckName name) {
    for (auto it = companies.begin(); it != companies.end(); ++it) {
        std::vector<CardID> & deck = it->getDeck(name);
        for (CardID card : deck)
            if (card == target)
                return it;
    }
    return companies.end();
}
std::list<Company>::iterator Side::findCompany(CompanyID id) {
    for (auto it = companies.begin(); it != companies.end(); ++it) {
        if (it->id == id)
            return it;
    }
    return companies.end();
}
SiteType Side::havenType() {
    return (deckType == HeroDeck) ? Haven : Darkhaven;
}

TurnSequence::TurnSequence()
    : phase(UntapPhase), part(Beginning)
{ ; }
TurnSequence::TurnSequence(Phase phase, Part part)
    : phase(phase), part(part)
{ ; }
void TurnSequence::setPhase(Phase newPhase, Part newPart) {
    phase = newPhase;
    part = newPart;
}
bool TurnSequence::operator>(const TurnSequence & turn) const {
    return phase > turn.phase || phase == turn.phase && part > turn.part;
}
bool TurnSequence::operator==(const TurnSequence & turn) const {
    return phase == turn.phase && part == turn.part;
}
bool TurnSequence::operator!=(const TurnSequence & turn) const {
    return phase != turn.phase || part != turn.part;
}

Game::Game(GameSettings & settings)
    : settings(settings)
{
    contextStack.push_back(std::make_shared<Context>(BottomContext()));
}
Side & Game::getCurrentSide() {
    return sides[currentPlayer];
}
Side & Game::getHazardSide() {
    return sides[hazardPlayer];
}
Side & Game::getSide(CardID card) {
    return sides[cards[card].owner];
}
ContextType & Game::getContext() {
    return contextStack.back()->context;
}
CardState Game::getCardState(CardID card) {
    return cards[card].state;
}
CardID Game::getParent(CardID card) {
    return cards[card].parent;
}
CardID Game::getRoot(CardID card) {
    CardID parent = getParent(card);
    while (parent >= 0) {
        card = parent;
        parent = getParent(card);
    }
    return card;
}
DeckName Game::getDeck(CardID card) {
    return cards[card].deck;
}
PathSymbol Game::getRegionType(RegionNumber number) {
    return std::get<Region>(settings.cardDefinitions[number]->type).type;
}
CompanyID Game::getCompanyID(CardID card) {
    Card & cardData = cards[card];
    CardID target = (cardData.deck == Characters || cardData.parent < 0) ? card : cardData.parent;
    Side & side = getSide(target);
    auto it = side.findCompany(target, Characters);
    return (it != side.companies.end()) ? it->id : -1;
}
CardType & Game::getCardType(CardID card) {
    return settings.cardDefinitions[cards[card].dataIndex]->type;
}
Character & Game::getCharacter(CardID card) {
    return std::get<Character>(getCardType(card));
}
Location & Game::getLocation(CardID card) {
    return std::get<Location>(getCardType(card));
}
Region & Game::getRegion(RegionNumber region) {
    return std::get<Region>(settings.cardDefinitions[region]->type);
}
Item & Game::getItem(CardID card) {
    return std::get<Item>(getCardType(card));
}
/* Returns the first item in the possession list that matches the given type */
CardID Game::getEquippedItem(CardID card, EquipmentType equipmentType) {
    for (CardID possession : cards[card].possessions) {
        CardType & type = getCardType(possession);
        if (std::holds_alternative<Item>(type)) {
            Item & item = std::get<Item>(type);
            if (item.equipmentType == equipmentType)
                return possession;
        }
    }
    return -1;
}
Race Game::getRace(CardID card) {
    CardType & type = getCardType(card);
    if (std::holds_alternative<Character>(type))
        return std::get<Character>(type).race;
    if (std::holds_alternative<HazardCreature>(type))
        return std::get<HazardCreature>(type).race;
    if (std::holds_alternative<Ally>(type))
        return std::get<Ally>(type).race;
    return std::get<Faction>(type).race;
}
Race Game::getAvatar(DeckType deckType) {
    return (deckType == HeroDeck) ? Wizard
        : ((deckType == MinionDeck) ? Ringwraith : Balrog);
}
CardID Game::avatarInPlay(Side & side) {
    Race avatar = getAvatar(side.deckType);
    for (Company & company : side.companies)
        for (CardID character : company.characters)
            if (getRace(character) == avatar && cards[character].dataIndex == side.revealedAvatar)
                return character;
    return -1;
}
CardCategory Game::getCardCategory(CardID card) {
    CardType & type = getCardType(card);
    if (std::holds_alternative<Character>(type))
        return CharacterCard;
    if (std::holds_alternative<Location>(type))
        return SiteCard;
    if (std::holds_alternative<Region>(type))
        return RegionCard;
    if (std::holds_alternative<HazardCreature>(type) || std::holds_alternative<HazardEvent>(type))
        return HazardCard;
    return ResourceCard;
}
CompanyID Game::getActiveCompany() {
    for (auto it = contextStack.begin(); it != contextStack.end(); ++it)
        if (std::holds_alternative<SelectCompany>((*it)->context))
            return std::get<SelectCompany>((*it)->context).selectedCompany;
    return -1;
}
CardID Game::getSite() {
    for (auto it = contextStack.begin(); it != contextStack.end(); ++it) {
        if (std::holds_alternative<Movement>((*it)->context)) {
            return std::get<Movement>((*it)->context).destination;
        }
        else if (std::holds_alternative<Explore>((*it)->context)) {
            return std::get<Explore>((*it)->context).site;
        }
    }
    return SiteInRegion;
}
CardID Game::findSiteID(Side & side, int dataIndex) {
    for (CardID location : side.locationDeck)
        if (cards[location].dataIndex == dataIndex)
            return location;
    for (Company & company : side.companies) {
        if (cards[company.currentLocation].dataIndex == dataIndex)
            return company.currentLocation;
        if (cards[company.newLocation].dataIndex == dataIndex)
            return company.newLocation;
    }
    return -1;
}
SitePath & Game::getSitePath(CardID originSite, CardID destinationSite) {
    Location & origin = getLocation(originSite);
    Location & destination = getLocation(destinationSite);
    SiteType havenType = getSide(originSite).havenType();
    if (origin.type == havenType && destination.type == havenType) {
        int havenNumber = cards[destinationSite].dataIndex;
        for (HavenConnection & connection : origin.sitePath)
            if (connection.nearestHaven == havenNumber)
                return connection.sitePath;
    }
    return (destination.type == havenType) ? origin.sitePath.back().sitePath
                                           : destination.sitePath.back().sitePath;
}
Movement & Game::getMovement() {
    auto it = contextStack.begin();
    while (!std::holds_alternative<Movement>((*it)->context))
        ++it;
    return std::get<Movement>((*it)->context);
}
BottomContext & Game::getBottomContext() {
    return std::get<BottomContext>(contextStack.front()->context);
}

bool matchingName(const std::string & name, const std::vector<std::string> & names) {
    for (const std::string & str : names) {
        if (str == name)
            return true;
    }
    return false;
}
bool Game::hasPossession(CardID target, const std::string & possessionName) {
    const auto & possessions = cards[target].possessions;
    for (CardID possession : possessions)
        if (matchingName(possessionName, getName(getCardType(possession))))
            return true;
    return false;
}
bool Game::uniquePossession(CardID id, const std::vector<std::string> & names) {
    for (Side & side : sides)
        for (Company & company : side.companies)
            for (CardID card : company.characters)
                if (card != id)
                    for (const std::string & name : names)
                        if (hasPossession(card, name))
                            return false;
    return true;
}
bool Game::uniqueInDeck(CardID id, const std::vector<std::string> & names, DeckName deckName) {
    for (Side & side : sides) {
        std::vector<CardID> & deck = side.getDeck(deckName);
        for (CardID card : deck) {
            if (card != id) {
                const auto & cardNames = getName(getCardType(card));
                for (const std::string & name : cardNames)
                    if (matchingName(name, names))
                        return false;
            }
        }
    }
    return true;
}
bool Game::uniqueCharacter(CardID id, const std::vector<std::string> & names) {
    for (Side & side : sides) {
        for (Company & company : side.companies) {
            for (CardID card : company.characters) {
                if (card != id) {
                    Character & character = getCharacter(card);
                    for (const std::string & name : names)
                        if (matchingName(name, character.name))
                            return false;
                }
            }
        }
    }
    return uniqueInDeck(id, names, Eliminated);
}
bool Game::uniqueItem(CardID id, const std::vector<std::string> & names) {
    if (!uniquePossession(id, names))
        return false;
    if (!uniqueInDeck(id, names, Stored))
        return false;
    return true;
}
bool Game::uniqueAlly(CardID id, const std::vector<std::string> & names) {
    if (!uniquePossession(id, names))
        return false;
    if (!uniqueInDeck(id, names, Eliminated))
        return false;
    return true;
}
bool Game::uniqueFaction(CardID id, const std::vector<std::string> & names) {
    return uniqueInDeck(id, names, Factions);
}
bool Game::isManifestation(CardID cardA, CardID cardB) {
    CardType typeA = getCardType(cardA);
    CardType typeB = getCardType(cardB);
    if (typeA.index() != typeB.index())
        return false;
    std::vector<std::string> namesA = getName(typeA);
    std::vector<std::string> namesB = getName(typeB);
    for (const std::string & name : namesA)
        if (matchingName(name, namesB))
            return true;
    return false;
}
bool Game::containsCardCategory(const std::vector<CardID> & deck, CardCategory category) {
    for (CardID card : deck)
        if (getCardCategory(card) == category)
            return true;
    return false;
}
bool Game::adjacentSites(CardID source, CardID destination) {
    int originIndex = cards[source].dataIndex;
    int locationIndex = cards[destination].dataIndex;
    Location & origin = std::get<Location>(settings.cardDefinitions[originIndex]->type);
    Location & location = std::get<Location>(settings.cardDefinitions[locationIndex]->type);
    SiteType havenType = getSide(source).havenType();
    if (origin.type == havenType && location.type == havenType) {
        for (HavenConnection & connection : origin.sitePath)
            if (connection.nearestHaven == locationIndex)
                return true;
    }
    else if (location.type == havenType) {
        if (origin.sitePath.back().nearestHaven == locationIndex)
            return true;
    }
    else if (origin.type == havenType) {
        if (location.sitePath.back().nearestHaven == originIndex)
            return true;
    }
    return false;
}
bool Game::siteCardInPlay(CardID site) {
    Side & side = getSide(site);
    for (Company & company : side.companies)
        if (company.currentLocation == site || company.newLocation == site)
            return true;
    return false;
}
bool Game::resourceEventInPlay(const std::string & name, EventType type) {
    DeckName deckName = (type == PermanentEvent) ? PermanentEvents : LongEvents;
    return !uniqueInDeck(-1, { name }, deckName);
}
bool Game::hazardEventInPlay(const std::string & name, EventType type) {
    DeckName deckName = (type == PermanentEvent) ? PermanentEvents : LongEvents;
    return !uniqueInDeck(-1, { name }, deckName);
}
bool Game::hazardEventInPlay(CardID id) {
    HazardEvent & hazardEvent = std::get<HazardEvent>(getCardType(id));
    DeckName deckName = (hazardEvent.type == PermanentEvent) ? PermanentEvents : LongEvents;
    if (!uniqueInDeck(id, hazardEvent.name, deckName))
        return true;
    if (!uniqueInDeck(id, hazardEvent.name, Pending))
        return true;
    return false;
}
bool Game::characterInCompany(CardID card, CompanyID id, int player) {
    // Can be also used to test possessions by passing its parent as parameter.
    if (card < 0)
        return false;
    if (id < 0) {
        id = getActiveCompany();
        if (id < 0)
            return false;
    }
    Side & side = (player < 0) ? sides[currentPlayer] : sides[player];
    auto it = side.findCompany(id);
    if (it == side.companies.end())
        return false;
    for (CardID character : it->characters)
        if (character == card)
            return true;
    return false;
}
bool Game::raceInCompany(Company & company, Race race) {
    for (CardID character : company.characters)
        if (getRace(character) == race)
            return true;
    return false;
}
bool Game::possessedByRingwraith(CardID possession) {
    CardID parent = getParent(possession);
    if (parent < 0)
        return false;
    CardType & type = getCardType(parent);
    if (!std::holds_alternative<Character>(type))
        return false;
    return std::get<Character>(type).race == Ringwraith;
}
// Checks if a given card is hosting any effect
bool Game::hasEffect(CardID card, int player, CompanyID id) {
    if (player < 0)
        return effects.matchingEffectHost(card);
    Side & side = sides[player];
    if (id < 0)
        return side.effects.matchingEffectHost(card);
    Company & company = *side.findCompany(id);
    return company.effects.matchingEffectHost(card);
}
bool findMatchingEffect(Game & game, int dataIndex, EffectList & effects) {
    for (EffectListElement & effect : effects)
        if (!effect.remove && game.cards[effect.effect->host].dataIndex == dataIndex)
            return true;
    return false;
}
bool Game::duplicateEffect(CardID card, int player, CompanyID id) {
    int dataIndex = cards[card].dataIndex;
    if (player < 0)
        return findMatchingEffect(*this, dataIndex, effects);
    Side & side = sides[player];
    if (id < 0)
        return findMatchingEffect(*this, dataIndex, side.effects);
    Company & company = *side.findCompany(id);
    return findMatchingEffect(*this, dataIndex, company.effects);
}
bool Game::duplicatePossession(CardID target, CardID possession) {
    int dataIndex = cards[possession].dataIndex;
    std::vector<CardID> & possessions = cards[target].possessions;
    for (CardID possession : possessions)
        if (cards[possession].dataIndex == dataIndex)
            return true;
    return false;
}
int Game::duplicatePossessionsInCompany(Company & company, CardID card) {
    int dataIndex = cards[card].dataIndex;
    int count = 0;
    for (CardID character : company.characters) {
        std::vector<CardID> & possessions = cards[character].possessions;
        for (CardID possession : possessions)
            if (cards[possession].dataIndex == dataIndex)
                count++;
    }
    return count;
}
bool Game::deckExhaustionLimitReached() {
    int exhaustionLimit;
    switch (settings.gameLength) {
    case GameLength::ShortGame:
        exhaustionLimit = 2;
        break;
    case GameLength::LongGame:
        exhaustionLimit = 3;
        break;
    case GameLength::CampaignGame:
        exhaustionLimit = 4;
        break;
    }
    for (Side & side : sides)
        if (side.deckExhaustions < exhaustionLimit)
            return false;
    return true;
}

void Game::addSide() {
    sides.emplace_back(sides.size());
    Side & side = sides.back();
    side.aliasTable.clone();
    CardAliasTable & aliasTable = *side.aliasTable;
    int nCards(cards.size());
    for (int id = 0; id < nCards; id++) {
        DeckName deck = cards[id].deck;
        if (deck != PlayDeck && deck != Hand && deck != LocationDeck && deck != Sideboard)
            aliasTable.add(id);
    }
}
void Game::setHostingDeck(CardID id, int recipient, DeckName destination) {
    DeckName deck;
    Card & card = cards[id];

    // Update alias tables
    int players(sides.size());
    for (int i = 0; i < players; i++) {
        bool fromBlackBox, toBlackBox;
        if (i == card.owner) {
            fromBlackBox = card.deck == PlayDeck;
            toBlackBox = destination == PlayDeck;
        }
        else {
            deck = card.deck;
            fromBlackBox = deck == PlayDeck || deck == Hand || deck == LocationDeck || deck == Sideboard;
            deck = destination;
            toBlackBox = deck == PlayDeck || deck == Hand || deck == LocationDeck || deck == Sideboard;
        }
        if (fromBlackBox != toBlackBox) {
            Side & side = sides[i];
            side.aliasTable.clone();
            if (fromBlackBox) {
                side.aliasTable->add(id);
            } else {
                side.aliasTable->remove(id);
            }
        }
    }

    card.deck = destination;
}

bool sitePathMatch(PathSymbol key, const SitePath & sitePath, const std::vector<KeyedToType> & playable) {
    int count = 0;
    for (const KeyedToType & keyType : playable)
        if (std::holds_alternative<PathSymbol>(keyType))
            if (std::get<PathSymbol>(keyType) == key)
                count++;
    if (count == 0)
        return false;
    for (const PathSymbol pathSymbol : sitePath)
        if (pathSymbol == key)
            count--;
    if (count > 0)
        return false;
    return true;
}
bool regionMatch(RegionNumber key, const std::vector<RegionNumber> & regions, const std::vector<KeyedToType> & playable) {
    if (regions.empty())
        return false;
    if (std::find(regions.cbegin(), regions.cend(), key) == regions.cend())
        return false;
    for (const KeyedToType & keyType : playable)
        if (std::holds_alternative<RegionNumber>(keyType) && (std::get<RegionNumber>(keyType) == key))
            return true;
    return false;
}
bool siteMatch(SiteType key, SiteType siteType, const std::vector<KeyedToType> & playable) {
    if (key != siteType)
        return false;
    for (const KeyedToType & keyType : playable)
        if (std::holds_alternative<SiteType>(keyType) && (std::get<SiteType>(keyType) == key))
            return true;
    return false;
}
bool nameMatch(const Location & destination, const std::vector<KeyedToType> & playable) {
    for (const KeyedToType & keyType : playable) {
        if (std::holds_alternative<std::string>(keyType)) {
            if (destination.name == std::get<std::string>(keyType))
                return true;
        } else if (std::holds_alternative<SiteType>(keyType) && std::get<SiteType>(keyType) == SiteInRegion) {
            for (const KeyedToType & keyType : playable)
                if (std::holds_alternative<RegionNumber>(keyType) && (std::get<RegionNumber>(keyType) == destination.region))
                    return true;
        }
    }
    return false;
}
