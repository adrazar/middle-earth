#include "../CardSets/CardSetLoader.h"
#include "../Middle-Earth/GameConfigurer.h"
#include "../Server/Server.h"
#include <boost/asio.hpp>

int main() {
    boost::asio::io_context ioContext;
    Server server(ioContext);
    server.start();
    ioContext.run();
}
